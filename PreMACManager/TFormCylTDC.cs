﻿using Arction.WinForms.Charting;
using Arction.WinForms.Charting.Axes;
using Arction.WinForms.Charting.Views.ViewXY;
using DM;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace EngineAnalyzer
{
    public partial class TFormCylTDC : Form
    {        
        public event EventHandler OnNeedChartUpdate;

        public TFormCylTDC()
        {
            InitializeComponent();

            typeof(Panel).InvokeMember("DoubleBuffered", BindingFlags.SetProperty
               | BindingFlags.Instance | BindingFlags.NonPublic, null,
               panelForm, new object[] { true });

            InitCyl();

            InitCombo();
            
            labelCaption.Font = TEA.fontMd16;
            labelCaption.Paint += TEA.labelAA_Paint;

            ChartInit();

            TEA.ControlCursorAdd(buttonTDCOffsetSet);
            TEA.ControlCursorAdd(buttonCalTDCApply);

            editTDCOffset.Font = TEA.fontMdCn40;

            panelForm.BackgroundImage = imgBack.Image;
            if (DicAI.Count < 24)
            {
                int i = 0;

                while (i < 12)
                {
                    Rectangle r = rectByIdx(i);

                    String s = "edit" + (i + 1).ToString() + "_1";

                    TextBox t = new TextBox();
                    panelForm.Controls.Add(t);
                    t.BorderStyle = BorderStyle.None;
                    t.Font = TEA.fontMdCn22;
                    t.ForeColor = TEA.ColorGridCellFont;
                    t.Size = r.Size;
                    t.Location = r.Location;
                    t.TextAlign = HorizontalAlignment.Center;
                    t.Visible = true;
                    t.Text = "";// i.ToString();
                    DicAI.Add(s, t);

                    s = "edit" + (i + 1).ToString() + "_2";
                    t = new TextBox();
                    panelForm.Controls.Add(t);
                    t.BorderStyle = BorderStyle.None;
                    t.Font = TEA.fontMdCn22;
                    t.ForeColor = TEA.ColorGridCellFont;
                    t.Size = r.Size;
                    t.Location = r.Location;
                    t.Top += (8 + 30);
                    t.TextAlign = HorizontalAlignment.Center;
                    t.Visible = true;
                    t.Text = "";//i.ToString();
                    DicAI.Add(s, t);

                    i++;
                }
            }
        }

        // TDC
        int FTDCOffset = 0;
        public int TDCOffset
        {
            get
            {
                return FTDCOffset;
            }
            set
            {
                FTDCOffset = value;
            }
        }

        // chart
        AxisY yAxis = null;

        public Boolean _bisfrst = true;
        public void ChartInit()
        {
            //LightningChartUltimate.SetDeploymentKey("lgCAAIT+iZ0B1tIBJABVcGRhdGVhYmxlVGlsbD0yMDE4LTA1LTI3I1JldmlzaW9uPTABfy0eAiydkK3kV/TcEfM6MRNGTGlNLKyCubEiAF9Bh3yUFUjIJF/df4WNkqQ2ME6NmLYOCBfq4kNBtn4cqbLwuA5cElUyTQZOfC4Tn6UzgVbMRuBeZJNvxQC1xSLBvDC138xq1knu3lZ6LHBXTptKH0SMmYmO3JKmXiA22kTI99aRXllgEzzDWh8UadBo3jh95lHbkjOp740Xf7XtJnWD/3uTsQgLz3N+N3Ez9Ou5tStJ6hqnNpqaUJhK4cdqERBXFo4s3GnQSI+e4InvDY5W6dFwzrZvUScDQ2gs5jU+/MhL6IczORqepQcRDZolO3ztjMw+H6cfueUCZWugDl+WihxgZiA9WzXRvi/c0trz1xuM4YLz92dwubFs41PhPxeRGs6juGT8MdDfyaWoeWuwf5C4f+C0reUfDvGUqSayXiPPxCvNsjLpzii6iSk5pL54Sf3QYp//VDTfQJ7eBmFnDt6Pm2yS5PkzuumW75aeIekaXIeB5N4MreIDSjdIfkp96Q==");
            LightningChartUltimate.SetDeploymentKey("lgCAAIT + iZ0B1tIBJABVcGRhdGVhYmxlVGlsbD0yMDE4LTA1LTI3I1JldmlzaW9uPTABfy0eAiydkK3kV / TcEfM6MRNGTGlNLKyCubEiAF9Bh3yUFUjIJF / df4WNkqQ2ME6NmLYOCBfq4kNBtn4cqbLwuA5cElUyTQZOfC4Tn6UzgVbMRuBeZJNvxQC1xSLBvDC138xq1knu3lZ6LHBXTptKH0SMmYmO3JKmXiA22kTI99aRXllgEzzDWh8UadBo3jh95lHbkjOp740Xf7XtJnWD / 3uTsQgLz3N + N3Ez9Ou5tStJ6hqnNpqaUJhK4cdqERBXFo4s3GnQSI + e4InvDY5W6dFwzrZvUScDQ2gs5jU +/ MhL6IczORqepQcRDZolO3ztjMw + H6cfueUCZWugDl + WihxgZiA9WzXRvi / c0trz1xuM4YLz92dwubFs41PhPxeRGs6juGT8MdDfyaWoeWuwf5C4f + C0reUfDvGUqSayXiPPxCvNsjLpzii6iSk5pL54Sf3QYp//VDTfQJ7eBmFnDt6Pm2yS5PkzuumW75aeIekaXIeB5N4MreIDSjdIfkp96Q==");

            //Disable rendering, strongly recommended before updating chart properties
            _chart.BeginUpdate();

            //Set active view as xy
            _chart.ActiveView = ActiveView.ViewXY;

            //Chart name
            _chart.Name = "Data chart";
            
            //Y-axis are stacked and has common x-axis drawn
            _chart.ViewXY.AxisLayout.YAxesLayout = YAxesLayout.Stacked;

            //_chart.ViewXY.LegendBoxes[0].Position = LegendBoxPositionXY.TopCenter - 24;//9999999999999999999999

            //_chart.ViewXY.LegendBoxes[0].Offset.Y = 0;//-(_chart.Height- _chart.ViewXY.LegendBoxes[0].Height);//탑메뉴 위치를 지정한다.
            //_chart.ViewXY.LegendBoxes[0].Width = _chart.Width;//탑메뉴 길이를 늘린다.
            //_chart.ViewXY.LegendBoxes[0].Height = 40;

            //_chart.ViewXY.LegendBoxes[0].Visible = true;

            _chart.ViewXY.AxisLayout.AutoAdjustMargins = false; //차트 수동변경을 위한 설정 셋팅
            _chart.ViewXY.Margins = new Padding(53, 18, 8, 38);

            _chart.Update();

            //Don't show x-axis title
            _chart.ViewXY.XAxes[0].Title.Visible = false;
            _chart.ViewXY.XAxes[0].ScrollMode = XAxisScrollMode.None;

            _chart.ViewXY.XAxes[0].ValueType = AxisValueType.Number;
            _chart.ViewXY.XAxes[0].Title.Text = "Period(ms)";
            
            AxisX xAxis = _chart.ViewXY.XAxes[0];

            if (yAxis == null)
            {
                ExampleUtils.DisposeAllAndClear(_chart.ViewXY.YAxes);
                yAxis = new AxisY(_chart.ViewXY);
                //yAxis.AllowAutoYFit = true;
                //yAxis.SetRange(-15, 15);
                yAxis.Units.Visible = false;
                yAxis.Title.Text = "";
                yAxis.MajorDivTickStyle.Visible = false;
                yAxis.MinorDivTickStyle.Visible = false;
                yAxis.AxisColor = Color.FromArgb(196, 198, 201);
                yAxis.AxisThickness = 1;
                yAxis.LabelsVisible = true;
                yAxis.LabelsColor = ColorTranslator.FromHtml("#95999B");
                yAxis.ScaleNibs.Color = Color.FromArgb(196, 198, 201);
                yAxis.AutoFormatLabels = false;
                yAxis.LabelsNumberFormat = "#,##0";
                yAxis.LabelsFont = TEA.fontMd10;
                _chart.ViewXY.YAxes.Add(yAxis);
            }

            LineSeriesCursor cursor = new LineSeriesCursor(_chart.ViewXY, xAxis);
            cursor.ValueAtXAxis = (xAxis.Maximum - xAxis.Minimum) / 2;
            cursor.SnapToPoints = false;
            cursor.Style = CursorStyle.VerticalNoTracking;
            cursor.MouseInteraction = false;
            cursor.LineStyle.Pattern = LinePattern.Solid;
            cursor.LineStyle.Width = 2;
            cursor.LineStyle.Color = ColorTranslator.FromHtml("#D63F24");

            _chart.ViewXY.LineSeriesCursors.Add(cursor);

            _chart.ViewXY.XAxes[0].LabelsFont = TEA.fontMd10;

            //Allow chart rendering
            _chart.EndUpdate();


            //GenerateData(_chart, ea.UseCHMax, 1440, 1);

            //CreateSpectrumChart();
            TEA.ControlCursorAdd(buttonGraphPAxisReset);
        }

        public Dictionary<PictureBox, int> DicCyl = new Dictionary<PictureBox, int>();
        public void InitCyl()
        {
            DicCyl.Add(imgCyl1, 0);
            DicCyl.Add(imgCyl2, 1);
            DicCyl.Add(imgCyl3, 2);
            DicCyl.Add(imgCyl4, 3);
            DicCyl.Add(imgCyl5, 4);
            DicCyl.Add(imgCyl6, 5);
            DicCyl.Add(imgCyl7, 6);
            DicCyl.Add(imgCyl8, 7);
            DicCyl.Add(imgCyl9, 8);
            DicCyl.Add(imgCyl10, 9);
            DicCyl.Add(imgCyl11, 10);
            DicCyl.Add(imgCyl12, 11);

            int i = 0;
            foreach (PictureBox p in DicCyl.Keys)
            {
                p.Click += imgCylOn_Click;
                p.Tag = i;
                i++;

                TEA.ControlCursorAdd(p);
            }
            CylSelect(0);
        }

        public int CylSelected
        {
            get
            {
                foreach (PictureBox p in DicCyl.Keys)
                {
                    if (p.Image != null)
                        return (int)p.Tag;
                }
                return 0;
            }
        }
        
        public void CylSelect(int i)
        {
            foreach (PictureBox p in DicCyl.Keys)
            {
                if ((int)p.Tag == i)
                {
                    p.Image = imgCylOn.Image;
                }
                else
                {
                    if (p.Image != null)
                        p.Image = null;
                }
            }
            _bisfrst = true;

            if (OnNeedChartUpdate != null)
                OnNeedChartUpdate(null, null);
            //buttonGraphPAxisReset_Click(null, null);
        }

        private void imgCylOn_Click(object sender, EventArgs e)
        {
            try
            {
                PictureBox p = (PictureBox)sender;
                int i = DicCyl[p];

                CylSelect(i);
            }
            catch (Exception E)
            {

            }
        }

        public void ChartRecalc(LightningChartUltimate AChart)
        {
            try
            {
                AChart.ViewXY.ZoomToFit();
                //Boolean b;
                //_chart.ViewXY.YAxes[0].Fit(10, out b, false, false);
                //_chart.ViewXY.XAxes[0].Fit(out b, false);
            }
            catch (Exception E)
            {

            }
        }

        public void GraphToUI()
        {
            if (editGraphPYMax.Focused == false)
                editGraphPYMax.Text = Convert.ToDecimal(_chart.ViewXY.YAxes[0].Maximum).ToString("0");

            if (editGraphPYMin.Focused == false)
                editGraphPYMin.Text = Convert.ToDecimal(_chart.ViewXY.YAxes[0].Minimum).ToString("0");
        }

        private void buttonGraphPAxisReset_Click(object sender, EventArgs e)
        {
            _bisfrst = true;
            editTemp.Focus();
            //ChartRecalc(_chart);
            //GraphToUI();

            if (OnNeedChartUpdate != null)
                OnNeedChartUpdate(null, null);
        }
        
        // combo

        public void InitCombo()
        {
            // combo
            comboY.SelectedIndexChanged += comboY_SelectedIndexChanged;
            TEA.setBorder(comboY, TEA.ColorButtonBorder, 1, BorderStyle.FixedSingle);

            TEA.ControlCursorAdd(comboYMax);
            TEA.ControlCursorAdd(comboYMin);
            TEA.ControlCursorAdd(comboY);

            comboInit(comboYMax);
            comboInit(comboYMin);
        }

        public void comboInit(Panel sender)
        {
            sender.Click += combo_Click;
        }

        private void comboOutside_Click(object sender, EventArgs e)
        {
            comboHide();
        }

        private void combo_Click(object sender, EventArgs e)
        {
            Panel p = (Panel)sender;

            if (comboY.Visible == true)
            {
                comboHide();
                return;
            }

            if (p == comboYMax)
            {
                comboY.Items.Clear();
                comboY.Items.Add("50");
                comboY.Items.Add("100");
                comboY.Items.Add("150");
            }
            else
            {
                comboY.Items.Clear();
                comboY.Items.Add("-10");
                comboY.Items.Add("0");
                comboY.Items.Add("10");
            }

            if (p == comboYMin)
                editComboDest = editGraphPYMin;
            else if (p == comboYMax)
                editComboDest = editGraphPYMax;

            if (editComboDest != null)
            {
                ComboShow(p);
            }
        }

        TextBox editComboDest = null;

        public void comboHide()
        {
            comboY.Visible = false;

            editComboDest.Parent.Click -= comboOutside_Click;
            editComboDest.Parent.Controls.Remove(comboY);
        }

        public void ComboShow(Panel ADest)
        {
            if (editComboDest.Parent != null)
            {
                editComboDest.Parent.Controls.Add(comboY);
                editComboDest.Parent.Click += comboOutside_Click;
            }
            comboY.Top = ADest.Top + 29;
            comboY.Left = ADest.Left - 39;
            comboY.Visible = true;
            comboY.BringToFront();
        }

        private void comboY_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (editComboDest != null)
            {
                editComboDest.Text = comboY.SelectedItem.ToString();
            }
            comboHide();
        }

        private void buttonTDCOffsetSet_Click(object sender, EventArgs e)
        {
            editTemp.Focus();

            int i = 0;
            if (Int32.TryParse(editTDCOffset.Text, out i))
            {
                TDCOffset = i;
                _bisfrst = true;
            }
            
            if (OnNeedChartUpdate != null)
                OnNeedChartUpdate(null, null);
        }

        private void editGraphPYMin_TextChanged(object sender, EventArgs e)
        {
            int i = 0;
            TextBox t = (TextBox)sender;
            if (Int32.TryParse(t.Text, out i))
            {
                String[] sa = t.Text.Split('\r');
                if (sa.Length > 0)
                {
                    t.Text = sa[0];
                }

                try
                {
                    _chart.ViewXY.YAxes[0].SetRange(Convert.ToDouble(editGraphPYMin.Text), Convert.ToDouble(editGraphPYMax.Text));
                }
                catch
                {

                }

                //ChartRecalc(_chart);
            }
            else
            {
                //t.Text = "0";
            }
        }

        private void editTDCOffset_TextChanged(object sender, EventArgs e)
        {

        }

        private void editTDCOffset_Leave(object sender, EventArgs e)
        {
            try
            {
                string s = editTDCOffset.Text.Replace(",","");
                int i = 0;
                if (Int32.TryParse(s, out i))
                {
                    editTDCOffset.Text = string.Format("{0:#,##0}", i);
                }
            }
            catch
            {

            }
        }

        private void editTDCOffset_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                buttonTDCOffsetSet_Click(null, null);
            }
            else if (e.KeyCode == Keys.Escape)
            {
                editTDCOffset.Text = TDCOffset.ToString();
                editTemp.Focus();
            }
        }

        public Rectangle rectByIdx(int idx)
        {
            int irow = idx / 6;
            int icol = idx % 6;

            int ix = 80 + 159;
            int iy = 690 + 21 - 75; // 75=상단 바 높이

            ix += (icol * (272 + 25));
            iy += (irow * (142 + 25));

            return new Rectangle(new Point(ix, iy), new Size(57, 30));
        }

        private void editAI_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                editTemp.Focus();
            }
            else if (e.KeyCode == Keys.Escape)
            {
                editTemp.Focus();
            }
        }

        Dictionary<String, TextBox> DicAI = new Dictionary<string, TextBox>();

        private void panelForm_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
            
            int idx = 0;

            while (idx < 12)
            {
                Rectangle r = rectByIdx(idx);
                
                // label
                SolidBrush solidBrush = new SolidBrush(TEA.ColorLabelFont);
                string sMin = "0";
                string sMax = "0";

                if (TCS.EAPAICalLast != null)
                {
                    String s = "edit" + (idx + 1).ToString() + "_1";
                    if (DicAI.ContainsKey(s))
                    {
                        DicAI[s].Text = TCS.EAPAICalLast.Get(13 + idx).ToString();
                    }
                    s = "edit" + (idx + 1).ToString() + "_2";
                    if (DicAI.ContainsKey(s))
                    {
                        DicAI[s].Text = TCS.EAPAICalLast.Get(13 + idx).ToString();
                    }
                }
                TextFormatFlags flags =
                    TextFormatFlags.HorizontalCenter |
                    TextFormatFlags.VerticalCenter |
                    TextFormatFlags.WordBreak;

                //TextRenderer.DrawText(e.Graphics, s, TEA.fontMdCn22, r, TEA.ColorGridCellFont, flags);

                r.Y += (8 + 30);
                //TextRenderer.DrawText(e.Graphics, sMin, TEA.fontMdCn22, r, TEA.ColorGridCellFont, flags);

                r.Y += (8 + 30);
                //TextRenderer.DrawText(e.Graphics, sMax, TEA.fontMdCn22, r, TEA.ColorGridCellFont, flags);

                idx++;
            }
        }

        public void TimerTick()
        {
        }

        private void timer500ms_Tick(object sender, EventArgs e)
        {
            panelForm.Invalidate();
        }
    }
}

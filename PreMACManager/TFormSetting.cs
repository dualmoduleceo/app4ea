﻿using ClosedXML.Excel;
using CsvHelper;
using DM;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CubeEA;

namespace EngineAnalyzer
{
    public partial class TFormSetting : Form
    {
        public List<int> ListCHShift = new List<int>();

        public Dictionary<String, List<Label>> DicLabel = new Dictionary<string, List<Label>>();
        public Dictionary<Label, Label> DicLabelSelected = new Dictionary<Label, Label>();
        
        public TFormSetting()
        {
            InitializeComponent();

            typeof(Panel).InvokeMember("DoubleBuffered", BindingFlags.SetProperty
               | BindingFlags.Instance | BindingFlags.NonPublic, null,
               panelGridNo, new object[] { true });

            panelForm.BackgroundImage = imgBack.Image;

            for (int i = 0; i < 12; i++)
            {
                ListCHShift.Add(0);
            }

            //-------------
            DicLabel.Add("tabGraph", new List<Label>());

            DicLabel["tabGraph"].Add(labelEDP);
            DicLabel["tabGraph"].Add(labelECP);
            DicLabel["tabGraph"].Add(labelEKP);

            DicLabelSelected.Add(labelEDP, labelEDP);

            //-------------
            foreach (KeyValuePair<String, List<Label>> vp in DicLabel)
            {
                foreach (Label label in vp.Value)
                {
                    label.Tag = vp;
                    label.Paint += label_Paint;
                    label.MouseDown += label_MouseDown;
                    label.MouseUp += label_MouseUp;
                    label.MouseEnter += label_MouseEnter;
                    label.MouseLeave += label_MouseLeave;

                    if (vp.Key.StartsWith("tab"))
                    {
                        label.Font = TEA.fontMd16;
                    }
                }
            }
            // ----------

            
            ControlCursorAdd(buttonGridScrollUp);
            ControlCursorAdd(buttonGridScrollDown);
            ControlCursorAdd(buttonGridScrollLeft);
            ControlCursorAdd(buttonGridScrollRight);

            ControlCursorAdd(buttonNext);
            ControlCursorAdd(buttonNextx2);
            ControlCursorAdd(buttonPrev);
            ControlCursorAdd(buttonPrev2x);
            ControlCursorAdd(buttonFirst);
            ControlCursorAdd(buttonNext);

            ControlCursorAdd(buttonEditor);

            ControlCursorAdd(buttonImport);
            ControlCursorAdd(buttonExport);

            ControlCursorAdd(buttonApply);

            TEA.ControlCursorAdd(buttonSaveSelect2);

            editGrid.Font = TEA.fontMd12;
            InitGrid();

            labelEPTitle.Font = TEA.fontMd16;
            labelEPTitle.Paint += TEA.labelAA_Paint;

            // msg

            labelBottomMsg.Font = TEA.fontMd14;
            labelBottomMsg.Paint += TEA.labelAA_Paint;
        }

        public void Msg(String AMsg, Boolean APopup = false)
        {
            labelBottomMsg.Text = AMsg;
            if (APopup)
            {
                MessageBox.Show(AMsg);
            }
        }

        public void ControlCursorAdd(Control sender)
        {
            sender.MouseLeave += obj_MouseLeave;
            sender.MouseEnter += obj_MouseEnter;
        }

        private void obj_MouseEnter(object sender, EventArgs e)
        {
            TEA.MouseHand();
            try
            {
                Panel p = (Panel)sender;
                if (p.Tag != null)
                {
                    String s = p.Tag.ToString();
                    Image img = Image.FromFile(TCube.GetPathResource() + "\\" + s + "_pressed.png");
                    p.BackgroundImage = img;
                }
            }
            catch
            {

            }
        }

        private void obj_MouseLeave(object sender, EventArgs e)
        {
            TEA.MouseNormal();

            try
            {
                Panel p = (Panel)sender;
                if (p.Tag != null)
                {
                    String s = p.Tag.ToString();
                    Image img = Image.FromFile(TCube.GetPathResource() + "\\" + s + "_normal.png");
                    p.BackgroundImage = img;
                }
            }
            catch
            {

            }
        }
        
        public Boolean tabSelected(Label ALabel)
        {
            return DicLabelSelected.ContainsKey(ALabel);
        }

        private void label_Paint(object sender, PaintEventArgs e)
        {
            //e.Graphics.TextRenderingHint = TextRenderingHint.AntiAlias;
            //base.OnPaint(e);

            Pen p = new Pen(TEA.ColorTabButtonBorder, 1);
            e.Graphics.DrawRectangle(p, new Rectangle(0, 0, ((Control)sender).Width, ((Control)sender).Height));
            //ControlPaint.DrawBorder(e.Graphics, ((Label)sender).DisplayRectangle, TEA.ColorTabButtonBorder, ButtonBorderStyle.Solid);

            TEA.labelAA_Paint(sender, e);
        }
        
        private void panel_Paint(object sender, PaintEventArgs e)
        {
            ControlPaint.DrawBorder(e.Graphics, ((Panel)sender).DisplayRectangle, TEA.ColorPanelBorder, ButtonBorderStyle.Solid);
        }

        private void label_MouseDown(object sender, MouseEventArgs e)
        {
            Label l = (Label)sender;
            KeyValuePair<String, List<Label>> vp = (KeyValuePair<String, List<Label>>)l.Tag;

            foreach (Label l1 in vp.Value)
            {
                if (l1 == l)
                {
                    l1.BackColor = TEA.ColorTabButtonDownBack;
                }
                else
                {
                    l1.BackColor = TEA.ColorTabButtonOffBack;
                }
            }
        }

        private void label_MouseEnter(object sender, EventArgs e)
        {
            TEA.MouseHand();
            Label l = (Label)sender;
            KeyValuePair<String, List<Label>> vp = (KeyValuePair<String, List<Label>>)l.Tag;

            foreach (Label l1 in vp.Value)
            {
                if (l1 == l)
                {
                    l1.BackColor = TEA.ColorTabButtonDownBack;
                }
                else
                {
                    if (tabSelected(l1))
                        l1.BackColor = TEA.ColorTabButtonDownBack;
                    else
                        l1.BackColor = TEA.ColorTabButtonOffBack;
                }
            }
        }

        private void label_MouseLeave(object sender, EventArgs e)
        {
            TEA.MouseNormal();
            Label l = (Label)sender;
            KeyValuePair<String, List<Label>> vp = (KeyValuePair<String, List<Label>>)l.Tag;

            foreach (Label l1 in vp.Value)
            {
                if (tabSelected(l1))
                {
                    l1.BackColor = TEA.ColorTabButtonOnBack;
                }
                else
                {
                    l1.BackColor = TEA.ColorTabButtonOffBack;
                }
            }
        }

        private void label_MouseUp(object sender, MouseEventArgs e)
        {
            Label l = (Label)sender;
            KeyValuePair<String, List<Label>> vp = (KeyValuePair<String, List<Label>>)l.Tag;

            foreach (Label l1 in vp.Value)
            {
                if (l1 == l)
                {
                    if (DicLabelSelected.ContainsKey(l1) == false)
                        DicLabelSelected.Add(l1, l1);
                    l1.BackColor = TEA.ColorTabButtonDownBack;
                }
                else
                {
                    if (DicLabelSelected.ContainsKey(l1))
                        DicLabelSelected.Remove(l1);
                    l1.BackColor = TEA.ColorTabButtonOffBack;
                }
            }

            tabChanged();
        }

        public void UIToFlagCmdParameter()
        {
            if (tabSelected(labelEDP))
            {
                TCS.eaCmd.FlagCmdParameter = 1;
                g.XlsxSheet(1);
                g.Invalidate();
            }
            else if (tabSelected(labelECP))
            {
                TCS.eaCmd.FlagCmdParameter = 2;
                g.RowTop = 0;
                g.XlsxSheet(2);
                g.Invalidate();
            }
            else if (tabSelected(labelEKP))
            {
                TCS.eaCmd.FlagCmdParameter = 3;
                g.RowTop = 0;
                g.XlsxSheet(3);
                g.Invalidate();
            }
        }

        public void tabChanged()
        {
            // tab
            if (tabSelected(labelEDP))
            {
                g.RowTop = 0;
                g.XlsxSheet(1);
                g.Invalidate();
            }
            else if (tabSelected(labelECP))
            {
                g.RowTop = 0;
                g.XlsxSheet(2);
                g.Invalidate();
            }
            else if (tabSelected(labelEKP))
            {
                g.RowTop = 0;
                g.XlsxSheet(3);
                g.Invalidate();
            }
        }

        public delegate void SaveChanged(Boolean AState);        
        public event SaveChanged OnSaveChanged;

        public TCubeGrid g = null;
        public void InitGrid()
        {
            g = new TCubeGrid(panelGridContent);

            g.PanelInfo = panelGridNo;
            g.PanelDrawOffset = new Point(3, 3);
            g.RowCount = 0;

            g.ColCount = 14;
            g.ColWidth[0] = c0.Width;
            g.ColWidth[1] = c1.Width;
            g.ColWidth[2] = c2.Width;
            g.ColWidth[3] = c3.Width;
            g.ColWidth[4] = c4.Width;
            g.ColWidth[5] = c5.Width;
            g.ColWidth[6] = c6.Width;
            g.ColWidth[7] = c7.Width;
            g.ColWidth[8] = c8.Width;
            g.ColWidth[9] = c9.Width;
            g.ColWidth[10] = c10.Width;
            g.ColWidth[11] = c11.Width;
            g.ColWidth[12] = c12.Width;
            g.ColWidth[13] = c13.Width;

            g.ColX[0] = c0.Location.X;
            g.ColX[1] = c1.Location.X;
            g.ColX[2] = c2.Location.X;
            g.ColX[3] = c3.Location.X;
            g.ColX[4] = c4.Location.X;
            g.ColX[5] = c5.Location.X;
            g.ColX[6] = c6.Location.X;
            g.ColX[7] = c7.Location.X;
            g.ColX[8] = c8.Location.X;
            g.ColX[9] = c9.Location.X;
            g.ColX[10] = c10.Location.X;
            g.ColX[11] = c11.Location.X;
            g.ColX[12] = c12.Location.X;
            g.ColX[13] = c13.Location.X;

            g.Edit = editGrid;

            g.Init();

            g.OnScrollChanged += GridScrollChanged;

            g.xlsxLoad(Application.StartupPath + "\\setting\\setting.csv");
            g.XlsxSheet(1);
            
            g.Invalidate();
        }

        int panelGridScrollTop = 254;
        public void GridScrollChanged(int AOld, int ANew)
        {
            try
            {
                int iMax = g.RowCount - g.RowCountShowing - 1;
                if (g.RowTop == 0)
                {
                    panelGridScrollTop = 254;
                }
                else if (g.RowTop == iMax)
                {
                    panelGridScrollTop = 825;
                }
                else
                {

                    double dMax = 825;
                    double dMin = 254;

                    double range = dMax - dMin;


                    double dp = g.RowTop / (double)iMax;

                    panelGridScrollTop = (int)(range * dp) + 254;
                }
            }
            catch
            {

            }
        }

        private void checkSave_Click(object sender, EventArgs e)
        {
            CheckExportChecked = !CheckExportChecked;
            if (OnSaveChanged != null)
            {
                OnSaveChanged(CheckExportChecked);
            }
        }

        public Boolean CheckExportChecked
        {
            get
            {
                return checkSave.Image != null;
            }
            set
            {
                if (value)
                {
                    checkSave.Image = checkOn.Image;
                }
                else
                {
                    checkSave.Image = null;
                }
            }
        }

        private void buttonApply_Paint(object sender, PaintEventArgs e)
        {

        }

        private void buttonApply_Click(object sender, EventArgs e)
        {
            TCube.SetValue("EditEAIP", editHost.Text);
            TCube.SetValue("EditEAPort", editPort.Text);
            
            TCube.SetValue("exportfolder", editFolder.Text);
            TCube.SetValue("editTO", editTO.Text);

            TCube.SetValue("editSaveInterval", editSaveInterval.Text);

            Msg("Setting Saved", true);
        }

        private void panelSaveSelect_Click(object sender, EventArgs e)
        {
            if (DialogFolder.ShowDialog() == DialogResult.OK)
            {
                editFolder.Text = DialogFolder.SelectedPath;
                TCube.SetValue("exportfolder", editFolder.Text);

                Msg("Raw Export Folder Changed");
            }
        }

        public class TCubeGrid
        {
            public TextBox Edit = null;
            public Panel PanelDraw = null;
            public Panel PanelInfo = null;
            public Point PanelDrawOffset = new Point(0, 0);

            public int RowCountShowing = 0;

            public int _RowCount = 0;
            public int RowCount
            {
                get
                {
                    return _RowCount;
                }
                set
                {
                    _RowCount = value;
                }
            }
            public int RowHeight = 29;
            public int RowSpcaing = 2;

            public int _ColCount = 0;
            public int ColCount
            {
                get
                {
                    return _ColCount;
                }
                set
                {
                    _ColCount = value;
                    ColWidth = new int[value];
                    ColX = new int[value];
                }
            }

            public delegate void EventGridScrollChanged(int AOld, int ANew);
            public event EventGridScrollChanged OnScrollChanged;

            public int[] ColWidth;
            public int[] ColX;

            public int RowTop = 0;

            public TCubeGrid(Panel AParent)
            {
                typeof(Panel).InvokeMember("DoubleBuffered", BindingFlags.SetProperty
                   | BindingFlags.Instance | BindingFlags.NonPublic, null,
                   AParent, new object[] { true });

                PanelDraw = AParent;

                PanelDraw.Paint += PanelDraw_Paint;
                PanelDraw.MouseWheel += PanelDraw_MouseWheel;
                PanelDraw.MouseDown += PanelDraw_MouseDown;
                PanelDraw.MouseUp += PanelDraw_MouseUp;
            }

            public void Init()
            {
                if (PanelDraw != null)
                {
                    RowCountShowing = (PanelDraw.Height - PanelDrawOffset.Y) / (RowHeight + RowSpcaing);
                }
                if (Edit != null)
                {
                    Edit.KeyDown += Edit_KeyDown;
                    Edit.Leave += Edit_Leave;
                }
            }

            public void Invalidate()
            {
                if (Edit != null)
                    if (Edit.Visible)
                        EditClose();
                if (PanelDraw != null)
                {
                    PanelDraw.Invalidate();
                }
                if (PanelInfo != null)
                {
                    PanelInfo.Invalidate();
                }
                if (OnScrollChanged != null)
                {
                    OnScrollChanged(RowTop, RowTop);
                }
            }

            //public void ColXCalc()
            //{
            //    int i = 0;
            //    int iSum = 0;
            //    while (i < ColCount)
            //    {
            //        ColX[i] = iSum;
            //        iSum += ColWidth[i];
            //        iSum += 
            //    }
            //}

            public void GetValue(int ARow, int ACol, ref String AValue)
            {
                try
                {
                    if (ws != null)
                    {
                        AValue = ws.Cell(ARow + 1 + XlsxRowStart - 1, ACol + 1).Value.ToString();
                    }
                    else if (csv != null)
                    {
                        if (ARow + 1 < lRows.Count)
                        {
                            if (ACol < lRows[ARow + 1].Count)
                            {
                                AValue = lRows[ARow + 1][ACol];
                            }
                        }
                    }
                }
                catch
                {

                }
                //AValue = "TEST";
            }
            
            public void PanelDraw_MouseWheel(object sender, MouseEventArgs e)
            {
                if (e.Delta / 120 > 0)
                {
                    Prev();
                }
                else if (e.Delta / 120 < 0)
                {
                    // 1
                    // 2 -
                    // 3 ]
                    // 4 ]
                    // 5 -
                    Next();
                }
            }

            public Rectangle GetRect(int x, int y)
            {
                Rectangle rect = new Rectangle(
                            PanelDrawOffset.X + ColX[x],
                            PanelDrawOffset.Y + ((RowHeight + RowSpcaing) * y),
                            ColWidth[x],
                            RowHeight);

                return rect;
            }

            private void PanelDraw_MouseDown(object sender, MouseEventArgs e)
            {

            }

            int EditRow = 0;
            int EditCol = 0;
            private void PanelDraw_MouseUp(object sender, MouseEventArgs e)
            {
                try
                {
                    Rectangle rnow = new Rectangle(e.Location, new Size(1, 1));
                    int y = 0;
                    while (y < RowCount)
                    {
                        int x = 0;
                        while (x < ColCount)
                        {
                            Rectangle r = GetRect(x, y);
                            if (rnow.IntersectsWith(r))
                            {
                                int irow = y + RowTop;

                                if (Edit != null)
                                {
                                    Edit.Width = r.Width - 4;

                                    int ix = r.X;
                                    int iy = r.Y + ((r.Height - Edit.Height) / 2);
                                    Edit.Location = new Point(ix, iy);
                                    Edit.Visible = true;

                                    String s = "";
                                    EditRow = irow;
                                    EditCol = x;
                                    GetValue(irow, x, ref s);
                                    Edit.Text = s;
                                    Edit.Focus();
                                    Edit.BringToFront();
                                }

                                return;
                            }
                            x++;
                        }
                        y++;
                    }
                }
                catch
                {

                }
            }

            public void PanelDraw_Paint(object sender, PaintEventArgs e)
            {
                int y = 0;
                while (y < RowCountShowing)
                {
                    int irow = RowTop + y;
                    int x = 0;
                    while (x < ColCount)
                    {
                        String s = "";
                        GetValue(irow, x, ref s);

                        //rect
                        Rectangle rect = GetRect(x, y);
                            //new Rectangle(
                            //PanelDrawOffset.X + ColX[x], 
                            //PanelDrawOffset.Y + ((RowHeight + RowSpcaing)* y), 
                            //ColWidth[x], 
                            //RowHeight);

                        TextFormatFlags flags = 
                            TextFormatFlags.HorizontalCenter |
                            TextFormatFlags.VerticalCenter | 
                            TextFormatFlags.WordBreak;

                        TextRenderer.DrawText(e.Graphics, s, TEA.fontMd12, rect, TEA.ColorGridCellFont, flags);
                        //e.Graphics.DrawRectangle(Pens.Black, rect);

                        x++;
                    }

                    y++;
                }
            }

            CsvReader csv = null;// new CsvReader(textReader );

            XLWorkbook wb = null;
            IXLWorksheet ws = null;
            public String XlsxFile = "";
            public int XlsxWorksheetNo = 1; // 1부터
            public int XlsxRowStart = 2; // 1부터
            public void xlsxLoad(String AFile)
            {
                try
                {
                    if (File.Exists(AFile))
                    {
                        XlsxFile = AFile;
                        if (AFile.Contains(".csv"))
                        {
                            
                        }
                        else
                        {                            
                            wb = new XLWorkbook(AFile);
                        }
                    }
                }
                catch
                {

                }
            }

            List<List<String>> lRows = new List<List<string>>();

            public void XlsxSheet(int AIndex) // 1부터
            {
                try
                {
                    if (ws == null)
                    {
                        XlsxFile = Application.StartupPath + "\\setting\\setting.csv";
                        XlsxFile = XlsxFile.Replace(".csv", AIndex.ToString() + ".csv");
                        using (StreamReader reader = new StreamReader(XlsxFile, Encoding.Default, true))
                        {
                            lRows.Clear();
                            csv = new CsvReader(reader);
                            csv.Configuration.HasHeaderRecord = false;
                            int i = 0;
                            while (csv.Read())
                            {
                                List<string> l = new List<string>();

                                int c = 0;
                                String sCol = "";
                                while (csv.TryGetField<String>(c, out sCol))
                                {
                                    l.Add(sCol);
                                    c++;
                                }

                                lRows.Add(l);

                                i++;
                            }

                            RowCount = i;// csv.GetRecords<String>().Count();
                        }
                    }
                    else
                    {
                        ws = wb.Worksheet(AIndex);
                        RowCount = ws.LastRowUsed().RowNumber();
                    }
                }
                catch
                {

                }
            }

            private void Edit_KeyDown(object sender, KeyEventArgs e)
            {
                try
                {
                    if (e.KeyCode == Keys.Escape)
                    {
                        EditClose();
                    }
                    else if (e.KeyCode == Keys.Return)
                    {
                        EditConfirm();
                        EditClose();

                        Invalidate();
                    }
                }
                catch
                {

                }
            }
            
            public void EditConfirm()
            {
                if (ws != null)
                {
                    ws.Cell(EditRow + 1 + XlsxRowStart - 1, EditCol + 1).Value = Edit.Text;
                    //Edit.Text = ws.Cell(EditRow + 1, EditCol + 1).Value.ToString();
                }
                else
                {
                    try
                    {
                        if (EditRow + 1 < lRows.Count)
                        {
                            if (EditCol < lRows[EditRow + 1].Count)
                            {
                                lRows[EditRow + 1][EditCol] = Edit.Text;
                            }
                        }
                    }
                    catch(Exception e)
                    {

                    }
                }
            }

            public void XlsxSave(String FileName = "")
            {
                if (FileName == "" && XlsxFile != "")
                {
                    if (wb != null)
                    {
                        wb.Save();
                    }
                    else
                    {
                        try
                        {
                            int i = 0;
                            StringBuilder sb = new StringBuilder();

                            while (i < lRows.Count)
                            {
                                sb.AppendLine(String.Join(",", lRows[i]));

                                i++;
                            }

                            System.IO.File.WriteAllText(XlsxFile, sb.ToString(), Encoding.UTF8);                            
                        }
                        catch (Exception e)
                        {

                        }
                    }
                }
            }

            private void Edit_Leave(object sender, EventArgs e)
            {
                EditClose();
            }

            public void EditClose()
            {

                Edit.Visible = false;
            }

            public void First()
            {
                RowTop = 0;
                Invalidate();
            }

            public void Prev(int AInc = 1)
            {
                // up
                RowTop -= AInc;
                if (RowTop < 0)
                    RowTop = 0;
                Invalidate();
            }

            public void Next(int AInc = 1)
            {
                RowTop += AInc;
                if (RowTop > RowCount - RowCountShowing - 1)
                {
                    RowTop = RowCount - RowCountShowing - 1;
                }
                Invalidate();
            }

            public void Last()
            {
                RowTop = RowCount - RowCountShowing - 1;
                Invalidate();
            }
        }

        private void editGrid_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void buttonExport_Click(object sender, EventArgs e)
        {
            if (g.XlsxFile != "")
            {
                g.XlsxSave();

                Msg("Setting Exported", true);
            }
        }

        private void editGrid_Leave(object sender, EventArgs e)
        {

        }

        private void TFormSetting_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    if (g.Edit != null)
                    {
                        g.EditClose();
                    }
                }
                else if (e.KeyCode == Keys.Return)
                {
                    if (g.Edit != null)
                    {
                        g.EditConfirm();
                        g.EditClose();
                    }
                }
            }
            catch
            {

            }
        }

        private void panelGridNo_Paint(object sender, PaintEventArgs e)
        {
            //rect
            Rectangle rect = new Rectangle(new Point(0,0), panelGridNo.Size);
            //new Rectangle(
            //PanelDrawOffset.X + ColX[x], 
            //PanelDrawOffset.Y + ((RowHeight + RowSpcaing)* y), 
            //ColWidth[x], 
            //RowHeight);

            TextFormatFlags flags =
                TextFormatFlags.HorizontalCenter |
                TextFormatFlags.VerticalCenter |
                TextFormatFlags.WordBreak;

            String s = "Record " + (g.RowTop + 1).ToString() + " of " + g.RowCount.ToString();
            TextRenderer.DrawText(e.Graphics, s, TEA.fontMd12, rect, TEA.ColorGridCellFont, flags);
        }

        private void buttonFirst_Click(object sender, EventArgs e)
        {
            g.First();
        }

        private void buttonPrev2x_Click(object sender, EventArgs e)
        {
            g.Prev(4);
        }

        private void buttonPrev_Click(object sender, EventArgs e)
        {
            g.Prev();
        }

        private void buttonNext_Click(object sender, EventArgs e)
        {
            g.Next();
        }

        private void buttonNextx2_Click(object sender, EventArgs e)
        {
            g.Next(4);
        }

        private void buttonLast_Click(object sender, EventArgs e)
        {
            g.Last();
        }

        private void buttonGridScrollUp_Clbuick(object sender, EventArgs e)
        {
            g.Prev();
        }

        private void buttonGridScrollDown_Click(object sender, EventArgs e)
        {
            g.Next();
        }

        private void timer100ms_Tick(object sender, EventArgs e)
        {
            if (panelGridScroll.Top != panelGridScrollTop)
            {
                panelGridScroll.Top = panelGridScrollTop;

            }
        }

        private void buttonGridScrollUp_Click(object sender, EventArgs e)
        {
            g.Prev();
        }

        private void panelForm_Click(object sender, EventArgs e)
        {
            try
            {
                if (g.Edit.Visible)
                    g.EditClose();
            }
            catch
            {
            }
        }

        private void buttonImport_Click(object sender, EventArgs e)
        {
            //if (g.XlsxFile != "")
            {
                g.xlsxLoad(Application.StartupPath + "\\setting\\setting.csv");
                g.XlsxSheet(1);

                g.Invalidate();

                Msg("Setting Imported", true);
            }
        }
    }
}

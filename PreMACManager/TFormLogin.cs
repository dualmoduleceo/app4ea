﻿using DM;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EngineAnalyzer
{
    public partial class TFormLogin : Form
    {
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        public TFormLogin()
        {
            TCS.LogDebug("BEFORE  TFormLogin()");
            InitializeComponent();
            TCS.LogDebug("AFTER   TFormLogin()");
        }

        private void imageLoginWall_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        // cursor
        public void ControlCursorAdd(Control sender)
        {
            sender.MouseLeave += obj_MouseLeave;
            sender.MouseEnter += obj_MouseEnter;
        }

        public void MouseHand()
        {
            Cursor = Cursors.Hand;
        }

        public void MouseNormal()
        {
            Cursor = Cursors.Arrow;
        }

        private void obj_MouseEnter(object sender, EventArgs e)
        {
            MouseHand();
            try
            {
                if (sender is Panel)
                {
                    Panel p = (Panel)sender;
                    if (p.Tag != null)
                    {
                        String s = p.Tag.ToString();
                        Image img = Image.FromFile(TCube.GetPathResource() + "\\" + s + "_pressed.png");
                        p.BackgroundImage = img;
                    }
                }
                else if (sender is PictureBox)
                {
                    PictureBox p = (PictureBox)sender;
                    if (p.Tag != null)
                    {
                        String s = p.Tag.ToString();
                        Image img = Image.FromFile(TCube.GetPathResource() + "\\" + s + "_pressed.png");
                        p.Image = img;
                    }
                }
            }
            catch
            {

            }
        }

        private void obj_MouseLeave(object sender, EventArgs e)
        {
            MouseNormal();

            try
            {
                if (sender is Panel)
                {
                    Panel p = (Panel)sender;
                    if (p.Tag != null)
                    {
                        String s = p.Tag.ToString();
                        Image img = Image.FromFile(TCube.GetPathResource() + "\\" + s + "_normal.png");
                        p.BackgroundImage = img;
                    }
                }
                else if (sender is PictureBox)
                {
                    PictureBox p = (PictureBox)sender;
                    if (p.Tag != null)
                    {
                        String s = p.Tag.ToString();
                        Image img = Image.FromFile(TCube.GetPathResource() + "\\" + s + "_normal.png");
                        p.Image = img;
                    }
                }
            }
            catch
            {

            }
        }

        PrivateFontCollection privateFonts;
        private void TFormLogin_Load(object sender, EventArgs e)
        {
            try
            {
                TCS.LogDebug("BEFORE  TFormLogin_Load()");

                TCS.LogDebug("BEFORE  Font Loaded");
                privateFonts = new PrivateFontCollection();
                privateFonts.AddFontFile("HelveticaNeueLTPro-Md.ttf");
                privateFonts.AddFontFile("HelveticaNeueLTPro-MdCn.ttf");
                Font fontMdCn10 = new Font(privateFonts.Families[1], 10, GraphicsUnit.Pixel);
                TCS.LogDebug("AFTER   Font Loaded");

                editID.Font = fontMdCn10;
                editPW.Font = fontMdCn10;

                ControlCursorAdd(panelLogin);

                panelLogin.Focus();
                TCS.LogDebug("AFTER   TFormLogin_Load()");
            }
            catch(Exception E)
            {
                TCS.LogError(E);
            }
        }

        private void panelLogin_MouseClick(object sender, MouseEventArgs e)
        {
            if (LoginCheck(editID.Text, editPW.Text))
            {
                TFormEA f = new TFormEA();
                f.formLogin = this;
                f.Show();
                Hide();
            }
            else
            {
                TCS.ShowMsg("Invaild ID or Password");
            }
        }

        public Boolean LoginCheck(String AID, String APW)
        {
            Boolean b = false;

            if (AID == "admin" && APW == "admin")
                return true;

            List<String> l = TCube.GetValue("IDPW", "").Split(',').ToList();

            foreach (String s in l)
            {
                List<String> l2 = s.Split('|').ToList();
                if (l2.Count == 2)
                {
                    if (l2[0] == AID && l2[1] == APW)
                    {
                        return true;
                    }
                }
            }

            return b;
        }

        private void panelExit_MouseClick(object sender, MouseEventArgs e)
        {
            Application.Exit();
        }

        private void TFormLogin_MouseClick(object sender, MouseEventArgs e)
        {
            textBox1.Focus();

            if (TCS.IsDebug)
            {
                TFormEA f = new TFormEA();
                f.Show();
                Hide();
            }
        }

        private void editID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                editPW.Focus();
            }
        }

        private void editPW_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                panelLogin_MouseClick(null, null);
            }
        }

        private void panelLogin_MouseEnter(object sender, EventArgs e)
        {
            Cursor = Cursors.Hand;
        }

        private void panelLogin_MouseLeave(object sender, EventArgs e)
        {
            Cursor = Cursors.Arrow;
        }
    }
}

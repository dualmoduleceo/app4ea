﻿namespace EngineAnalyzer
{
    partial class TFormCylTDC
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TFormCylTDC));
            this.panelForm = new System.Windows.Forms.Panel();
            this.labelCaption = new System.Windows.Forms.Label();
            this.labelBottomMsg = new System.Windows.Forms.Label();
            this.buttonCalTDCApply = new System.Windows.Forms.Panel();
            this.imgBack = new System.Windows.Forms.PictureBox();
            this.comboY = new System.Windows.Forms.ListBox();
            this.panelChartTDC = new System.Windows.Forms.Panel();
            this.buttonTDCOffsetSet = new System.Windows.Forms.Panel();
            this.editTDCOffset = new System.Windows.Forms.TextBox();
            this._chart = new Arction.WinForms.Charting.LightningChartUltimate();
            this.panelGraphRight = new System.Windows.Forms.Panel();
            this.comboYMax = new System.Windows.Forms.Panel();
            this.comboYMin = new System.Windows.Forms.Panel();
            this.editGraphPYMax = new System.Windows.Forms.TextBox();
            this.editGraphPYMin = new System.Windows.Forms.TextBox();
            this.buttonGraphPAxisReset = new System.Windows.Forms.Panel();
            this.imgCyl12 = new System.Windows.Forms.PictureBox();
            this.imgCyl11 = new System.Windows.Forms.PictureBox();
            this.imgCyl10 = new System.Windows.Forms.PictureBox();
            this.imgCyl1 = new System.Windows.Forms.PictureBox();
            this.imgCyl9 = new System.Windows.Forms.PictureBox();
            this.imgCyl2 = new System.Windows.Forms.PictureBox();
            this.imgCyl8 = new System.Windows.Forms.PictureBox();
            this.imgCyl3 = new System.Windows.Forms.PictureBox();
            this.imgCyl7 = new System.Windows.Forms.PictureBox();
            this.imgCyl4 = new System.Windows.Forms.PictureBox();
            this.imgCyl6 = new System.Windows.Forms.PictureBox();
            this.imgCyl5 = new System.Windows.Forms.PictureBox();
            this.imgCylOn = new System.Windows.Forms.PictureBox();
            this.editTemp = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.timer500ms = new System.Windows.Forms.Timer(this.components);
            this.panelForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgBack)).BeginInit();
            this.panelChartTDC.SuspendLayout();
            this.panelGraphRight.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCylOn)).BeginInit();
            this.SuspendLayout();
            // 
            // panelForm
            // 
            this.panelForm.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panelForm.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelForm.BackgroundImage")));
            this.panelForm.Controls.Add(this.panel1);
            this.panelForm.Controls.Add(this.labelCaption);
            this.panelForm.Controls.Add(this.labelBottomMsg);
            this.panelForm.Controls.Add(this.buttonCalTDCApply);
            this.panelForm.Controls.Add(this.imgBack);
            this.panelForm.Controls.Add(this.comboY);
            this.panelForm.Controls.Add(this.panelChartTDC);
            this.panelForm.Controls.Add(this.imgCylOn);
            this.panelForm.Controls.Add(this.editTemp);
            this.panelForm.Location = new System.Drawing.Point(0, 0);
            this.panelForm.Name = "panelForm";
            this.panelForm.Size = new System.Drawing.Size(1920, 1005);
            this.panelForm.TabIndex = 1;
            this.panelForm.Visible = false;
            this.panelForm.Paint += new System.Windows.Forms.PaintEventHandler(this.panelForm_Paint);
            // 
            // labelCaption
            // 
            this.labelCaption.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(230)))), ((int)(((byte)(232)))));
            this.labelCaption.Font = new System.Drawing.Font("맑은 고딕", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelCaption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(89)))), ((int)(((byte)(178)))));
            this.labelCaption.Location = new System.Drawing.Point(77, 44);
            this.labelCaption.Name = "labelCaption";
            this.labelCaption.Size = new System.Drawing.Size(149, 21);
            this.labelCaption.TabIndex = 97;
            this.labelCaption.Tag = "";
            this.labelCaption.Text = "TDC Offset && AI";
            // 
            // labelBottomMsg
            // 
            this.labelBottomMsg.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(230)))), ((int)(((byte)(232)))));
            this.labelBottomMsg.Font = new System.Drawing.Font("맑은 고딕", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelBottomMsg.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(63)))), ((int)(((byte)(36)))));
            this.labelBottomMsg.Location = new System.Drawing.Point(77, 933);
            this.labelBottomMsg.Name = "labelBottomMsg";
            this.labelBottomMsg.Size = new System.Drawing.Size(1707, 21);
            this.labelBottomMsg.TabIndex = 96;
            // 
            // buttonCalTDCApply
            // 
            this.buttonCalTDCApply.BackColor = System.Drawing.Color.Transparent;
            this.buttonCalTDCApply.Location = new System.Drawing.Point(1798, 914);
            this.buttonCalTDCApply.Name = "buttonCalTDCApply";
            this.buttonCalTDCApply.Size = new System.Drawing.Size(58, 54);
            this.buttonCalTDCApply.TabIndex = 35;
            // 
            // imgBack
            // 
            this.imgBack.Image = ((System.Drawing.Image)(resources.GetObject("imgBack.Image")));
            this.imgBack.Location = new System.Drawing.Point(385, 12);
            this.imgBack.Name = "imgBack";
            this.imgBack.Size = new System.Drawing.Size(60, 23);
            this.imgBack.TabIndex = 34;
            this.imgBack.TabStop = false;
            this.imgBack.Visible = false;
            // 
            // comboY
            // 
            this.comboY.FormattingEnabled = true;
            this.comboY.ItemHeight = 12;
            this.comboY.Items.AddRange(new object[] {
            "1",
            "2",
            "3"});
            this.comboY.Location = new System.Drawing.Point(437, 2041);
            this.comboY.Name = "comboY";
            this.comboY.Size = new System.Drawing.Size(69, 40);
            this.comboY.TabIndex = 32;
            this.comboY.Visible = false;
            // 
            // panelChartTDC
            // 
            this.panelChartTDC.BackColor = System.Drawing.Color.Transparent;
            this.panelChartTDC.Controls.Add(this.buttonTDCOffsetSet);
            this.panelChartTDC.Controls.Add(this.editTDCOffset);
            this.panelChartTDC.Controls.Add(this._chart);
            this.panelChartTDC.Controls.Add(this.panelGraphRight);
            this.panelChartTDC.Controls.Add(this.imgCyl12);
            this.panelChartTDC.Controls.Add(this.imgCyl11);
            this.panelChartTDC.Controls.Add(this.imgCyl10);
            this.panelChartTDC.Controls.Add(this.imgCyl1);
            this.panelChartTDC.Controls.Add(this.imgCyl9);
            this.panelChartTDC.Controls.Add(this.imgCyl2);
            this.panelChartTDC.Controls.Add(this.imgCyl8);
            this.panelChartTDC.Controls.Add(this.imgCyl3);
            this.panelChartTDC.Controls.Add(this.imgCyl7);
            this.panelChartTDC.Controls.Add(this.imgCyl4);
            this.panelChartTDC.Controls.Add(this.imgCyl6);
            this.panelChartTDC.Controls.Add(this.imgCyl5);
            this.panelChartTDC.Location = new System.Drawing.Point(80, 101);
            this.panelChartTDC.Name = "panelChartTDC";
            this.panelChartTDC.Size = new System.Drawing.Size(1760, 475);
            this.panelChartTDC.TabIndex = 30;
            // 
            // buttonTDCOffsetSet
            // 
            this.buttonTDCOffsetSet.BackColor = System.Drawing.Color.Transparent;
            this.buttonTDCOffsetSet.Location = new System.Drawing.Point(256, 372);
            this.buttonTDCOffsetSet.Name = "buttonTDCOffsetSet";
            this.buttonTDCOffsetSet.Size = new System.Drawing.Size(58, 54);
            this.buttonTDCOffsetSet.TabIndex = 34;
            this.buttonTDCOffsetSet.Click += new System.EventHandler(this.buttonTDCOffsetSet_Click);
            // 
            // editTDCOffset
            // 
            this.editTDCOffset.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.editTDCOffset.Font = new System.Drawing.Font("굴림", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.editTDCOffset.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(73)))), ((int)(((byte)(75)))), ((int)(((byte)(76)))));
            this.editTDCOffset.Location = new System.Drawing.Point(66, 298);
            this.editTDCOffset.Name = "editTDCOffset";
            this.editTDCOffset.Size = new System.Drawing.Size(221, 46);
            this.editTDCOffset.TabIndex = 33;
            this.editTDCOffset.Text = "0";
            this.editTDCOffset.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.editTDCOffset.TextChanged += new System.EventHandler(this.editTDCOffset_TextChanged);
            this.editTDCOffset.KeyDown += new System.Windows.Forms.KeyEventHandler(this.editTDCOffset_KeyDown);
            this.editTDCOffset.Leave += new System.EventHandler(this.editTDCOffset_Leave);
            // 
            // _chart
            // 
            this._chart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(243)))), ((int)(((byte)(245)))));
            this._chart.Background = ((Arction.WinForms.Charting.Fill)(resources.GetObject("_chart.Background")));
            this._chart.ChartManager = null;
            this._chart.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this._chart.HorizontalScrollBars = ((System.Collections.Generic.List<Arction.WinForms.Charting.HorizontalScrollBar>)(resources.GetObject("_chart.HorizontalScrollBars")));
            this._chart.Location = new System.Drawing.Point(335, 9);
            this._chart.Margin = new System.Windows.Forms.Padding(3, 8, 3, 8);
            this._chart.MinimumSize = new System.Drawing.Size(60, 79);
            this._chart.Name = "_chart";
            this._chart.Options = ((Arction.WinForms.Charting.ChartOptions)(resources.GetObject("_chart.Options")));
            this._chart.OutputStream = null;
            this._chart.RenderOptions = ((Arction.WinForms.Charting.Views.RenderOptionsCommon)(resources.GetObject("_chart.RenderOptions")));
            this._chart.Size = new System.Drawing.Size(1271, 458);
            this._chart.TabIndex = 8;
            this._chart.Title = ((Arction.WinForms.Charting.Titles.ChartTitle)(resources.GetObject("_chart.Title")));
            this._chart.VerticalScrollBars = ((System.Collections.Generic.List<Arction.WinForms.Charting.VerticalScrollBar>)(resources.GetObject("_chart.VerticalScrollBars")));
            this._chart.View3D = ((Arction.WinForms.Charting.Views.View3D.View3D)(resources.GetObject("_chart.View3D")));
            this._chart.ViewPie3D = ((Arction.WinForms.Charting.Views.ViewPie3D.ViewPie3D)(resources.GetObject("_chart.ViewPie3D")));
            this._chart.ViewPolar = ((Arction.WinForms.Charting.Views.ViewPolar.ViewPolar)(resources.GetObject("_chart.ViewPolar")));
            this._chart.ViewSmith = ((Arction.WinForms.Charting.Views.ViewSmith.ViewSmith)(resources.GetObject("_chart.ViewSmith")));
            this._chart.ViewXY = ((Arction.WinForms.Charting.Views.ViewXY.ViewXY)(resources.GetObject("_chart.ViewXY")));
            // 
            // panelGraphRight
            // 
            this.panelGraphRight.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelGraphRight.BackgroundImage")));
            this.panelGraphRight.Controls.Add(this.comboYMax);
            this.panelGraphRight.Controls.Add(this.comboYMin);
            this.panelGraphRight.Controls.Add(this.editGraphPYMax);
            this.panelGraphRight.Controls.Add(this.editGraphPYMin);
            this.panelGraphRight.Controls.Add(this.buttonGraphPAxisReset);
            this.panelGraphRight.Location = new System.Drawing.Point(1612, 2);
            this.panelGraphRight.Name = "panelGraphRight";
            this.panelGraphRight.Size = new System.Drawing.Size(144, 469);
            this.panelGraphRight.TabIndex = 31;
            // 
            // comboYMax
            // 
            this.comboYMax.BackColor = System.Drawing.Color.Transparent;
            this.comboYMax.Location = new System.Drawing.Point(71, 360);
            this.comboYMax.Name = "comboYMax";
            this.comboYMax.Size = new System.Drawing.Size(30, 30);
            this.comboYMax.TabIndex = 21;
            // 
            // comboYMin
            // 
            this.comboYMin.BackColor = System.Drawing.Color.Transparent;
            this.comboYMin.Location = new System.Drawing.Point(71, 308);
            this.comboYMin.Name = "comboYMin";
            this.comboYMin.Size = new System.Drawing.Size(30, 30);
            this.comboYMin.TabIndex = 20;
            // 
            // editGraphPYMax
            // 
            this.editGraphPYMax.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.editGraphPYMax.Font = new System.Drawing.Font("굴림", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.editGraphPYMax.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.editGraphPYMax.Location = new System.Drawing.Point(33, 370);
            this.editGraphPYMax.Multiline = true;
            this.editGraphPYMax.Name = "editGraphPYMax";
            this.editGraphPYMax.Size = new System.Drawing.Size(38, 17);
            this.editGraphPYMax.TabIndex = 13;
            this.editGraphPYMax.Text = "0";
            this.editGraphPYMax.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.editGraphPYMax.TextChanged += new System.EventHandler(this.editGraphPYMin_TextChanged);
            // 
            // editGraphPYMin
            // 
            this.editGraphPYMin.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.editGraphPYMin.Font = new System.Drawing.Font("굴림", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.editGraphPYMin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.editGraphPYMin.Location = new System.Drawing.Point(33, 318);
            this.editGraphPYMin.Multiline = true;
            this.editGraphPYMin.Name = "editGraphPYMin";
            this.editGraphPYMin.Size = new System.Drawing.Size(38, 17);
            this.editGraphPYMin.TabIndex = 12;
            this.editGraphPYMin.Text = "0";
            this.editGraphPYMin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.editGraphPYMin.TextChanged += new System.EventHandler(this.editGraphPYMin_TextChanged);
            // 
            // buttonGraphPAxisReset
            // 
            this.buttonGraphPAxisReset.BackColor = System.Drawing.Color.Transparent;
            this.buttonGraphPAxisReset.Location = new System.Drawing.Point(20, 73);
            this.buttonGraphPAxisReset.Name = "buttonGraphPAxisReset";
            this.buttonGraphPAxisReset.Size = new System.Drawing.Size(82, 70);
            this.buttonGraphPAxisReset.TabIndex = 10;
            this.buttonGraphPAxisReset.Click += new System.EventHandler(this.buttonGraphPAxisReset_Click);
            // 
            // imgCyl12
            // 
            this.imgCyl12.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl12.Location = new System.Drawing.Point(218, 218);
            this.imgCyl12.Name = "imgCyl12";
            this.imgCyl12.Size = new System.Drawing.Size(71, 29);
            this.imgCyl12.TabIndex = 28;
            this.imgCyl12.TabStop = false;
            // 
            // imgCyl11
            // 
            this.imgCyl11.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl11.Location = new System.Drawing.Point(139, 218);
            this.imgCyl11.Name = "imgCyl11";
            this.imgCyl11.Size = new System.Drawing.Size(71, 29);
            this.imgCyl11.TabIndex = 27;
            this.imgCyl11.TabStop = false;
            // 
            // imgCyl10
            // 
            this.imgCyl10.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl10.Location = new System.Drawing.Point(57, 218);
            this.imgCyl10.Name = "imgCyl10";
            this.imgCyl10.Size = new System.Drawing.Size(71, 29);
            this.imgCyl10.TabIndex = 26;
            this.imgCyl10.TabStop = false;
            // 
            // imgCyl1
            // 
            this.imgCyl1.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl1.Image = ((System.Drawing.Image)(resources.GetObject("imgCyl1.Image")));
            this.imgCyl1.Location = new System.Drawing.Point(57, 116);
            this.imgCyl1.Name = "imgCyl1";
            this.imgCyl1.Size = new System.Drawing.Size(71, 29);
            this.imgCyl1.TabIndex = 17;
            this.imgCyl1.TabStop = false;
            // 
            // imgCyl9
            // 
            this.imgCyl9.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl9.Location = new System.Drawing.Point(218, 184);
            this.imgCyl9.Name = "imgCyl9";
            this.imgCyl9.Size = new System.Drawing.Size(71, 29);
            this.imgCyl9.TabIndex = 25;
            this.imgCyl9.TabStop = false;
            // 
            // imgCyl2
            // 
            this.imgCyl2.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl2.Location = new System.Drawing.Point(139, 116);
            this.imgCyl2.Name = "imgCyl2";
            this.imgCyl2.Size = new System.Drawing.Size(71, 29);
            this.imgCyl2.TabIndex = 18;
            this.imgCyl2.TabStop = false;
            // 
            // imgCyl8
            // 
            this.imgCyl8.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl8.Location = new System.Drawing.Point(139, 184);
            this.imgCyl8.Name = "imgCyl8";
            this.imgCyl8.Size = new System.Drawing.Size(71, 29);
            this.imgCyl8.TabIndex = 24;
            this.imgCyl8.TabStop = false;
            // 
            // imgCyl3
            // 
            this.imgCyl3.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl3.Location = new System.Drawing.Point(218, 116);
            this.imgCyl3.Name = "imgCyl3";
            this.imgCyl3.Size = new System.Drawing.Size(71, 29);
            this.imgCyl3.TabIndex = 19;
            this.imgCyl3.TabStop = false;
            // 
            // imgCyl7
            // 
            this.imgCyl7.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl7.Location = new System.Drawing.Point(57, 184);
            this.imgCyl7.Name = "imgCyl7";
            this.imgCyl7.Size = new System.Drawing.Size(71, 29);
            this.imgCyl7.TabIndex = 23;
            this.imgCyl7.TabStop = false;
            // 
            // imgCyl4
            // 
            this.imgCyl4.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl4.Location = new System.Drawing.Point(57, 150);
            this.imgCyl4.Name = "imgCyl4";
            this.imgCyl4.Size = new System.Drawing.Size(71, 29);
            this.imgCyl4.TabIndex = 20;
            this.imgCyl4.TabStop = false;
            // 
            // imgCyl6
            // 
            this.imgCyl6.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl6.Location = new System.Drawing.Point(218, 150);
            this.imgCyl6.Name = "imgCyl6";
            this.imgCyl6.Size = new System.Drawing.Size(71, 29);
            this.imgCyl6.TabIndex = 22;
            this.imgCyl6.TabStop = false;
            // 
            // imgCyl5
            // 
            this.imgCyl5.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl5.Location = new System.Drawing.Point(139, 150);
            this.imgCyl5.Name = "imgCyl5";
            this.imgCyl5.Size = new System.Drawing.Size(71, 29);
            this.imgCyl5.TabIndex = 21;
            this.imgCyl5.TabStop = false;
            // 
            // imgCylOn
            // 
            this.imgCylOn.BackColor = System.Drawing.Color.Transparent;
            this.imgCylOn.Image = ((System.Drawing.Image)(resources.GetObject("imgCylOn.Image")));
            this.imgCylOn.Location = new System.Drawing.Point(12, 3);
            this.imgCylOn.Name = "imgCylOn";
            this.imgCylOn.Size = new System.Drawing.Size(71, 29);
            this.imgCylOn.TabIndex = 24;
            this.imgCylOn.TabStop = false;
            this.imgCylOn.Visible = false;
            // 
            // editTemp
            // 
            this.editTemp.Location = new System.Drawing.Point(591, 259);
            this.editTemp.Name = "editTemp";
            this.editTemp.Size = new System.Drawing.Size(112, 21);
            this.editTemp.TabIndex = 31;
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(400, 200);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(936, 320);
            this.panel1.TabIndex = 98;
            this.panel1.Visible = false;
            // 
            // timer500ms
            // 
            this.timer500ms.Enabled = true;
            this.timer500ms.Interval = 500;
            this.timer500ms.Tick += new System.EventHandler(this.timer500ms_Tick);
            // 
            // TFormCylTDC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1904, 966);
            this.Controls.Add(this.panelForm);
            this.Name = "TFormCylTDC";
            this.Text = "TFormCylTDC";
            this.panelForm.ResumeLayout(false);
            this.panelForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgBack)).EndInit();
            this.panelChartTDC.ResumeLayout(false);
            this.panelChartTDC.PerformLayout();
            this.panelGraphRight.ResumeLayout(false);
            this.panelGraphRight.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCylOn)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Panel panelForm;
        private System.Windows.Forms.PictureBox imgCyl12;
        private System.Windows.Forms.PictureBox imgCyl11;
        private System.Windows.Forms.PictureBox imgCyl10;
        private System.Windows.Forms.PictureBox imgCyl1;
        private System.Windows.Forms.PictureBox imgCyl9;
        private System.Windows.Forms.PictureBox imgCyl2;
        private System.Windows.Forms.PictureBox imgCyl8;
        private System.Windows.Forms.PictureBox imgCyl3;
        private System.Windows.Forms.PictureBox imgCyl7;
        private System.Windows.Forms.PictureBox imgCyl4;
        private System.Windows.Forms.PictureBox imgCyl6;
        private System.Windows.Forms.PictureBox imgCyl5;
        private System.Windows.Forms.PictureBox imgCylOn;
        private System.Windows.Forms.Panel panelGraphRight;
        private System.Windows.Forms.Panel comboYMax;
        private System.Windows.Forms.Panel comboYMin;
        private System.Windows.Forms.TextBox editGraphPYMax;
        private System.Windows.Forms.TextBox editGraphPYMin;
        private System.Windows.Forms.Panel buttonGraphPAxisReset;
        private System.Windows.Forms.TextBox editTemp;
        private System.Windows.Forms.ListBox comboY;
        private System.Windows.Forms.TextBox editTDCOffset;
        public Arction.WinForms.Charting.LightningChartUltimate _chart;
        private System.Windows.Forms.Panel buttonTDCOffsetSet;
        private System.Windows.Forms.PictureBox imgBack;
        public System.Windows.Forms.Panel panelChartTDC;
        private System.Windows.Forms.Panel buttonCalTDCApply;
        private System.Windows.Forms.Label labelBottomMsg;
        private System.Windows.Forms.Label labelCaption;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Timer timer500ms;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Windows.Forms;

namespace EngineAnalyzer
{
    public partial class TFormStatus : Form
    {
        TFormEA FormEA; 

        public TFormStatus()
        {
            InitializeComponent();
        }

        public TFormStatus(TFormEA AForm)
        {
            InitializeComponent();

            FormEA = AForm;
        }

        private void TFormStatus_Load(object sender, EventArgs e)
        {
            Init();
        }

        public void Init()
        {
            GridStatus.ColumnCount = 9;
            GridStatus.RowCount = 2;
            GridStatus.Invalidate();
            GridStatus.Rows[0].Frozen = true;
            

            //for (int i = 0;i < GridStatus.ColumnCount;i++)
            //{
            //    GridStatus.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            //}
            //GridStatus.Rows[0].Visible = false;
        }

        public void UpdateStatus()
        {
            GridStatus.RowCount = FormEA.DicTag.Count + 1;
        }

        private void GridStatus_CellValueNeeded(object sender, DataGridViewCellValueEventArgs e)
        {
            if (e.RowIndex == 0)
            {
                if (e.ColumnIndex == 0)
                {
                    e.Value = "Label";
                }
                else if (e.ColumnIndex == 1)
                {
                    e.Value = "Value";
                }
                else if (e.ColumnIndex == 2)
                {
                    e.Value = "Addr";
                }
                else if (e.ColumnIndex == 3)
                {
                    e.Value = "Default";
                }
                else if (e.ColumnIndex == 4)
                {
                    e.Value = "Raw Min";
                }
                else if (e.ColumnIndex == 5)
                {
                    e.Value = "Raw Max";
                }
                else if (e.ColumnIndex == 6)
                {
                    e.Value = "Value Min";
                }
                else if (e.ColumnIndex == 7)
                {
                    e.Value = "Value Max";
                }
                else if (e.ColumnIndex == 8)
                {
                    e.Value = "Unit Factor";
                }
            }
            else
            {
                if (e.RowIndex < FormEA.DicTag.Count)
                {
                    TTag t = FormEA.DicTag.Values.ToList()[e.RowIndex];

                    if (e.ColumnIndex == 0)
                    {
                        e.Value = t.Label;
                    }
                    else if (e.ColumnIndex == 1)
                    {
                        e.Value = t.Value;
                    }
                    else if (e.ColumnIndex == 2)
                    {
                        e.Value = t.Address;
                    }
                    else if (e.ColumnIndex == 3)
                    {
                        e.Value = t.Default;
                    }
                    else if (e.ColumnIndex == 4)
                    {
                        e.Value = t.RawMin;
                    }
                    else if (e.ColumnIndex == 5)
                    {
                        e.Value = t.RawMax;
                    }
                    else if (e.ColumnIndex == 6)
                    {
                        e.Value = t.ValueMin;
                    }
                    else if (e.ColumnIndex == 7)
                    {
                        e.Value = t.ValueMax;
                    }
                    else if (e.ColumnIndex == 8)
                    {
                        e.Value = t.UnitFactor;
                    }
                }
            }
        }

        private void TimerTick_Tick(object sender, EventArgs e)
        {
            GridStatus.Invalidate();
        }
    }
}

﻿
using System.Windows.Forms.DataVisualization.Charting;
using Arction.WinForms.Charting;
using Arction.WinForms.Charting.Axes;
using Arction.WinForms.Charting.SeriesXY;
using DM;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Diagnostics;

namespace EngineAnalyzer
{
    public partial class TFormDignosis : Form
    {
        public int DataType = 0;
        public Dictionary<String, List<Label>> DicLabel = new Dictionary<string, List<Label>>();
        public Dictionary<Label, Label> DicLabelSelected = new Dictionary<Label, Label>();

        public Dictionary<String, Rectangle> DicSeriesEvent = new Dictionary<String, Rectangle>();
        public Dictionary<String, Rectangle> DicSeriesEventDown = new Dictionary<String, Rectangle>();

        public Dictionary<String, Rectangle> DicSeriesRadioEvent = new Dictionary<String, Rectangle>();
        public Dictionary<String, Rectangle> DicSeriesRadioEventDown = new Dictionary<String, Rectangle>();

        Thread ThreadExporter;
        Thread ThreadUI;

        public TFormCylTDC FormCalTDC = null;// new TFormCylTDC();

        public TFormDignosis()
        {
            InitializeComponent();

            panelForm.BackgroundImage = imgBack.Image;
            InitSelection();
            
            labelAA1.Font = TEA.fontMd16;
            labelAA1.Paint += TEA.labelAA_Paint;

            //-------------
            DicLabel.Add("tabGraph", new List<Label>());

            DicLabel["tabGraph"].Add(labelGraph);
            DicLabel["tabGraph"].Add(labelGraphCalc);
            DicLabel["tabGraph"].Add(labelTDC);
            DicLabel["tabGraph"].Add(labelKnock);

            DicLabelSelected.Add(labelGraph, labelGraph);

            //-------------
            foreach (KeyValuePair<String, List<Label>> vp in DicLabel)
            {
                foreach (Label label in vp.Value)
                {
                    label.Tag = vp;
                    label.Paint += label_Paint;
                    label.MouseDown += label_MouseDown;
                    label.MouseUp += label_MouseUp;
                    label.MouseEnter += label_MouseEnter;
                    label.MouseLeave += label_MouseLeave;

                    if (vp.Key.StartsWith("tab"))
                    {
                        label.Font = TEA.fontMd16;
                    }
                }
            }
            // ----------
            UIInit();

            ChartInit();

            panelGraph.Location = new Point(80, 452);
            panelGraphCalc.Location = new Point(80, 452);
            panelGraphTDC.Location = new Point(80, 452);
            panelGraphKnock.Location = new Point(80, 452);

            editTimeBegin.Text = DateTime.Now.ToString("yyyy-MM-dd 00:00");
            editTimeEnd.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm");

            typeof(Panel).InvokeMember("DoubleBuffered", BindingFlags.SetProperty
               | BindingFlags.Instance | BindingFlags.NonPublic, null,
               panelPreview, new object[] { true });

            typeof(Panel).InvokeMember("DoubleBuffered", BindingFlags.SetProperty
               | BindingFlags.Instance | BindingFlags.NonPublic, null,
               panelGraphCalc, new object[] { true });

            typeof(Panel).InvokeMember("DoubleBuffered", BindingFlags.SetProperty
               | BindingFlags.Instance | BindingFlags.NonPublic, null,
               panelGraph, new object[] { true });

            typeof(Panel).InvokeMember("DoubleBuffered", BindingFlags.SetProperty
               | BindingFlags.Instance | BindingFlags.NonPublic, null,
               panelGraphKnock, new object[] { true });
            
            typeof(Panel).InvokeMember("DoubleBuffered", BindingFlags.SetProperty
               | BindingFlags.Instance | BindingFlags.NonPublic, null,
               panelGraphTDC, new object[] { true });

            TEA.ControlCursorAdd(buttonLoad);

            // alarm filter
            TEA.ControlCursorAdd(buttonExpression);
            TEA.ControlCursorAdd(buttonClear);
            TEA.ControlCursorAdd(checkAlarm);
            TEA.ControlCursorAdd(buttonExport);

            // combo
            comboY.SelectedIndexChanged += comboY_SelectedIndexChanged;
            setBorder(comboY, TEA.ColorButtonBorder, 1, BorderStyle.FixedSingle);

            TEA.ControlCursorAdd(comboYMax);
            TEA.ControlCursorAdd(comboYMin);
            TEA.ControlCursorAdd(comboY);

            comboInit(comboYMax);
            comboInit(comboYMin);

            ////////////

            editAlarmFilter.Font = TEA.fontMd10;

            editGraphPYMin.Font = TEA.fontMd10;
            editGraphPYMax.Font = TEA.fontMd10;
            editCalcDataYMax.Font = TEA.fontMd10;
            editCalcDataYMin.Font = TEA.fontMd10;
            editCalcDataRPMYMax.Font = TEA.fontMd10;
            editCalcDataRPMYMin.Font = TEA.fontMd10;

            comboY.Font = TEA.fontMd10;

            TEA.ControlCursorAdd(buttonGraphPAxisReset);
            TEA.ControlCursorAdd(panelCalcDataRPMYReset);
            TEA.ControlCursorAdd(panelCalcDataYReset);

            // tdc
            FormCalTDC = new TFormCylTDC();
            FormCalTDC.TopLevel = false;

            GenerateData(FormCalTDC._chart, 2, 1440, 1);

            FormCalTDC._chart.ViewXY.SampleDataSeries[0].Visible = true;
            FormCalTDC._chart.ViewXY.SampleDataSeries[1].Visible = true;

            FormCalTDC._chart.ViewXY.SampleDataSeries[0].Title.Text = "Original";
            FormCalTDC._chart.ViewXY.SampleDataSeries[1].Title.Text = "Shift";

            panelGraphTDC.Controls.Add(FormCalTDC.panelChartTDC);
            FormCalTDC.panelChartTDC.Location = new Point(0, 0);
            FormCalTDC.panelChartTDC.BringToFront();

            FormCalTDC.OnNeedChartUpdate += chartTDCneedUpdate;

            // chat

            chartKnock.ChartAreas[0].AxisY.LabelStyle.Format = "#,##0";
            chartKnock.ChartAreas[0].AxisY.LabelStyle.Font = TEA.fontMd10;
            chartKnock.ChartAreas[0].AxisX.LabelStyle.Format = "#,##0";
            chartKnock.ChartAreas[0].AxisX.LabelStyle.Font = TEA.fontMd10;


            _chartCalcData.ChartAreas[0].AxisY.LabelStyle.Format = "#,##0";
            _chartCalcData.ChartAreas[0].AxisY.LabelStyle.Font = TEA.fontMd10;
            _chartCalcData.ChartAreas[0].AxisX.LabelStyle.Format = "#,##0";
            _chartCalcData.ChartAreas[0].AxisX.LabelStyle.Font = TEA.fontMd10;

            _chartCalcDataRPM.ChartAreas[0].AxisY.LabelStyle.Format = "#,##0";
            _chartCalcDataRPM.ChartAreas[0].AxisY.LabelStyle.Font = TEA.fontMd10;
            _chartCalcDataRPM.ChartAreas[0].AxisX.LabelStyle.Format = "#,##0";
            _chartCalcDataRPM.ChartAreas[0].AxisX.LabelStyle.Font = TEA.fontMd10;

            // thread



            ThreadUI = new Thread(() => DoThreadUI());
            ThreadUI.Start();
        }

        private void chartTDCneedUpdate(object sender, EventArgs e)
        {
            try
            {
                PreviewToRAW(cs);
            }
            catch
            {

            }
        }

        void setBorder(Control ctl, Color col, int width, BorderStyle style)
        {
            if (col == Color.Transparent)
            {
                Panel pan = ctl.Parent as Panel;
                if (pan == null) { throw new Exception("control not in border panel!"); }
                ctl.Location = new Point(pan.Left + width, pan.Top + width);
                ctl.Parent = pan.Parent;
                pan.Dispose();

            }
            else
            {
                Panel pan = new Panel();
                pan.BorderStyle = style;
                pan.Size = new Size(ctl.Width + width * 2, ctl.Height + width * 2);
                pan.Location = new Point(ctl.Left - width, ctl.Top - width);
                pan.BackColor = col;
                pan.Parent = ctl.Parent;
                ctl.Parent = pan;
                ctl.Location = new Point(width, width);
            }
        }

        private void comboOutside_Click(object sender, EventArgs e)
        {
            comboHide();
        }

        private void combo_Click(object sender, EventArgs e)
        {
            Panel p = (Panel)sender;

            if (comboY.Visible == true)
            {
                comboHide();
                return;
            }

            if (p == comboCalcDataRPMMax || p == comboCalcDataYMax || p == comboYMax)
            {
                comboY.Items.Clear();
                comboY.Items.Add("50");
                comboY.Items.Add("100");
                comboY.Items.Add("150");
            }
            else
            {
                comboY.Items.Clear();
                comboY.Items.Add("-10");
                comboY.Items.Add("0");
                comboY.Items.Add("10");
            }

            if (p == comboCalcDataRPMMax)
                editComboDest = editCalcDataRPMYMax;
            else if (p == comboCalcDataYMax)
                editComboDest = editCalcDataYMax;
            else if (p == comboYMax)
                editComboDest = editGraphPYMax;
            else if (p == comboYMin)
                editComboDest = editGraphPYMin;
            else if (p == comboCalcDataRPMYMin)
                editComboDest = editCalcDataRPMYMin;
            else if (p == comboCalcDataYMin)
                editComboDest = editCalcDataYMin;
            else if (p == comboYMin)
                editComboDest = editGraphPYMin;

            if (editComboDest != null)
            {
                ComboShow(p);
            }
        }

        TextBox editComboDest = null;

        public void comboHide()
        {
            comboY.Visible = false;

            editComboDest.Parent.Click -= comboOutside_Click;
            editComboDest.Parent.Controls.Remove(comboY);
        }

        public void ComboShow(Panel ADest)
        {
            if (editComboDest.Parent != null)
            {
                editComboDest.Parent.Controls.Add(comboY);
                editComboDest.Parent.Click += comboOutside_Click;
            }
            comboY.Top = ADest.Top + 29;
            comboY.Left = ADest.Left - 39;
            comboY.Visible = true;
            comboY.BringToFront();
        }

        private void comboY_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (editComboDest != null)
            {
                editComboDest.Text = comboY.SelectedItem.ToString();
            }
            comboHide();
        }

        public void comboInit(Panel sender)
        {
            sender.Click += combo_Click;
        }

        public void ControlCursorAdd(Control sender)
        {
            sender.MouseLeave += obj_MouseLeave;
            sender.MouseEnter += obj_MouseEnter;
        }

        private void obj_MouseEnter(object sender, EventArgs e)
        {
            TEA.MouseHand();
            try
            {
                Panel p = (Panel)sender;
                if (p.Tag != null)
                {
                    String s = p.Tag.ToString();
                    Image img = Image.FromFile(TCube.GetPathResource() + "\\" + s + "_pressed.png");
                    p.BackgroundImage = img;
                }
            }
            catch
            {

            }
        }

        private void obj_MouseLeave(object sender, EventArgs e)
        {
            TEA.MouseNormal();

            try
            {
                Panel p = (Panel)sender;
                if (p.Tag != null)
                {
                    String s = p.Tag.ToString();
                    Image img = Image.FromFile(TCube.GetPathResource() + "\\" + s + "_normal.png");
                    p.BackgroundImage = img;
                }
            }
            catch
            {

            }
        }

        public void MouseHand()
        {
            Cursor = Cursors.Hand;
        }

        public void MouseNormal()
        {
            Cursor = Cursors.Arrow;
        }

        public Boolean tabSelected(Label ALabel)
        {
            return DicLabelSelected.ContainsKey(ALabel);
        }

        private void label_Paint(object sender, PaintEventArgs e)
        {

            Pen p = new Pen(TEA.ColorTabButtonBorder, 1);
            e.Graphics.DrawRectangle(p, new Rectangle(0, 0, ((Control)sender).Width, ((Control)sender).Height));

            TEA.labelAA_Paint(sender, e);
            //ControlPaint.DrawBorder(e.Graphics, ((Label)sender).DisplayRectangle, TEA.ColorTabButtonBorder, ButtonBorderStyle.Solid);
        }
        
        private void panel_Paint(object sender, PaintEventArgs e)
        {
            ControlPaint.DrawBorder(e.Graphics, ((Panel)sender).DisplayRectangle, TEA.ColorPanelBorder, ButtonBorderStyle.Solid);
        }
        
        private void label_MouseDown(object sender, MouseEventArgs e)
        {
            Label l = (Label)sender;
            KeyValuePair<String, List<Label>> vp = (KeyValuePair<String, List<Label>>)l.Tag;

            foreach (Label l1 in vp.Value)
            {
                if (l1 == l)
                {
                    l1.BackColor = TEA.ColorTabButtonDownBack;
                }
                else
                {
                    l1.BackColor = TEA.ColorTabButtonOffBack;
                }
            }
        }

        private void label_MouseEnter(object sender, EventArgs e)
        {
            TEA.MouseHand();
            Label l = (Label)sender;
            KeyValuePair<String, List<Label>> vp = (KeyValuePair<String, List<Label>>)l.Tag;

            foreach (Label l1 in vp.Value)
            {
                if (l1 == l)
                {
                    l1.BackColor = TEA.ColorTabButtonDownBack;
                }
                else
                {
                    if (tabSelected(l1))
                        l1.BackColor = TEA.ColorTabButtonDownBack;
                    else
                        l1.BackColor = TEA.ColorTabButtonOffBack;
                }
            }
        }

        private void label_MouseLeave(object sender, EventArgs e)
        {
            TEA.MouseNormal();
            Label l = (Label)sender;
            KeyValuePair<String, List<Label>> vp = (KeyValuePair<String, List<Label>>)l.Tag;

            foreach (Label l1 in vp.Value)
            {
                if (tabSelected(l1))
                {
                    l1.BackColor = TEA.ColorTabButtonOnBack;
                }
                else
                {
                    l1.BackColor = TEA.ColorTabButtonOffBack;
                }
            }
        }

        private void label_MouseUp(object sender, MouseEventArgs e)
        {
            Label l = (Label)sender;
            KeyValuePair<String, List<Label>> vp = (KeyValuePair<String, List<Label>>)l.Tag;

            foreach (Label l1 in vp.Value)
            {
                if (l1 == l)
                {
                    if (DicLabelSelected.ContainsKey(l1) == false)
                        DicLabelSelected.Add(l1, l1);
                    l1.BackColor = TEA.ColorTabButtonDownBack;
                }
                else
                {
                    if (DicLabelSelected.ContainsKey(l1))
                        DicLabelSelected.Remove(l1);
                    l1.BackColor = TEA.ColorTabButtonOffBack;
                }
            }

            tabChanged();
        }

        public void tabChanged()
        {
            // tab
            if (tabSelected(labelGraph))
            {
                panelGraph.Visible = true;
                panelGraphCalc.Visible = false;
                panelGraphTDC.Visible = false;
                panelGraphKnock.Visible = false;

                chartRawUpdate();
            }
            else if (tabSelected(labelGraphCalc))
            {
                panelGraph.Visible = false;
                panelGraphCalc.Visible = true;
                panelGraphTDC.Visible = false;
                panelGraphKnock.Visible = false;

                chartCalcUpadte();
            }
            else if (tabSelected(labelTDC))
            {
                panelGraphKnock.Visible = false;
                panelGraphKnock.Visible = false;
                panelGraphTDC.Visible = true;
                panelGraphKnock.Visible = false;

                chartTDCUpdate();
            }
            else if (tabSelected(labelKnock))
            {
                panelGraphKnock.Visible = false;
                panelGraphKnock.Visible = false;
                panelGraphTDC.Visible = false;
                panelGraphKnock.Visible = true;

                chartKnockUpdate();
            }
        }

        // gggggggggggggggggggggggg
        private void panelGraph_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;

            ControlPaint.DrawBorder(g, ((Panel)sender).DisplayRectangle, TEA.ColorPanelBorder, ButtonBorderStyle.Solid);

            //if (tabSelected(labelGraphp) || tabSelected(labelGraphCalc))
            {
                int y = 6;
                int x = 31;
                int i = 0;

                Pen pen = new Pen(TEA.ColorPanelBorder);

                while (i < 12)
                {
                    y += 35;

                    // circle
                    Rectangle rectCircle = new Rectangle(x, y, 10, 10);
                    Boolean bb = false;
                    if (i < _chart.ViewXY.SampleDataSeries.Count)
                    {
                        if (_chart.ViewXY.SampleDataSeries[i].Visible)
                        {
                            SolidBrush b = new SolidBrush(TEA.ListColorCH[i]);
                            g.FillEllipse(b, rectCircle);

                            bb = true;
                        }
                    }

                    if (bb == false)
                    {
                        Pen p = new Pen(TEA.ColorPanelBorder);
                        g.DrawEllipse(p, rectCircle);
                    }

                    // label
                    SolidBrush solidBrush = new SolidBrush(TEA.ColorLabelFont);
                    string s = "Cyl. " + (i + 1).ToString();

                    g.TextRenderingHint = TextRenderingHint.AntiAlias;
                    g.DrawString(s, TEA.fontMd14, solidBrush, new PointF(x + 16, y - 3));

                    // line
                    if (i < 11)
                        g.DrawLine(pen, x - 6, y + 5 + 16, x - 6 + 75, y + 5 + 16);

                    //
                    String sKey = "checkCHVisible" + i.ToString();
                    if (DicSeriesEvent.ContainsKey(sKey) == false)
                    {
                        DicSeriesEvent.Add(sKey, new Rectangle(x, y - 3, 75, 16));
                    }

                    i++;
                }
            }
            //catch
            //{

            //}
        }

        private void panelGraph_MouseMove(object sender, MouseEventArgs e)
        {
            Rectangle r = new Rectangle(0, 0, 120, 475);

            if (r.IntersectsWith(new Rectangle(e.Location, new Size(1, 1))))
            {
                Boolean bIsFind = false;
                foreach (KeyValuePair<String, Rectangle> vp in DicSeriesEvent)
                {
                    if (vp.Value.IntersectsWith(new Rectangle(e.Location, new Size(1, 1))))
                    {
                        bIsFind = true;
                        break;
                    }
                }
                if (bIsFind)
                {
                    TEA.MouseHand();
                }
                else
                {
                    TEA.MouseNormal();
                }
            }
        }

        private void panelGraph_MouseDown(object sender, MouseEventArgs e)
        {
            foreach (KeyValuePair<String, Rectangle> vp in DicSeriesEvent)
            {
                if (vp.Value.IntersectsWith(new Rectangle(e.Location, new Size(1, 1))))
                {
                    if (DicSeriesEventDown.ContainsKey(vp.Key) == false)
                        DicSeriesEventDown.Add(vp.Key, vp.Value);
                }
            }
        }

        private void panelGraph_MouseUp(object sender, MouseEventArgs e)
        {
            foreach (KeyValuePair<String, Rectangle> vp in DicSeriesEvent)
            {
                if (DicSeriesEventDown.ContainsKey(vp.Key))
                {
                    if (vp.Value.IntersectsWith(new Rectangle(e.Location, new Size(1, 1))))
                    {
                        Boolean bIsCtrl = false;
                        if (e.Button == MouseButtons.Left && (ModifierKeys & Keys.Control) == Keys.Control)
                        {
                            bIsCtrl = true;
                        }
                        EventToggleCH(vp.Key, bIsCtrl);
                    }
                }
            }
            DicSeriesEventDown.Clear();
        }

        public void EventToggleCH(String AParam, Boolean ACtrl = false)
        {
            try
            {
                if (AParam.StartsWith("checkCHVisible"))
                {
                    if (ACtrl)
                    {
                        int iCHFind = Convert.ToInt32(AParam.Substring(14));

                        Boolean b = !_chart.ViewXY.SampleDataSeries[iCHFind].Visible;

                        int iCH = 0;
                        while (iCH < 12)
                        {
                            _chart.ViewXY.SampleDataSeries[iCH].Visible = b;
                            if (iCH < _chartCalcData.Series.Count)
                                _chartCalcData.Series[iCH].Enabled = b;
                            iCH++;
                        }
                    }
                    else
                    {
                        int iCH = Convert.ToInt32(AParam.Substring(14));

                        _chart.ViewXY.SampleDataSeries[iCH].Visible = !_chart.ViewXY.SampleDataSeries[iCH].Visible;

                        if (iCH < _chartCalcData.Series.Count)
                            _chartCalcData.Series[iCH].Enabled = _chart.ViewXY.SampleDataSeries[iCH].Visible;
                    }
                }
            }
            catch
            {

            }

            if (panelGraph.Visible)
                panelGraph.Invalidate();
            if (panelGraphCalc.Visible)
                panelGraphCalc.Invalidate();
        }


        public void ChartInit()
        {            
            //Disable rendering, strongly recommended before updating chart properties
            _chart.BeginUpdate();

            //Set active view as xy
            _chart.ActiveView = ActiveView.ViewXY;

            //Chart name
            _chart.Name = "Data chart";

            
            //Y-axis are stacked and has common x-axis drawn
            _chart.ViewXY.AxisLayout.YAxesLayout = YAxesLayout.Stacked;

            //_chart.ViewXY.LegendBoxes[0].Position = LegendBoxPositionXY.TopCenter - 24;//9999999999999999999999

            //_chart.ViewXY.LegendBoxes[0].Offset.Y = 0;//-(_chart.Height- _chart.ViewXY.LegendBoxes[0].Height);//탑메뉴 위치를 지정한다.
            //_chart.ViewXY.LegendBoxes[0].Width = _chart.Width;//탑메뉴 길이를 늘린다.
            //_chart.ViewXY.LegendBoxes[0].Height = 40;

            //_chart.ViewXY.LegendBoxes[0].Visible = true;

            //_chart.ViewXY.AxisLayout.AutoAdjustMargins = false; //차트 수동변경을 위한 설정 셋팅
            //_chart.ViewXY.Margins = new Padding(53, 50, 12, 58);

            _chart.Update();

            //Don't show x-axis title
            _chart.ViewXY.XAxes[0].Title.Visible = false;
            _chart.ViewXY.XAxes[0].ScrollMode = XAxisScrollMode.None;

            _chart.ViewXY.XAxes[0].ValueType = AxisValueType.Number;
            _chart.ViewXY.XAxes[0].Title.Text = "Period(ms)";

            _chart.ViewXY.XAxes[0].LabelsFont = TEA.fontMd10;
            //Allow chart rendering
            _chart.EndUpdate();
            
            GenerateData(_chart, TEA.CHMaxStatic, 1440, 1);

            //CreateSpectrumChart();
        }

        AxisY yAxis = null;
        private void GenerateData(LightningChartUltimate AChart, int channelCount, double frequency, double length)
        {
            //Cursor currentCursor = Cursor;
            //Cursor = Cursors.WaitCursor; //Change cursor to wait

            int sampleCount = (int)(frequency * length); //Samples points

            //Disable rendering
            AChart.BeginUpdate();

            //Remove existing series
            ExampleUtils.DisposeAllAndClear(AChart.ViewXY.SampleDataSeries);
            //Remove existing y-axes

            if (yAxis == null)
            {
                ExampleUtils.DisposeAllAndClear(AChart.ViewXY.YAxes);
                yAxis = new AxisY(AChart.ViewXY);
                //yAxis.AllowAutoYFit = true;
                //yAxis.SetRange(-15, 15);
                yAxis.Units.Visible = false;
                yAxis.Title.Text = "";
                yAxis.MajorDivTickStyle.Visible = false;
                yAxis.MinorDivTickStyle.Visible = false;
                yAxis.AxisColor = Color.FromArgb(196, 198, 201);
                yAxis.AxisThickness = 1;
                yAxis.LabelsVisible = true;
                yAxis.LabelsColor = ColorTranslator.FromHtml("#95999B");
                yAxis.ScaleNibs.Color = Color.FromArgb(196, 198, 201);
                yAxis.AutoFormatLabels = false;
                yAxis.LabelsNumberFormat = "#,##0";
                yAxis.LabelsFont = TEA.fontMd10;
                AChart.ViewXY.YAxes.Add(yAxis);
            }
            for (int channelIndex = 0; channelIndex < channelCount; channelIndex++)
            {
                SampleDataSeries sds = new SampleDataSeries();
                sds.SampleFormat = SampleFormat.SingleFloat;
                sds.FirstSampleTimeStamp = -360; // 1.0 / frequency;
                sds.SamplingFrequency = 2;// frequency;

                sds.MouseInteraction = false;
                sds.LineStyle.Width = 2;
                sds.LineStyle.AntiAliasing = LineAntialias.Normal;
                sds.LineStyle.Color = TEA.ListColorCH[channelIndex];// DefaultColors.SeriesForBlackBackground[channelIndex % DefaultColors.SeriesForBlackBackground.Length];

                if (channelCount == 1)
                    sds.Title.Text = "Cylinder Pressure";
                else
                    sds.Title.Text = "CH" + (channelIndex + 1).ToString();
                sds.DataBreaking.Enabled = true;
                sds.DataBreaking.Value = float.NaN; // set data gap defining value (default = NaN)

                AChart.ViewXY.SampleDataSeries.Add(sds);
            }

            //Update x-axis range
            AChart.ViewXY.XAxes[0].SetRange(0, (double)sampleCount / frequency);
            AChart.ViewXY.XAxes[0].AutoDivSpacing = false;
            AChart.ViewXY.XAxes[0].MajorDiv = 5;

            //Allow rendering
            AChart.EndUpdate();

            //Cursor = currentCursor;
        }

        // calc
        public int radioCalcData = 0;

        public Dictionary<String, TControlEvent> DicEvent = new Dictionary<String, TControlEvent>();
        public Dictionary<String, TControlEvent> DicEventMouseDown = new Dictionary<String, TControlEvent>();

        // uuuuuuuuuuuuuuuu
        public void UIInit()
        {
            int ix = 260;

            Rectangle r = new Rectangle(ix, 150, ix + 500 + 190, 22 + 80);

            EventAdd(panelGraphCalc, "radiopmax", new Rectangle(ix, 190, 90, 22), r);
            ix += 100;
            EventAdd(panelGraphCalc, "radioimep", new Rectangle(ix, 190, 90, 22), r);
            ix += 100;
            EventAdd(panelGraphCalc, "radiohr", new Rectangle(ix, 190, 90, 22), r);
            ix += 100;
            EventAdd(panelGraphCalc, "radiomisfire", new Rectangle(ix, 190, 90, 22), r);
            ix += 100;
            EventAdd(panelGraphCalc, "radiopegging", new Rectangle(ix, 190, 90, 22), r);
            ix += 100;
            EventAdd(panelGraphCalc, "radioknockinst", new Rectangle(ix, 190, 90, 22), r);
            ix += 100;
        }

        // cccccccccccccccccccccccccccccccccccc
        private void panelGraphCalc_Paint(object sender, PaintEventArgs e)
        {
            if (panelGraphCalc.Visible == false)
                return;

            panelGraph_Paint(sender, e);

            Graphics g = e.Graphics;
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;

            ControlPaint.DrawBorder(g, ((Panel)sender).DisplayRectangle, TEA.ColorPanelBorder, ButtonBorderStyle.Solid);

            //
            label(g, 122, 25, "RPM", TEA.fontMd16);// "Md16");
            label(g, 122, 190, "Calculation Data", TEA.fontMd16);// "Md16");

            int iX = 260;
            radio(g, iX, 190, "Pmax", TEA.fontMd12, radioCalcData == 0); iX += 100;
            radio(g, iX, 190, "IMEP", TEA.fontMd12, radioCalcData == 1); iX += 100;
            radio(g, iX, 190, "HR", TEA.fontMd12, radioCalcData == 2); iX += 100;
            radio(g, iX, 190, "Misfire", TEA.fontMd12, radioCalcData == 3); iX += 100;
            radio(g, iX, 190, "Pegging", TEA.fontMd12, radioCalcData == 4); iX += 100;
            radio(g, iX, 190, "Knock Inst.", TEA.fontMd12, radioCalcData == 5); iX += 100;
        }

        public void EventMouseMove(object ASender, Point ALocation)
        {
            Control ctrl = (Control)ASender;

            int iFind = -1;

            foreach (KeyValuePair<String, TControlEvent> kvp in DicEvent)
            {
                if (kvp.Key.Split(':').ToArray()[0] == ctrl.Name)
                {
                    if (kvp.Value.MouseInsideRect.IntersectsWith(new Rectangle(ALocation, new Size(1, 1))))
                    {
                        if (iFind == -1)
                            iFind = 0;
                        if (kvp.Value.Rect.IntersectsWith(new Rectangle(ALocation, new Size(1, 1))))
                        {
                            iFind++;
                        }
                    }
                }
            }
            if (iFind > 0)
            {
                TEA.MouseHand();
            }
            else if (iFind == 0)
            {
                TEA.MouseNormal();
            }
            else if (iFind == -1)
            {
            }
        }

        public void EventMouseDown(object ASender, Point ALocation)
        {
            Control ctrl = (Control)ASender;

            foreach (KeyValuePair<String, TControlEvent> kvp in DicEvent)
            {
                if (kvp.Key.Split(':').ToArray()[0] == ctrl.Name)
                {
                    if (kvp.Value.Rect.IntersectsWith(new Rectangle(ALocation, new Size(1, 1))))
                    {
                        if (DicEventMouseDown.ContainsKey(kvp.Key) == false)
                            DicEventMouseDown.Add(kvp.Key, kvp.Value);
                    }
                }
            }
        }

        public void EventMouseUp(object ASender, Point ALocation)
        {
            Control ctrl = (Control)ASender;

            foreach (KeyValuePair<String, TControlEvent> kvp in DicEvent)
            {
                if (kvp.Key.Split(':').ToArray()[0] == ctrl.Name)
                {
                    foreach (TControlEvent ce in DicEventMouseDown.Values)
                    {
                        if (ce.Rect.IntersectsWith(new Rectangle(ALocation, new Size(1, 1))))
                        {
                            EventDo(ce.Key);
                        }
                    }
                }
            }

            DicEventMouseDown.Clear();
        }

        public void EventDo(String AKey)
        {
            TControlEvent ce = null;

            if (DicEvent.TryGetValue(AKey, out ce) == false)
                return;

            Boolean bIsUpdateCalcData = false;

            {
                // radiocalcdata
                int radioCalcDataLast = radioCalcData;

                if (ce.ID == "radiopmax")
                    radioCalcData = 0;
                else if (ce.ID == "radioimep")
                    radioCalcData = 1;
                else if (ce.ID == "radiohr")
                    radioCalcData = 2;
                else if (ce.ID == "radiomisfire")
                    radioCalcData = 3;
                else if (ce.ID == "radiopegging")
                    radioCalcData = 4;
                else if (ce.ID == "radioknockinst")
                    radioCalcData = 5;

                if (radioCalcData != radioCalcDataLast)
                {
                    bIsUpdateCalcData = true;

                    chartCalcUpadte();
                }
            }

            if (bIsUpdateCalcData)
            {
                ce.Dest.Invalidate();
            }
        }

        public void EventAdd(Control AControl, String AID, Rectangle ARect, Rectangle AMouseInsdeRect)
        {
            String sKey = AControl.Name + ":" + AID;
            if (DicEvent.ContainsKey(sKey) == false)
            {
                TControlEvent ce = new TControlEvent
                {
                    Dest = AControl,
                    Key = sKey,
                    ID = AID,
                    Rect = ARect,
                    MouseInsideRect = AMouseInsdeRect
                };
                DicEvent.Add(sKey, ce);
            }
        }

        public void label(Graphics AG, int AX, int AY, String AText, Font AFont)
        {
            SolidBrush solidBrush = new SolidBrush(ColorTranslator.FromHtml("#6C6F70"));

            //if (AFont == "Md12")
            //    f = EA.fontMd12;
            //else if (AFont == "Md14")
            //    f = EA.fontMd14;
            //else if (AFont == "Md16")
            //    f = EA.fontMd16;
            //else if (AFont == "MdCn14")
            //    f = EA.fontMdCn14;
            //else if (AFont == "MdCn20")
            //    f = EA.fontMdCn20;

            AG.TextRenderingHint = TextRenderingHint.AntiAlias;
            AG.DrawString(AText, AFont, solidBrush, new PointF(AX, AY));


        }

        public void radio(Graphics AG, int AX, int AY, String AText, Font AFont, Boolean AChecked)
        {
            Rectangle rectCircle = new Rectangle(AX, AY, 22, 22);
            Rectangle rectCheck = new Rectangle(AX + 6, AY + 6, 10, 10);

            SolidBrush b = new SolidBrush(ColorTranslator.FromHtml("#FFFFFF"));
            AG.FillEllipse(b, rectCircle);

            Pen p = new Pen(ColorTranslator.FromHtml("#CFCFD4"));
            AG.DrawEllipse(p, rectCircle);

            if (AChecked)
            {
                b = new SolidBrush(ColorTranslator.FromHtml("#95999B"));
                AG.FillEllipse(b, rectCheck);
            }

            // label
            SolidBrush solidBrush = new SolidBrush(ColorTranslator.FromHtml("#6C6F70"));

            AG.TextRenderingHint = TextRenderingHint.AntiAlias;
            AG.DrawString(AText, AFont, solidBrush, new PointF(AX + 27, AY + 5));
        }

        private void panelGraphCalc_MouseMove(object sender, MouseEventArgs e)
        {
            panelGraph_MouseMove(sender, e);

            EventMouseMove(sender, e.Location);
        }

        private void panelGraphCalc_MouseDown(object sender, MouseEventArgs e)
        {
            panelGraph_MouseDown(sender, e);

            EventMouseDown(sender, e.Location);
        }

        private void panelGraphCalc_MouseUp(object sender, MouseEventArgs e)
        {
            panelGraph_MouseUp(sender, e);

            EventMouseUp(sender, e.Location);
        }

        private void TFormDignosis_Load(object sender, EventArgs e)
        {
        }

        DateTime dt1, dt2;

        // 시작 ~ 종료 파일 목록 구하기
        List<String> lRawFiles = new List<string>();

        private void buttonLoad_Click(object sender, EventArgs e)
        {
            try
            {
                chartPreview.Series.Clear();


                int i = 0;
                string s = "";

                s = editTimeBegin.Text + ":00";
                DateTime.TryParse(s, out dt1);

                s = editTimeEnd.Text + ":00";
                DateTime.TryParse(s, out dt2);

                if (dt1 > dt2)
                {
                    TCS.ShowMsg("Start time must lower then End time");
                    return;
                }

                Int64 i1 = Convert.ToInt64(dt1.ToString("yyMMddHHmm"));
                Int64 i2 = Convert.ToInt64(dt2.ToString("yyMMddHHmm"));
                
                String sDir = TCube.GetValue("exportfolder", Application.StartupPath + "\\raw\\");
                DirectoryInfo di = new DirectoryInfo(sDir);
                lRawFiles.Clear();
                foreach (var vFile in di.GetFiles())
                {
                    String sTime = vFile.Name.Replace("RAW_", "").Replace("_", "").Replace(".dat", "").Substring(0, 10);
                    Int64 iTime = Convert.ToInt64(sTime);
                    if (iTime >= i1 && iTime <= i2)
                    {
                        lRawFiles.Add(sDir + "\\" + vFile.Name);
                    }
                }

                if (lRawFiles.Count == 0)
                {
                    Msg("Not found RAW Files", true);
                    return;
                }

                RAWToPreview(lRawFiles, null, null, false);
            }
            catch
            {

            }
        }

        public void Msg(String AMsg, Boolean AIsPopup = false)
        {
            labelBottomMsg.Text = AMsg;
            if (AIsPopup)
            {
                MessageBox.Show(AMsg);
            }
        }

        DateTime[] dtTime = null;
        String[] sDTFile = null;
        float[,,] dCalcData = null;
        int iCntCalcData = 0;
        int iIdxCalcData = 0;
        public void RAWToPreview(List<String> AFiles, DataGridView AGrid, ProgressBar ABar, Boolean AAppend = false)
        {
            // calc data 
            // 총 데이터 길이 / 1000등분
            // 하나의 구간에서 min avg max 구하기

            Boolean bFilter = true;

            try
            {
                try
                {
                    arrX = new float[TEA.CHMaxStatic][];
                    Int16[] iaOffset = new Int16[TEA.CHMaxStatic];
                    Boolean bHasAngle = false;

                    if (AAppend == false)
                    {
                        lock (ListRaw)
                        {
                            ListRaw.Clear();
                        }
                    }

                    // 1. 각 파일의 사이즈를 구해 prgoress 계산
                    long lTotal = 0;
                    foreach (String AFile in AFiles)
                    {
                        if (File.Exists(AFile))
                        {
                            long length = new System.IO.FileInfo(AFile).Length;
                            lTotal += length;
                        }
                    }
                    int iCalcDataCnt = (int)(lTotal / (34656));
                    iCalcDataCnt /= 2;

                    iCntCalcData = iCalcDataCnt;
                    iIdxCalcData = 0;
                    dCalcData = new float[7, 12, iCntCalcData];
                    dtTime = new DateTime[iCntCalcData];
                    sDTFile = new String[iCntCalcData];

                    int iStep = Convert.ToInt32(lTotal / 100);
                    int iCntProgress = 0; // 0 ~ 총 파일의 크기만큼 증가

                    if (ABar != null)
                    {
                        this.Invoke(new Action(delegate () // this == Form 이다. Form이 아닌 컨트롤의 Invoke를 직접호출해도 무방하다.
                        {
                            //ProgressHistory.Maximum = 100;
                            //ProgressHistory.Value = 0;
                            //ProgressHistory.Visible = true;
                        }));
                    }

                    // 3. 각 파일 loop
                    foreach (String AFile in AFiles)
                    {
                        long length = new System.IO.FileInfo(AFile).Length;

                        FileStream fs = new FileStream(AFile, FileMode.Open);

                        Boolean bFile = true;
                        while (bFile)
                        {
                            iCntProgress += TEA.RowSizeInRAWFile; // 각 row 사이즈만큼 증가
                            if (iCntProgress >= iStep)
                            {
                                iCntProgress = iCntProgress % iStep;
                                if (ABar != null)
                                {
                                    //this.Invoke(new Action(delegate () // this == Form 이다. Form이 아닌 컨트롤의 Invoke를 직접호출해도 무방하다.
                                    //{
                                    //    int i = ProgressHistory.Value + 1;
                                    //    if (i < ProgressHistory.Maximum)
                                    //        ProgressHistory.Value = i;
                                    //}));
                                }
                            }

                            if (bIsCancelHistory)
                            {
                                return;
                            }

                            // 1.
                            int iRowLen = 8 + (2888 * 12);

                            byte[] b;
                            b = new byte[852];
                            int iRemain;
                            
                            try
                            {
                                if (fs.Read(b, 0, 8) == 0) break;

                                long nVal = BitConverter.ToInt64(b, 0);
                                DateTime dtNow = DateTime.FromBinary(nVal);
                                dtTime[iIdxCalcData] = dtNow;
                                sDTFile[iIdxCalcData] = AFile;

                                if (fs.Read(b, 0, 4) == 0) break;

                                if (b[0] == 0xFF && b[1] == 0xFF)
                                //if (b[8] == 0xFF && b[9] == 0xFF)
                                {
                                    // ch offset
                                    fs.Seek(24, SeekOrigin.Current);
                                    // calc data
                                    b = new byte[296];
                                    if (fs.Read(b, 0, 296) == 0) break;
                                    CalcDataToList(b);

                                    // remain
                                    fs.Seek(34332, SeekOrigin.Current);
                                }
                                else
                                {
                                    fs.Seek(34652, SeekOrigin.Current);
                                }
                            }
                            catch
                            {
                                bFile = false;
                            }
                        }
                        fs.Close();
                    }
                }
                finally
                {
                    this.Invoke(new Action(delegate () // this == Form 이다. Form이 아닌 컨트롤의 Invoke를 직접호출해도 무방하다.
                    {
                        int iKind = 0;
                        while (iKind < 6)
                        {
                            int iCHNew = 0;
                            while (chartPreview.Series.Count < _MaxSeries)
                            {
                                Series s = new Series();

                                s.Name = CalcDataString(iKind) + " CH" + (iCHNew + 1).ToString();
                                s.ChartType = SeriesChartType.FastLine;
                                s.XValueType = ChartValueType.DateTime;
                                s.YValueType = ChartValueType.Double;
                                chartPreview.Series.Add(s);
                                iCHNew++;
                            }
                            iKind++;
                        }

                        {
                            Series s = new Series();

                            s.Name = "RPM";
                            s.ChartType = SeriesChartType.FastLine;
                            s.XValueType = ChartValueType.DateTime;
                            s.YValueType = ChartValueType.Double;
                            chartPreview.Series.Add(s);
                        }
                        
                        chartPreview.Series.SuspendUpdates();
                        try
                        {
                            float d;
                            iKind = 0;
                            while (iKind < 6)
                            {
                                int iCH = 0;
                                while (iCH < TEA.CHMaxStatic)
                                {
                                    int i = 0;
                                    while (i < iCntCalcData)
                                    {
                                        //if (_chartCalcData.Series[iCH].Points.Count > 100)
                                        //    _chartCalcData.Series[iCH].Points.RemoveAt(0);

                                        d = dCalcData[iKind, iCH, i];                                        
                                        chartPreview.Series[iKind * 12 + iCH].Points.Add(d);

                                        i++;
                                    }
                                    iCH++;
                                }
                                iKind++;
                            }
                            iKind = 6; // rpm
                            {
                                int iCH = 0;
                                
                                int i = 0;
                                while (i < iCntCalcData)
                                {
                                    //if (_chartCalcData.Series[iCH].Points.Count > 100)
                                    //    _chartCalcData.Series[iCH].Points.RemoveAt(0);

                                    d = dCalcData[iKind, iCH, i];
                                    chartPreview.Series[72].Points.Add(d); // rpm 순서가 ch다음이라 CHMax사용

                                    i++;
                                }
                            }
                        }
                        finally
                        {
                            chartPreview.Series.ResumeUpdates();
                            chartPreview.Series.Invalidate();

                            if (editCalcDataYMax.Focused == false)
                            {
                                editCalcDataYMax.Tag = 1;
                                editCalcDataYMax.Text = chartPreview.ChartAreas[0].AxisY.Maximum.ToString("0");
                                editCalcDataYMax.Tag = null;
                            }
                            if (editCalcDataYMin.Focused == false)
                            {
                                editCalcDataYMin.Tag = 1;
                                editCalcDataYMin.Text = chartPreview.ChartAreas[0].AxisY.Minimum.ToString("0");
                                editCalcDataYMin.Tag = null;
                            }
                        }
                        
                        int iMid = (int)(iCntCalcData / 2);
                        cs.Start = iMid - 5;
                        cs.End = iMid + 5;

                        if (cs.Start < 0)
                            cs.Start = 0;
                        if (cs.End > iCntCalcData - 1)
                            cs.End = iCntCalcData - 1;
                        panelPreview.Invalidate();
                    }));
                    
                    PreviewToRAW(cs);

                    if (AGrid != null)
                    {
                        //this.Invoke(new Action(delegate () // this == Form 이다. Form이 아닌 컨트롤의 Invoke를 직접호출해도 무방하다.
                        //{
                        //    vscrollRows.Maximum = ListRaw.Count * 1440;

                        //    EditResultCycleCount.Text = ListRaw.Count.ToString();

                        //    InitGrid(AGrid);
                        //    if (ABar != null)
                        //    {
                        //        ProgressHistory.Visible = false;
                        //    }
                        //}));
                    }

                    if (bIsCancelHistory)
                    {
                        if (ThreadExporter != null)
                        {
                            ThreadExporter.Abort();
                        }
                    }
                    //ThreadRawsToGraphTemp = new Thread(() => ThreadRawsToGraph(
                    //        0, ListRaw.Count
                    //        ));
                    //ThreadRawsToGraphTemp.Start();
                    //ThreadExporter = null;
                }
            }
            catch (Exception E)
            {
                TCS.LogError(E);
            }
        }

        public int _MaxSeries = 73;//12 x 6 + 1(rpm)

        public String CalcDataString(int AKind)
        {
            if (AKind == 0)
                return "PMax";
            else if (AKind == 0)
                return "IMEP";
            else if (AKind == 0)
                return "HR";
            else if (AKind == 0)
                return "Misfire";
            else if (AKind == 0)
                return "Pegging";
            else if (AKind == 0)
                return "KnockInst";
            else
            {
                return "";
            }

        }

        public int CalcDataBaseAddr(String AKind)
        {
            if (AKind == "pmax")
                return 537;
            else if (AKind == "imep")
                return 561;
            else if (AKind == "hr")
                return 585;
            else if (AKind == "misfire")
                return 609;
            else if (AKind == "pegging")
                return 621;
            else if (AKind == "knockinst")
                return 633;
            else
                return 0;
        }

        public int CalcDataBaseAddr(int AKind)
        {
            if (AKind == 0)
                return 537;
            else if (AKind == 1)
                return 561;
            else if (AKind == 2)
                return 585;
            else if (AKind == 3)
                return 609;
            else if (AKind == 4)
                return 621;
            else if (AKind == 5)
                return 633;
            else if (AKind == 6)
                return 506;
            else
                return 0;
        }

        int[] HR = new int[9999];
        public void CalcDataToList(byte[] ABuffer)
        {
            int iHR = 501;
            int i = 0;
            while (i < ABuffer.Length)
            {
                UInt16 w = (UInt16)(BitConverter.ToUInt16(ABuffer, i));
                //w = SwapBytes(w);
                HR[iHR] = w;
                
                i = i + 2;
                iHR++;
            }

            //lock (darrCalcDataQueue)
            {
                int iKind = radioCalcData;
                while (iKind < 7)
                {
                    int iCH = 0;
                    while (iCH < 12)
                    {
                        if (iKind == 6)
                        {
                            if (iCH > 0) // RPM = CH1 
                            {
                                break;
                            }
                        }

                        int iBase = CalcDataBaseAddr(iKind);// - 501;

                        //UInt16 w = (UInt16)(BitConverter.ToUInt16(ABuffer, (iBase + iCH) * 2));
                        //dCalcData[iKind, iCH, iIdxCalcData] = w / 10;
                        float w = (float)(HR[iBase + iCH] / 10.0);
                        dCalcData[iKind, iCH, iIdxCalcData] = w;
                        
                        iCH++;
                    }

                    iKind++;
                }
            }

            iIdxCalcData++; 
        }

        // stx 03 00 00

        public int iLastCycle = -1;
        public int iMaxCycle = -1;

        float[][] arrX = new float[TEA.CHMaxStatic][];
        float[] arrXPS;// = new float[1440];
        float[] arrXPSORG;// = new float[1440];

        List<TRaw> ListRaw = new List<TRaw>();
        Boolean bIsCancelHistory = false;
        Dictionary<String, DataGridView> DicRAWGrid = new Dictionary<string, DataGridView>(); // file : GridView
        public void RAWLoad(List<String> AFiles, DateTime dtBegin, DateTime dtEnd, Boolean AAppend = false)
        {
            Boolean bFilter = true;
            try
            {
                try
                {
                    arrX = new float[TEA.CHMaxStatic][];
                    Int16[] iaOffset = new Int16[TEA.CHMaxStatic];
                    Boolean bHasAngle = false;

                    if (AAppend == false)
                    {
                        lock (ListRaw)
                        {
                            ListRaw.Clear();
                        }
                    }

                    // 1. 각 파일의 사이즈를 구해 prgoress 계산
                    long lTotal = 0;
                    foreach (String AFile in AFiles)
                    {
                        if (File.Exists(AFile))
                        {
                            long length = new System.IO.FileInfo(AFile).Length;
                            lTotal += length;
                        }
                    }

                    int iStep = Convert.ToInt32(lTotal / 100);
                    int iCntProgress = 0; // 0 ~ 총 파일의 크기만큼 증가

                    //if (ABar != null)
                    //{
                    //    this.Invoke(new Action(delegate () // this == Form 이다. Form이 아닌 컨트롤의 Invoke를 직접호출해도 무방하다.
                    //    {
                    //        //ProgressHistory.Maximum = 100;
                    //        //ProgressHistory.Value = 0;
                    //        //ProgressHistory.Visible = true;
                    //    }));
                    //}

                    // 3. 각 파일 loop
                    foreach (String AFile in AFiles)
                    {
                        long length = new System.IO.FileInfo(AFile).Length;

                        FileStream fs = new FileStream(AFile, FileMode.Open);

                        TRaw r = null;

                        while (true)
                        {
                            iCntProgress += TEA.RowSizeInRAWFile; // 각 row 사이즈만큼 증가
                            if (iCntProgress >= iStep)
                            {
                                iCntProgress = iCntProgress % iStep;
                                //if (ABar != null)
                                {
                                    //this.Invoke(new Action(delegate () // this == Form 이다. Form이 아닌 컨트롤의 Invoke를 직접호출해도 무방하다.
                                    //{
                                    //    int i = ProgressHistory.Value + 1;
                                    //    if (i < ProgressHistory.Maximum)
                                    //        ProgressHistory.Value = i;
                                    //}));
                                }
                            }

                            if (bIsCancelHistory)
                            {
                                return;
                            }

                            if (r == null)
                                r = new TRaw();

                            byte[] b;
                            // 1. time=8
                            b = new byte[8];
                            int iRemain = fs.Read(b, 0, 8);
                            if (iRemain == 0)
                            {
                                break;
                            }

                            // 2. data body
                            iRemain = fs.Read(r.Raws, 0, 2888 * 12);
                            if (iRemain == 0)
                            {
                                break;
                            }

                            long nVal = BitConverter.ToInt64(b, 0);
                            DateTime dtNow = DateTime.FromBinary(nVal);
                            r.Time = dtNow;

                            if ((bFilter == false) || ((dtNow >= dtBegin) && (dtNow <= dtEnd)))
                            {
                                // 시간 뒤에 4바이트가 모두 FF면 정보 Row

                                if ((r.Raws[0] == 0xFF) && (r.Raws[1] == 0xFF))
                                {
                                    // 0-3. CH's Offset 
                                    int iCH = 0;
                                    while (iCH < 12)
                                    {
                                        int iOFS = 4 + (Convert.ToInt32(iCH) * 2);
                                        Int16 wOffset = BitConverter.ToInt16(r.Raws, iOFS);

                                        iaOffset[iCH] = wOffset;
                                        iCH++;
                                    }
                                    bHasAngle = true;

                                    int iOffset = 28; // 시간8 제외하고 2 + 1 + 1 + 24;
                                    // 0-4. Calc Data
                                    r.BufCalcData = new Byte[288 + 8];
                                    Buffer.BlockCopy(r.Raws, iOffset, r.BufCalcData, 0, 288 + 8);
                                    iOffset += 288 + 8;

                                    // 0-5. Knock Data
                                    r.BufKnockData = new Byte[TEA.CHMaxStatic * (512 + 8)];
                                    Buffer.BlockCopy(r.Raws, iOffset, r.BufKnockData, 0, r.BufKnockData.Length);
                                }
                                else
                                {
                                    // 3. cycle
                                    r.Cycle = BitConverter.ToUInt16(r.Raws, 6);

                                    if (bHasAngle)
                                    {
                                        for (int j = 0; j < 12; j++)
                                        {
                                            r.AngleOffset[j] = iaOffset[j] / 10;
                                        }
                                    }

                                    if (iLastCycle == -1)
                                    {
                                        iLastCycle = r.Cycle - 1;
                                        iMaxCycle = r.Cycle;
                                    }

                                    //if (r.Cycle != iLastCycle + 1)
                                    //{
                                    //    TRaw rEmpty = new TRaw();
                                    //    rEmpty.Time = DateTime.Now;
                                    //    rEmpty.Cycle = iLastCycle + 1;
                                    //    rEmpty.IsEmpty = true;
                                    //    lock (ListRaw)
                                    //    {
                                    //        ListRaw.Add(rEmpty);
                                    //    }
                                    //}
                                    iLastCycle = r.Cycle;

                                    if (r.Cycle > iMaxCycle)
                                        iMaxCycle = r.Cycle;

                                    // 4. Row List add
                                    lock (ListRaw)
                                    {
                                        ListRaw.Add(r);
                                    }
                                    r = null;

                                    //if (ListRaw.Count > 20000)
                                    //{
                                    //    this.Invoke(new Action(delegate () // this == Form 이다. Form이 아닌 컨트롤의 Invoke를 직접호출해도 무방하다.
                                    //    {
                                    //        TCS.ShowMsg("메모리 크기 제한으로 20000개의 Cycle만 Loading 합니다.");
                                    //    }));

                                    //    fs.Close();
                                    //    return;
                                    //}
                                    // 5. List 추가 후 그래프 추가 쓰레드에서 byte -> float 후 byte는 삭제
                                }
                            }
                            else
                            {
                                r = null;
                            }
                        }
                        fs.Close();
                    }
                }
                finally
                {
                    if (bIsCancelHistory)
                    {
                        if (ThreadExporter != null)
                        {
                            ThreadExporter.Abort();
                        }
                    }
                    ThreadRawsToGraphTemp = new Thread(() => ThreadRawsToGraph(
                            0, ListRaw.Count
                            ));
                    ThreadRawsToGraphTemp.Start();
                    ThreadExporter = null;

                    chartCalcUpadte();
                }
            }
            catch (Exception E)
            {
                TCS.LogError(E);
            }
        }

        int LastGraphShowRow = -1;
        private void GridData_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //GraphShow(e.RowIndex);
        }
        Thread ThreadRawsToGraphTemp = null;
        public void GraphShow(int ARow)
        {
            //try
            //{
            //    if (ThreadRawsToGraphTemp != null)
            //        return;
            //    int iRow = ARow + vscrollRows.Value;
            //    int iList = iRow / (1440);

            //    if (iList < ListRaw.Count)
            //    {
            //        if (LastGraphShowRow != iList)
            //        {
            //            LastGraphShowRow = iList;

            //            ThreadRawsToGraphTemp = new Thread(() => ThreadRawsToGraph(
            //                iList, 360
            //                ));
            //            ThreadRawsToGraphTemp.Start();
            //        }
            //    }
            //}
            //catch (Exception E)
            //{
            //    TCS.LogError(E);
            //}
        }

        int[][] dKnock = new int [TEA.CHMaxStatic][];
        public void ThreadRawsToGraph(int AFrom = -1, int ACount = -1)
        {
            List<TRaw> ListRawTemp = null;
            if (ListRaw.Count > 0)
            {
                ListRawTemp = ListRaw;

                //lock (ListRaw)
                //{
                //    ListRaw = new List<TRaw>();
                //}
                try
                {
                    if (AFrom == -1)
                        AFrom = 0;
                    if (ACount == -1)
                        ACount = ListRawTemp.Count;
                    // CH당 200만 포인트 허용 ===> 1000 Count
                    if (ACount > 200000)
                    {
                        this.Invoke(new Action(delegate () // this == Form 이다. Form이 아닌 컨트롤의 Invoke를 직접호출해도 무방하다.
                        {
                            TCS.ShowMsg("그래프 표현 제한으로 최대 200000 Cycle만 표시됩니다.");

                        }));
                        ACount = 200000;
                    }

                    // knock init
                    int iStepCH = ACount * 1440;

                    for (int iCHTemp = 0; iCHTemp < TEA.CHMaxStatic; iCHTemp++)
                    {
                        arrX[iCHTemp] = new float[iStepCH];
                        dKnock[iCHTemp] = new int[(512 / 2) * ACount]; // /2는 512바이트 -> 256word // ACount=1로 하면 첫번째 cycle사용
                    }
                    //arrMax = new AreaSeriesPoint[iStepCH];

                    this.Invoke(new Action(delegate () // this == Form 이다. Form이 아닌 컨트롤의 Invoke를 직접호출해도 무방하다.
                    {
                        //ProgressHistory.Visible = true;
                        //ProgressHistory.Maximum = ACount;
                        //ProgressHistory.Value = 0;
                        _chart.Visible = false;
                    }));

                    // tdc
                    arrXPS = new float[iStepCH];
                    arrXPSORG = new float[iStepCH];
                                        
                    int iProgressStep = Convert.ToInt32(ListRawTemp.Count / 100);
                    int iProgress = 0;

                    TRaw r = null;
                    //foreach (TRaw r in ListRawTemp)
                    for (int iRow = AFrom; iRow < AFrom + ACount; iRow++)
                    {
                        iProgress++;
                        if (iProgress >= iProgressStep)
                        {
                            //this.Invoke(new Action(delegate () // this == Form 이다. Form이 아닌 컨트롤의 Invoke를 직접호출해도 무방하다.
                            //{
                            //    ProgressHistory.Value = ProgressHistory.Value + iProgress;
                            //}));
                            iProgress = 0;
                        }
                        r = ListRawTemp[iRow];

                        //if (r.IsEmpty == false)
                        {
                            int iCH = 0;
                            while (iCH < 12)
                            {
                                // 1. RAW
                                int iIdx = iCH * 2888 + 8;
                                int iArray;
                                int iCnt = 0;
                                while (iCnt < 1440)
                                {
                                    iArray = ((iRow - AFrom) * 1440) + iCnt;
                                    if (r.IsEmpty)
                                    {
                                        arrX[iCH][iArray] = float.NaN;
                                    }
                                    else
                                    {
                                        float f = 0;
                                        if (DataType == 0)
                                        {
                                            f = (Int16)(BitConverter.ToInt16(r.Raws, iIdx));
                                        }
                                        else if (DataType == 1)
                                        {
                                            f = (UInt16)(BitConverter.ToUInt16(r.Raws, iIdx));
                                        }
                                        arrX[iCH][iArray] = f * 0.1f;

                                        //if ((w > arrMax[iArray].Y) || (iArray == 0))
                                        //{
                                        //    arrMax[iArray].Y = w;
                                        //    arrMax[iArray].X = iArray;
                                        //}
                                    }
                                    iIdx += 2;
                                    iCnt++;
                                }

                                if (FormCalTDC.CylSelected != -1)
                                {
                                    if (iCH == FormCalTDC.CylSelected)
                                    {
                                        iIdx = iCH * 2888 + 8;
                                        iCnt = 0;
                                        while (iCnt < 1440)
                                        {
                                            // PHASE SHIFT 
                                            // iPSOffset = -1440 ~ 1439 기본=0 
                                            int iCntPS = iCnt - FormCalTDC.TDCOffset;
                                            if (iCntPS < 0)
                                                iCntPS = 1440 + iCntPS;
                                            if (iCntPS >= 1440)
                                            {
                                                iCntPS = iCntPS - 1440;
                                            }

                                            iArray = ((iRow - AFrom) * 1440) + iCntPS;

                                            int iArrayPS = arrXPS.Length - iArray - 1;
                                            int iArrayPSORG = arrXPS.Length - (iArray - iCntPS + iCnt) - 1;
                                            if (r.IsEmpty)
                                            {
                                                arrXPS[iArrayPS] = float.NaN;
                                                arrXPSORG[iArray] = float.NaN;
                                            }
                                            else
                                            {
                                                float f = 0;
                                                if (DataType == 0)
                                                {
                                                    f = (Int16)(BitConverter.ToInt16(r.Raws, iIdx));
                                                }
                                                else if (DataType == 1)
                                                {
                                                    f = (UInt16)(BitConverter.ToUInt16(r.Raws, iIdx));
                                                }

                                                arrXPS[iArrayPS] = f;
                                                arrXPSORG[iArray] = f;
                                            }

                                            iIdx += 2;
                                            iCnt++;
                                        }
                                    }
                                }

                                // 3. Knock
                                {
                                    int iKnockValueIdx = iRow - AFrom;
                                    iKnockValueIdx *= 256; // r 하나에 256개

                                    int idxBufThisCH = iCH * (512 + 8);
                                    int i = idxBufThisCH;
                                    while (i < idxBufThisCH + 512)
                                    {                                        
                                        UInt16 w = (UInt16)(BitConverter.ToUInt16(r.BufKnockData, i));

                                        dKnock[iCH][iKnockValueIdx] = w;

                                        iKnockValueIdx++;
                                        i = i + 2;
                                    }
                                }

                                //if (ListCHShift[iCH] != 0)
                                //{
                                //    iIdx = iCH * 2888 + 8;
                                //    iCnt = 0;
                                //    while (iCnt < 1440)
                                //    {
                                //        // PHASE SHIFT 
                                //        // iPSOffset = -1440 ~ 1439 기본=0 
                                //        double dr = 1440 / 720;
                                //        double dd = dr * ListCHShift[iCH];

                                //        iPSOffset = Convert.ToInt32(dd); //ListCHShift[iCH];

                                //        int iCntPS = iCnt - iPSOffset;
                                //        if (iCntPS < 0)
                                //            iCntPS = 1440 + iCntPS;
                                //        if (iCntPS >= 1440)
                                //        {
                                //            iCntPS = iCntPS - 1440;
                                //        }

                                //        iArray = ((iRow - AFrom) * 1440) + iCntPS;

                                //        int iArrayPS = arrX[iCH].Length - (iArray - iCntPS + iCnt) - 1;
                                //        if (r.IsEmpty)
                                //        {
                                //            arrX[iCH][iArray] = float.NaN;
                                //        }
                                //        else
                                //        {
                                //            float f = 0;
                                //            if (DataType == 0)
                                //            {
                                //                f = (Int16)(BitConverter.ToInt16(r.Raws, iIdx));
                                //            }
                                //            else if (DataType == 1)
                                //            {
                                //                f = (UInt16)(BitConverter.ToUInt16(r.Raws, iIdx));
                                //            }
                                //            arrX[iCH][iArray] = f * 0.1f;
                                //        }

                                //        iIdx += 2;
                                //        iCnt++;
                                //    }
                                //}

                                iCH++;
                            }
                        }
                    }
                }
                catch (Exception E)
                {
                    TCS.LogError(E);
                }


                //ListRawTemp.Clear();
            }
            this.Invoke(new Action(delegate () // this == Form 이다. Form이 아닌 컨트롤의 Invoke를 직접호출해도 무방하다.
            {
                GenerateData(TEA.CHMaxUse, 1440, ACount);
                ////Boolean b;
                ////_chart.ViewXY.YAxes[0].Fit(10, out b, false, false);

                //ProgressHistory.Value = ProgressHistory.Maximum;
                //ProgressHistory.Visible = false;
                _chart.Visible = true;

                // knock
                tabChanged();
                
                editIP.Focus();
                ////_zoomChart.ViewXY.XAxes[0].SetRange(0, 1440 * ACount);
                ////_zoomChart.ViewXY.AreaSeries[0].Points = arrMax;
            }));
            ThreadRawsToGraphTemp = null;
        }

        public void chartKnockUpdate()
        {
            try
            {
                if (dKnock == null)
                    return;
                chartKnock.Series[0].Points.SuspendUpdates();
                chartKnock.Series[0].Points.Clear();

                int[][] d = new int[TEA.CHMaxStatic][];
                d[iChartKnockSeriesIdx] = new int[512 / 2];

                int i = 0;
                while (i < d[iChartKnockSeriesIdx].Length)
                {
                    if (i < dKnock[iChartKnockSeriesIdx].Length)
                    {
                        d[iChartKnockSeriesIdx][i] = dKnock[iChartKnockSeriesIdx][i];
                    }
                    i++;
                }

                chartKnock.Series[0].Points.DataBindY(d[iChartKnockSeriesIdx]);
                chartKnock.Series[0].Points.ResumeUpdates();
            }
            catch
            {

            }
        }

        public void chartTDCUpdate()
        {
            try
            {
                // Cal - TDC
                if (FormCalTDC.CylSelected >= 0)
                {
                    float[] xorg = new float[1440];
                    float[] x = new float[1440];

                    int i = 0;
                    while (i < 1440)
                    {
                        if (i < arrXPS.Length)
                            x[i] = arrXPS[i];
                        if (i < arrXPSORG.Length)
                            xorg[i] = arrXPSORG[i];
                        i++;
                    }

                    FormCalTDC._chart.ViewXY.SampleDataSeries[0].SamplesSingle = xorg;// arrXPSORG;
                    FormCalTDC._chart.ViewXY.SampleDataSeries[1].SamplesSingle = x;// arrXPS;
                }

                FormCalTDC._bisfrst = false;
                Boolean b;
                                
                FormCalTDC._chart.ViewXY.YAxes[0].Fit(10, out b, false, false);
                FormCalTDC._chart.ViewXY.YAxes[0].Fit(10, out b, false, false);
                
                FormCalTDC._chart.ViewXY.ZoomToFit();

                FormCalTDC.GraphToUI();
            }
            catch
            {

            }
        }

        private void GenerateData(int channelCount, double frequency, double length)
        {
            System.Windows.Forms.Cursor currentCursor = Cursor;
            Cursor = Cursors.WaitCursor; //Change cursor to wait

            int sampleCount = (int)(frequency * length); //Samples points

            //Disable rendering
            _chart.BeginUpdate();

            //Remove existing series
            ExampleUtils.DisposeAllAndClear(_chart.ViewXY.SampleDataSeries);
            //ExampleUtils.DisposeAllAndClear(_chart.ViewXY.PointLineSeries);

            //Remove existing y-axes
            //ExampleUtils.DisposeAllAndClear(_chart.ViewXY.YAxes);

            for (int channelIndex = 0; channelIndex < channelCount; channelIndex++)
            {
                SampleDataSeries sds = new SampleDataSeries();
                sds.SampleFormat = SampleFormat.SingleFloat;
                sds.FirstSampleTimeStamp = 1; // 1.0 / frequency;
                sds.SamplingFrequency = 1;

                sds.MouseInteraction = false;
                sds.LineStyle.Width = 1;
                sds.LineStyle.AntiAliasing = LineAntialias.None;
                sds.LineStyle.Color = DefaultColors.SeriesForBlackBackground[channelIndex % DefaultColors.SeriesForBlackBackground.Length];

                sds.Title.Text = "CH" + (channelIndex + 1).ToString();
                sds.DataBreaking.Enabled = true;
                sds.DataBreaking.Value = float.NaN; // set data gap defining value (default = NaN)

                _chart.ViewXY.SampleDataSeries.Add(sds);
            }

            for (int channelIndex = 0; channelIndex < channelCount; channelIndex++)
            {
                _chart.ViewXY.SampleDataSeries[channelIndex].LineStyle.Color = TEA.ListColorCH[channelIndex];
            }


            //Update x-axis range
            _chart.ViewXY.XAxes[0].SetRange(0, (double)sampleCount / frequency);

            //Update series with new samples
            for (int channelIndex = 0; channelIndex < channelCount; channelIndex++)
            {
                _chart.ViewXY.SampleDataSeries[channelIndex].SamplesSingle = arrX[channelIndex];
            }

            // -------- for tracking
            //ViewXY v = _chart.ViewXY;
            //AxisX xAxis = v.XAxes[0];
            //AxisY yAxis = v.YAxes[0];

            //PointLineSeries pls = new PointLineSeries(v, xAxis, yAxis);
            //pls.LineStyle.Width = 1;
            //pls.MouseInteraction = false;
            //v.PointLineSeries.Add(pls);

            //int pointCount = Convert.ToInt32(frequency * length);

            //SeriesPoint[] points = new SeriesPoint[pointCount];

            //int iStep = 0;
            //while (iStep < pointCount)
            //{
            //    points[iStep].X = iStep;
            //    points[iStep].Y = 0;

            //    iStep += 1440;
            //}
            //pls.Points = points;


            Cursor = currentCursor;

            //Allow rendering


            try
            {
                _chart.ViewXY.ZoomToFit();
                //Boolean b;
                //_chart.ViewXY.YAxes[0].Fit(10, out b, false, false);
                //_chart.ViewXY.XAxes[0].Fit(out b, false);
            }
            catch (Exception E)
            {

            }
            _chart.EndUpdate();
        }

        public void CHAdd(ref float[] AData, int ACycle = 0)
        {
            String CurrentTime;
            Random Rand_Num = new Random();

            CurrentTime = DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString();

            int nAmountToUpdate = AData.Length / TEA.CHMaxStatic;


        }

        // Window
        public class TCubeSelection
        {
            public TCubeSelection()
            {

            }

            public int StartLast;
            public int EndLast;
            public int Length;
            public void MouseClick(Point p)
            {
                StartLast = Start;
                EndLast = End;
            }

            public Boolean Enabled
            {
                get
                {
                    return true;
                }
            }

            private int _Start = 0;
            public int Start
            {
                get
                {
                    return _Start;
                }
                set
                {
                    _Start = value;
                }
            }
            private int _End = 0;
            public int End
            {
                get
                {
                    return _End;
                }
                set
                {
                    _End = value;

                    Length = _End - _Start;
                }
            }
            
            public int Index; // 0~
        }

        Dictionary<int, TCubeSelection> DicSelection = new Dictionary<int, TCubeSelection>();

        TCubeSelection cs = new TCubeSelection();
        public void InitSelection()
        {
            cs.Index = 0;

            DicSelection.Add(0, cs);
        }

        RectangleF _rectPreviewSelection;
        private void chartPreview_PostPaint(object sender, ChartPaintEventArgs e)
        {

        }

        private void panelPreview_Paint(object sender, PaintEventArgs e)
        {
            // label
            try
            {
                RectangleF _rectfont = _rectPreviewSelection;

                if (_rectfont.Width < 60)
                {
                    int iDiff = 60 - (int)_rectfont.Width;
                    _rectfont.Width += iDiff;
                    _rectfont.X -= (int)(iDiff / 2);
                }
                _rectfont.Height = 10;

                SolidBrush solidBrush = new SolidBrush(TEA.ColorLabelFont);
                string s1 = dtTime[cs.Start].ToString("yy-MM-dd HH:mm:ss").Replace(" ", "\r\n");
                string s2 = dtTime[cs.End].ToString("yy-MM-dd HH:mm:ss").Replace(" ", "\r\n");

                StringFormat stringFormat = new StringFormat();
                stringFormat.Alignment = StringAlignment.Center;
                stringFormat.LineAlignment = StringAlignment.Center;

                // text size = 60 x 30

                Rectangle r1 = new Rectangle();
                r1.X = (int)(_rectPreviewSelection.X - 30);
                r1.Y = 0;
                r1.Width = 60;
                r1.Height = panelPreview.Height;

                e.Graphics.TextRenderingHint = TextRenderingHint.AntiAlias;          
                e.Graphics.DrawString(s1, TEA.fontMd10, solidBrush, r1, stringFormat);

                Rectangle r2 = new Rectangle();
                r2.X = (int)(_rectPreviewSelection.X + _rectPreviewSelection.Width - 30);
                r2.Y = 0;
                r2.Width = 60;
                r2.Height = panelPreview.Height;
                e.Graphics.TextRenderingHint = TextRenderingHint.AntiAlias;
                e.Graphics.DrawString(s1, TEA.fontMd10, solidBrush, r2, stringFormat);

            }
            catch
            {

            }
        }

        private void chartPreview_Paint(object sender, PaintEventArgs e)
        {
            try
            {
                double d1 = dtTime[cs.Start].ToOADate();
                double d2 = dtTime[cs.End].ToOADate();

                var l = (float)chartPreview.ChartAreas[0].AxisX.ValueToPixelPosition(cs.Start);
                var t = 0;// (float)chartPreview.ChartAreas[0].AxisY.ValueToPixelPosition(chartPreview.ChartAreas[0].AxisY.Maximum);
                var r = (float)chartPreview.ChartAreas[0].AxisX.ValueToPixelPosition(cs.End);
                var b = chartPreview.Height;// (float)chartPreview.ChartAreas[0].AxisY.ValueToPixelPosition(chartPreview.ChartAreas[0].AxisY.Minimum);
                _rectPreviewSelection = RectangleF.FromLTRB(l, t, r, b);
                using (var br = new SolidBrush(Color.FromArgb(127, 177, 183, 185)))
                    e.Graphics.FillRectangle(br, _rectPreviewSelection.X, _rectPreviewSelection.Y, _rectPreviewSelection.Width, _rectPreviewSelection.Height);
                e.Graphics.DrawRectangle(new Pen(Color.FromArgb(255, 177, 183, 185)), _rectPreviewSelection.X, _rectPreviewSelection.Y, _rectPreviewSelection.Width, _rectPreviewSelection.Height);
            }
            catch
            {

            }
        }

        public int iPreviewMouseMode = 0;
        public int iPreviewMouseClick = 0;
        public int iPreviewMoveStartX = -1;
        public int iPreviewXToStart = 0;
        public int iPreviewXToEnd = 0;

        private void chartPreview_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                int iOffset = 5;

                if (iPreviewMouseClick == 0)
                {
                    if ((e.Location.X >= _rectPreviewSelection.X - 5) && (e.Location.X <= _rectPreviewSelection.X + 5))
                    {
                        // Left
                        Cursor = Cursors.VSplit;
                    }
                    else if ((e.Location.X >= _rectPreviewSelection.Right - 5) && (e.Location.X <= _rectPreviewSelection.Right + 5))
                    {
                        // Right
                        Cursor = Cursors.VSplit;
                    }
                    else if ((e.Location.X >= _rectPreviewSelection.X + 5) && (e.Location.X <= _rectPreviewSelection.Right - 5))
                    {
                        Cursor = Cursors.Hand;
                    }
                }
                else if (iPreviewMouseClick == 1)
                {
                    int ixi = (int)chartPreview.ChartAreas[0].AxisX.PixelPositionToValue(e.X);

                    int i1 = ixi - iPreviewXToStart;
                    if (i1 < 0)
                        i1 = 0;

                    if (iPreviewMouseMode == -1)
                    {
                        if (i1 >= cs.End - 1)
                            i1 = cs.End - 1;
                        cs.Start = i1;
                    }
                    else if (iPreviewMouseMode == 1)
                    {
                        int iXMax = (int)chartPreview.ChartAreas[0].AxisX.Maximum;
                        int i2 = ixi - iPreviewXToEnd;
                        if (i2 > iXMax)
                            i2 = iXMax;
                        else if (i2 <= cs.Start)
                            i2 = cs.Start + 1;
                        //cs.Length = i2 - cs.Start;
                        cs.End = i2;
                    }
                    else if (iPreviewMouseMode == 2)
                    {
                        int iXMax = (int)chartPreview.ChartAreas[0].AxisX.Maximum;
                        int i2 = i1 + cs.Length;
                        if (i2 > iXMax)
                        {
                            i2 = iXMax;
                            i1 = i2 - cs.Length;
                        }

                        cs.Start = i1;
                        cs.End = i2;
                    }

                    swPreviewChange.Restart();
                    chartPreview.Invalidate();
                }
            }
            catch
            {

            }
            panelPreview.Invalidate();
        }
        
        private void chartPreview_MouseDown(object sender, MouseEventArgs e)
        {
            try
            {
                if (iPreviewMouseClick == 0)
                {
                    iPreviewMouseClick = 1;
                    if ((e.Location.X >= _rectPreviewSelection.X - 5) && (e.Location.X <= _rectPreviewSelection.X + 5))
                    {
                        // Left
                        iPreviewMouseMode = -1;
                    }
                    else if ((e.Location.X >= _rectPreviewSelection.Right - 5) && (e.Location.X <= _rectPreviewSelection.Right + 5))
                    {
                        // Right
                        iPreviewMouseMode = 1;
                    }
                    else if ((e.Location.X >= _rectPreviewSelection.X + 5) && (e.Location.X <= _rectPreviewSelection.Right - 5))
                    {
                        iPreviewMouseMode = 2;
                    }
                }
                iPreviewMoveStartX = (int)chartPreview.ChartAreas[0].AxisX.PixelPositionToValue(e.X); // 클릭 시의 X Index
                iPreviewXToStart = iPreviewMoveStartX - cs.Start;
                iPreviewXToEnd = iPreviewMoveStartX - cs.End;
                cs.MouseClick(e.Location);
            }catch
            {

            }
        }

        private void chartPreview_MouseUp(object sender, MouseEventArgs e)
        {
            iPreviewMouseMode = 0;
            iPreviewMouseClick = 0;
        }

        private void chartPreview_MouseLeave(object sender, EventArgs e)
        {
            iPreviewMouseMode = 0;
            iPreviewMouseClick = 0;
        }

        public void PreviewToRAW(TCubeSelection ACS)
        {
            // cs에 해당하는 날짜 파일 목록
            List<String> l = new List<string>();

            int i = cs.Start;
            while (i < cs.End)
            {
                try
                {
                    l.Add(sDTFile[i]);
                }
                catch
                {

                }
                i++;
            }

            try
            {
                if (dtTime != null)
                {
                    if (cs.Start < dtTime.Length)
                        if (cs.End < dtTime.Length)
                        {
                            ThreadExporter = new Thread(() => RAWLoad(l, dtTime[cs.Start], dtTime[cs.End], false));
                            ThreadExporter.Start();
                        }
                }
            }
            catch
            {

            }
        }

        public List<String> GetRawFiles(DateTime dt1, DateTime dt2)
        {
            // 시작 ~ 종료 파일 목록 구하기
            List<String> l = new List<string>();

            int i = 0;
            string s = "";
          
            Int64 i1 = Convert.ToInt64(dt1.ToString("yyMMddHHmm"));
            Int64 i2 = Convert.ToInt64(dt2.ToString("yyMMddHHmm"));

            String sDir = TCube.GetValue("exportfolder", Application.StartupPath + "\\raw\\");
            DirectoryInfo di = new DirectoryInfo(sDir);

            foreach (var vFile in di.GetFiles())
            {
                String sTime = vFile.Name.Replace("RAW_", "").Replace("_", "").Replace(".dat", "").Substring(0, 10);
                Int64 iTime = Convert.ToInt64(sTime);
                if (iTime >= i1 && iTime <= i2)
                {
                    l.Add(sDir + "\\" + vFile.Name);
                }
            }

            return l;
        }

        public Stopwatch swPreviewChange = new Stopwatch();
        public void DoThreadUI()
        {
            while (true)
            {
                if (swPreviewChange.IsRunning)
                {
                    if (swPreviewChange.ElapsedMilliseconds >= 500)
                    {
                        swPreviewChange.Stop();
                        PreviewToRAW(cs);
                    }
                }

                Thread.Sleep(500);
            }
        }
        
        private void editTimeBegin_Click(object sender, EventArgs e)
        {
            TFormDT f = new TFormDT();

            DateTime dt = DateTime.MinValue;
            if (DateTime.TryParse(editTimeBegin.Text, out dt))
            {
                f.dtp.Value = dt;
            }
            if (f.ShowDialog() == DialogResult.OK) 
            {
                editTimeBegin.Text = f.dtp.Value.ToString("yyyy-MM-dd HH:mm");
            }
        }

        private void editTimeEnd_Click(object sender, EventArgs e)
        {
            TFormDT f = new TFormDT();

            DateTime dt = DateTime.MinValue;
            if (DateTime.TryParse(editTimeEnd.Text, out dt))
            {
                f.dtp.Value = dt;
            }
            if (f.ShowDialog() == DialogResult.OK)
            {
                editTimeEnd.Text = f.dtp.Value.ToString("yyyy-MM-dd HH:mm");
            }
        }

        public void chartRawUpdate()
        {
            panelGraph.Invalidate();

            if (editGraphPYMax.Focused == false)
                editGraphPYMax.Text = Convert.ToDecimal(_chart.ViewXY.YAxes[0].Maximum).ToString("0");

            if (editGraphPYMin.Focused == false)
                editGraphPYMin.Text = Convert.ToDecimal(_chart.ViewXY.YAxes[0].Minimum).ToString("0");
        }

        public void chartCalcUpadte()
        {
            if (dCalcData == null)
                return;
            float[,,] darrCalcData;
            int CalcDataCnt = 0;

            // queue -> real
            //lock (dCalcData)
            {
                darrCalcData = dCalcData;
                CalcDataCnt = iCntCalcData;
            }

            double d = 0;

            this.Invoke(new Action(delegate () // this == Form 이다. Form이 아닌 컨트롤의 Invoke를 직접호출해도 무방하다.
            {
                _chartCalcData.Series.Clear();
                //if (_chartCalcDataRPM.ChartAreas[0].AxisY.auto!= bIsCalcDataRPMYAuto)
                //{

                //}

                int iCHNew = 0;
                while (_chartCalcData.Series.Count < TEA.CHMaxStatic)
                {
                    Series s = new Series();

                    //s.Name = "TEMP_STACK_BOILER_1";
                    if (iCHNew < _chart.ViewXY.SampleDataSeries.Count)
                    {
                        s.Enabled = _chart.ViewXY.SampleDataSeries[iCHNew].Visible;
                        s.ChartType = SeriesChartType.FastLine;
                        s.XValueType = ChartValueType.DateTime;
                        s.YValueType = ChartValueType.Double;
                        s.Color = TEA.ListColorCH[iCHNew];
                        _chartCalcData.Series.Add(s);
                    }
                    iCHNew++;
                }

                _chartCalcDataRPM.Series.SuspendUpdates();
                try
                {
                    _chartCalcDataRPM.Series[0].Points.Clear();
                    int i = cs.Start;
                    while (i <= cs.End)
                    {
                        if (_chartCalcDataRPM.Series[0].Points.Count > 100)
                            _chartCalcDataRPM.Series[0].Points.RemoveAt(0);

                        // RPM
                        d = darrCalcData[6, 0, i];
                        _chartCalcDataRPM.Series[0].Points.Add(d);

                        i++;
                    }
                }
                finally
                {
                    _chartCalcDataRPM.Series.ResumeUpdates();
                    _chartCalcDataRPM.Series.Invalidate();
                    //_chartCalcDataRPM.Series.SuspendUpdates();
                    chartCalcRPMAxis();
                }

                _chartCalcData.Series.SuspendUpdates();
                try
                {
                    int iKind = radioCalcData;
                    while (iKind < 6)
                    {
                        int iCH = 0;
                        while (iCH < TEA.CHMaxStatic)
                        {
                            int i = cs.Start;
                            while (i <= cs.End)
                            {
                                if (_chartCalcData.Series[iCH].Points.Count > 100)
                                    _chartCalcData.Series[iCH].Points.RemoveAt(0);

                                d = darrCalcData[iKind, iCH, i];
                                _chartCalcData.Series[iCH].Points.Add(d);

                                i++;
                            }
                            iCH++;
                        }
                        iKind++;
                    }
                }
                finally
                {
                    _chartCalcData.Series.ResumeUpdates();
                    _chartCalcData.Series.Invalidate();
                    //_chartCalcData.Series.SuspendUpdates();

                    chartCalcAxis();

                }
            }));
        }

        public void chartCalcRPMAxis()
        {
            if (editCalcDataRPMYMax.Focused == false)
            {
                editCalcDataRPMYMax.Tag = 1;
                editCalcDataRPMYMax.Text = _chartCalcDataRPM.ChartAreas[0].AxisY.Maximum.ToString("0");
                editCalcDataRPMYMax.Tag = null;
            }
            if (editCalcDataRPMYMin.Focused == false)
            {
                editCalcDataRPMYMin.Tag = 1;
                editCalcDataRPMYMin.Text = _chartCalcDataRPM.ChartAreas[0].AxisY.Minimum.ToString("0");
                editCalcDataRPMYMin.Tag = null;
            }
        }

        public void chartCalcAxis()
        {
            if (editCalcDataYMax.Focused == false)
            {
                editCalcDataYMax.Tag = 1;
                editCalcDataYMax.Text = _chartCalcData.ChartAreas[0].AxisY.Maximum.ToString("0");
                editCalcDataYMax.Tag = null;
            }
            if (editCalcDataYMin.Focused == false)
            {
                editCalcDataYMin.Tag = 1;
                editCalcDataYMin.Text = _chartCalcData.ChartAreas[0].AxisY.Minimum.ToString("0");
                editCalcDataYMin.Tag = null;
            }
        }

        private void panelGraphPressReset_Click(object sender, EventArgs e)
        {
            editIP.Focus();

            _chart.ViewXY.ZoomToFit();
            chartRawUpdate();
        }
        
        private void editGraphPYMin_TextChanged(object sender, EventArgs e)
        {
            int i = 0;
            TextBox t = (TextBox)sender;
            if (Int32.TryParse(t.Text, out i))
            {
                String[] sa = t.Text.Split('\r');
                if (sa.Length > 0)
                {
                    t.Text = sa[0];
                }

                try
                {
                    _chart.ViewXY.YAxes[0].SetRange(Convert.ToDouble(editGraphPYMin.Text), Convert.ToDouble(editGraphPYMax.Text));
                }
                catch
                {

                }

                //ChartRecalc(_chart);
            }
            else
            {
                //t.Text = "0";
            }
        }
        Boolean bIsCalcDataRPMYAuto = false;
        private void panelCalcDataRPMYReset_Click(object sender, EventArgs e)
        {
            bIsCalcDataRPMYAuto = true;

            _chartCalcDataRPM.ChartAreas[0].AxisY.Maximum = Double.NaN;
            _chartCalcDataRPM.ChartAreas[0].AxisY.Minimum = Double.NaN;

            editIP.Focus();
        }
        Boolean bIsCalcDataYAuto = false;
        private void panelCalcDataYReset_MouseClick(object sender, MouseEventArgs e)
        {
            
                bIsCalcDataYAuto = true;

                _chartCalcData.ChartAreas[0].AxisY.Maximum = Double.NaN;
                _chartCalcData.ChartAreas[0].AxisY.Minimum = Double.NaN;

            editIP.Focus();
        }
        private void editCalcDataRPMYMin_TextChanged(object sender, EventArgs e)
        {
            if (editCalcDataRPMYMin.Tag != null || editCalcDataRPMYMax.Tag != null)
                return;

            int i = 0;
            TextBox t = (TextBox)sender;
            if (Int32.TryParse(t.Text, out i))
            {
                String[] sa = t.Text.Split('\r');
                if (sa.Length > 0)
                {
                    t.Text = sa[0];
                }

                try
                {
                    double dMin = Convert.ToDouble(editCalcDataRPMYMin.Text);
                    double dMax = Convert.ToDouble(editCalcDataRPMYMax.Text);

                    if (dMin < dMax)
                    {
                        if (dMin < _chartCalcDataRPM.ChartAreas[0].AxisY.Maximum)
                        {
                            _chartCalcDataRPM.ChartAreas[0].AxisY.Minimum = dMin;
                            _chartCalcDataRPM.ChartAreas[0].AxisY.Maximum = dMax;
                        }
                        else
                        {
                            _chartCalcDataRPM.ChartAreas[0].AxisY.Maximum = dMax;
                            _chartCalcDataRPM.ChartAreas[0].AxisY.Minimum = dMin;
                        }
                    }
                }
                catch
                {

                }

                //ChartRecalc(_chart);
            }
            else
            {
                //t.Text = "0";
            }
        }

        private void editCalcDataYMin_TextChanged(object sender, EventArgs e)
        {
            if (editCalcDataYMin.Tag != null || editCalcDataYMax.Tag != null)
                return;

            int i = 0;
            TextBox t = (TextBox)sender;
            if (Int32.TryParse(t.Text, out i))
            {
                String[] sa = t.Text.Split('\r');
                if (sa.Length > 0)
                {
                    t.Text = sa[0];
                }

                try
                {
                    double dMin = Convert.ToDouble(editCalcDataYMin.Text);
                    double dMax = Convert.ToDouble(editCalcDataYMax.Text);

                    if (dMin < dMax)
                    {
                        if (dMin < _chartCalcData.ChartAreas[0].AxisY.Maximum)
                        {
                            _chartCalcData.ChartAreas[0].AxisY.Minimum = dMin;
                            _chartCalcData.ChartAreas[0].AxisY.Maximum = dMax;
                        }
                        else
                        {
                            _chartCalcData.ChartAreas[0].AxisY.Maximum = dMax;
                            _chartCalcData.ChartAreas[0].AxisY.Minimum = dMin;
                        }
                    }
                }
                catch
                {

                }

                //ChartRecalc(_chart);
            }
            else
            {
                //t.Text = "0";
            }
        }

        int iChartKnockSeriesIdx = 0;
        private void panelGraphKnock_Paint(object sender, PaintEventArgs e)
        {
            if (panelGraphKnock.Visible == false)
                return;

            Graphics g = e.Graphics;
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;

            ControlPaint.DrawBorder(g, ((Panel)sender).DisplayRectangle, TEA.ColorPanelBorder, ButtonBorderStyle.Solid);

            //if (tabSelected(labelGraphp) || tabSelected(labelGraphCalc))
            {
                int y = 6;
                int x = 31;
                int i = 0;

                Pen pen = new Pen(TEA.ColorPanelBorder);

                while (i < 12)
                {
                    y += 35;

                    // circle
                    Rectangle rectCircle = new Rectangle(x, y, 10, 10);
                    Boolean bb = false;
                    if (i < TEA.CHMaxStatic)
                    {
                        if (i == iChartKnockSeriesIdx)
                        {
                            SolidBrush b = new SolidBrush(TEA.ListColorCH[i]);
                            g.FillEllipse(b, rectCircle);

                            bb = true;
                        }
                    }

                    if (bb == false)
                    {
                        Pen p = new Pen(TEA.ColorPanelBorder);
                        g.DrawEllipse(p, rectCircle);
                    }

                    // label
                    SolidBrush solidBrush = new SolidBrush(TEA.ColorLabelFont);
                    string s = "Cyl. " + (i + 1).ToString();

                    g.TextRenderingHint = TextRenderingHint.AntiAlias;
                    g.DrawString(s, TEA.fontMd14, solidBrush, new PointF(x + 16, y - 3));

                    // line
                    if (i < 11)
                        g.DrawLine(pen, x - 6, y + 5 + 16, x - 6 + 75, y + 5 + 16);

                    //
                    String sKey = "checkCHVisible" + i.ToString();
                    if (DicSeriesRadioEvent.ContainsKey(sKey) == false)
                    {
                        DicSeriesRadioEvent.Add(sKey, new Rectangle(x, y - 3, 75, 16));
                    }

                    i++;
                }
            }
        }

        private void panelGraphKnock_MouseMove(object sender, MouseEventArgs e)
        {
            Rectangle r = new Rectangle(0, 0, 120, 475);

            if (r.IntersectsWith(new Rectangle(e.Location, new Size(1, 1))))
            {
                Boolean bIsFind = false;
                foreach (KeyValuePair<String, Rectangle> vp in DicSeriesRadioEvent)
                {
                    if (vp.Value.IntersectsWith(new Rectangle(e.Location, new Size(1, 1))))
                    {
                        bIsFind = true;
                        break;
                    }
                }
                if (bIsFind)
                {
                    TEA.MouseHand();
                }
                else
                {
                    TEA.MouseNormal();
                }
            }

            EventMouseMove(sender, e.Location);
        }

        private void panelGraphKnock_MouseDown(object sender, MouseEventArgs e)
        {
            foreach (KeyValuePair<String, Rectangle> vp in DicSeriesRadioEvent)
            {
                if (vp.Value.IntersectsWith(new Rectangle(e.Location, new Size(1, 1))))
                {
                    if (DicSeriesRadioEventDown.ContainsKey(vp.Key) == false)
                        DicSeriesRadioEventDown.Add(vp.Key, vp.Value);
                }
            }

            EventMouseDown(sender, e.Location);
        }

        private void panelGraphKnock_MouseUp(object sender, MouseEventArgs e)
        {
            foreach (KeyValuePair<String, Rectangle> vp in DicSeriesRadioEvent)
            {
                if (DicSeriesRadioEventDown.ContainsKey(vp.Key))
                {
                    if (vp.Value.IntersectsWith(new Rectangle(e.Location, new Size(1, 1))))
                    {
                        Boolean bIsCtrl = false;
                        if (e.Button == MouseButtons.Left && (ModifierKeys & Keys.Control) == Keys.Control)
                        {
                            bIsCtrl = true;
                        }
                        try
                        {
                            iChartKnockSeriesIdx = Convert.ToInt32(vp.Key.Substring(14));
                            if (tabSelected(labelKnock))
                            {
                                chartKnockUpdate();
                            }
                        }
                        catch
                        {
                        }
                    }
                }
            }
            DicSeriesRadioEventDown.Clear();

            panelGraphKnock.Invalidate();

            EventMouseUp(sender, e.Location);
        }

        public Boolean IsCheckAlarm()
        {
            return checkAlarm.Image != null;
        }

        private void checkAlarm_Click(object sender, EventArgs e)
        {
            if (checkAlarm.Image != null)
            {
                checkAlarm.Image = null;
            }
            else
            {
                checkAlarm.Image = checkOn.Image;
            }
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            editAlarmFilter.Text = "";

            focusOff();
        }

        public void focusOff()
        {
            editIP.Focus();
        }

        private void buttonExpression_Click(object sender, EventArgs e)
        {
            focusOff();
        }

        private void buttonExport_Click(object sender, EventArgs e)
        {
            if (DialogSave.ShowDialog() == DialogResult.OK)
            {
                ThreadExporter = new Thread(() => RAWToCSV(
                            lRawFiles,
                            null,
                            ProgressHistory));
                ThreadExporter.Start();
            }
        }


        public String Delimiter = ",";

        // 측정각도 가로
        public void RAWToCSV(List<String> AFiles, DataGridView AGrid, ProgressBar ABar, Boolean AAppend = false)
        {
            Boolean bFilter = true;

            try
            {
                try
                {
                    arrX = new float[TEA.CHMaxStatic][];

                    //if (AAppend == false)
                    //{
                    //    lock (ListRaw)
                    //    {
                    //        ListRaw.Clear();
                    //    }
                    //}

                    // 1. 각 파일의 사이즈를 구해 prgoress 계산
                    long lTotal = 0;
                    foreach (String AFile in AFiles)
                    {
                        if (File.Exists(AFile))
                        {
                            long length = new System.IO.FileInfo(AFile).Length;
                            lTotal += length;
                        }
                    }

                    int iStep = Convert.ToInt32(lTotal / 100);
                    int iCntProgress = 0; // 0 ~ 총 파일의 크기만큼 증가

                    if (ABar != null)
                    {
                        this.Invoke(new Action(delegate () // this == Form 이다. Form이 아닌 컨트롤의 Invoke를 직접호출해도 무방하다.
                        {
                            ProgressHistory.Maximum = 100;
                            ProgressHistory.Value = 0;
                            ProgressHistory.Visible = true;
                        }));
                    }

                    StringBuilder sb = new StringBuilder();
                    String sRow = "";

                    // 5. CSV

                    System.IO.StreamWriter file = new System.IO.StreamWriter(DialogSave.FileName);

                    sRow = "time" + Delimiter + "cylinder";
                    sb.Append(sRow);

                    int iCH = 0;
                    while (iCH < 1440)
                    {
                        //sRow += Delimiter + (iCH + 1).ToString();
                        sb.Append(Delimiter + (iCH + 1).ToString());
                        iCH++;
                    }

                    // 3. Calc Data
                    iCH = 0;
                    while (iCH < 144)
                    {
                        sb.Append(Delimiter + "ADDR " + (iCH + 501).ToString());
                        iCH++;
                    }

                    //// 4. Konck
                    //iCH = 0;
                    //while (iCH < 144)
                    //{
                    //    sb.Append(Delimiter + "ADDR " + (iCH + 501).ToString());
                    //    iCH++;
                    //}

                    sb.Append("\r\n");
                    //sb.Append(sRow + "\r\n");

                    file.Write(sb.ToString());
                    sb.Clear();

                    // 3. 각 파일 loop
                    foreach (String AFile in AFiles)
                    {
                        long length = new System.IO.FileInfo(AFile).Length;

                        FileStream fs = new FileStream(AFile, FileMode.Open);

                        TRaw r = null;
                        StringBuilder sbRowEnd = new StringBuilder();

                        while (true)
                        {
                            iCntProgress += TEA.RowSizeInRAWFile; // 각 row 사이즈만큼 증가
                            if (iCntProgress >= iStep)
                            {
                                iCntProgress = iCntProgress % iStep;
                                if (ABar != null)
                                {
                                    this.Invoke(new Action(delegate () // this == Form 이다. Form이 아닌 컨트롤의 Invoke를 직접호출해도 무방하다.
                                    {
                                        int i = ProgressHistory.Value + 1;
                                        if (i < ProgressHistory.Maximum)
                                            ProgressHistory.Value = i;
                                    }));
                                }
                            }

                            if (bIsCancelHistory)
                            {
                                return;
                            }

                            if (r == null)
                            {
                                r = new TRaw();
                            }

                            byte[] b;
                            // 1. time=8
                            b = new byte[8];
                            int iRemain = fs.Read(b, 0, 8);
                            if (iRemain == 0)
                            {
                                break;
                            }

                            // 2. data body
                            iRemain = fs.Read(r.Raws, 0, 2888 * 12);
                            if (iRemain == 0)
                            {
                                break;
                            }

                            long nVal = BitConverter.ToInt64(b, 0);
                            DateTime dtNow = DateTime.FromBinary(nVal);
                            r.Time = dtNow;

                            if ((bFilter == false) || ((dtNow >= dt1) && (dtNow <= dt2)))
                            {
                                if ((r.Raws[0] == 0xFF) && (r.Raws[1] == 0xFF))
                                {
                                    // r.Raws의 28부터 시작
                                    // 3. Calc Data
                                    int iOffset = 28 + 8;
                                    int iCnt = 0;
                                    while (iCnt < 144)
                                    {
                                        float f = 0;
                                        if (DataType == 0)
                                        {
                                            f = (Int16)(BitConverter.ToInt16(r.Raws, iOffset));
                                        }
                                        else if (DataType == 1)
                                        {
                                            f = (UInt16)(BitConverter.ToUInt16(r.Raws, iOffset));
                                        }

                                        String sValue = f.ToString("0.0000");

                                        iOffset += 2;
                                        iCnt++;

                                        sbRowEnd.Append(Delimiter + sValue);
                                    }

                                    //// 4. Knock Data
                                    //iIdx = 8;
                                    //iCnt = 0;
                                    //while (iIdx < r.BufKnockData.Length)
                                    //{
                                    //    float f = 0;
                                    //    if (DataType == 0)
                                    //    {
                                    //        f = (Int16)(BitConverter.ToInt16(r.BufKnockData, iIdx));
                                    //    }
                                    //    else if (DataType == 1)
                                    //    {
                                    //        f = (UInt16)(BitConverter.ToUInt16(r.BufKnockData, iIdx));
                                    //    }

                                    //    sValue = f.ToString("0.0000");

                                    //    iIdx += 2;
                                    //    iCnt++;

                                    //    sbRow.Append(Delimiter + sValue);
                                    //}

                                    continue; // 다음 row = data row
                                }
                                else
                                {
                                    // 3. cycle
                                    r.Cycle = BitConverter.ToUInt16(r.Raws, 6);

                                    if (iLastCycle == -1)
                                    {
                                        iLastCycle = r.Cycle - 1;
                                        iMaxCycle = r.Cycle;
                                    }

                                    iLastCycle = r.Cycle;

                                    if (r.Cycle > iMaxCycle)
                                        iMaxCycle = r.Cycle;

                                    // 4. Row List add
                                    //lock (ListRaw)
                                    //{
                                    //    ListRaw.Add(r);
                                    //}

                                    sRow = "";

                                    iCH = 0;
                                    while (iCH < 12)
                                    {
                                        StringBuilder sbRowBegin = new StringBuilder();

                                        //sRow = r.Time.ToString("yyyy-MM-dd HH:mm:ss.fff");
                                        sbRowBegin.Append(r.Time.ToString("yyyy-MM-dd HH:mm:ss.fff"));

                                        //sRow += Delimiter + (iCH + 1).ToString();
                                        sbRowBegin.Append(Delimiter + (iCH + 1).ToString());

                                        int iIdx = iCH * 2888 + 8;
                                        int iArray;
                                        int iCnt = 0;
                                        String sValue = "";
                                        while (iCnt < 1440)
                                        {
                                            iArray = iCnt;
                                            if (r.IsEmpty)
                                            {
                                                //arrX[iCH][iArray] = float.NaN;
                                            }
                                            else
                                            {
                                                float f = 0;
                                                if (DataType == 0)
                                                {
                                                    f = (Int16)(BitConverter.ToInt16(r.Raws, iIdx));
                                                }
                                                else if (DataType == 1)
                                                {
                                                    f = (UInt16)(BitConverter.ToUInt16(r.Raws, iIdx));
                                                }

                                                sValue = f.ToString("0.0000");
                                                //arrX[iCH][iArray] = w;

                                                //if ((w > arrMax[iArray].Y) || (iArray == 0))
                                                //{
                                                //    arrMax[iArray].Y = w;
                                                //    arrMax[iArray].X = iArray;
                                                //}
                                            }
                                            iIdx += 2;
                                            iCnt++;

                                            sbRowBegin.Append(Delimiter + sValue);
                                            //sRow += Delimiter + sValue;
                                        }

                                        if (sbRowEnd.Length == 0)
                                        {
                                            int iTemp = 0;
                                            while (iTemp < 144)
                                            {
                                                sbRowEnd.Append(Delimiter);
                                                iTemp++;
                                            }
                                        }
                                        sb.Append(sbRowBegin.ToString() + sbRowEnd.ToString() + "\r\n");

                                        file.Write(sb.ToString());
                                        sb.Clear();

                                        iCH++;
                                    }

                                    sbRowEnd.Clear();
                                    r = null;
                                }


                                //if (ListRaw.Count > 20000)
                                //{
                                //    this.Invoke(new Action(delegate () // this == Form 이다. Form이 아닌 컨트롤의 Invoke를 직접호출해도 무방하다.
                                //    {
                                //        TCS.ShowMsg("메모리 크기 제한으로 20000개의 Cycle만 Loading 합니다.");
                                //    }));

                                //    fs.Close();
                                //    return;
                                //}
                                // 5. List 추가 후 그래프 추가 쓰레드에서 byte -> float 후 byte는 삭제
                            }
                        }
                        fs.Close();
                    }

                    file.Close();
                    file = null;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    //if (AGrid != null)
                    //{
                    //    this.Invoke(new Action(delegate () // this == Form 이다. Form이 아닌 컨트롤의 Invoke를 직접호출해도 무방하다.
                    //    {
                    //        vscrollRows.Maximum = ListRaw.Count * 1440;

                    //        EditResultCycleCount.Text = ListRaw.Count.ToString();

                    //        InitGrid(AGrid);
                    //        if (ABar != null)
                    //        {
                    //            ProgressHistory.Visible = false;
                    //        }
                    //    }));
                    //}

                    if (bIsCancelHistory)
                    {
                        if (ThreadExporter != null)
                        {
                            ThreadExporter.Abort();
                        }
                    }
                    else
                    {
                        this.Invoke(new Action(delegate () // this == Form 이다. Form이 아닌 컨트롤의 Invoke를 직접호출해도 무방하다.
                        {
                            if (MessageBox.Show("CSV 저장이 완료되었습니다. 파일을 여시겠습니까?", "파일 저장 완료", MessageBoxButtons.YesNo)
                                == System.Windows.Forms.DialogResult.Yes)
                            {
                                try
                                {
                                    Process process = new Process();
                                    process.StartInfo.FileName = DialogSave.FileName;
                                    process.Start();
                                }
                                catch
                                {

                                }
                            }
                        }));
                    }
                    //ThreadRawsToGraphTemp = new Thread(() => ThreadRawsToGraph(
                    //        0, ListRaw.Count
                    //        ));
                    //ThreadRawsToGraphTemp.Start();
                    ThreadExporter = null;
                    this.Invoke(new Action(delegate () // this == Form 이다. Form이 아닌 컨트롤의 Invoke를 직접호출해도 무방하다.
                    {
                        ProgressHistory.Visible = false;
                    }));
                }
            }
            catch (Exception E)
            {
                TCS.LogError(E);
            }
        }

        private void panelGraphTDC_Paint(object sender, PaintEventArgs e)
        {
            if (panelGraphTDC.Visible == false)
                return;

            Graphics g = e.Graphics;
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;

            ControlPaint.DrawBorder(g, ((Panel)sender).DisplayRectangle, TEA.ColorPanelBorder, ButtonBorderStyle.Solid);
        }

        public void RAWToCSV2(List<String> AFiles, DataGridView AGrid, ProgressBar ABar, Boolean AAppend = false)
        {
            Boolean bFilter = true;

            try
            {
                try
                {
                    arrX = new float[TEA.CHMaxStatic][];

                    Int16[] iaOffset = new Int16[TEA.CHMaxStatic];
                    Boolean bHasAngle = false;
                    int iCHMaxThisRaw = 12;

                    //if (AAppend == false)
                    //{
                    //    lock (ListRaw)
                    //    {
                    //        ListRaw.Clear();
                    //    }
                    //}

                    // 1. 각 파일의 사이즈를 구해 prgoress 계산
                    long lTotal = 0;
                    foreach (String AFile in AFiles)
                    {
                        if (File.Exists(AFile))
                        {
                            long length = new System.IO.FileInfo(AFile).Length;
                            lTotal += length;
                        }
                    }

                    int iStep = Convert.ToInt32(lTotal / 100);
                    int iCntProgress = 0; // 0 ~ 총 파일의 크기만큼 증가

                    if (ABar != null)
                    {
                        this.Invoke(new Action(delegate () // this == Form 이다. Form이 아닌 컨트롤의 Invoke를 직접호출해도 무방하다.
                        {
                            ProgressHistory.Maximum = 100;
                            ProgressHistory.Value = 0;
                            ProgressHistory.Visible = true;
                        }));
                    }

                    StringBuilder sb = new StringBuilder();
                    String sRow = "";

                    // 5. CSV
                    if (File.Exists(DialogSave.FileName) == false)
                    {
                        sRow = "CrankAngle /deg;Pressure SAE#01 /bar;Pressure SAE#02 /bar;Pressure SAE#03 /bar;Pressure SAE#04 /bar;Pressure SAE#05 /bar;Pressure SAE#06 /bar;Pressure SAE#07 /bar;Pressure SAE#08 /bar;Time" + "\r\n";
                        if (Delimiter == ",")
                        {
                            sRow = sRow.Replace(";", ",");
                        }
                    }

                    System.IO.StreamWriter file = new System.IO.StreamWriter(DialogSave.FileName);

                    if (sRow != "")
                    {
                        file.Write(sRow);
                    }

                    int iCH = 0;
                    ////while (iCH < 1440)
                    ////{
                    ////    sRow += "," + (iCH + 1).ToString();
                    ////    iCH++;
                    ////}
                    //sb.Append(sRow + "\r\n");

                    // 3. 각 파일 loop
                    foreach (String AFile in AFiles)
                    {
                        long length = new System.IO.FileInfo(AFile).Length;

                        FileStream fs = new FileStream(AFile, FileMode.Open);

                        while (true)
                        {
                            iCntProgress += TEA.RowSizeInRAWFile; // 각 row 사이즈만큼 증가
                            if (iCntProgress >= iStep)
                            {
                                iCntProgress = iCntProgress % iStep;
                                if (ABar != null)
                                {
                                    this.Invoke(new Action(delegate () // this == Form 이다. Form이 아닌 컨트롤의 Invoke를 직접호출해도 무방하다.
                                    {
                                        int i = ProgressHistory.Value + 1;
                                        if (i < ProgressHistory.Maximum)
                                            ProgressHistory.Value = i;
                                    }));
                                }
                            }

                            if (bIsCancelHistory)
                            {
                                return;
                            }

                            TRaw r = new TRaw();

                            byte[] b;
                            // 1. time=8
                            b = new byte[8];
                            int iRemain = fs.Read(b, 0, 8);
                            if (iRemain == 0)
                            {
                                break;
                            }

                            // 2. data body
                            iRemain = fs.Read(r.Raws, 0, 2888 * 12); if (iRemain == 0)
                            {
                                break;
                            }

                            long nVal = BitConverter.ToInt64(b, 0);
                            DateTime dtNow = DateTime.FromBinary(nVal);
                            r.Time = dtNow;

                            if ((bFilter == false) || ((dtNow >= dt1) && (dtNow <= dt2)))
                            {
                                if ((r.Raws[0] == 0xFF) && (r.Raws[1] == 0xFF))
                                {
                                    iCHMaxThisRaw = r.Raws[2];

                                    // 0-3. CH's Offset 
                                    int iCHInOffset = 0;
                                    while (iCHInOffset < iCHMaxThisRaw)
                                    {
                                        int iOffset = 4 + (Convert.ToInt32(iCHInOffset) * 2);
                                        Int16 wOffset = BitConverter.ToInt16(r.Raws, iOffset);

                                        iaOffset[iCHInOffset] = wOffset;
                                        iCHInOffset++;
                                    }
                                    bHasAngle = true;
                                }
                                else
                                {
                                    // 3. cycle
                                    r.Cycle = BitConverter.ToUInt16(r.Raws, 6);

                                    if (bHasAngle)
                                    {
                                        for (int j = 0; j < 12; j++)
                                        {
                                            r.AngleOffset[j] = iaOffset[j] / 10;
                                        }
                                    }

                                    if (iLastCycle == -1)
                                    {
                                        iLastCycle = r.Cycle - 1;
                                        iMaxCycle = r.Cycle;
                                    }

                                    iLastCycle = r.Cycle;

                                    if (r.Cycle > iMaxCycle)
                                        iMaxCycle = r.Cycle;

                                    // 4. Row List add
                                    //lock (ListRaw)
                                    //{
                                    //    ListRaw.Add(r);
                                    //}

                                    string[] sa1Cycle = new string[1440];

                                    iCH = 0;
                                    while (iCH < iCHMaxThisRaw)
                                    {
                                        int iIdx = iCH * 2888 + 8;
                                        int iArray;
                                        int iCnt = 0;
                                        String sValue = "";
                                        while (iCnt < 1440)
                                        {
                                            // CH=0일때 각도 추가
                                            if (iCH == 0)
                                            {
                                                double d = iCnt;
                                                d = (d / 2) - 360;

                                                // offset = -1
                                                // org = -360
                                                // after = -361

                                                d = d + r.AngleOffset[iCH];

                                                sa1Cycle[iCnt] = d.ToString("0.0"); //r.Time.ToString("yyyy-MM-dd HH:mm:ss.fff");
                                            }

                                            iArray = iCnt;
                                            if (r.IsEmpty)
                                            {
                                                //arrX[iCH][iArray] = float.NaN;
                                                //sValue = "";
                                            }
                                            else
                                            {
                                                float f = 0;
                                                if (DataType == 0)
                                                {
                                                    f = (Int16)(BitConverter.ToInt16(r.Raws, iIdx));
                                                }
                                                else if (DataType == 1)
                                                {
                                                    f = (UInt16)(BitConverter.ToUInt16(r.Raws, iIdx));
                                                }

                                                Single ss = f / 10;
                                                sValue = ss.ToString("0.0000");
                                                //arrX[iCH][iArray] = w;

                                                //if ((w > arrMax[iArray].Y) || (iArray == 0))
                                                //{
                                                //    arrMax[iArray].Y = w;
                                                //    arrMax[iArray].X = iArray;
                                                //}
                                            }
                                            sa1Cycle[iCnt] = sa1Cycle[iCnt] + Delimiter + sValue;

                                            if (iCnt == 0)
                                            {
                                                if (iCH == iCHMaxThisRaw - 1)
                                                {
                                                    sa1Cycle[iCnt] = sa1Cycle[iCnt] + Delimiter + r.Time.ToString("yyyy-MM-dd HH:mm:ss.fff");
                                                }
                                            }

                                            iIdx += 2;
                                            iCnt++;
                                        }
                                        iCH++;
                                    }

                                    String sTotal = String.Join("\r\n", sa1Cycle) + "\r\n";

                                    file.Write(sTotal);
                                    //sb.Clear();



                                    //if (ListRaw.Count > 20000)
                                    //{
                                    //    this.Invoke(new Action(delegate () // this == Form 이다. Form이 아닌 컨트롤의 Invoke를 직접호출해도 무방하다.
                                    //    {
                                    //        TCS.ShowMsg("메모리 크기 제한으로 20000개의 Cycle만 Loading 합니다.");
                                    //    }));

                                    //    fs.Close();
                                    //    return;
                                    //}
                                    // 5. List 추가 후 그래프 추가 쓰레드에서 byte -> float 후 byte는 삭제
                                }
                            }
                        }
                        fs.Close();
                    }

                    file = null;
                }
                finally
                {
                    //if (AGrid != null)
                    //{
                    //    this.Invoke(new Action(delegate () // this == Form 이다. Form이 아닌 컨트롤의 Invoke를 직접호출해도 무방하다.
                    //    {
                    //        vscrollRows.Maximum = ListRaw.Count * 1440;

                    //        EditResultCycleCount.Text = ListRaw.Count.ToString();

                    //        InitGrid(AGrid);
                    //        if (ABar != null)
                    //        {
                    //            ProgressHistory.Visible = false;
                    //        }
                    //    }));
                    //}

                    if (bIsCancelHistory)
                    {
                        if (ThreadExporter != null)
                        {
                            ThreadExporter.Abort();
                        }
                    }
                    else
                    {
                        this.Invoke(new Action(delegate () // this == Form 이다. Form이 아닌 컨트롤의 Invoke를 직접호출해도 무방하다.
                        {
                            if (MessageBox.Show("CSV 저장이 완료되었습니다. 파일을 여시겠습니까?", "파일 저장 완료", MessageBoxButtons.YesNo)
                                == System.Windows.Forms.DialogResult.Yes)
                            {
                                try
                                {
                                    Process process = new Process();
                                    process.StartInfo.FileName = DialogSave.FileName;
                                    process.Start();
                                }
                                catch
                                {

                                }
                            }
                        }));
                    }
                    //ThreadRawsToGraphTemp = new Thread(() => ThreadRawsToGraph(
                    //        0, ListRaw.Count
                    //        ));
                    //ThreadRawsToGraphTemp.Start();
                    ThreadExporter = null;
                }
            }
            catch (Exception E)
            {
                TCS.LogError(E);
            }
        }

    }
}

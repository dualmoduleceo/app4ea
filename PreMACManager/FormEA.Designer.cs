﻿namespace EngineAnalyzer
{
    partial class TFormEA
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TFormEA));
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.TabEA = new System.Windows.Forms.TabControl();
            this.TabGraph = new System.Windows.Forms.TabPage();
            this.TabControlGraph = new System.Windows.Forms.TabControl();
            this.TabCycle1 = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label24 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label26 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.splitter5 = new System.Windows.Forms.Splitter();
            this.tabKnockIntensity = new System.Windows.Forms.TabPage();
            this.splitter7 = new System.Windows.Forms.Splitter();
            this.chartCyl = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chartKnock = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.comboKnockCH = new System.Windows.Forms.ComboBox();
            this.label27 = new System.Windows.Forms.Label();
            this.splitter4 = new System.Windows.Forms.Splitter();
            this.splitter3 = new System.Windows.Forms.Splitter();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label22 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.buttonCHSet = new System.Windows.Forms.Button();
            this.ButtonYAxis = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.CheckListCH = new System.Windows.Forms.CheckedListBox();
            this.CheckCHAllVIsible = new System.Windows.Forms.CheckBox();
            this.PanelData = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.tabSetting = new System.Windows.Forms.TabPage();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.ButtonReq = new System.Windows.Forms.Button();
            this.TabHistory = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.checkLog = new System.Windows.Forms.CheckBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.EditPeriod = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.EditFrequency = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.ButtonDelaySet = new System.Windows.Forms.Button();
            this.EditDelayForRecv = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.EditReqTarget = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.ButtonEADisconnect = new System.Windows.Forms.Button();
            this.ButtonEAConnect = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.buttonDataTypeSet = new System.Windows.Forms.Button();
            this.comboDataType = new System.Windows.Forms.ComboBox();
            this.ButtonCHMaxSet = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.EditCHMax = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.ButtonCycleCountSet = new System.Windows.Forms.Button();
            this.EditCycleCount = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.CheckGraphSkip = new System.Windows.Forms.CheckBox();
            this.CheckYAuto = new System.Windows.Forms.CheckBox();
            this.ButtonYAxisSet = new System.Windows.Forms.Button();
            this.labelymax = new System.Windows.Forms.Label();
            this.labelymin = new System.Windows.Forms.Label();
            this.EditXCount = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ButtonFolderSelect = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ButtonDisconnect = new System.Windows.Forms.Button();
            this.ButtonConnect = new System.Windows.Forms.Button();
            this.EditPort = new System.Windows.Forms.TextBox();
            this.EditIP = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this._chart = new Arction.WinForms.Charting.LightningChartUltimate();
            this.CheckRT1 = new System.Windows.Forms.CheckBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.LabelConnection = new System.Windows.Forms.ToolStripStatusLabel();
            this.LabelEALive = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.LabelDPS = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel4 = new System.Windows.Forms.ToolStripStatusLabel();
            this.LabelExport = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.LabelReq = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel5 = new System.Windows.Forms.ToolStripStatusLabel();
            this.LabelCHGetCnt = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel6 = new System.Windows.Forms.ToolStripStatusLabel();
            this.LabelCycle = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel9 = new System.Windows.Forms.ToolStripStatusLabel();
            this.LabelCSC = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel10 = new System.Windows.Forms.ToolStripStatusLabel();
            this.LabelCSPS = new System.Windows.Forms.ToolStripStatusLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ButtonStop = new System.Windows.Forms.Button();
            this.ButtonStart = new System.Windows.Forms.Button();
            this.ButtonConnectToggle = new System.Windows.Forms.Button();
            this.EditStatus = new System.Windows.Forms.Label();
            this.DialogFolder = new System.Windows.Forms.FolderBrowserDialog();
            this.DialogOpen = new System.Windows.Forms.OpenFileDialog();
            this.TimerTick = new System.Windows.Forms.Timer(this.components);
            this.statusStrip2 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel7 = new System.Windows.Forms.ToolStripStatusLabel();
            this.LabelRawQueue = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel8 = new System.Windows.Forms.ToolStripStatusLabel();
            this.LabelSaveQueue = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel11 = new System.Windows.Forms.ToolStripStatusLabel();
            this.LabelCPS = new System.Windows.Forms.ToolStripStatusLabel();
            this.PanelDisconnect = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.imageConOff = new System.Windows.Forms.PictureBox();
            this.imageConOn = new System.Windows.Forms.PictureBox();
            this.chartPMax = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panelGraph = new System.Windows.Forms.Panel();
            this.panelGraphRight = new System.Windows.Forms.Panel();
            this.comboYMax = new System.Windows.Forms.Panel();
            this.comboYMin = new System.Windows.Forms.Panel();
            this.editGraphPYMax = new System.Windows.Forms.TextBox();
            this.editGraphPYMin = new System.Windows.Forms.TextBox();
            this.buttonGraphPAxisReset = new System.Windows.Forms.Panel();
            this.comboY = new System.Windows.Forms.ListBox();
            this.panelGraphCalcLogginData = new System.Windows.Forms.Panel();
            this.comboCalcDataYMax = new System.Windows.Forms.Panel();
            this.comboCalcDataYMin = new System.Windows.Forms.Panel();
            this.comboCalcDataRPMMax = new System.Windows.Forms.Panel();
            this.comboCalcDataRPMYMin = new System.Windows.Forms.Panel();
            this.editCalcDataRPMYMax = new System.Windows.Forms.TextBox();
            this.editCalcDataRPMYMin = new System.Windows.Forms.TextBox();
            this.panelCalcDataYReset = new System.Windows.Forms.Panel();
            this.editCalcDataYMax = new System.Windows.Forms.TextBox();
            this.editCalcDataYMin = new System.Windows.Forms.TextBox();
            this.panelCalcDataRPMYReset = new System.Windows.Forms.Panel();
            this.panelGraphCalc = new System.Windows.Forms.Panel();
            this._chartCalcData = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this._chartCalcDataRPM = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.labelAlarmSys = new System.Windows.Forms.Label();
            this.panelAlarm = new System.Windows.Forms.Panel();
            this.labelAlarmHKnock = new System.Windows.Forms.Label();
            this.labelAlarmLKnock = new System.Windows.Forms.Label();
            this.labelAlarmMisfire = new System.Windows.Forms.Label();
            this.labelAlarmFault = new System.Windows.Forms.Label();
            this.panelCalcData = new System.Windows.Forms.Panel();
            this.panelGraphCalcDataBottom = new System.Windows.Forms.Panel();
            this.label28 = new System.Windows.Forms.Label();
            this.labelCalcDataAvg1 = new System.Windows.Forms.Label();
            this.labelCalcDataAvg = new System.Windows.Forms.Label();
            this.labelCalcDataKnockIntst = new System.Windows.Forms.Label();
            this.labelCalcDataPegging = new System.Windows.Forms.Label();
            this.labelCalcDataMisfire = new System.Windows.Forms.Label();
            this.labelCalcDataHR = new System.Windows.Forms.Label();
            this.labelCalcDataIMEP = new System.Windows.Forms.Label();
            this.labelCalcDataPmax = new System.Windows.Forms.Label();
            this.labelGraph = new System.Windows.Forms.Label();
            this.labelGraphCalc = new System.Windows.Forms.Label();
            this.labelBottomMsg = new System.Windows.Forms.Label();
            this.labelMenuSelected = new System.Windows.Forms.Label();
            this.labelRPMTitle = new System.Windows.Forms.Label();
            this.labelAlarm = new System.Windows.Forms.Label();
            this.labelCalcData = new System.Windows.Forms.Label();
            this.panelRPM = new System.Windows.Forms.Panel();
            this.labelRPM = new System.Windows.Forms.Label();
            this.panelSetting = new System.Windows.Forms.Panel();
            this.buttonSettingClose = new System.Windows.Forms.Button();
            this.panelTop = new System.Windows.Forms.Panel();
            this.panelMenuDignosis = new System.Windows.Forms.Panel();
            this.labelMenuDiagnosis = new System.Windows.Forms.Label();
            this.panelMenuCalibration = new System.Windows.Forms.Panel();
            this.labelMenuCal = new System.Windows.Forms.Label();
            this.panelMonitoring = new System.Windows.Forms.Panel();
            this.labelMenuMonitoring = new System.Windows.Forms.Label();
            this.imgBack = new System.Windows.Forms.PictureBox();
            this.buttonUser = new System.Windows.Forms.Panel();
            this.buttonLogout = new System.Windows.Forms.Panel();
            this.imageLoggingOn = new System.Windows.Forms.PictureBox();
            this.imageLoggingOff = new System.Windows.Forms.PictureBox();
            this.panelExit = new System.Windows.Forms.Panel();
            this.buttonSetting = new System.Windows.Forms.Panel();
            this.panelForm = new System.Windows.Forms.Panel();
            this.panelMenuCal = new System.Windows.Forms.Panel();
            this.panelMenuCylKnock = new System.Windows.Forms.Panel();
            this.imgCylKnock = new System.Windows.Forms.PictureBox();
            this.panelMenuCylTDC = new System.Windows.Forms.Panel();
            this.imgCylTDC = new System.Windows.Forms.PictureBox();
            this.TabEA.SuspendLayout();
            this.TabGraph.SuspendLayout();
            this.TabControlGraph.SuspendLayout();
            this.TabCycle1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel6.SuspendLayout();
            this.tabKnockIntensity.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartCyl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartKnock)).BeginInit();
            this.panel10.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.PanelData.SuspendLayout();
            this.tabSetting.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel13.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EditPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EditFrequency)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.statusStrip2.SuspendLayout();
            this.PanelDisconnect.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageConOff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageConOn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartPMax)).BeginInit();
            this.panelGraph.SuspendLayout();
            this.panelGraphRight.SuspendLayout();
            this.panelGraphCalcLogginData.SuspendLayout();
            this.panelGraphCalc.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._chartCalcData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._chartCalcDataRPM)).BeginInit();
            this.panelCalcData.SuspendLayout();
            this.panelRPM.SuspendLayout();
            this.panelSetting.SuspendLayout();
            this.panelTop.SuspendLayout();
            this.panelMenuDignosis.SuspendLayout();
            this.panelMenuCalibration.SuspendLayout();
            this.panelMonitoring.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageLoggingOn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageLoggingOff)).BeginInit();
            this.panelForm.SuspendLayout();
            this.panelMenuCal.SuspendLayout();
            this.panelMenuCylKnock.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgCylKnock)).BeginInit();
            this.panelMenuCylTDC.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgCylTDC)).BeginInit();
            this.SuspendLayout();
            // 
            // TabEA
            // 
            this.TabEA.Controls.Add(this.TabGraph);
            this.TabEA.Controls.Add(this.tabSetting);
            this.TabEA.Controls.Add(this.TabHistory);
            this.TabEA.Location = new System.Drawing.Point(488, 924);
            this.TabEA.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.TabEA.Name = "TabEA";
            this.TabEA.SelectedIndex = 0;
            this.TabEA.Size = new System.Drawing.Size(1276, 631);
            this.TabEA.TabIndex = 1;
            this.TabEA.Visible = false;
            this.TabEA.SelectedIndexChanged += new System.EventHandler(this.TabEA_TabIndexChanged);
            this.TabEA.TabIndexChanged += new System.EventHandler(this.TabEA_TabIndexChanged);
            // 
            // TabGraph
            // 
            this.TabGraph.Controls.Add(this.TabControlGraph);
            this.TabGraph.Controls.Add(this.splitter4);
            this.TabGraph.Controls.Add(this.splitter3);
            this.TabGraph.Controls.Add(this.splitter2);
            this.TabGraph.Controls.Add(this.splitter1);
            this.TabGraph.Controls.Add(this.panel5);
            this.TabGraph.Controls.Add(this.panel4);
            this.TabGraph.Controls.Add(this.panel3);
            this.TabGraph.Controls.Add(this.PanelData);
            this.TabGraph.Location = new System.Drawing.Point(4, 24);
            this.TabGraph.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.TabGraph.Name = "TabGraph";
            this.TabGraph.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.TabGraph.Size = new System.Drawing.Size(1268, 603);
            this.TabGraph.TabIndex = 0;
            this.TabGraph.Text = "Graph";
            this.TabGraph.UseVisualStyleBackColor = true;
            // 
            // TabControlGraph
            // 
            this.TabControlGraph.Controls.Add(this.TabCycle1);
            this.TabControlGraph.Controls.Add(this.tabKnockIntensity);
            this.TabControlGraph.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabControlGraph.Location = new System.Drawing.Point(253, 39);
            this.TabControlGraph.Name = "TabControlGraph";
            this.TabControlGraph.SelectedIndex = 0;
            this.TabControlGraph.Size = new System.Drawing.Size(845, 505);
            this.TabControlGraph.TabIndex = 9;
            this.TabControlGraph.SelectedIndexChanged += new System.EventHandler(this.TabControlGraph_SelectedIndexChanged);
            // 
            // TabCycle1
            // 
            this.TabCycle1.Controls.Add(this.panel2);
            this.TabCycle1.Controls.Add(this.splitter5);
            this.TabCycle1.Location = new System.Drawing.Point(4, 24);
            this.TabCycle1.Name = "TabCycle1";
            this.TabCycle1.Padding = new System.Windows.Forms.Padding(3);
            this.TabCycle1.Size = new System.Drawing.Size(837, 477);
            this.TabCycle1.TabIndex = 1;
            this.TabCycle1.Text = "Real-Time(Cycle 기반)";
            this.TabCycle1.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel8);
            this.panel2.Controls.Add(this.panel7);
            this.panel2.Controls.Add(this.panel6);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(2);
            this.panel2.Size = new System.Drawing.Size(831, 109);
            this.panel2.TabIndex = 11;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.label24);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel8.Location = new System.Drawing.Point(623, 2);
            this.panel8.Name = "panel8";
            this.panel8.Padding = new System.Windows.Forms.Padding(3);
            this.panel8.Size = new System.Drawing.Size(457, 105);
            this.panel8.TabIndex = 2;
            // 
            // label24
            // 
            this.label24.Dock = System.Windows.Forms.DockStyle.Top;
            this.label24.Location = new System.Drawing.Point(3, 3);
            this.label24.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(451, 15);
            this.label24.TabIndex = 0;
            this.label24.Text = "IMEP";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.label26);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel7.Location = new System.Drawing.Point(166, 2);
            this.panel7.Name = "panel7";
            this.panel7.Padding = new System.Windows.Forms.Padding(3);
            this.panel7.Size = new System.Drawing.Size(457, 105);
            this.panel7.TabIndex = 1;
            // 
            // label26
            // 
            this.label26.Dock = System.Windows.Forms.DockStyle.Top;
            this.label26.Location = new System.Drawing.Point(3, 3);
            this.label26.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(451, 15);
            this.label26.TabIndex = 0;
            this.label26.Text = "Max Cyilder Pressure";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.label7);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel6.Location = new System.Drawing.Point(2, 2);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(164, 105);
            this.panel6.TabIndex = 0;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(68, 13);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(32, 15);
            this.label7.TabIndex = 0;
            this.label7.Text = "RPM";
            // 
            // splitter5
            // 
            this.splitter5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter5.Location = new System.Drawing.Point(3, 471);
            this.splitter5.Name = "splitter5";
            this.splitter5.Size = new System.Drawing.Size(831, 3);
            this.splitter5.TabIndex = 9;
            this.splitter5.TabStop = false;
            // 
            // tabKnockIntensity
            // 
            this.tabKnockIntensity.Controls.Add(this.splitter7);
            this.tabKnockIntensity.Controls.Add(this.chartCyl);
            this.tabKnockIntensity.Controls.Add(this.chartKnock);
            this.tabKnockIntensity.Controls.Add(this.panel10);
            this.tabKnockIntensity.Location = new System.Drawing.Point(4, 22);
            this.tabKnockIntensity.Name = "tabKnockIntensity";
            this.tabKnockIntensity.Padding = new System.Windows.Forms.Padding(3);
            this.tabKnockIntensity.Size = new System.Drawing.Size(829, 453);
            this.tabKnockIntensity.TabIndex = 3;
            this.tabKnockIntensity.Text = "Knock Intensity";
            this.tabKnockIntensity.UseVisualStyleBackColor = true;
            // 
            // splitter7
            // 
            this.splitter7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter7.Location = new System.Drawing.Point(3, 265);
            this.splitter7.Name = "splitter7";
            this.splitter7.Size = new System.Drawing.Size(823, 3);
            this.splitter7.TabIndex = 15;
            this.splitter7.TabStop = false;
            // 
            // chartCyl
            // 
            chartArea1.AxisX.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea1.Name = "ChartArea1";
            this.chartCyl.ChartAreas.Add(chartArea1);
            this.chartCyl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartCyl.Location = new System.Drawing.Point(3, 43);
            this.chartCyl.Margin = new System.Windows.Forms.Padding(0);
            this.chartCyl.Name = "chartCyl";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            series1.Name = "SeriesCyl";
            this.chartCyl.Series.Add(series1);
            this.chartCyl.Size = new System.Drawing.Size(823, 225);
            this.chartCyl.TabIndex = 16;
            this.chartCyl.Text = "Cylinder Pressure";
            // 
            // chartKnock
            // 
            chartArea2.AxisX.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea2.Name = "ChartArea1";
            this.chartKnock.ChartAreas.Add(chartArea2);
            this.chartKnock.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.chartKnock.Location = new System.Drawing.Point(3, 268);
            this.chartKnock.Name = "chartKnock";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            series2.Name = "Series1";
            this.chartKnock.Series.Add(series2);
            this.chartKnock.Size = new System.Drawing.Size(823, 182);
            this.chartKnock.TabIndex = 18;
            this.chartKnock.Text = "Knock Intensity";
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.panel11);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel10.Location = new System.Drawing.Point(3, 3);
            this.panel10.Name = "panel10";
            this.panel10.Padding = new System.Windows.Forms.Padding(2);
            this.panel10.Size = new System.Drawing.Size(823, 40);
            this.panel10.TabIndex = 13;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.comboKnockCH);
            this.panel11.Controls.Add(this.label27);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel11.Location = new System.Drawing.Point(2, 2);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(387, 36);
            this.panel11.TabIndex = 0;
            // 
            // comboKnockCH
            // 
            this.comboKnockCH.FormattingEnabled = true;
            this.comboKnockCH.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12"});
            this.comboKnockCH.Location = new System.Drawing.Point(77, 3);
            this.comboKnockCH.Name = "comboKnockCH";
            this.comboKnockCH.Size = new System.Drawing.Size(49, 23);
            this.comboKnockCH.TabIndex = 8;
            this.comboKnockCH.Text = "1";
            this.comboKnockCH.SelectedIndexChanged += new System.EventHandler(this.comboKnockCH_SelectedIndexChanged);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(20, 6);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(51, 15);
            this.label27.TabIndex = 6;
            this.label27.Text = "Cylinder";
            // 
            // splitter4
            // 
            this.splitter4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter4.Location = new System.Drawing.Point(253, 544);
            this.splitter4.Name = "splitter4";
            this.splitter4.Size = new System.Drawing.Size(845, 10);
            this.splitter4.TabIndex = 8;
            this.splitter4.TabStop = false;
            // 
            // splitter3
            // 
            this.splitter3.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter3.Location = new System.Drawing.Point(1098, 39);
            this.splitter3.Name = "splitter3";
            this.splitter3.Size = new System.Drawing.Size(8, 515);
            this.splitter3.TabIndex = 7;
            this.splitter3.TabStop = false;
            // 
            // splitter2
            // 
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter2.Location = new System.Drawing.Point(253, 31);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(853, 8);
            this.splitter2.TabIndex = 6;
            this.splitter2.TabStop = false;
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(245, 31);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(8, 523);
            this.splitter1.TabIndex = 5;
            this.splitter1.TabStop = false;
            // 
            // panel5
            // 
            this.panel5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel5.Location = new System.Drawing.Point(245, 554);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(861, 45);
            this.panel5.TabIndex = 3;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label22);
            this.panel4.Controls.Add(this.label19);
            this.panel4.Controls.Add(this.label23);
            this.panel4.Controls.Add(this.label21);
            this.panel4.Controls.Add(this.buttonCHSet);
            this.panel4.Controls.Add(this.ButtonYAxis);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(245, 4);
            this.panel4.Name = "panel4";
            this.panel4.Padding = new System.Windows.Forms.Padding(2);
            this.panel4.Size = new System.Drawing.Size(861, 27);
            this.panel4.TabIndex = 2;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Dock = System.Windows.Forms.DockStyle.Left;
            this.label22.Location = new System.Drawing.Point(269, 2);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(122, 15);
            this.label22.TabIndex = 13;
            this.label22.Text = "Offset(-360.0~369.5)";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Dock = System.Windows.Forms.DockStyle.Left;
            this.label19.Location = new System.Drawing.Point(218, 2);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(51, 15);
            this.label19.TabIndex = 4;
            this.label19.Text = "Cylinder";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(376, 3);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(0, 15);
            this.label23.TabIndex = 18;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(1005, 7);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(115, 15);
            this.label21.TabIndex = 6;
            this.label21.Text = "Offset Res.(0~1440)";
            this.label21.Visible = false;
            // 
            // buttonCHSet
            // 
            this.buttonCHSet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(145)))), ((int)(((byte)(210)))));
            this.buttonCHSet.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonCHSet.FlatAppearance.BorderSize = 0;
            this.buttonCHSet.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCHSet.ForeColor = System.Drawing.Color.White;
            this.buttonCHSet.Location = new System.Drawing.Point(88, 2);
            this.buttonCHSet.Name = "buttonCHSet";
            this.buttonCHSet.Size = new System.Drawing.Size(130, 23);
            this.buttonCHSet.TabIndex = 19;
            this.buttonCHSet.Text = "Cylinder Shift Setup";
            this.buttonCHSet.UseVisualStyleBackColor = false;
            this.buttonCHSet.Click += new System.EventHandler(this.buttonCHSet_Click);
            // 
            // ButtonYAxis
            // 
            this.ButtonYAxis.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(98)))), ((int)(((byte)(174)))), ((int)(((byte)(89)))));
            this.ButtonYAxis.Dock = System.Windows.Forms.DockStyle.Left;
            this.ButtonYAxis.FlatAppearance.BorderSize = 0;
            this.ButtonYAxis.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonYAxis.ForeColor = System.Drawing.Color.White;
            this.ButtonYAxis.Location = new System.Drawing.Point(2, 2);
            this.ButtonYAxis.Name = "ButtonYAxis";
            this.ButtonYAxis.Size = new System.Drawing.Size(86, 23);
            this.ButtonYAxis.TabIndex = 1;
            this.ButtonYAxis.Text = "자동 축 조정";
            this.ButtonYAxis.UseVisualStyleBackColor = false;
            this.ButtonYAxis.Click += new System.EventHandler(this.ButtonYAxis_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.CheckListCH);
            this.panel3.Controls.Add(this.CheckCHAllVIsible);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(1106, 4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(159, 595);
            this.panel3.TabIndex = 1;
            this.panel3.Visible = false;
            // 
            // CheckListCH
            // 
            this.CheckListCH.CheckOnClick = true;
            this.CheckListCH.Dock = System.Windows.Forms.DockStyle.Top;
            this.CheckListCH.FormattingEnabled = true;
            this.CheckListCH.Location = new System.Drawing.Point(0, 19);
            this.CheckListCH.Name = "CheckListCH";
            this.CheckListCH.Size = new System.Drawing.Size(159, 310);
            this.CheckListCH.TabIndex = 7;
            this.CheckListCH.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.CheckListCH_ItemCheck);
            // 
            // CheckCHAllVIsible
            // 
            this.CheckCHAllVIsible.AutoSize = true;
            this.CheckCHAllVIsible.Checked = true;
            this.CheckCHAllVIsible.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CheckCHAllVIsible.Dock = System.Windows.Forms.DockStyle.Top;
            this.CheckCHAllVIsible.Location = new System.Drawing.Point(0, 0);
            this.CheckCHAllVIsible.Name = "CheckCHAllVIsible";
            this.CheckCHAllVIsible.Size = new System.Drawing.Size(159, 19);
            this.CheckCHAllVIsible.TabIndex = 8;
            this.CheckCHAllVIsible.Text = "All Show";
            this.CheckCHAllVIsible.UseVisualStyleBackColor = true;
            this.CheckCHAllVIsible.CheckedChanged += new System.EventHandler(this.CheckCHAllVIsible_CheckedChanged);
            // 
            // PanelData
            // 
            this.PanelData.Controls.Add(this.label8);
            this.PanelData.Dock = System.Windows.Forms.DockStyle.Left;
            this.PanelData.Location = new System.Drawing.Point(3, 4);
            this.PanelData.Name = "PanelData";
            this.PanelData.Size = new System.Drawing.Size(242, 595);
            this.PanelData.TabIndex = 0;
            this.PanelData.SizeChanged += new System.EventHandler(this.PanelData_SizeChanged);
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.DimGray;
            this.label8.Dock = System.Windows.Forms.DockStyle.Top;
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(0, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(242, 25);
            this.label8.TabIndex = 0;
            this.label8.Text = "DATA";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabSetting
            // 
            this.tabSetting.Controls.Add(this.panel9);
            this.tabSetting.Controls.Add(this.ButtonReq);
            this.tabSetting.Location = new System.Drawing.Point(4, 22);
            this.tabSetting.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabSetting.Name = "tabSetting";
            this.tabSetting.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabSetting.Size = new System.Drawing.Size(1268, 605);
            this.tabSetting.TabIndex = 1;
            this.tabSetting.Text = "Setting";
            this.tabSetting.UseVisualStyleBackColor = true;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.panel12);
            this.panel9.Location = new System.Drawing.Point(426, 396);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(468, 252);
            this.panel9.TabIndex = 8;
            this.panel9.Visible = false;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.panel13);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel12.Location = new System.Drawing.Point(0, 0);
            this.panel12.Name = "panel12";
            this.panel12.Padding = new System.Windows.Forms.Padding(2);
            this.panel12.Size = new System.Drawing.Size(468, 40);
            this.panel12.TabIndex = 16;
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.label5);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel13.Location = new System.Drawing.Point(2, 2);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(387, 36);
            this.panel13.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 6);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 15);
            this.label5.TabIndex = 6;
            this.label5.Text = "Cylinder";
            // 
            // ButtonReq
            // 
            this.ButtonReq.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(73)))), ((int)(((byte)(126)))), ((int)(((byte)(67)))));
            this.ButtonReq.FlatAppearance.BorderSize = 0;
            this.ButtonReq.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonReq.Location = new System.Drawing.Point(221, 392);
            this.ButtonReq.Name = "ButtonReq";
            this.ButtonReq.Size = new System.Drawing.Size(75, 36);
            this.ButtonReq.TabIndex = 5;
            this.ButtonReq.Text = "Req";
            this.ButtonReq.UseVisualStyleBackColor = false;
            this.ButtonReq.Visible = false;
            this.ButtonReq.Click += new System.EventHandler(this.ButtonReq_Click);
            // 
            // TabHistory
            // 
            this.TabHistory.Location = new System.Drawing.Point(4, 22);
            this.TabHistory.Name = "TabHistory";
            this.TabHistory.Padding = new System.Windows.Forms.Padding(3);
            this.TabHistory.Size = new System.Drawing.Size(1268, 605);
            this.TabHistory.TabIndex = 2;
            this.TabHistory.Text = "History";
            this.TabHistory.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.checkLog);
            this.groupBox5.Controls.Add(this.label18);
            this.groupBox5.Controls.Add(this.label20);
            this.groupBox5.Controls.Add(this.EditPeriod);
            this.groupBox5.Controls.Add(this.label17);
            this.groupBox5.Controls.Add(this.numericUpDown1);
            this.groupBox5.Controls.Add(this.label16);
            this.groupBox5.Controls.Add(this.EditFrequency);
            this.groupBox5.Controls.Add(this.label15);
            this.groupBox5.Location = new System.Drawing.Point(733, 158);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(371, 215);
            this.groupBox5.TabIndex = 7;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Program";
            // 
            // checkLog
            // 
            this.checkLog.AutoSize = true;
            this.checkLog.Location = new System.Drawing.Point(17, 26);
            this.checkLog.Name = "checkLog";
            this.checkLog.Size = new System.Drawing.Size(50, 19);
            this.checkLog.TabIndex = 18;
            this.checkLog.Text = "로그";
            this.checkLog.UseVisualStyleBackColor = true;
            this.checkLog.CheckedChanged += new System.EventHandler(this.checkLog_CheckedChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(312, 161);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(23, 15);
            this.label18.TabIndex = 17;
            this.label18.Text = "ms";
            this.label18.Visible = false;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(312, 190);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(22, 15);
            this.label20.TabIndex = 15;
            this.label20.Text = "Hz";
            this.label20.Visible = false;
            // 
            // EditPeriod
            // 
            this.EditPeriod.Enabled = false;
            this.EditPeriod.Location = new System.Drawing.Point(217, 156);
            this.EditPeriod.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.EditPeriod.Minimum = new decimal(new int[] {
            999999,
            0,
            0,
            -2147483648});
            this.EditPeriod.Name = "EditPeriod";
            this.EditPeriod.Size = new System.Drawing.Size(89, 23);
            this.EditPeriod.TabIndex = 14;
            this.EditPeriod.Value = new decimal(new int[] {
            165,
            0,
            0,
            0});
            this.EditPeriod.Visible = false;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(17, 158);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(41, 15);
            this.label17.TabIndex = 13;
            this.label17.Text = "Period";
            this.label17.Visible = false;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Enabled = false;
            this.numericUpDown1.Location = new System.Drawing.Point(217, 127);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            999999,
            0,
            0,
            -2147483648});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(89, 23);
            this.numericUpDown1.TabIndex = 12;
            this.numericUpDown1.Value = new decimal(new int[] {
            1440,
            0,
            0,
            0});
            this.numericUpDown1.Visible = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(17, 129);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(63, 15);
            this.label16.TabIndex = 11;
            this.label16.Text = "Resolution";
            this.label16.Visible = false;
            // 
            // EditFrequency
            // 
            this.EditFrequency.Enabled = false;
            this.EditFrequency.Location = new System.Drawing.Point(217, 185);
            this.EditFrequency.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.EditFrequency.Minimum = new decimal(new int[] {
            999999,
            0,
            0,
            -2147483648});
            this.EditFrequency.Name = "EditFrequency";
            this.EditFrequency.Size = new System.Drawing.Size(89, 23);
            this.EditFrequency.TabIndex = 10;
            this.EditFrequency.Visible = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(17, 187);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(122, 15);
            this.label15.TabIndex = 9;
            this.label15.Text = "Frequency(자동 감지)";
            this.label15.Visible = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.ButtonDelaySet);
            this.groupBox4.Controls.Add(this.EditDelayForRecv);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.EditReqTarget);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.ButtonEADisconnect);
            this.groupBox4.Controls.Add(this.ButtonEAConnect);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Location = new System.Drawing.Point(85, 158);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(371, 215);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Socket";
            // 
            // ButtonDelaySet
            // 
            this.ButtonDelaySet.Location = new System.Drawing.Point(238, 161);
            this.ButtonDelaySet.Name = "ButtonDelaySet";
            this.ButtonDelaySet.Size = new System.Drawing.Size(115, 32);
            this.ButtonDelaySet.TabIndex = 12;
            this.ButtonDelaySet.Text = "설정";
            this.ButtonDelaySet.UseVisualStyleBackColor = true;
            this.ButtonDelaySet.Visible = false;
            this.ButtonDelaySet.Click += new System.EventHandler(this.ButtonDelaySet_Click);
            // 
            // EditDelayForRecv
            // 
            this.EditDelayForRecv.Location = new System.Drawing.Point(238, 132);
            this.EditDelayForRecv.Name = "EditDelayForRecv";
            this.EditDelayForRecv.Size = new System.Drawing.Size(115, 23);
            this.EditDelayForRecv.TabIndex = 11;
            this.EditDelayForRecv.Text = "0";
            this.EditDelayForRecv.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(28, 135);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(139, 15);
            this.label12.TabIndex = 10;
            this.label12.Text = "수신 실행 전 DELAY(ms)";
            this.label12.Visible = false;
            // 
            // EditReqTarget
            // 
            this.EditReqTarget.Location = new System.Drawing.Point(238, 103);
            this.EditReqTarget.Name = "EditReqTarget";
            this.EditReqTarget.Size = new System.Drawing.Size(115, 23);
            this.EditReqTarget.TabIndex = 9;
            this.EditReqTarget.Text = "1000";
            this.EditReqTarget.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(28, 106);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(133, 15);
            this.label11.TabIndex = 8;
            this.label11.Text = "Request 초당 전송 횟수";
            this.label11.Visible = false;
            // 
            // ButtonEADisconnect
            // 
            this.ButtonEADisconnect.Location = new System.Drawing.Point(238, 62);
            this.ButtonEADisconnect.Name = "ButtonEADisconnect";
            this.ButtonEADisconnect.Size = new System.Drawing.Size(115, 32);
            this.ButtonEADisconnect.TabIndex = 7;
            this.ButtonEADisconnect.Text = "Disconnect";
            this.ButtonEADisconnect.UseVisualStyleBackColor = true;
            this.ButtonEADisconnect.Click += new System.EventHandler(this.ButtonEADisconnect_Click);
            // 
            // ButtonEAConnect
            // 
            this.ButtonEAConnect.Location = new System.Drawing.Point(238, 25);
            this.ButtonEAConnect.Name = "ButtonEAConnect";
            this.ButtonEAConnect.Size = new System.Drawing.Size(115, 32);
            this.ButtonEAConnect.TabIndex = 6;
            this.ButtonEAConnect.Text = "Connect";
            this.ButtonEAConnect.UseVisualStyleBackColor = true;
            this.ButtonEAConnect.Click += new System.EventHandler(this.ButtonEAConnect_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(28, 66);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(29, 15);
            this.label9.TabIndex = 3;
            this.label9.Text = "Port";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(28, 29);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(17, 15);
            this.label10.TabIndex = 1;
            this.label10.Text = "IP";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.buttonDataTypeSet);
            this.groupBox3.Controls.Add(this.comboDataType);
            this.groupBox3.Controls.Add(this.ButtonCHMaxSet);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.EditCHMax);
            this.groupBox3.Controls.Add(this.label25);
            this.groupBox3.Controls.Add(this.ButtonCycleCountSet);
            this.groupBox3.Controls.Add(this.EditCycleCount);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.CheckGraphSkip);
            this.groupBox3.Controls.Add(this.CheckYAuto);
            this.groupBox3.Controls.Add(this.ButtonYAxisSet);
            this.groupBox3.Controls.Add(this.labelymax);
            this.groupBox3.Controls.Add(this.labelymin);
            this.groupBox3.Controls.Add(this.EditXCount);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Location = new System.Drawing.Point(462, 37);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(265, 336);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Graph";
            // 
            // buttonDataTypeSet
            // 
            this.buttonDataTypeSet.Location = new System.Drawing.Point(201, 219);
            this.buttonDataTypeSet.Name = "buttonDataTypeSet";
            this.buttonDataTypeSet.Size = new System.Drawing.Size(52, 25);
            this.buttonDataTypeSet.TabIndex = 21;
            this.buttonDataTypeSet.Text = "Set";
            this.buttonDataTypeSet.UseVisualStyleBackColor = true;
            this.buttonDataTypeSet.Click += new System.EventHandler(this.buttonDataTypeSet_Click);
            // 
            // comboDataType
            // 
            this.comboDataType.FormattingEnabled = true;
            this.comboDataType.Items.AddRange(new object[] {
            "INT16",
            "UINT16"});
            this.comboDataType.Location = new System.Drawing.Point(121, 219);
            this.comboDataType.Name = "comboDataType";
            this.comboDataType.Size = new System.Drawing.Size(74, 23);
            this.comboDataType.TabIndex = 20;
            this.comboDataType.Text = "INT16";
            // 
            // ButtonCHMaxSet
            // 
            this.ButtonCHMaxSet.Location = new System.Drawing.Point(201, 54);
            this.ButtonCHMaxSet.Name = "ButtonCHMaxSet";
            this.ButtonCHMaxSet.Size = new System.Drawing.Size(52, 26);
            this.ButtonCHMaxSet.TabIndex = 19;
            this.ButtonCHMaxSet.Text = "Set";
            this.ButtonCHMaxSet.UseVisualStyleBackColor = true;
            this.ButtonCHMaxSet.Click += new System.EventHandler(this.ButtonCHMaxSet_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Enabled = false;
            this.label6.Location = new System.Drawing.Point(18, 224);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 15);
            this.label6.TabIndex = 19;
            this.label6.Text = "표시 데이터형식";
            // 
            // EditCHMax
            // 
            this.EditCHMax.Location = new System.Drawing.Point(133, 55);
            this.EditCHMax.Name = "EditCHMax";
            this.EditCHMax.Size = new System.Drawing.Size(62, 23);
            this.EditCHMax.TabIndex = 18;
            this.EditCHMax.Tag = "";
            this.EditCHMax.Text = "12";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(18, 59);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(79, 15);
            this.label25.TabIndex = 17;
            this.label25.Text = "Cyilnder 개수";
            // 
            // ButtonCycleCountSet
            // 
            this.ButtonCycleCountSet.Location = new System.Drawing.Point(201, 24);
            this.ButtonCycleCountSet.Name = "ButtonCycleCountSet";
            this.ButtonCycleCountSet.Size = new System.Drawing.Size(52, 26);
            this.ButtonCycleCountSet.TabIndex = 16;
            this.ButtonCycleCountSet.Text = "Set";
            this.ButtonCycleCountSet.UseVisualStyleBackColor = true;
            this.ButtonCycleCountSet.Click += new System.EventHandler(this.ButtonCycleCountSet_Click);
            // 
            // EditCycleCount
            // 
            this.EditCycleCount.Location = new System.Drawing.Point(133, 25);
            this.EditCycleCount.Name = "EditCycleCount";
            this.EditCycleCount.Size = new System.Drawing.Size(62, 23);
            this.EditCycleCount.TabIndex = 15;
            this.EditCycleCount.Tag = "";
            this.EditCycleCount.Text = "1";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(18, 29);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(104, 15);
            this.label14.TabIndex = 14;
            this.label14.Text = "실시간 Cycle 개수";
            // 
            // CheckGraphSkip
            // 
            this.CheckGraphSkip.AutoSize = true;
            this.CheckGraphSkip.Checked = true;
            this.CheckGraphSkip.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CheckGraphSkip.Location = new System.Drawing.Point(21, 93);
            this.CheckGraphSkip.Name = "CheckGraphSkip";
            this.CheckGraphSkip.Size = new System.Drawing.Size(138, 19);
            this.CheckGraphSkip.TabIndex = 13;
            this.CheckGraphSkip.Text = "Cycle Skip 표시 안함";
            this.CheckGraphSkip.UseVisualStyleBackColor = true;
            // 
            // CheckYAuto
            // 
            this.CheckYAuto.AutoSize = true;
            this.CheckYAuto.Checked = true;
            this.CheckYAuto.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CheckYAuto.Location = new System.Drawing.Point(21, 118);
            this.CheckYAuto.Name = "CheckYAuto";
            this.CheckYAuto.Size = new System.Drawing.Size(73, 19);
            this.CheckYAuto.TabIndex = 12;
            this.CheckYAuto.Text = "Y축 자동";
            this.CheckYAuto.UseVisualStyleBackColor = true;
            this.CheckYAuto.CheckedChanged += new System.EventHandler(this.CheckYAuto_CheckedChanged);
            // 
            // ButtonYAxisSet
            // 
            this.ButtonYAxisSet.Enabled = false;
            this.ButtonYAxisSet.Location = new System.Drawing.Point(201, 143);
            this.ButtonYAxisSet.Name = "ButtonYAxisSet";
            this.ButtonYAxisSet.Size = new System.Drawing.Size(52, 25);
            this.ButtonYAxisSet.TabIndex = 11;
            this.ButtonYAxisSet.Text = "Set";
            this.ButtonYAxisSet.UseVisualStyleBackColor = true;
            this.ButtonYAxisSet.Click += new System.EventHandler(this.ButtonYAxisSet_ItemClick);
            // 
            // labelymax
            // 
            this.labelymax.AutoSize = true;
            this.labelymax.Enabled = false;
            this.labelymax.Location = new System.Drawing.Point(18, 184);
            this.labelymax.Name = "labelymax";
            this.labelymax.Size = new System.Drawing.Size(54, 15);
            this.labelymax.TabIndex = 9;
            this.labelymax.Text = "Y축 최대";
            // 
            // labelymin
            // 
            this.labelymin.AutoSize = true;
            this.labelymin.Enabled = false;
            this.labelymin.Location = new System.Drawing.Point(18, 147);
            this.labelymin.Name = "labelymin";
            this.labelymin.Size = new System.Drawing.Size(54, 15);
            this.labelymin.TabIndex = 7;
            this.labelymin.Text = "Y축 최대";
            // 
            // EditXCount
            // 
            this.EditXCount.Location = new System.Drawing.Point(133, 25);
            this.EditXCount.Name = "EditXCount";
            this.EditXCount.Size = new System.Drawing.Size(120, 23);
            this.EditXCount.TabIndex = 6;
            this.EditXCount.Tag = "";
            this.EditXCount.Text = "1440";
            this.EditXCount.Visible = false;
            this.EditXCount.TextChanged += new System.EventHandler(this.EditXCount_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 15);
            this.label4.TabIndex = 5;
            this.label4.Text = "가로축 표현 개수";
            this.label4.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ButtonFolderSelect);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(733, 37);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(371, 115);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Save";
            // 
            // ButtonFolderSelect
            // 
            this.ButtonFolderSelect.Location = new System.Drawing.Point(238, 64);
            this.ButtonFolderSelect.Name = "ButtonFolderSelect";
            this.ButtonFolderSelect.Size = new System.Drawing.Size(115, 32);
            this.ButtonFolderSelect.TabIndex = 7;
            this.ButtonFolderSelect.Text = "Select";
            this.ButtonFolderSelect.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(28, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 15);
            this.label3.TabIndex = 5;
            this.label3.Text = "Folder";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ButtonDisconnect);
            this.groupBox1.Controls.Add(this.ButtonConnect);
            this.groupBox1.Controls.Add(this.EditPort);
            this.groupBox1.Controls.Add(this.EditIP);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(85, 37);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(371, 115);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "MODBUS";
            // 
            // ButtonDisconnect
            // 
            this.ButtonDisconnect.Location = new System.Drawing.Point(238, 62);
            this.ButtonDisconnect.Name = "ButtonDisconnect";
            this.ButtonDisconnect.Size = new System.Drawing.Size(115, 32);
            this.ButtonDisconnect.TabIndex = 7;
            this.ButtonDisconnect.Text = "Disconnect";
            this.ButtonDisconnect.UseVisualStyleBackColor = true;
            this.ButtonDisconnect.Click += new System.EventHandler(this.ButtonDisconnect_Click);
            // 
            // ButtonConnect
            // 
            this.ButtonConnect.Location = new System.Drawing.Point(238, 25);
            this.ButtonConnect.Name = "ButtonConnect";
            this.ButtonConnect.Size = new System.Drawing.Size(115, 32);
            this.ButtonConnect.TabIndex = 6;
            this.ButtonConnect.Text = "Connect";
            this.ButtonConnect.UseVisualStyleBackColor = true;
            this.ButtonConnect.Click += new System.EventHandler(this.ButtonConnect_ItemClick);
            // 
            // EditPort
            // 
            this.EditPort.Location = new System.Drawing.Point(112, 63);
            this.EditPort.Name = "EditPort";
            this.EditPort.Size = new System.Drawing.Size(120, 23);
            this.EditPort.TabIndex = 5;
            this.EditPort.Text = "502";
            // 
            // EditIP
            // 
            this.EditIP.Location = new System.Drawing.Point(112, 26);
            this.EditIP.Name = "EditIP";
            this.EditIP.Size = new System.Drawing.Size(120, 23);
            this.EditIP.TabIndex = 4;
            this.EditIP.Text = "192.168.10.15";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 15);
            this.label2.TabIndex = 3;
            this.label2.Text = "Port";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "IP";
            // 
            // _chart
            // 
            this._chart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(243)))), ((int)(((byte)(245)))));
            this._chart.Background = ((Arction.WinForms.Charting.Fill)(resources.GetObject("_chart.Background")));
            this._chart.ChartManager = null;
            this._chart.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this._chart.HorizontalScrollBars = ((System.Collections.Generic.List<Arction.WinForms.Charting.HorizontalScrollBar>)(resources.GetObject("_chart.HorizontalScrollBars")));
            this._chart.Location = new System.Drawing.Point(100, 8);
            this._chart.Margin = new System.Windows.Forms.Padding(3, 8, 3, 8);
            this._chart.MinimumSize = new System.Drawing.Size(60, 79);
            this._chart.Name = "_chart";
            this._chart.Options = ((Arction.WinForms.Charting.ChartOptions)(resources.GetObject("_chart.Options")));
            this._chart.OutputStream = null;
            this._chart.RenderOptions = ((Arction.WinForms.Charting.Views.RenderOptionsCommon)(resources.GetObject("_chart.RenderOptions")));
            this._chart.Size = new System.Drawing.Size(1505, 458);
            this._chart.TabIndex = 8;
            this._chart.Title = ((Arction.WinForms.Charting.Titles.ChartTitle)(resources.GetObject("_chart.Title")));
            this._chart.VerticalScrollBars = ((System.Collections.Generic.List<Arction.WinForms.Charting.VerticalScrollBar>)(resources.GetObject("_chart.VerticalScrollBars")));
            this._chart.View3D = ((Arction.WinForms.Charting.Views.View3D.View3D)(resources.GetObject("_chart.View3D")));
            this._chart.ViewPie3D = ((Arction.WinForms.Charting.Views.ViewPie3D.ViewPie3D)(resources.GetObject("_chart.ViewPie3D")));
            this._chart.ViewPolar = ((Arction.WinForms.Charting.Views.ViewPolar.ViewPolar)(resources.GetObject("_chart.ViewPolar")));
            this._chart.ViewSmith = ((Arction.WinForms.Charting.Views.ViewSmith.ViewSmith)(resources.GetObject("_chart.ViewSmith")));
            this._chart.ViewXY = ((Arction.WinForms.Charting.Views.ViewXY.ViewXY)(resources.GetObject("_chart.ViewXY")));
            // 
            // CheckRT1
            // 
            this.CheckRT1.AutoSize = true;
            this.CheckRT1.Enabled = false;
            this.CheckRT1.Location = new System.Drawing.Point(6, 3);
            this.CheckRT1.Name = "CheckRT1";
            this.CheckRT1.Size = new System.Drawing.Size(45, 19);
            this.CheckRT1.TabIndex = 7;
            this.CheckRT1.Text = "Use";
            this.CheckRT1.UseVisualStyleBackColor = true;
            this.CheckRT1.Visible = false;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.statusStrip1.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel3,
            this.LabelConnection,
            this.LabelEALive,
            this.toolStripStatusLabel2,
            this.LabelDPS,
            this.toolStripStatusLabel4,
            this.LabelExport,
            this.toolStripStatusLabel1,
            this.LabelReq,
            this.toolStripStatusLabel5,
            this.LabelCHGetCnt,
            this.toolStripStatusLabel6,
            this.LabelCycle,
            this.toolStripStatusLabel9,
            this.LabelCSC,
            this.toolStripStatusLabel10,
            this.LabelCSPS});
            this.statusStrip1.Location = new System.Drawing.Point(208, 1060);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(732, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Visible = false;
            this.statusStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.statusStrip1_ItemClicked);
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(96, 17);
            this.toolStripStatusLabel3.Text = "Communication";
            // 
            // LabelConnection
            // 
            this.LabelConnection.Name = "LabelConnection";
            this.LabelConnection.Size = new System.Drawing.Size(80, 17);
            this.LabelConnection.Text = "Disconnected";
            // 
            // LabelEALive
            // 
            this.LabelEALive.Name = "LabelEALive";
            this.LabelEALive.Size = new System.Drawing.Size(0, 17);
            this.LabelEALive.Click += new System.EventHandler(this.toolStripStatusLabel1_Click);
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(124, 17);
            this.toolStripStatusLabel2.Text = "Data Process Speed";
            // 
            // LabelDPS
            // 
            this.LabelDPS.Name = "LabelDPS";
            this.LabelDPS.Size = new System.Drawing.Size(14, 17);
            this.LabelDPS.Text = "0";
            // 
            // toolStripStatusLabel4
            // 
            this.toolStripStatusLabel4.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.toolStripStatusLabel4.Name = "toolStripStatusLabel4";
            this.toolStripStatusLabel4.Size = new System.Drawing.Size(75, 17);
            this.toolStripStatusLabel4.Text = "Save Status";
            // 
            // LabelExport
            // 
            this.LabelExport.Name = "LabelExport";
            this.LabelExport.Size = new System.Drawing.Size(0, 17);
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(93, 17);
            this.toolStripStatusLabel1.Text = "Request Count";
            // 
            // LabelReq
            // 
            this.LabelReq.Name = "LabelReq";
            this.LabelReq.Size = new System.Drawing.Size(14, 17);
            this.LabelReq.Text = "0";
            // 
            // toolStripStatusLabel5
            // 
            this.toolStripStatusLabel5.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.toolStripStatusLabel5.Name = "toolStripStatusLabel5";
            this.toolStripStatusLabel5.Size = new System.Drawing.Size(93, 17);
            this.toolStripStatusLabel5.Text = "Recv CH Count";
            this.toolStripStatusLabel5.Visible = false;
            // 
            // LabelCHGetCnt
            // 
            this.LabelCHGetCnt.Name = "LabelCHGetCnt";
            this.LabelCHGetCnt.Size = new System.Drawing.Size(14, 17);
            this.LabelCHGetCnt.Text = "0";
            this.LabelCHGetCnt.Visible = false;
            // 
            // toolStripStatusLabel6
            // 
            this.toolStripStatusLabel6.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.toolStripStatusLabel6.Name = "toolStripStatusLabel6";
            this.toolStripStatusLabel6.Size = new System.Drawing.Size(37, 17);
            this.toolStripStatusLabel6.Text = "Cycle";
            // 
            // LabelCycle
            // 
            this.LabelCycle.Name = "LabelCycle";
            this.LabelCycle.Size = new System.Drawing.Size(14, 17);
            this.LabelCycle.Text = "0";
            // 
            // toolStripStatusLabel9
            // 
            this.toolStripStatusLabel9.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.toolStripStatusLabel9.Name = "toolStripStatusLabel9";
            this.toolStripStatusLabel9.Size = new System.Drawing.Size(104, 17);
            this.toolStripStatusLabel9.Text = "Cycle Skip Count";
            // 
            // LabelCSC
            // 
            this.LabelCSC.Name = "LabelCSC";
            this.LabelCSC.Size = new System.Drawing.Size(14, 17);
            this.LabelCSC.Text = "0";
            // 
            // toolStripStatusLabel10
            // 
            this.toolStripStatusLabel10.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.toolStripStatusLabel10.Name = "toolStripStatusLabel10";
            this.toolStripStatusLabel10.Size = new System.Drawing.Size(36, 17);
            this.toolStripStatusLabel10.Text = "CSPS";
            // 
            // LabelCSPS
            // 
            this.LabelCSPS.Name = "LabelCSPS";
            this.LabelCSPS.Size = new System.Drawing.Size(14, 17);
            this.LabelCSPS.Text = "0";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DimGray;
            this.panel1.Controls.Add(this.ButtonStop);
            this.panel1.Controls.Add(this.ButtonStart);
            this.panel1.Controls.Add(this.ButtonConnectToggle);
            this.panel1.Controls.Add(this.EditStatus);
            this.panel1.ForeColor = System.Drawing.Color.White;
            this.panel1.Location = new System.Drawing.Point(581, 81);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(2);
            this.panel1.Size = new System.Drawing.Size(1276, 40);
            this.panel1.TabIndex = 3;
            this.panel1.Visible = false;
            // 
            // ButtonStop
            // 
            this.ButtonStop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(38)))), ((int)(((byte)(38)))));
            this.ButtonStop.Dock = System.Windows.Forms.DockStyle.Left;
            this.ButtonStop.Enabled = false;
            this.ButtonStop.FlatAppearance.BorderSize = 0;
            this.ButtonStop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonStop.Location = new System.Drawing.Point(346, 2);
            this.ButtonStop.Name = "ButtonStop";
            this.ButtonStop.Size = new System.Drawing.Size(75, 36);
            this.ButtonStop.TabIndex = 2;
            this.ButtonStop.Text = "Stop";
            this.ButtonStop.UseVisualStyleBackColor = false;
            this.ButtonStop.Visible = false;
            this.ButtonStop.Click += new System.EventHandler(this.ButtonStop_ItemClick);
            // 
            // ButtonStart
            // 
            this.ButtonStart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(130)))), ((int)(((byte)(60)))));
            this.ButtonStart.Dock = System.Windows.Forms.DockStyle.Left;
            this.ButtonStart.Enabled = false;
            this.ButtonStart.FlatAppearance.BorderSize = 0;
            this.ButtonStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonStart.Location = new System.Drawing.Point(271, 2);
            this.ButtonStart.Name = "ButtonStart";
            this.ButtonStart.Size = new System.Drawing.Size(75, 36);
            this.ButtonStart.TabIndex = 0;
            this.ButtonStart.Text = "Start";
            this.ButtonStart.UseVisualStyleBackColor = false;
            this.ButtonStart.Visible = false;
            this.ButtonStart.Click += new System.EventHandler(this.ButtonStart_ItemClick);
            // 
            // ButtonConnectToggle
            // 
            this.ButtonConnectToggle.BackColor = System.Drawing.Color.Gray;
            this.ButtonConnectToggle.Dock = System.Windows.Forms.DockStyle.Left;
            this.ButtonConnectToggle.FlatAppearance.BorderSize = 0;
            this.ButtonConnectToggle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonConnectToggle.Location = new System.Drawing.Point(180, 2);
            this.ButtonConnectToggle.Name = "ButtonConnectToggle";
            this.ButtonConnectToggle.Size = new System.Drawing.Size(91, 36);
            this.ButtonConnectToggle.TabIndex = 3;
            this.ButtonConnectToggle.Text = "Connect";
            this.ButtonConnectToggle.UseVisualStyleBackColor = false;
            this.ButtonConnectToggle.Click += new System.EventHandler(this.ButtonConnectToggle_Click);
            // 
            // EditStatus
            // 
            this.EditStatus.Dock = System.Windows.Forms.DockStyle.Left;
            this.EditStatus.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold);
            this.EditStatus.Location = new System.Drawing.Point(2, 2);
            this.EditStatus.Name = "EditStatus";
            this.EditStatus.Size = new System.Drawing.Size(178, 36);
            this.EditStatus.TabIndex = 1;
            this.EditStatus.Text = "Stopped";
            this.EditStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // DialogOpen
            // 
            this.DialogOpen.Filter = "RAW File|*.dat";
            // 
            // TimerTick
            // 
            this.TimerTick.Enabled = true;
            this.TimerTick.Interval = 500;
            this.TimerTick.Tick += new System.EventHandler(this.TimerTick_Tick);
            // 
            // statusStrip2
            // 
            this.statusStrip2.Dock = System.Windows.Forms.DockStyle.None;
            this.statusStrip2.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel7,
            this.LabelRawQueue,
            this.toolStripStatusLabel8,
            this.LabelSaveQueue,
            this.toolStripStatusLabel11,
            this.LabelCPS});
            this.statusStrip2.Location = new System.Drawing.Point(208, 899);
            this.statusStrip2.Name = "statusStrip2";
            this.statusStrip2.Size = new System.Drawing.Size(320, 22);
            this.statusStrip2.TabIndex = 8;
            this.statusStrip2.Text = "statusStrip2";
            // 
            // toolStripStatusLabel7
            // 
            this.toolStripStatusLabel7.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.toolStripStatusLabel7.Name = "toolStripStatusLabel7";
            this.toolStripStatusLabel7.Size = new System.Drawing.Size(81, 17);
            this.toolStripStatusLabel7.Text = "Graph Queue";
            // 
            // LabelRawQueue
            // 
            this.LabelRawQueue.Name = "LabelRawQueue";
            this.LabelRawQueue.Size = new System.Drawing.Size(14, 17);
            this.LabelRawQueue.Text = "0";
            // 
            // toolStripStatusLabel8
            // 
            this.toolStripStatusLabel8.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.toolStripStatusLabel8.Name = "toolStripStatusLabel8";
            this.toolStripStatusLabel8.Size = new System.Drawing.Size(74, 17);
            this.toolStripStatusLabel8.Text = "Save Queue";
            // 
            // LabelSaveQueue
            // 
            this.LabelSaveQueue.Name = "LabelSaveQueue";
            this.LabelSaveQueue.Size = new System.Drawing.Size(14, 17);
            this.LabelSaveQueue.Text = "0";
            // 
            // toolStripStatusLabel11
            // 
            this.toolStripStatusLabel11.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold);
            this.toolStripStatusLabel11.ForeColor = System.Drawing.Color.Red;
            this.toolStripStatusLabel11.Name = "toolStripStatusLabel11";
            this.toolStripStatusLabel11.Size = new System.Drawing.Size(106, 17);
            this.toolStripStatusLabel11.Text = "Cycle Per Second";
            // 
            // LabelCPS
            // 
            this.LabelCPS.ActiveLinkColor = System.Drawing.Color.Red;
            this.LabelCPS.ForeColor = System.Drawing.Color.Red;
            this.LabelCPS.Name = "LabelCPS";
            this.LabelCPS.Size = new System.Drawing.Size(14, 17);
            this.LabelCPS.Text = "0";
            this.LabelCPS.Click += new System.EventHandler(this.toolStripStatusLabel12_Click);
            // 
            // PanelDisconnect
            // 
            this.PanelDisconnect.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(84)))), ((int)(((byte)(46)))));
            this.PanelDisconnect.Controls.Add(this.label13);
            this.PanelDisconnect.Font = new System.Drawing.Font("맑은 고딕", 20F);
            this.PanelDisconnect.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.PanelDisconnect.Location = new System.Drawing.Point(523, 5);
            this.PanelDisconnect.Name = "PanelDisconnect";
            this.PanelDisconnect.Size = new System.Drawing.Size(512, 53);
            this.PanelDisconnect.TabIndex = 11;
            this.PanelDisconnect.Visible = false;
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(11, 1);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(478, 41);
            this.label13.TabIndex = 0;
            this.label13.Text = "접속이 해제되었습니다.";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // imageConOff
            // 
            this.imageConOff.BackColor = System.Drawing.Color.Transparent;
            this.imageConOff.Image = ((System.Drawing.Image)(resources.GetObject("imageConOff.Image")));
            this.imageConOff.InitialImage = null;
            this.imageConOff.Location = new System.Drawing.Point(1240, 0);
            this.imageConOff.Name = "imageConOff";
            this.imageConOff.Size = new System.Drawing.Size(40, 75);
            this.imageConOff.TabIndex = 15;
            this.imageConOff.TabStop = false;
            this.imageConOff.Click += new System.EventHandler(this.imageConOn_Click);
            this.imageConOff.MouseEnter += new System.EventHandler(this.obj_MouseEnter);
            this.imageConOff.MouseLeave += new System.EventHandler(this.obj_MouseLeave);
            // 
            // imageConOn
            // 
            this.imageConOn.Image = ((System.Drawing.Image)(resources.GetObject("imageConOn.Image")));
            this.imageConOn.Location = new System.Drawing.Point(1223, 0);
            this.imageConOn.Name = "imageConOn";
            this.imageConOn.Size = new System.Drawing.Size(40, 75);
            this.imageConOn.TabIndex = 16;
            this.imageConOn.TabStop = false;
            this.imageConOn.Visible = false;
            this.imageConOn.Click += new System.EventHandler(this.imageConOn_Click);
            this.imageConOn.MouseEnter += new System.EventHandler(this.obj_MouseEnter);
            this.imageConOn.MouseLeave += new System.EventHandler(this.obj_MouseLeave);
            // 
            // chartPMax
            // 
            this.chartPMax.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(243)))), ((int)(((byte)(245)))));
            this.chartPMax.BorderlineWidth = 0;
            this.chartPMax.BorderSkin.BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            this.chartPMax.BorderSkin.PageColor = System.Drawing.Color.Empty;
            chartArea3.AxisX.IsLabelAutoFit = false;
            chartArea3.AxisX.IsMarginVisible = false;
            chartArea3.AxisX.LabelAutoFitMaxFontSize = 12;
            chartArea3.AxisX.LabelAutoFitMinFontSize = 12;
            chartArea3.AxisX.LabelStyle.Enabled = false;
            chartArea3.AxisX.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            chartArea3.AxisX.LabelStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            chartArea3.AxisX.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(191)))), ((int)(((byte)(193)))));
            chartArea3.AxisX.MajorGrid.Enabled = false;
            chartArea3.AxisX.MajorTickMark.Enabled = false;
            chartArea3.AxisX.TitleForeColor = System.Drawing.Color.DimGray;
            chartArea3.AxisX2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(191)))), ((int)(((byte)(193)))));
            chartArea3.AxisY.IsLabelAutoFit = false;
            chartArea3.AxisY.LabelStyle.Font = new System.Drawing.Font("HelveticaNeueLT Pro 67 MdCn", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            chartArea3.AxisY.LabelStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            chartArea3.AxisY.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(191)))), ((int)(((byte)(193)))));
            chartArea3.AxisY.MajorGrid.Enabled = false;
            chartArea3.AxisY.MajorTickMark.Enabled = false;
            chartArea3.AxisY2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(191)))), ((int)(((byte)(193)))));
            chartArea3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(243)))), ((int)(((byte)(245)))));
            chartArea3.Name = "ChartArea1";
            this.chartPMax.ChartAreas.Add(chartArea3);
            this.chartPMax.Location = new System.Drawing.Point(1, 40);
            this.chartPMax.Margin = new System.Windows.Forms.Padding(0);
            this.chartPMax.Name = "chartPMax";
            this.chartPMax.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            series3.ChartArea = "ChartArea1";
            series3.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(89)))), ((int)(((byte)(178)))));
            series3.Name = "SeriesPMax";
            series4.BorderWidth = 2;
            series4.ChartArea = "ChartArea1";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series4.Color = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(63)))), ((int)(((byte)(36)))));
            series4.Name = "SeriesAvg";
            this.chartPMax.Series.Add(series3);
            this.chartPMax.Series.Add(series4);
            this.chartPMax.Size = new System.Drawing.Size(878, 130);
            this.chartPMax.TabIndex = 2;
            // 
            // panelGraph
            // 
            this.panelGraph.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(244)))));
            this.panelGraph.Controls.Add(this.panelGraphRight);
            this.panelGraph.Controls.Add(this._chart);
            this.panelGraph.ForeColor = System.Drawing.Color.Red;
            this.panelGraph.Location = new System.Drawing.Point(80, 524);
            this.panelGraph.Name = "panelGraph";
            this.panelGraph.Size = new System.Drawing.Size(1758, 475);
            this.panelGraph.TabIndex = 19;
            this.panelGraph.Paint += new System.Windows.Forms.PaintEventHandler(this.panelGraph_Paint);
            this.panelGraph.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panelGraph_MouseDown);
            this.panelGraph.MouseEnter += new System.EventHandler(this.panelGraph_MouseEnter);
            this.panelGraph.MouseLeave += new System.EventHandler(this.panelGraph_MouseLeave);
            this.panelGraph.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panelGraph_MouseMove);
            this.panelGraph.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panelGraph_MouseUp);
            // 
            // panelGraphRight
            // 
            this.panelGraphRight.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelGraphRight.BackgroundImage")));
            this.panelGraphRight.Controls.Add(this.comboYMax);
            this.panelGraphRight.Controls.Add(this.comboYMin);
            this.panelGraphRight.Controls.Add(this.editGraphPYMax);
            this.panelGraphRight.Controls.Add(this.editGraphPYMin);
            this.panelGraphRight.Controls.Add(this.buttonGraphPAxisReset);
            this.panelGraphRight.Location = new System.Drawing.Point(1611, 3);
            this.panelGraphRight.Name = "panelGraphRight";
            this.panelGraphRight.Size = new System.Drawing.Size(144, 469);
            this.panelGraphRight.TabIndex = 11;
            this.panelGraphRight.Paint += new System.Windows.Forms.PaintEventHandler(this.panelGraphRight_Paint);
            // 
            // comboYMax
            // 
            this.comboYMax.BackColor = System.Drawing.Color.Transparent;
            this.comboYMax.Location = new System.Drawing.Point(71, 360);
            this.comboYMax.Name = "comboYMax";
            this.comboYMax.Size = new System.Drawing.Size(30, 30);
            this.comboYMax.TabIndex = 21;
            // 
            // comboYMin
            // 
            this.comboYMin.BackColor = System.Drawing.Color.Transparent;
            this.comboYMin.Location = new System.Drawing.Point(71, 308);
            this.comboYMin.Name = "comboYMin";
            this.comboYMin.Size = new System.Drawing.Size(30, 30);
            this.comboYMin.TabIndex = 20;
            // 
            // editGraphPYMax
            // 
            this.editGraphPYMax.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.editGraphPYMax.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.editGraphPYMax.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.editGraphPYMax.Location = new System.Drawing.Point(33, 371);
            this.editGraphPYMax.Multiline = true;
            this.editGraphPYMax.Name = "editGraphPYMax";
            this.editGraphPYMax.Size = new System.Drawing.Size(38, 17);
            this.editGraphPYMax.TabIndex = 13;
            this.editGraphPYMax.Text = "0";
            this.editGraphPYMax.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.editGraphPYMax.TextChanged += new System.EventHandler(this.editGraphPYMin_TextChanged);
            // 
            // editGraphPYMin
            // 
            this.editGraphPYMin.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.editGraphPYMin.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.editGraphPYMin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.editGraphPYMin.Location = new System.Drawing.Point(33, 319);
            this.editGraphPYMin.Multiline = true;
            this.editGraphPYMin.Name = "editGraphPYMin";
            this.editGraphPYMin.Size = new System.Drawing.Size(38, 17);
            this.editGraphPYMin.TabIndex = 12;
            this.editGraphPYMin.Text = "0";
            this.editGraphPYMin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.editGraphPYMin.TextChanged += new System.EventHandler(this.editGraphPYMin_TextChanged);
            // 
            // buttonGraphPAxisReset
            // 
            this.buttonGraphPAxisReset.BackColor = System.Drawing.Color.Transparent;
            this.buttonGraphPAxisReset.Location = new System.Drawing.Point(20, 73);
            this.buttonGraphPAxisReset.Name = "buttonGraphPAxisReset";
            this.buttonGraphPAxisReset.Size = new System.Drawing.Size(82, 70);
            this.buttonGraphPAxisReset.TabIndex = 10;
            this.buttonGraphPAxisReset.Click += new System.EventHandler(this.buttonGraphPAxisReset_Click);
            // 
            // comboY
            // 
            this.comboY.FormattingEnabled = true;
            this.comboY.ItemHeight = 15;
            this.comboY.Items.AddRange(new object[] {
            "1",
            "2",
            "3"});
            this.comboY.Location = new System.Drawing.Point(1491, 1096);
            this.comboY.Name = "comboY";
            this.comboY.Size = new System.Drawing.Size(69, 34);
            this.comboY.TabIndex = 22;
            this.comboY.Visible = false;
            // 
            // panelGraphCalcLogginData
            // 
            this.panelGraphCalcLogginData.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelGraphCalcLogginData.BackgroundImage")));
            this.panelGraphCalcLogginData.Controls.Add(this.comboCalcDataYMax);
            this.panelGraphCalcLogginData.Controls.Add(this.comboCalcDataYMin);
            this.panelGraphCalcLogginData.Controls.Add(this.comboCalcDataRPMMax);
            this.panelGraphCalcLogginData.Controls.Add(this.comboCalcDataRPMYMin);
            this.panelGraphCalcLogginData.Controls.Add(this.editCalcDataRPMYMax);
            this.panelGraphCalcLogginData.Controls.Add(this.editCalcDataRPMYMin);
            this.panelGraphCalcLogginData.Controls.Add(this.panelCalcDataYReset);
            this.panelGraphCalcLogginData.Controls.Add(this.editCalcDataYMax);
            this.panelGraphCalcLogginData.Controls.Add(this.editCalcDataYMin);
            this.panelGraphCalcLogginData.Controls.Add(this.panelCalcDataRPMYReset);
            this.panelGraphCalcLogginData.Location = new System.Drawing.Point(1611, 3);
            this.panelGraphCalcLogginData.Name = "panelGraphCalcLogginData";
            this.panelGraphCalcLogginData.Size = new System.Drawing.Size(144, 469);
            this.panelGraphCalcLogginData.TabIndex = 12;
            // 
            // comboCalcDataYMax
            // 
            this.comboCalcDataYMax.BackColor = System.Drawing.Color.Transparent;
            this.comboCalcDataYMax.Location = new System.Drawing.Point(90, 400);
            this.comboCalcDataYMax.Name = "comboCalcDataYMax";
            this.comboCalcDataYMax.Size = new System.Drawing.Size(30, 30);
            this.comboCalcDataYMax.TabIndex = 19;
            this.comboCalcDataYMax.Click += new System.EventHandler(this.combo_Click);
            this.comboCalcDataYMax.MouseEnter += new System.EventHandler(this.obj_MouseEnter);
            this.comboCalcDataYMax.MouseLeave += new System.EventHandler(this.obj_MouseLeave);
            // 
            // comboCalcDataYMin
            // 
            this.comboCalcDataYMin.BackColor = System.Drawing.Color.Transparent;
            this.comboCalcDataYMin.Location = new System.Drawing.Point(90, 348);
            this.comboCalcDataYMin.Name = "comboCalcDataYMin";
            this.comboCalcDataYMin.Size = new System.Drawing.Size(30, 30);
            this.comboCalcDataYMin.TabIndex = 18;
            this.comboCalcDataYMin.Click += new System.EventHandler(this.combo_Click);
            this.comboCalcDataYMin.MouseEnter += new System.EventHandler(this.obj_MouseEnter);
            this.comboCalcDataYMin.MouseLeave += new System.EventHandler(this.obj_MouseLeave);
            // 
            // comboCalcDataRPMMax
            // 
            this.comboCalcDataRPMMax.BackColor = System.Drawing.Color.Transparent;
            this.comboCalcDataRPMMax.Location = new System.Drawing.Point(90, 113);
            this.comboCalcDataRPMMax.Name = "comboCalcDataRPMMax";
            this.comboCalcDataRPMMax.Size = new System.Drawing.Size(30, 30);
            this.comboCalcDataRPMMax.TabIndex = 17;
            this.comboCalcDataRPMMax.Click += new System.EventHandler(this.combo_Click);
            this.comboCalcDataRPMMax.MouseEnter += new System.EventHandler(this.obj_MouseEnter);
            this.comboCalcDataRPMMax.MouseLeave += new System.EventHandler(this.obj_MouseLeave);
            // 
            // comboCalcDataRPMYMin
            // 
            this.comboCalcDataRPMYMin.BackColor = System.Drawing.Color.Transparent;
            this.comboCalcDataRPMYMin.Location = new System.Drawing.Point(90, 61);
            this.comboCalcDataRPMYMin.Name = "comboCalcDataRPMYMin";
            this.comboCalcDataRPMYMin.Size = new System.Drawing.Size(30, 30);
            this.comboCalcDataRPMYMin.TabIndex = 16;
            this.comboCalcDataRPMYMin.Click += new System.EventHandler(this.combo_Click);
            this.comboCalcDataRPMYMin.MouseEnter += new System.EventHandler(this.obj_MouseEnter);
            this.comboCalcDataRPMYMin.MouseLeave += new System.EventHandler(this.obj_MouseLeave);
            // 
            // editCalcDataRPMYMax
            // 
            this.editCalcDataRPMYMax.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.editCalcDataRPMYMax.Font = new System.Drawing.Font("맑은 고딕", 10F);
            this.editCalcDataRPMYMax.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.editCalcDataRPMYMax.Location = new System.Drawing.Point(52, 120);
            this.editCalcDataRPMYMax.Multiline = true;
            this.editCalcDataRPMYMax.Name = "editCalcDataRPMYMax";
            this.editCalcDataRPMYMax.Size = new System.Drawing.Size(38, 17);
            this.editCalcDataRPMYMax.TabIndex = 15;
            this.editCalcDataRPMYMax.Text = "0";
            this.editCalcDataRPMYMax.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.editCalcDataRPMYMax.TextChanged += new System.EventHandler(this.editCalcDataRPMYMin_TextChanged);
            // 
            // editCalcDataRPMYMin
            // 
            this.editCalcDataRPMYMin.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.editCalcDataRPMYMin.Font = new System.Drawing.Font("맑은 고딕", 10F);
            this.editCalcDataRPMYMin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.editCalcDataRPMYMin.Location = new System.Drawing.Point(52, 68);
            this.editCalcDataRPMYMin.Multiline = true;
            this.editCalcDataRPMYMin.Name = "editCalcDataRPMYMin";
            this.editCalcDataRPMYMin.Size = new System.Drawing.Size(38, 17);
            this.editCalcDataRPMYMin.TabIndex = 14;
            this.editCalcDataRPMYMin.Text = "0";
            this.editCalcDataRPMYMin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.editCalcDataRPMYMin.TextChanged += new System.EventHandler(this.editCalcDataRPMYMin_TextChanged);
            // 
            // panelCalcDataYReset
            // 
            this.panelCalcDataYReset.BackColor = System.Drawing.Color.Transparent;
            this.panelCalcDataYReset.Location = new System.Drawing.Point(1, 306);
            this.panelCalcDataYReset.Name = "panelCalcDataYReset";
            this.panelCalcDataYReset.Size = new System.Drawing.Size(45, 81);
            this.panelCalcDataYReset.TabIndex = 11;
            this.panelCalcDataYReset.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panelCalcDataYReset_MouseClick);
            // 
            // editCalcDataYMax
            // 
            this.editCalcDataYMax.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.editCalcDataYMax.Font = new System.Drawing.Font("맑은 고딕", 10F);
            this.editCalcDataYMax.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.editCalcDataYMax.Location = new System.Drawing.Point(52, 407);
            this.editCalcDataYMax.Multiline = true;
            this.editCalcDataYMax.Name = "editCalcDataYMax";
            this.editCalcDataYMax.Size = new System.Drawing.Size(38, 17);
            this.editCalcDataYMax.TabIndex = 13;
            this.editCalcDataYMax.Text = "0";
            this.editCalcDataYMax.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.editCalcDataYMax.TextChanged += new System.EventHandler(this.editCalcDataYMin_TextChanged);
            // 
            // editCalcDataYMin
            // 
            this.editCalcDataYMin.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.editCalcDataYMin.Font = new System.Drawing.Font("맑은 고딕", 10F);
            this.editCalcDataYMin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.editCalcDataYMin.Location = new System.Drawing.Point(52, 355);
            this.editCalcDataYMin.Multiline = true;
            this.editCalcDataYMin.Name = "editCalcDataYMin";
            this.editCalcDataYMin.Size = new System.Drawing.Size(38, 17);
            this.editCalcDataYMin.TabIndex = 12;
            this.editCalcDataYMin.Text = "0";
            this.editCalcDataYMin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.editCalcDataYMin.TextChanged += new System.EventHandler(this.editCalcDataYMin_TextChanged);
            // 
            // panelCalcDataRPMYReset
            // 
            this.panelCalcDataRPMYReset.BackColor = System.Drawing.Color.Transparent;
            this.panelCalcDataRPMYReset.Location = new System.Drawing.Point(0, 19);
            this.panelCalcDataRPMYReset.Name = "panelCalcDataRPMYReset";
            this.panelCalcDataRPMYReset.Size = new System.Drawing.Size(45, 81);
            this.panelCalcDataRPMYReset.TabIndex = 10;
            this.panelCalcDataRPMYReset.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panelCalcDataRPMYReset_MouseClick);
            // 
            // panelGraphCalc
            // 
            this.panelGraphCalc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(244)))));
            this.panelGraphCalc.Controls.Add(this.panelGraphCalcLogginData);
            this.panelGraphCalc.Controls.Add(this._chartCalcData);
            this.panelGraphCalc.Controls.Add(this._chartCalcDataRPM);
            this.panelGraphCalc.Location = new System.Drawing.Point(80, 524);
            this.panelGraphCalc.Name = "panelGraphCalc";
            this.panelGraphCalc.Size = new System.Drawing.Size(1758, 475);
            this.panelGraphCalc.TabIndex = 12;
            this.panelGraphCalc.Visible = false;
            this.panelGraphCalc.Paint += new System.Windows.Forms.PaintEventHandler(this.panelGraphCalc_Paint);
            this.panelGraphCalc.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panelGraphCalc_MouseDown);
            this.panelGraphCalc.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panelGraphCalc_MouseMove);
            this.panelGraphCalc.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panelGraphCalc_MouseUp);
            // 
            // _chartCalcData
            // 
            this._chartCalcData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(243)))), ((int)(((byte)(245)))));
            this._chartCalcData.BorderlineWidth = 0;
            this._chartCalcData.BorderSkin.BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            this._chartCalcData.BorderSkin.PageColor = System.Drawing.Color.Empty;
            chartArea4.AxisX.IsLabelAutoFit = false;
            chartArea4.AxisX.IsMarginVisible = false;
            chartArea4.AxisX.LabelAutoFitMaxFontSize = 12;
            chartArea4.AxisX.LabelAutoFitMinFontSize = 12;
            chartArea4.AxisX.LabelStyle.Enabled = false;
            chartArea4.AxisX.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            chartArea4.AxisX.LabelStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            chartArea4.AxisX.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(191)))), ((int)(((byte)(193)))));
            chartArea4.AxisX.MajorGrid.Enabled = false;
            chartArea4.AxisX.MajorTickMark.Enabled = false;
            chartArea4.AxisX.TitleForeColor = System.Drawing.Color.DimGray;
            chartArea4.AxisX2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(191)))), ((int)(((byte)(193)))));
            chartArea4.AxisY.IsLabelAutoFit = false;
            chartArea4.AxisY.LabelStyle.Font = new System.Drawing.Font("HelveticaNeueLT Pro 67 MdCn", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            chartArea4.AxisY.LabelStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            chartArea4.AxisY.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(191)))), ((int)(((byte)(193)))));
            chartArea4.AxisY.MajorGrid.Enabled = false;
            chartArea4.AxisY.MajorTickMark.Enabled = false;
            chartArea4.AxisY2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(191)))), ((int)(((byte)(193)))));
            chartArea4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(243)))), ((int)(((byte)(245)))));
            chartArea4.InnerPlotPosition.Auto = false;
            chartArea4.InnerPlotPosition.Height = 98F;
            chartArea4.InnerPlotPosition.Width = 98F;
            chartArea4.InnerPlotPosition.X = 2F;
            chartArea4.InnerPlotPosition.Y = 2F;
            chartArea4.Name = "ChartArea1";
            chartArea4.Position.Auto = false;
            chartArea4.Position.Height = 94F;
            chartArea4.Position.Width = 96.5F;
            chartArea4.Position.X = 0.5F;
            chartArea4.Position.Y = 3F;
            this._chartCalcData.ChartAreas.Add(chartArea4);
            this._chartCalcData.Location = new System.Drawing.Point(122, 215);
            this._chartCalcData.Margin = new System.Windows.Forms.Padding(0);
            this._chartCalcData.Name = "_chartCalcData";
            this._chartCalcData.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            this._chartCalcData.Size = new System.Drawing.Size(1515, 226);
            this._chartCalcData.TabIndex = 13;
            // 
            // _chartCalcDataRPM
            // 
            this._chartCalcDataRPM.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(243)))), ((int)(((byte)(245)))));
            this._chartCalcDataRPM.BorderlineWidth = 0;
            this._chartCalcDataRPM.BorderSkin.BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            this._chartCalcDataRPM.BorderSkin.PageColor = System.Drawing.Color.Empty;
            chartArea5.AxisX.IsLabelAutoFit = false;
            chartArea5.AxisX.IsMarginVisible = false;
            chartArea5.AxisX.LabelAutoFitMaxFontSize = 12;
            chartArea5.AxisX.LabelAutoFitMinFontSize = 12;
            chartArea5.AxisX.LabelStyle.Enabled = false;
            chartArea5.AxisX.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            chartArea5.AxisX.LabelStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            chartArea5.AxisX.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(191)))), ((int)(((byte)(193)))));
            chartArea5.AxisX.MajorGrid.Enabled = false;
            chartArea5.AxisX.MajorTickMark.Enabled = false;
            chartArea5.AxisX.TitleForeColor = System.Drawing.Color.DimGray;
            chartArea5.AxisX2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(191)))), ((int)(((byte)(193)))));
            chartArea5.AxisY.IsLabelAutoFit = false;
            chartArea5.AxisY.LabelStyle.Font = new System.Drawing.Font("HelveticaNeueLT Pro 67 MdCn", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            chartArea5.AxisY.LabelStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            chartArea5.AxisY.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(191)))), ((int)(((byte)(193)))));
            chartArea5.AxisY.MajorGrid.Enabled = false;
            chartArea5.AxisY.MajorTickMark.Enabled = false;
            chartArea5.AxisY2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(191)))), ((int)(((byte)(193)))));
            chartArea5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(243)))), ((int)(((byte)(245)))));
            chartArea5.InnerPlotPosition.Auto = false;
            chartArea5.InnerPlotPosition.Height = 98F;
            chartArea5.InnerPlotPosition.Width = 98F;
            chartArea5.InnerPlotPosition.X = 2F;
            chartArea5.InnerPlotPosition.Y = 2F;
            chartArea5.Name = "ChartArea1";
            chartArea5.Position.Auto = false;
            chartArea5.Position.Height = 94F;
            chartArea5.Position.Width = 96.5F;
            chartArea5.Position.X = 0.5F;
            chartArea5.Position.Y = 3F;
            this._chartCalcDataRPM.ChartAreas.Add(chartArea5);
            this._chartCalcDataRPM.Location = new System.Drawing.Point(122, 50);
            this._chartCalcDataRPM.Margin = new System.Windows.Forms.Padding(0);
            this._chartCalcDataRPM.Name = "_chartCalcDataRPM";
            this._chartCalcDataRPM.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            series5.ChartArea = "ChartArea1";
            series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            series5.Name = "SeriesRPMLogging";
            this._chartCalcDataRPM.Series.Add(series5);
            this._chartCalcDataRPM.Size = new System.Drawing.Size(1515, 106);
            this._chartCalcDataRPM.TabIndex = 3;
            // 
            // labelAlarmSys
            // 
            this.labelAlarmSys.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.labelAlarmSys.ForeColor = System.Drawing.Color.White;
            this.labelAlarmSys.Location = new System.Drawing.Point(420, 176);
            this.labelAlarmSys.Name = "labelAlarmSys";
            this.labelAlarmSys.Size = new System.Drawing.Size(101, 39);
            this.labelAlarmSys.TabIndex = 20;
            this.labelAlarmSys.Text = "Sys. Alarm";
            this.labelAlarmSys.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelAlarm
            // 
            this.panelAlarm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(244)))));
            this.panelAlarm.ForeColor = System.Drawing.Color.Black;
            this.panelAlarm.Location = new System.Drawing.Point(420, 215);
            this.panelAlarm.Name = "panelAlarm";
            this.panelAlarm.Size = new System.Drawing.Size(500, 230);
            this.panelAlarm.TabIndex = 22;
            this.panelAlarm.Paint += new System.Windows.Forms.PaintEventHandler(this.panelAlarm_Paint);
            // 
            // labelAlarmHKnock
            // 
            this.labelAlarmHKnock.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(198)))), ((int)(((byte)(201)))));
            this.labelAlarmHKnock.ForeColor = System.Drawing.Color.White;
            this.labelAlarmHKnock.Location = new System.Drawing.Point(820, 176);
            this.labelAlarmHKnock.Name = "labelAlarmHKnock";
            this.labelAlarmHKnock.Size = new System.Drawing.Size(100, 39);
            this.labelAlarmHKnock.TabIndex = 24;
            this.labelAlarmHKnock.Text = "H. Knock";
            this.labelAlarmHKnock.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelAlarmLKnock
            // 
            this.labelAlarmLKnock.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(198)))), ((int)(((byte)(201)))));
            this.labelAlarmLKnock.ForeColor = System.Drawing.Color.White;
            this.labelAlarmLKnock.Location = new System.Drawing.Point(720, 176);
            this.labelAlarmLKnock.Name = "labelAlarmLKnock";
            this.labelAlarmLKnock.Size = new System.Drawing.Size(101, 39);
            this.labelAlarmLKnock.TabIndex = 23;
            this.labelAlarmLKnock.Text = "L. Knock";
            this.labelAlarmLKnock.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelAlarmMisfire
            // 
            this.labelAlarmMisfire.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(198)))), ((int)(((byte)(201)))));
            this.labelAlarmMisfire.ForeColor = System.Drawing.Color.White;
            this.labelAlarmMisfire.Location = new System.Drawing.Point(620, 176);
            this.labelAlarmMisfire.Name = "labelAlarmMisfire";
            this.labelAlarmMisfire.Size = new System.Drawing.Size(101, 39);
            this.labelAlarmMisfire.TabIndex = 22;
            this.labelAlarmMisfire.Text = "Misfire";
            this.labelAlarmMisfire.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelAlarmFault
            // 
            this.labelAlarmFault.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(198)))), ((int)(((byte)(201)))));
            this.labelAlarmFault.ForeColor = System.Drawing.Color.White;
            this.labelAlarmFault.Location = new System.Drawing.Point(520, 176);
            this.labelAlarmFault.Name = "labelAlarmFault";
            this.labelAlarmFault.Size = new System.Drawing.Size(101, 39);
            this.labelAlarmFault.TabIndex = 21;
            this.labelAlarmFault.Text = "Fault";
            this.labelAlarmFault.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelCalcData
            // 
            this.panelCalcData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(244)))));
            this.panelCalcData.Controls.Add(this.panelGraphCalcDataBottom);
            this.panelCalcData.Controls.Add(this.chartPMax);
            this.panelCalcData.Controls.Add(this.label28);
            this.panelCalcData.Controls.Add(this.labelCalcDataAvg1);
            this.panelCalcData.Controls.Add(this.labelCalcDataAvg);
            this.panelCalcData.ForeColor = System.Drawing.Color.Black;
            this.panelCalcData.Location = new System.Drawing.Point(960, 215);
            this.panelCalcData.Name = "panelCalcData";
            this.panelCalcData.Size = new System.Drawing.Size(880, 230);
            this.panelCalcData.TabIndex = 23;
            this.panelCalcData.Paint += new System.Windows.Forms.PaintEventHandler(this.panelCalcData_Paint);
            // 
            // panelGraphCalcDataBottom
            // 
            this.panelGraphCalcDataBottom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(244)))));
            this.panelGraphCalcDataBottom.Location = new System.Drawing.Point(1, 166);
            this.panelGraphCalcDataBottom.Name = "panelGraphCalcDataBottom";
            this.panelGraphCalcDataBottom.Size = new System.Drawing.Size(878, 63);
            this.panelGraphCalcDataBottom.TabIndex = 6;
            this.panelGraphCalcDataBottom.Paint += new System.Windows.Forms.PaintEventHandler(this.panelGraphCalcDataBottom_Paint);
            // 
            // label28
            // 
            this.label28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(63)))), ((int)(((byte)(36)))));
            this.label28.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(63)))), ((int)(((byte)(36)))));
            this.label28.Location = new System.Drawing.Point(40, 31);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(28, 2);
            this.label28.TabIndex = 3;
            // 
            // labelCalcDataAvg1
            // 
            this.labelCalcDataAvg1.AutoSize = true;
            this.labelCalcDataAvg1.BackColor = System.Drawing.Color.Transparent;
            this.labelCalcDataAvg1.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelCalcDataAvg1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.labelCalcDataAvg1.Location = new System.Drawing.Point(76, 25);
            this.labelCalcDataAvg1.Name = "labelCalcDataAvg1";
            this.labelCalcDataAvg1.Size = new System.Drawing.Size(25, 12);
            this.labelCalcDataAvg1.TabIndex = 4;
            this.labelCalcDataAvg1.Text = "Avg.";
            // 
            // labelCalcDataAvg
            // 
            this.labelCalcDataAvg.BackColor = System.Drawing.Color.Transparent;
            this.labelCalcDataAvg.Font = new System.Drawing.Font("맑은 고딕", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelCalcDataAvg.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(73)))), ((int)(((byte)(75)))), ((int)(((byte)(76)))));
            this.labelCalcDataAvg.Location = new System.Drawing.Point(100, 15);
            this.labelCalcDataAvg.Name = "labelCalcDataAvg";
            this.labelCalcDataAvg.Size = new System.Drawing.Size(100, 31);
            this.labelCalcDataAvg.TabIndex = 5;
            this.labelCalcDataAvg.Text = "000.0";
            this.labelCalcDataAvg.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelCalcDataAvg.Click += new System.EventHandler(this.labelCalcDataAvg_Click);
            // 
            // labelCalcDataKnockIntst
            // 
            this.labelCalcDataKnockIntst.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(198)))), ((int)(((byte)(201)))));
            this.labelCalcDataKnockIntst.ForeColor = System.Drawing.Color.White;
            this.labelCalcDataKnockIntst.Location = new System.Drawing.Point(1460, 176);
            this.labelCalcDataKnockIntst.Name = "labelCalcDataKnockIntst";
            this.labelCalcDataKnockIntst.Size = new System.Drawing.Size(101, 39);
            this.labelCalcDataKnockIntst.TabIndex = 31;
            this.labelCalcDataKnockIntst.Text = "Knock Intst.";
            this.labelCalcDataKnockIntst.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelCalcDataPegging
            // 
            this.labelCalcDataPegging.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(198)))), ((int)(((byte)(201)))));
            this.labelCalcDataPegging.ForeColor = System.Drawing.Color.White;
            this.labelCalcDataPegging.Location = new System.Drawing.Point(1360, 176);
            this.labelCalcDataPegging.Name = "labelCalcDataPegging";
            this.labelCalcDataPegging.Size = new System.Drawing.Size(101, 39);
            this.labelCalcDataPegging.TabIndex = 30;
            this.labelCalcDataPegging.Text = "Pegging";
            this.labelCalcDataPegging.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelCalcDataMisfire
            // 
            this.labelCalcDataMisfire.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(198)))), ((int)(((byte)(201)))));
            this.labelCalcDataMisfire.ForeColor = System.Drawing.Color.White;
            this.labelCalcDataMisfire.Location = new System.Drawing.Point(1260, 176);
            this.labelCalcDataMisfire.Name = "labelCalcDataMisfire";
            this.labelCalcDataMisfire.Size = new System.Drawing.Size(101, 39);
            this.labelCalcDataMisfire.TabIndex = 29;
            this.labelCalcDataMisfire.Text = "Misfire";
            this.labelCalcDataMisfire.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelCalcDataHR
            // 
            this.labelCalcDataHR.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(198)))), ((int)(((byte)(201)))));
            this.labelCalcDataHR.ForeColor = System.Drawing.Color.White;
            this.labelCalcDataHR.Location = new System.Drawing.Point(1160, 176);
            this.labelCalcDataHR.Name = "labelCalcDataHR";
            this.labelCalcDataHR.Size = new System.Drawing.Size(101, 39);
            this.labelCalcDataHR.TabIndex = 28;
            this.labelCalcDataHR.Text = "HR";
            this.labelCalcDataHR.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelCalcDataIMEP
            // 
            this.labelCalcDataIMEP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(198)))), ((int)(((byte)(201)))));
            this.labelCalcDataIMEP.ForeColor = System.Drawing.Color.White;
            this.labelCalcDataIMEP.Location = new System.Drawing.Point(1060, 176);
            this.labelCalcDataIMEP.Name = "labelCalcDataIMEP";
            this.labelCalcDataIMEP.Size = new System.Drawing.Size(101, 39);
            this.labelCalcDataIMEP.TabIndex = 27;
            this.labelCalcDataIMEP.Text = "IMEP";
            this.labelCalcDataIMEP.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelCalcDataPmax
            // 
            this.labelCalcDataPmax.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.labelCalcDataPmax.ForeColor = System.Drawing.Color.White;
            this.labelCalcDataPmax.Location = new System.Drawing.Point(960, 176);
            this.labelCalcDataPmax.Name = "labelCalcDataPmax";
            this.labelCalcDataPmax.Size = new System.Drawing.Size(101, 39);
            this.labelCalcDataPmax.TabIndex = 26;
            this.labelCalcDataPmax.Text = "Pmax";
            this.labelCalcDataPmax.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelGraph
            // 
            this.labelGraph.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.labelGraph.ForeColor = System.Drawing.Color.White;
            this.labelGraph.Location = new System.Drawing.Point(80, 485);
            this.labelGraph.Name = "labelGraph";
            this.labelGraph.Size = new System.Drawing.Size(221, 39);
            this.labelGraph.TabIndex = 32;
            this.labelGraph.Text = "Cyl. Pressure Data";
            this.labelGraph.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelGraphCalc
            // 
            this.labelGraphCalc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(198)))), ((int)(((byte)(201)))));
            this.labelGraphCalc.ForeColor = System.Drawing.Color.White;
            this.labelGraphCalc.Location = new System.Drawing.Point(300, 485);
            this.labelGraphCalc.Name = "labelGraphCalc";
            this.labelGraphCalc.Size = new System.Drawing.Size(220, 39);
            this.labelGraphCalc.TabIndex = 33;
            this.labelGraphCalc.Text = "Calculated Logging Data";
            this.labelGraphCalc.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelBottomMsg
            // 
            this.labelBottomMsg.Font = new System.Drawing.Font("맑은 고딕", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelBottomMsg.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(63)))), ((int)(((byte)(36)))));
            this.labelBottomMsg.Location = new System.Drawing.Point(80, 1012);
            this.labelBottomMsg.Name = "labelBottomMsg";
            this.labelBottomMsg.Size = new System.Drawing.Size(1760, 19);
            this.labelBottomMsg.TabIndex = 34;
            // 
            // labelMenuSelected
            // 
            this.labelMenuSelected.Font = new System.Drawing.Font("맑은 고딕", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelMenuSelected.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(89)))), ((int)(((byte)(178)))));
            this.labelMenuSelected.Location = new System.Drawing.Point(80, 125);
            this.labelMenuSelected.Name = "labelMenuSelected";
            this.labelMenuSelected.Size = new System.Drawing.Size(93, 21);
            this.labelMenuSelected.TabIndex = 35;
            this.labelMenuSelected.Tag = "";
            this.labelMenuSelected.Text = "Monitoring";
            // 
            // labelRPMTitle
            // 
            this.labelRPMTitle.AutoSize = true;
            this.labelRPMTitle.Font = new System.Drawing.Font("맑은 고딕", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelRPMTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(73)))), ((int)(((byte)(75)))), ((int)(((byte)(76)))));
            this.labelRPMTitle.Location = new System.Drawing.Point(80, 151);
            this.labelRPMTitle.Name = "labelRPMTitle";
            this.labelRPMTitle.Size = new System.Drawing.Size(44, 21);
            this.labelRPMTitle.TabIndex = 36;
            this.labelRPMTitle.Text = "RPM";
            // 
            // labelAlarm
            // 
            this.labelAlarm.AutoSize = true;
            this.labelAlarm.Font = new System.Drawing.Font("맑은 고딕", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelAlarm.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(73)))), ((int)(((byte)(75)))), ((int)(((byte)(76)))));
            this.labelAlarm.Location = new System.Drawing.Point(420, 151);
            this.labelAlarm.Name = "labelAlarm";
            this.labelAlarm.Size = new System.Drawing.Size(53, 21);
            this.labelAlarm.TabIndex = 37;
            this.labelAlarm.Text = "Alarm";
            // 
            // labelCalcData
            // 
            this.labelCalcData.AutoSize = true;
            this.labelCalcData.Font = new System.Drawing.Font("맑은 고딕", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelCalcData.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(73)))), ((int)(((byte)(75)))), ((int)(((byte)(76)))));
            this.labelCalcData.Location = new System.Drawing.Point(960, 151);
            this.labelCalcData.Name = "labelCalcData";
            this.labelCalcData.Size = new System.Drawing.Size(129, 21);
            this.labelCalcData.TabIndex = 38;
            this.labelCalcData.Text = "Calculation Data";
            // 
            // panelRPM
            // 
            this.panelRPM.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(243)))), ((int)(((byte)(244)))));
            this.panelRPM.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelRPM.BackgroundImage")));
            this.panelRPM.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panelRPM.Controls.Add(this.labelRPM);
            this.panelRPM.ForeColor = System.Drawing.Color.Black;
            this.panelRPM.Location = new System.Drawing.Point(80, 176);
            this.panelRPM.Name = "panelRPM";
            this.panelRPM.Size = new System.Drawing.Size(300, 269);
            this.panelRPM.TabIndex = 39;
            this.panelRPM.Paint += new System.Windows.Forms.PaintEventHandler(this.panelRPM_Paint);
            // 
            // labelRPM
            // 
            this.labelRPM.BackColor = System.Drawing.Color.Transparent;
            this.labelRPM.Font = new System.Drawing.Font("맑은 고딕", 62F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelRPM.Location = new System.Drawing.Point(36, 91);
            this.labelRPM.Name = "labelRPM";
            this.labelRPM.Size = new System.Drawing.Size(228, 80);
            this.labelRPM.TabIndex = 18;
            this.labelRPM.Text = "0";
            this.labelRPM.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelSetting
            // 
            this.panelSetting.Controls.Add(this.buttonSettingClose);
            this.panelSetting.Controls.Add(this.groupBox5);
            this.panelSetting.Controls.Add(this.groupBox1);
            this.panelSetting.Controls.Add(this.groupBox2);
            this.panelSetting.Controls.Add(this.groupBox4);
            this.panelSetting.Controls.Add(this.groupBox3);
            this.panelSetting.Location = new System.Drawing.Point(58, 2512);
            this.panelSetting.Name = "panelSetting";
            this.panelSetting.Size = new System.Drawing.Size(1808, 906);
            this.panelSetting.TabIndex = 40;
            this.panelSetting.Visible = false;
            // 
            // buttonSettingClose
            // 
            this.buttonSettingClose.Location = new System.Drawing.Point(1129, 28);
            this.buttonSettingClose.Name = "buttonSettingClose";
            this.buttonSettingClose.Size = new System.Drawing.Size(133, 41);
            this.buttonSettingClose.TabIndex = 8;
            this.buttonSettingClose.Text = "닫기";
            this.buttonSettingClose.UseVisualStyleBackColor = true;
            this.buttonSettingClose.Visible = false;
            this.buttonSettingClose.Click += new System.EventHandler(this.buttonSettingClose_Click);
            // 
            // panelTop
            // 
            this.panelTop.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelTop.BackgroundImage")));
            this.panelTop.Controls.Add(this.panelMenuDignosis);
            this.panelTop.Controls.Add(this.panelMenuCalibration);
            this.panelTop.Controls.Add(this.panelMonitoring);
            this.panelTop.Controls.Add(this.imgBack);
            this.panelTop.Controls.Add(this.buttonUser);
            this.panelTop.Controls.Add(this.buttonLogout);
            this.panelTop.Controls.Add(this.imageLoggingOn);
            this.panelTop.Controls.Add(this.imageLoggingOff);
            this.panelTop.Controls.Add(this.panelExit);
            this.panelTop.Controls.Add(this.imageConOn);
            this.panelTop.Controls.Add(this.buttonSetting);
            this.panelTop.Controls.Add(this.imageConOff);
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(1920, 75);
            this.panelTop.TabIndex = 41;
            this.panelTop.Paint += new System.Windows.Forms.PaintEventHandler(this.panelTop_Paint);
            this.panelTop.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panelTop_MouseDown);
            // 
            // panelMenuDignosis
            // 
            this.panelMenuDignosis.BackColor = System.Drawing.Color.Transparent;
            this.panelMenuDignosis.Controls.Add(this.labelMenuDiagnosis);
            this.panelMenuDignosis.ForeColor = System.Drawing.Color.Transparent;
            this.panelMenuDignosis.Location = new System.Drawing.Point(480, 4);
            this.panelMenuDignosis.Name = "panelMenuDignosis";
            this.panelMenuDignosis.Size = new System.Drawing.Size(116, 67);
            this.panelMenuDignosis.TabIndex = 22;
            this.panelMenuDignosis.Click += new System.EventHandler(this.panelMenuDignosis_Click);
            // 
            // labelMenuDiagnosis
            // 
            this.labelMenuDiagnosis.AutoSize = true;
            this.labelMenuDiagnosis.BackColor = System.Drawing.Color.Transparent;
            this.labelMenuDiagnosis.Font = new System.Drawing.Font("맑은 고딕", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelMenuDiagnosis.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.labelMenuDiagnosis.Location = new System.Drawing.Point(15, 23);
            this.labelMenuDiagnosis.Name = "labelMenuDiagnosis";
            this.labelMenuDiagnosis.Size = new System.Drawing.Size(80, 21);
            this.labelMenuDiagnosis.TabIndex = 39;
            this.labelMenuDiagnosis.Text = "Diagnosis";
            this.labelMenuDiagnosis.Click += new System.EventHandler(this.panelMenuDignosis_Click);
            // 
            // panelMenuCalibration
            // 
            this.panelMenuCalibration.BackColor = System.Drawing.Color.Transparent;
            this.panelMenuCalibration.Controls.Add(this.labelMenuCal);
            this.panelMenuCalibration.ForeColor = System.Drawing.Color.Transparent;
            this.panelMenuCalibration.Location = new System.Drawing.Point(360, 4);
            this.panelMenuCalibration.Name = "panelMenuCalibration";
            this.panelMenuCalibration.Size = new System.Drawing.Size(116, 67);
            this.panelMenuCalibration.TabIndex = 19;
            this.panelMenuCalibration.Click += new System.EventHandler(this.panelMenuCalibration_Click);
            // 
            // labelMenuCal
            // 
            this.labelMenuCal.AutoSize = true;
            this.labelMenuCal.BackColor = System.Drawing.Color.Transparent;
            this.labelMenuCal.Font = new System.Drawing.Font("맑은 고딕", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelMenuCal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.labelMenuCal.Location = new System.Drawing.Point(17, 23);
            this.labelMenuCal.Name = "labelMenuCal";
            this.labelMenuCal.Size = new System.Drawing.Size(89, 21);
            this.labelMenuCal.TabIndex = 39;
            this.labelMenuCal.Text = "Calibration";
            this.labelMenuCal.Click += new System.EventHandler(this.panelMenuCalibration_Click);
            // 
            // panelMonitoring
            // 
            this.panelMonitoring.BackColor = System.Drawing.Color.Transparent;
            this.panelMonitoring.Controls.Add(this.labelMenuMonitoring);
            this.panelMonitoring.ForeColor = System.Drawing.Color.Transparent;
            this.panelMonitoring.Location = new System.Drawing.Point(231, 4);
            this.panelMonitoring.Name = "panelMonitoring";
            this.panelMonitoring.Size = new System.Drawing.Size(127, 67);
            this.panelMonitoring.TabIndex = 12;
            this.panelMonitoring.Click += new System.EventHandler(this.panelMonitoring_Click);
            this.panelMonitoring.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panelMonitoring_MouseClick);
            this.panelMonitoring.MouseEnter += new System.EventHandler(this.panelMonitoring_MouseEnter);
            // 
            // labelMenuMonitoring
            // 
            this.labelMenuMonitoring.AutoSize = true;
            this.labelMenuMonitoring.BackColor = System.Drawing.Color.Transparent;
            this.labelMenuMonitoring.Font = new System.Drawing.Font("맑은 고딕", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelMenuMonitoring.ForeColor = System.Drawing.Color.White;
            this.labelMenuMonitoring.Location = new System.Drawing.Point(26, 23);
            this.labelMenuMonitoring.Name = "labelMenuMonitoring";
            this.labelMenuMonitoring.Size = new System.Drawing.Size(93, 21);
            this.labelMenuMonitoring.TabIndex = 38;
            this.labelMenuMonitoring.Text = "Monitoring";
            this.labelMenuMonitoring.Click += new System.EventHandler(this.panelMonitoring_Click);
            // 
            // imgBack
            // 
            this.imgBack.Image = ((System.Drawing.Image)(resources.GetObject("imgBack.Image")));
            this.imgBack.Location = new System.Drawing.Point(3, 5);
            this.imgBack.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.imgBack.Name = "imgBack";
            this.imgBack.Size = new System.Drawing.Size(60, 29);
            this.imgBack.TabIndex = 39;
            this.imgBack.TabStop = false;
            this.imgBack.Visible = false;
            // 
            // buttonUser
            // 
            this.buttonUser.BackColor = System.Drawing.Color.Transparent;
            this.buttonUser.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonUser.BackgroundImage")));
            this.buttonUser.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonUser.Location = new System.Drawing.Point(1556, 26);
            this.buttonUser.Name = "buttonUser";
            this.buttonUser.Size = new System.Drawing.Size(30, 34);
            this.buttonUser.TabIndex = 21;
            this.buttonUser.Tag = "user";
            this.buttonUser.MouseEnter += new System.EventHandler(this.obj_MouseEnter);
            this.buttonUser.MouseLeave += new System.EventHandler(this.obj_MouseLeave);
            // 
            // buttonLogout
            // 
            this.buttonLogout.BackColor = System.Drawing.Color.Transparent;
            this.buttonLogout.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonLogout.BackgroundImage")));
            this.buttonLogout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonLogout.Location = new System.Drawing.Point(1649, 23);
            this.buttonLogout.Name = "buttonLogout";
            this.buttonLogout.Size = new System.Drawing.Size(30, 34);
            this.buttonLogout.TabIndex = 20;
            this.buttonLogout.Tag = "logout";
            this.buttonLogout.Click += new System.EventHandler(this.buttonLogout_Click);
            this.buttonLogout.MouseEnter += new System.EventHandler(this.obj_MouseEnter);
            this.buttonLogout.MouseLeave += new System.EventHandler(this.obj_MouseLeave);
            // 
            // imageLoggingOn
            // 
            this.imageLoggingOn.Image = ((System.Drawing.Image)(resources.GetObject("imageLoggingOn.Image")));
            this.imageLoggingOn.Location = new System.Drawing.Point(1420, 0);
            this.imageLoggingOn.Name = "imageLoggingOn";
            this.imageLoggingOn.Size = new System.Drawing.Size(40, 75);
            this.imageLoggingOn.TabIndex = 17;
            this.imageLoggingOn.TabStop = false;
            this.imageLoggingOn.Visible = false;
            this.imageLoggingOn.Click += new System.EventHandler(this.imageLogginOn_Click);
            this.imageLoggingOn.MouseEnter += new System.EventHandler(this.obj_MouseEnter);
            this.imageLoggingOn.MouseLeave += new System.EventHandler(this.obj_MouseLeave);
            // 
            // imageLoggingOff
            // 
            this.imageLoggingOff.BackColor = System.Drawing.Color.Transparent;
            this.imageLoggingOff.Image = ((System.Drawing.Image)(resources.GetObject("imageLoggingOff.Image")));
            this.imageLoggingOff.InitialImage = null;
            this.imageLoggingOff.Location = new System.Drawing.Point(1449, 0);
            this.imageLoggingOff.Name = "imageLoggingOff";
            this.imageLoggingOff.Size = new System.Drawing.Size(40, 75);
            this.imageLoggingOff.TabIndex = 18;
            this.imageLoggingOff.TabStop = false;
            this.imageLoggingOff.Click += new System.EventHandler(this.imageLogginOff_Click);
            this.imageLoggingOff.MouseEnter += new System.EventHandler(this.obj_MouseEnter);
            this.imageLoggingOff.MouseLeave += new System.EventHandler(this.obj_MouseLeave);
            // 
            // panelExit
            // 
            this.panelExit.BackColor = System.Drawing.Color.Transparent;
            this.panelExit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelExit.BackgroundImage")));
            this.panelExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panelExit.Location = new System.Drawing.Point(1886, 15);
            this.panelExit.Name = "panelExit";
            this.panelExit.Size = new System.Drawing.Size(29, 32);
            this.panelExit.TabIndex = 11;
            this.panelExit.Tag = "end";
            this.panelExit.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panelExit_MouseClick);
            this.panelExit.MouseEnter += new System.EventHandler(this.obj_MouseEnter);
            this.panelExit.MouseLeave += new System.EventHandler(this.obj_MouseLeave);
            // 
            // buttonSetting
            // 
            this.buttonSetting.BackColor = System.Drawing.Color.Transparent;
            this.buttonSetting.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonSetting.BackgroundImage")));
            this.buttonSetting.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonSetting.Location = new System.Drawing.Point(1060, 21);
            this.buttonSetting.Name = "buttonSetting";
            this.buttonSetting.Size = new System.Drawing.Size(41, 43);
            this.buttonSetting.TabIndex = 0;
            this.buttonSetting.Tag = "setting";
            this.buttonSetting.MouseClick += new System.Windows.Forms.MouseEventHandler(this.buttonSetting_MouseClick);
            this.buttonSetting.MouseEnter += new System.EventHandler(this.obj_MouseEnter);
            this.buttonSetting.MouseLeave += new System.EventHandler(this.obj_MouseLeave);
            // 
            // panelForm
            // 
            this.panelForm.Controls.Add(this.labelGraphCalc);
            this.panelForm.Controls.Add(this.labelAlarmHKnock);
            this.panelForm.Controls.Add(this.labelAlarmLKnock);
            this.panelForm.Controls.Add(this.labelAlarmMisfire);
            this.panelForm.Controls.Add(this.labelAlarmFault);
            this.panelForm.Controls.Add(this.labelCalcDataKnockIntst);
            this.panelForm.Controls.Add(this.labelCalcDataPegging);
            this.panelForm.Controls.Add(this.labelCalcDataMisfire);
            this.panelForm.Controls.Add(this.labelCalcDataHR);
            this.panelForm.Controls.Add(this.labelCalcDataIMEP);
            this.panelForm.Controls.Add(this.panelSetting);
            this.panelForm.Controls.Add(this.TabEA);
            this.panelForm.Controls.Add(this.comboY);
            this.panelForm.Controls.Add(this.panelGraph);
            this.panelForm.Controls.Add(this.panelRPM);
            this.panelForm.Controls.Add(this.statusStrip1);
            this.panelForm.Controls.Add(this.panelGraphCalc);
            this.panelForm.Controls.Add(this.panel1);
            this.panelForm.Controls.Add(this.statusStrip2);
            this.panelForm.Controls.Add(this.PanelDisconnect);
            this.panelForm.Controls.Add(this.labelCalcData);
            this.panelForm.Controls.Add(this.labelAlarm);
            this.panelForm.Controls.Add(this.panelAlarm);
            this.panelForm.Controls.Add(this.labelRPMTitle);
            this.panelForm.Controls.Add(this.panelCalcData);
            this.panelForm.Controls.Add(this.labelMenuSelected);
            this.panelForm.Controls.Add(this.labelCalcDataPmax);
            this.panelForm.Controls.Add(this.labelBottomMsg);
            this.panelForm.Controls.Add(this.labelAlarmSys);
            this.panelForm.Controls.Add(this.labelGraph);
            this.panelForm.Location = new System.Drawing.Point(0, 0);
            this.panelForm.Name = "panelForm";
            this.panelForm.Size = new System.Drawing.Size(1920, 1080);
            this.panelForm.TabIndex = 42;
            // 
            // panelMenuCal
            // 
            this.panelMenuCal.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelMenuCal.BackgroundImage")));
            this.panelMenuCal.Controls.Add(this.panelMenuCylKnock);
            this.panelMenuCal.Controls.Add(this.panelMenuCylTDC);
            this.panelMenuCal.Location = new System.Drawing.Point(380, 75);
            this.panelMenuCal.Name = "panelMenuCal";
            this.panelMenuCal.Size = new System.Drawing.Size(210, 80);
            this.panelMenuCal.TabIndex = 43;
            this.panelMenuCal.Visible = false;
            // 
            // panelMenuCylKnock
            // 
            this.panelMenuCylKnock.BackColor = System.Drawing.Color.Transparent;
            this.panelMenuCylKnock.Controls.Add(this.imgCylKnock);
            this.panelMenuCylKnock.Location = new System.Drawing.Point(0, 40);
            this.panelMenuCylKnock.Name = "panelMenuCylKnock";
            this.panelMenuCylKnock.Size = new System.Drawing.Size(210, 40);
            this.panelMenuCylKnock.TabIndex = 1;
            this.panelMenuCylKnock.Click += new System.EventHandler(this.panelMenuCylKnock_Click);
            this.panelMenuCylKnock.MouseEnter += new System.EventHandler(this.panelMenuCylKnock_MouseEnter);
            this.panelMenuCylKnock.MouseLeave += new System.EventHandler(this.panelMenuCylKnock_MouseLeave);
            // 
            // imgCylKnock
            // 
            this.imgCylKnock.BackColor = System.Drawing.Color.Transparent;
            this.imgCylKnock.Image = ((System.Drawing.Image)(resources.GetObject("imgCylKnock.Image")));
            this.imgCylKnock.Location = new System.Drawing.Point(108, 6);
            this.imgCylKnock.Name = "imgCylKnock";
            this.imgCylKnock.Size = new System.Drawing.Size(71, 29);
            this.imgCylKnock.TabIndex = 25;
            this.imgCylKnock.TabStop = false;
            this.imgCylKnock.Visible = false;
            // 
            // panelMenuCylTDC
            // 
            this.panelMenuCylTDC.BackColor = System.Drawing.Color.Transparent;
            this.panelMenuCylTDC.Controls.Add(this.imgCylTDC);
            this.panelMenuCylTDC.Location = new System.Drawing.Point(0, 0);
            this.panelMenuCylTDC.Name = "panelMenuCylTDC";
            this.panelMenuCylTDC.Size = new System.Drawing.Size(210, 40);
            this.panelMenuCylTDC.TabIndex = 0;
            this.panelMenuCylTDC.Click += new System.EventHandler(this.panelMenuCylTDC_Click);
            this.panelMenuCylTDC.MouseEnter += new System.EventHandler(this.panelMenuCylTDC_MouseEnter);
            this.panelMenuCylTDC.MouseLeave += new System.EventHandler(this.panelMenuCylTDC_MouseLeave);
            // 
            // imgCylTDC
            // 
            this.imgCylTDC.BackColor = System.Drawing.Color.Transparent;
            this.imgCylTDC.Image = ((System.Drawing.Image)(resources.GetObject("imgCylTDC.Image")));
            this.imgCylTDC.Location = new System.Drawing.Point(70, 6);
            this.imgCylTDC.Name = "imgCylTDC";
            this.imgCylTDC.Size = new System.Drawing.Size(71, 29);
            this.imgCylTDC.TabIndex = 26;
            this.imgCylTDC.TabStop = false;
            this.imgCylTDC.Visible = false;
            // 
            // TFormEA
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(230)))), ((int)(((byte)(232)))));
            this.ClientSize = new System.Drawing.Size(1916, 1045);
            this.Controls.Add(this.panelMenuCal);
            this.Controls.Add(this.panelTop);
            this.Controls.Add(this.panelForm);
            this.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.ForeColor = System.Drawing.Color.DimGray;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "TFormEA";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Engine Analyzer 2.0.0";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormMain_FormClosed);
            this.Load += new System.EventHandler(this.FormEA_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.TFormEA_Paint);
            this.TabEA.ResumeLayout(false);
            this.TabGraph.ResumeLayout(false);
            this.TabControlGraph.ResumeLayout(false);
            this.TabCycle1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.tabKnockIntensity.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartCyl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartKnock)).EndInit();
            this.panel10.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.PanelData.ResumeLayout(false);
            this.tabSetting.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EditPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EditFrequency)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.statusStrip2.ResumeLayout(false);
            this.statusStrip2.PerformLayout();
            this.PanelDisconnect.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imageConOff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageConOn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartPMax)).EndInit();
            this.panelGraph.ResumeLayout(false);
            this.panelGraphRight.ResumeLayout(false);
            this.panelGraphRight.PerformLayout();
            this.panelGraphCalcLogginData.ResumeLayout(false);
            this.panelGraphCalcLogginData.PerformLayout();
            this.panelGraphCalc.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._chartCalcData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._chartCalcDataRPM)).EndInit();
            this.panelCalcData.ResumeLayout(false);
            this.panelCalcData.PerformLayout();
            this.panelRPM.ResumeLayout(false);
            this.panelSetting.ResumeLayout(false);
            this.panelTop.ResumeLayout(false);
            this.panelMenuDignosis.ResumeLayout(false);
            this.panelMenuDignosis.PerformLayout();
            this.panelMenuCalibration.ResumeLayout(false);
            this.panelMenuCalibration.PerformLayout();
            this.panelMonitoring.ResumeLayout(false);
            this.panelMonitoring.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageLoggingOn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageLoggingOff)).EndInit();
            this.panelForm.ResumeLayout(false);
            this.panelForm.PerformLayout();
            this.panelMenuCal.ResumeLayout(false);
            this.panelMenuCylKnock.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imgCylKnock)).EndInit();
            this.panelMenuCylTDC.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imgCylTDC)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl TabEA;
        private System.Windows.Forms.TabPage tabSetting;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox EditIP;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox EditPort;
        private System.Windows.Forms.ToolStripStatusLabel LabelConnection;
        private System.Windows.Forms.ToolStripStatusLabel LabelDPS;
        private System.Windows.Forms.Button ButtonConnect;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label EditStatus;
        private System.Windows.Forms.Button ButtonStart;
        private System.Windows.Forms.Button ButtonStop;
        private System.Windows.Forms.Button ButtonFolderSelect;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.FolderBrowserDialog DialogFolder;
        private System.Windows.Forms.OpenFileDialog DialogOpen;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox EditXCount;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel4;
        private System.Windows.Forms.ToolStripStatusLabel LabelExport;
        private System.Windows.Forms.Label labelymax;
        private System.Windows.Forms.Label labelymin;
        private System.Windows.Forms.Timer TimerTick;
        private System.Windows.Forms.Button ButtonDisconnect;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button ButtonEADisconnect;
        private System.Windows.Forms.Button ButtonEAConnect;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ToolStripStatusLabel LabelEALive;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel LabelReq;
        private System.Windows.Forms.TextBox EditReqTarget;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel5;
        private System.Windows.Forms.ToolStripStatusLabel LabelCHGetCnt;
        private System.Windows.Forms.Button ButtonReq;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel6;
        private System.Windows.Forms.ToolStripStatusLabel LabelCycle;
        private System.Windows.Forms.StatusStrip statusStrip2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel7;
        private System.Windows.Forms.ToolStripStatusLabel LabelRawQueue;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel8;
        private System.Windows.Forms.ToolStripStatusLabel LabelSaveQueue;
        private System.Windows.Forms.TextBox EditDelayForRecv;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button ButtonDelaySet;
        private System.Windows.Forms.CheckBox CheckYAuto;
        private System.Windows.Forms.TabPage TabHistory;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel9;
        private System.Windows.Forms.ToolStripStatusLabel LabelCSC;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel10;
        private System.Windows.Forms.ToolStripStatusLabel LabelCSPS;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel11;
        private System.Windows.Forms.ToolStripStatusLabel LabelCPS;
        private System.Windows.Forms.CheckBox CheckGraphSkip;
        private System.Windows.Forms.TabPage TabGraph;
        private System.Windows.Forms.TabControl TabControlGraph;
        private System.Windows.Forms.TabPage TabCycle1;
        private System.Windows.Forms.CheckBox CheckRT1;
        private System.Windows.Forms.Splitter splitter4;
        private System.Windows.Forms.Splitter splitter3;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button ButtonYAxis;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.CheckedListBox CheckListCH;
        private System.Windows.Forms.CheckBox CheckCHAllVIsible;
        private System.Windows.Forms.Panel PanelData;
        private System.Windows.Forms.Label label8;
        private Arction.WinForms.Charting.LightningChartUltimate _chart;
        private System.Windows.Forms.Button ButtonConnectToggle;
        private System.Windows.Forms.TextBox EditCycleCount;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button ButtonCycleCountSet;
        private System.Windows.Forms.Button ButtonYAxisSet;
        private System.Windows.Forms.Splitter splitter5;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.NumericUpDown EditFrequency;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.NumericUpDown EditPeriod;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button ButtonCHMaxSet;
        private System.Windows.Forms.TextBox EditCHMax;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Panel PanelDisconnect;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Button buttonCHSet;
        private System.Windows.Forms.CheckBox checkLog;
        private System.Windows.Forms.Button buttonDataTypeSet;
        private System.Windows.Forms.ComboBox comboDataType;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TabPage tabKnockIntensity;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartCyl;
        private System.Windows.Forms.Splitter splitter7;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartKnock;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboKnockCH;
        private System.Windows.Forms.PictureBox imageConOff;
        private System.Windows.Forms.PictureBox imageConOn;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartPMax;
        private System.Windows.Forms.Panel panelGraph;
        private System.Windows.Forms.Label labelAlarmSys;
        private System.Windows.Forms.Panel panelAlarm;
        private System.Windows.Forms.Label labelAlarmHKnock;
        private System.Windows.Forms.Label labelAlarmLKnock;
        private System.Windows.Forms.Label labelAlarmMisfire;
        private System.Windows.Forms.Label labelAlarmFault;
        private System.Windows.Forms.Panel panelCalcData;
        private System.Windows.Forms.Label labelCalcDataKnockIntst;
        private System.Windows.Forms.Label labelCalcDataPegging;
        private System.Windows.Forms.Label labelCalcDataMisfire;
        private System.Windows.Forms.Label labelCalcDataHR;
        private System.Windows.Forms.Label labelCalcDataIMEP;
        private System.Windows.Forms.Label labelCalcDataPmax;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label labelCalcDataAvg1;
        private System.Windows.Forms.Label labelCalcDataAvg;
        private System.Windows.Forms.Label labelGraph;
        private System.Windows.Forms.Label labelGraphCalc;
        private System.Windows.Forms.Panel panelGraphRight;
        private System.Windows.Forms.Panel buttonGraphPAxisReset;
        private System.Windows.Forms.TextBox editGraphPYMax;
        private System.Windows.Forms.TextBox editGraphPYMin;
        private System.Windows.Forms.Label labelBottomMsg;
        private System.Windows.Forms.Label labelMenuSelected;
        private System.Windows.Forms.Label labelRPMTitle;
        private System.Windows.Forms.Label labelAlarm;
        private System.Windows.Forms.Label labelCalcData;
        private System.Windows.Forms.Panel panelRPM;
        private System.Windows.Forms.Label labelRPM;
        private System.Windows.Forms.Panel panelGraphCalcDataBottom;
        private System.Windows.Forms.Panel panelSetting;
        private System.Windows.Forms.Button buttonSettingClose;
        private System.Windows.Forms.Panel panelTop;
        private System.Windows.Forms.Panel buttonSetting;
        private System.Windows.Forms.Panel panelGraphCalcLogginData;
        private System.Windows.Forms.Panel panelCalcDataYReset;
        private System.Windows.Forms.TextBox editCalcDataYMax;
        private System.Windows.Forms.TextBox editCalcDataYMin;
        private System.Windows.Forms.Panel panelCalcDataRPMYReset;
        private System.Windows.Forms.TextBox editCalcDataRPMYMax;
        private System.Windows.Forms.TextBox editCalcDataRPMYMin;
        private System.Windows.Forms.Panel panelExit;
        private System.Windows.Forms.Panel panelGraphCalc;
        private System.Windows.Forms.DataVisualization.Charting.Chart _chartCalcDataRPM;
        private System.Windows.Forms.DataVisualization.Charting.Chart _chartCalcData;
        private System.Windows.Forms.Panel panelMonitoring;
        private System.Windows.Forms.PictureBox imageLoggingOff;
        private System.Windows.Forms.PictureBox imageLoggingOn;
        private System.Windows.Forms.Panel comboCalcDataRPMYMin;
        private System.Windows.Forms.Panel comboCalcDataYMax;
        private System.Windows.Forms.Panel comboCalcDataYMin;
        private System.Windows.Forms.Panel comboCalcDataRPMMax;
        private System.Windows.Forms.Panel comboYMax;
        private System.Windows.Forms.Panel comboYMin;
        private System.Windows.Forms.ListBox comboY;
        private System.Windows.Forms.Panel panelMenuCalibration;
        private System.Windows.Forms.Panel panelForm;
        private System.Windows.Forms.Panel buttonLogout;
        private System.Windows.Forms.Panel buttonUser;
        private System.Windows.Forms.Panel panelMenuCal;
        private System.Windows.Forms.Panel panelMenuCylTDC;
        private System.Windows.Forms.Panel panelMenuCylKnock;
        private System.Windows.Forms.PictureBox imgCylKnock;
        private System.Windows.Forms.PictureBox imgCylTDC;
        private System.Windows.Forms.Panel panelMenuDignosis;
        private System.Windows.Forms.Label labelMenuMonitoring;
        private System.Windows.Forms.PictureBox imgBack;
        private System.Windows.Forms.Label labelMenuDiagnosis;
        private System.Windows.Forms.Label labelMenuCal;
    }
}
﻿namespace EngineAnalyzer
{
    partial class TFormSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TFormSetting));
            this.panelForm = new System.Windows.Forms.Panel();
            this.labelBottomMsg = new System.Windows.Forms.Label();
            this.labelEPTitle = new System.Windows.Forms.Label();
            this.labelEKP = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panelGridScroll = new System.Windows.Forms.Panel();
            this.panelGridNo = new System.Windows.Forms.Panel();
            this.buttonNextx2 = new System.Windows.Forms.Panel();
            this.buttonNext = new System.Windows.Forms.Panel();
            this.buttonLast = new System.Windows.Forms.Panel();
            this.buttonPrev2x = new System.Windows.Forms.Panel();
            this.buttonFirst = new System.Windows.Forms.Panel();
            this.buttonPrev = new System.Windows.Forms.Panel();
            this.buttonGridScrollLeft = new System.Windows.Forms.Panel();
            this.buttonEditor = new System.Windows.Forms.Panel();
            this.buttonGridScrollRight = new System.Windows.Forms.Panel();
            this.buttonGridScrollDown = new System.Windows.Forms.Panel();
            this.buttonGridScrollUp = new System.Windows.Forms.Panel();
            this.buttonSaveSelect = new System.Windows.Forms.Panel();
            this.panelExportBack = new System.Windows.Forms.Panel();
            this.labelECP = new System.Windows.Forms.Label();
            this.labelEDP = new System.Windows.Forms.Label();
            this.panelGridContent = new System.Windows.Forms.Panel();
            this.editGrid = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.c0 = new System.Windows.Forms.Panel();
            this.c0r1 = new System.Windows.Forms.Panel();
            this.c1 = new System.Windows.Forms.Panel();
            this.c13 = new System.Windows.Forms.Panel();
            this.c2 = new System.Windows.Forms.Panel();
            this.c12 = new System.Windows.Forms.Panel();
            this.c3 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.c11 = new System.Windows.Forms.Panel();
            this.c4 = new System.Windows.Forms.Panel();
            this.c10 = new System.Windows.Forms.Panel();
            this.c5 = new System.Windows.Forms.Panel();
            this.c9 = new System.Windows.Forms.Panel();
            this.c6 = new System.Windows.Forms.Panel();
            this.c8 = new System.Windows.Forms.Panel();
            this.c7 = new System.Windows.Forms.Panel();
            this.editFolder = new System.Windows.Forms.TextBox();
            this.editSaveInterval = new System.Windows.Forms.TextBox();
            this.checkSave = new System.Windows.Forms.PictureBox();
            this.checkOn = new System.Windows.Forms.PictureBox();
            this.editTO = new System.Windows.Forms.TextBox();
            this.editPort = new System.Windows.Forms.TextBox();
            this.editHost = new System.Windows.Forms.TextBox();
            this.imgBack = new System.Windows.Forms.PictureBox();
            this.editTemp = new System.Windows.Forms.TextBox();
            this._chart = new Arction.WinForms.Charting.LightningChartUltimate();
            this.panelGraphRight = new System.Windows.Forms.Panel();
            this.comboYMax = new System.Windows.Forms.Panel();
            this.comboYMin = new System.Windows.Forms.Panel();
            this.editGraphPYMax = new System.Windows.Forms.TextBox();
            this.editGraphPYMin = new System.Windows.Forms.TextBox();
            this.buttonGraphPAxisReset = new System.Windows.Forms.Panel();
            this.imgCyl12 = new System.Windows.Forms.PictureBox();
            this.imgCyl11 = new System.Windows.Forms.PictureBox();
            this.imgCyl10 = new System.Windows.Forms.PictureBox();
            this.imgCyl1 = new System.Windows.Forms.PictureBox();
            this.imgCyl9 = new System.Windows.Forms.PictureBox();
            this.imgCyl2 = new System.Windows.Forms.PictureBox();
            this.imgCyl8 = new System.Windows.Forms.PictureBox();
            this.imgCyl3 = new System.Windows.Forms.PictureBox();
            this.imgCyl7 = new System.Windows.Forms.PictureBox();
            this.imgCyl4 = new System.Windows.Forms.PictureBox();
            this.imgCyl6 = new System.Windows.Forms.PictureBox();
            this.imgCyl5 = new System.Windows.Forms.PictureBox();
            this.DialogFolder = new System.Windows.Forms.FolderBrowserDialog();
            this.timer100ms = new System.Windows.Forms.Timer(this.components);
            this.buttonSaveSelect2 = new System.Windows.Forms.Panel();
            this.buttonApply = new System.Windows.Forms.Panel();
            this.buttonImport = new System.Windows.Forms.Panel();
            this.buttonExport = new System.Windows.Forms.Panel();
            this.panelForm.SuspendLayout();
            this.buttonSaveSelect.SuspendLayout();
            this.panelExportBack.SuspendLayout();
            this.panelGridContent.SuspendLayout();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkOn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgBack)).BeginInit();
            this.panelGraphRight.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl5)).BeginInit();
            this.SuspendLayout();
            // 
            // panelForm
            // 
            this.panelForm.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panelForm.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelForm.BackgroundImage")));
            this.panelForm.Controls.Add(this.buttonApply);
            this.panelForm.Controls.Add(this.labelBottomMsg);
            this.panelForm.Controls.Add(this.labelEPTitle);
            this.panelForm.Controls.Add(this.labelEKP);
            this.panelForm.Controls.Add(this.label1);
            this.panelForm.Controls.Add(this.panelGridScroll);
            this.panelForm.Controls.Add(this.panelGridNo);
            this.panelForm.Controls.Add(this.buttonNextx2);
            this.panelForm.Controls.Add(this.buttonNext);
            this.panelForm.Controls.Add(this.buttonLast);
            this.panelForm.Controls.Add(this.buttonPrev2x);
            this.panelForm.Controls.Add(this.buttonFirst);
            this.panelForm.Controls.Add(this.buttonPrev);
            this.panelForm.Controls.Add(this.buttonGridScrollLeft);
            this.panelForm.Controls.Add(this.buttonEditor);
            this.panelForm.Controls.Add(this.buttonGridScrollRight);
            this.panelForm.Controls.Add(this.buttonGridScrollDown);
            this.panelForm.Controls.Add(this.buttonGridScrollUp);
            this.panelForm.Controls.Add(this.buttonSaveSelect);
            this.panelForm.Controls.Add(this.panelExportBack);
            this.panelForm.Controls.Add(this.labelECP);
            this.panelForm.Controls.Add(this.labelEDP);
            this.panelForm.Controls.Add(this.panelGridContent);
            this.panelForm.Controls.Add(this.editFolder);
            this.panelForm.Controls.Add(this.editSaveInterval);
            this.panelForm.Controls.Add(this.checkSave);
            this.panelForm.Controls.Add(this.checkOn);
            this.panelForm.Controls.Add(this.editTO);
            this.panelForm.Controls.Add(this.editPort);
            this.panelForm.Controls.Add(this.editHost);
            this.panelForm.Controls.Add(this.imgBack);
            this.panelForm.Controls.Add(this.editTemp);
            this.panelForm.Location = new System.Drawing.Point(0, 0);
            this.panelForm.Name = "panelForm";
            this.panelForm.Size = new System.Drawing.Size(1920, 1005);
            this.panelForm.TabIndex = 2;
            this.panelForm.Visible = false;
            this.panelForm.Click += new System.EventHandler(this.panelForm_Click);
            // 
            // labelBottomMsg
            // 
            this.labelBottomMsg.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(230)))), ((int)(((byte)(232)))));
            this.labelBottomMsg.Font = new System.Drawing.Font("맑은 고딕", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelBottomMsg.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(63)))), ((int)(((byte)(36)))));
            this.labelBottomMsg.Location = new System.Drawing.Point(77, 933);
            this.labelBottomMsg.Name = "labelBottomMsg";
            this.labelBottomMsg.Size = new System.Drawing.Size(1707, 21);
            this.labelBottomMsg.TabIndex = 95;
            // 
            // labelEPTitle
            // 
            this.labelEPTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(243)))), ((int)(((byte)(244)))));
            this.labelEPTitle.Font = new System.Drawing.Font("맑은 고딕", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelEPTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(73)))), ((int)(((byte)(75)))), ((int)(((byte)(76)))));
            this.labelEPTitle.Location = new System.Drawing.Point(116, 161);
            this.labelEPTitle.Name = "labelEPTitle";
            this.labelEPTitle.Size = new System.Drawing.Size(177, 21);
            this.labelEPTitle.TabIndex = 94;
            this.labelEPTitle.Text = "Engine Parameter";
            // 
            // labelEKP
            // 
            this.labelEKP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(198)))), ((int)(((byte)(201)))));
            this.labelEKP.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.labelEKP.ForeColor = System.Drawing.Color.White;
            this.labelEKP.Location = new System.Drawing.Point(561, 194);
            this.labelEKP.Name = "labelEKP";
            this.labelEKP.Size = new System.Drawing.Size(280, 39);
            this.labelEKP.TabIndex = 93;
            this.labelEKP.Text = "Engine Knock Control Parameter";
            this.labelEKP.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1814, 221);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 12);
            this.label1.TabIndex = 92;
            this.label1.Text = "1786,254,825";
            this.label1.Visible = false;
            // 
            // panelGridScroll
            // 
            this.panelGridScroll.BackColor = System.Drawing.Color.Transparent;
            this.panelGridScroll.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelGridScroll.BackgroundImage")));
            this.panelGridScroll.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panelGridScroll.Location = new System.Drawing.Point(1784, 825);
            this.panelGridScroll.Name = "panelGridScroll";
            this.panelGridScroll.Size = new System.Drawing.Size(13, 23);
            this.panelGridScroll.TabIndex = 91;
            this.panelGridScroll.Tag = "";
            // 
            // panelGridNo
            // 
            this.panelGridNo.BackColor = System.Drawing.Color.Transparent;
            this.panelGridNo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panelGridNo.Location = new System.Drawing.Point(177, 869);
            this.panelGridNo.Name = "panelGridNo";
            this.panelGridNo.Size = new System.Drawing.Size(201, 23);
            this.panelGridNo.TabIndex = 90;
            this.panelGridNo.Tag = "";
            this.panelGridNo.Paint += new System.Windows.Forms.PaintEventHandler(this.panelGridNo_Paint);
            // 
            // buttonNextx2
            // 
            this.buttonNextx2.BackColor = System.Drawing.Color.Transparent;
            this.buttonNextx2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonNextx2.Location = new System.Drawing.Point(396, 869);
            this.buttonNextx2.Name = "buttonNextx2";
            this.buttonNextx2.Size = new System.Drawing.Size(20, 23);
            this.buttonNextx2.TabIndex = 89;
            this.buttonNextx2.Tag = "";
            this.buttonNextx2.Click += new System.EventHandler(this.buttonNextx2_Click);
            // 
            // buttonNext
            // 
            this.buttonNext.BackColor = System.Drawing.Color.Transparent;
            this.buttonNext.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonNext.Location = new System.Drawing.Point(377, 869);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(20, 23);
            this.buttonNext.TabIndex = 88;
            this.buttonNext.Tag = "";
            this.buttonNext.Click += new System.EventHandler(this.buttonNext_Click);
            // 
            // buttonLast
            // 
            this.buttonLast.BackColor = System.Drawing.Color.Transparent;
            this.buttonLast.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonLast.Location = new System.Drawing.Point(415, 869);
            this.buttonLast.Name = "buttonLast";
            this.buttonLast.Size = new System.Drawing.Size(20, 23);
            this.buttonLast.TabIndex = 87;
            this.buttonLast.Tag = "";
            this.buttonLast.Click += new System.EventHandler(this.buttonLast_Click);
            // 
            // buttonPrev2x
            // 
            this.buttonPrev2x.BackColor = System.Drawing.Color.Transparent;
            this.buttonPrev2x.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonPrev2x.Location = new System.Drawing.Point(139, 869);
            this.buttonPrev2x.Name = "buttonPrev2x";
            this.buttonPrev2x.Size = new System.Drawing.Size(20, 23);
            this.buttonPrev2x.TabIndex = 86;
            this.buttonPrev2x.Tag = "";
            this.buttonPrev2x.Click += new System.EventHandler(this.buttonPrev2x_Click);
            // 
            // buttonFirst
            // 
            this.buttonFirst.BackColor = System.Drawing.Color.Transparent;
            this.buttonFirst.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonFirst.Location = new System.Drawing.Point(120, 869);
            this.buttonFirst.Name = "buttonFirst";
            this.buttonFirst.Size = new System.Drawing.Size(20, 23);
            this.buttonFirst.TabIndex = 86;
            this.buttonFirst.Tag = "";
            this.buttonFirst.Click += new System.EventHandler(this.buttonFirst_Click);
            // 
            // buttonPrev
            // 
            this.buttonPrev.BackColor = System.Drawing.Color.Transparent;
            this.buttonPrev.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonPrev.Location = new System.Drawing.Point(158, 869);
            this.buttonPrev.Name = "buttonPrev";
            this.buttonPrev.Size = new System.Drawing.Size(20, 23);
            this.buttonPrev.TabIndex = 85;
            this.buttonPrev.Tag = "";
            this.buttonPrev.Click += new System.EventHandler(this.buttonPrev_Click);
            // 
            // buttonGridScrollLeft
            // 
            this.buttonGridScrollLeft.BackColor = System.Drawing.Color.Transparent;
            this.buttonGridScrollLeft.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonGridScrollLeft.Location = new System.Drawing.Point(528, 869);
            this.buttonGridScrollLeft.Name = "buttonGridScrollLeft";
            this.buttonGridScrollLeft.Size = new System.Drawing.Size(26, 23);
            this.buttonGridScrollLeft.TabIndex = 84;
            this.buttonGridScrollLeft.Tag = "";
            // 
            // buttonEditor
            // 
            this.buttonEditor.BackColor = System.Drawing.Color.Transparent;
            this.buttonEditor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonEditor.Location = new System.Drawing.Point(434, 869);
            this.buttonEditor.Name = "buttonEditor";
            this.buttonEditor.Size = new System.Drawing.Size(95, 23);
            this.buttonEditor.TabIndex = 83;
            this.buttonEditor.Tag = "";
            // 
            // buttonGridScrollRight
            // 
            this.buttonGridScrollRight.BackColor = System.Drawing.Color.Transparent;
            this.buttonGridScrollRight.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonGridScrollRight.Location = new System.Drawing.Point(1756, 868);
            this.buttonGridScrollRight.Name = "buttonGridScrollRight";
            this.buttonGridScrollRight.Size = new System.Drawing.Size(26, 23);
            this.buttonGridScrollRight.TabIndex = 82;
            this.buttonGridScrollRight.Tag = "";
            // 
            // buttonGridScrollDown
            // 
            this.buttonGridScrollDown.BackColor = System.Drawing.Color.Transparent;
            this.buttonGridScrollDown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonGridScrollDown.Location = new System.Drawing.Point(1779, 849);
            this.buttonGridScrollDown.Name = "buttonGridScrollDown";
            this.buttonGridScrollDown.Size = new System.Drawing.Size(26, 23);
            this.buttonGridScrollDown.TabIndex = 81;
            this.buttonGridScrollDown.Tag = "";
            this.buttonGridScrollDown.Click += new System.EventHandler(this.buttonGridScrollDown_Click);
            // 
            // buttonGridScrollUp
            // 
            this.buttonGridScrollUp.BackColor = System.Drawing.Color.Transparent;
            this.buttonGridScrollUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonGridScrollUp.Location = new System.Drawing.Point(1777, 233);
            this.buttonGridScrollUp.Name = "buttonGridScrollUp";
            this.buttonGridScrollUp.Size = new System.Drawing.Size(26, 23);
            this.buttonGridScrollUp.TabIndex = 80;
            this.buttonGridScrollUp.Tag = "";
            this.buttonGridScrollUp.Click += new System.EventHandler(this.buttonGridScrollUp_Click);
            // 
            // buttonSaveSelect
            // 
            this.buttonSaveSelect.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(230)))), ((int)(((byte)(232)))));
            this.buttonSaveSelect.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonSaveSelect.Controls.Add(this.buttonSaveSelect2);
            this.buttonSaveSelect.Location = new System.Drawing.Point(1214, 77);
            this.buttonSaveSelect.Name = "buttonSaveSelect";
            this.buttonSaveSelect.Size = new System.Drawing.Size(41, 43);
            this.buttonSaveSelect.TabIndex = 79;
            this.buttonSaveSelect.Tag = "";
            // 
            // panelExportBack
            // 
            this.panelExportBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(243)))), ((int)(((byte)(244)))));
            this.panelExportBack.Controls.Add(this.buttonExport);
            this.panelExportBack.Controls.Add(this.buttonImport);
            this.panelExportBack.Location = new System.Drawing.Point(1666, 181);
            this.panelExportBack.Name = "panelExportBack";
            this.panelExportBack.Size = new System.Drawing.Size(142, 52);
            this.panelExportBack.TabIndex = 78;
            // 
            // labelECP
            // 
            this.labelECP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(198)))), ((int)(((byte)(201)))));
            this.labelECP.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.labelECP.ForeColor = System.Drawing.Color.White;
            this.labelECP.Location = new System.Drawing.Point(341, 194);
            this.labelECP.Name = "labelECP";
            this.labelECP.Size = new System.Drawing.Size(220, 39);
            this.labelECP.TabIndex = 77;
            this.labelECP.Text = "Engine Control Parameter";
            this.labelECP.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelEDP
            // 
            this.labelEDP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.labelEDP.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.labelEDP.ForeColor = System.Drawing.Color.White;
            this.labelEDP.Location = new System.Drawing.Point(121, 194);
            this.labelEDP.Name = "labelEDP";
            this.labelEDP.Size = new System.Drawing.Size(220, 39);
            this.labelEDP.TabIndex = 76;
            this.labelEDP.Text = "Engine Definition Parameter";
            this.labelEDP.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelGridContent
            // 
            this.panelGridContent.BackColor = System.Drawing.Color.Transparent;
            this.panelGridContent.Controls.Add(this.editGrid);
            this.panelGridContent.Controls.Add(this.panel1);
            this.panelGridContent.Controls.Add(this.panel2);
            this.panelGridContent.Controls.Add(this.c0);
            this.panelGridContent.Controls.Add(this.c0r1);
            this.panelGridContent.Controls.Add(this.c1);
            this.panelGridContent.Controls.Add(this.c13);
            this.panelGridContent.Controls.Add(this.c2);
            this.panelGridContent.Controls.Add(this.c12);
            this.panelGridContent.Controls.Add(this.c3);
            this.panelGridContent.Controls.Add(this.panel11);
            this.panelGridContent.Controls.Add(this.c4);
            this.panelGridContent.Controls.Add(this.c10);
            this.panelGridContent.Controls.Add(this.c5);
            this.panelGridContent.Controls.Add(this.c9);
            this.panelGridContent.Controls.Add(this.c6);
            this.panelGridContent.Controls.Add(this.c8);
            this.panelGridContent.Controls.Add(this.c7);
            this.panelGridContent.Location = new System.Drawing.Point(120, 276);
            this.panelGridContent.Name = "panelGridContent";
            this.panelGridContent.Size = new System.Drawing.Size(1660, 593);
            this.panelGridContent.TabIndex = 75;
            // 
            // editGrid
            // 
            this.editGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.editGrid.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.editGrid.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(73)))), ((int)(((byte)(75)))), ((int)(((byte)(60)))));
            this.editGrid.Location = new System.Drawing.Point(495, 228);
            this.editGrid.Name = "editGrid";
            this.editGrid.Size = new System.Drawing.Size(114, 14);
            this.editGrid.TabIndex = 78;
            this.editGrid.Text = "0";
            this.editGrid.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.editGrid.Visible = false;
            this.editGrid.KeyDown += new System.Windows.Forms.KeyEventHandler(this.editGrid_KeyDown);
            this.editGrid.Leave += new System.EventHandler(this.editGrid_Leave);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(3, 65);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(34, 29);
            this.panel1.TabIndex = 75;
            this.panel1.Visible = false;
            // 
            // panel2
            // 
            this.panel2.Location = new System.Drawing.Point(3, 96);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(34, 29);
            this.panel2.TabIndex = 76;
            this.panel2.Visible = false;
            // 
            // c0
            // 
            this.c0.Location = new System.Drawing.Point(3, 3);
            this.c0.Name = "c0";
            this.c0.Size = new System.Drawing.Size(34, 29);
            this.c0.TabIndex = 61;
            this.c0.Visible = false;
            // 
            // c0r1
            // 
            this.c0r1.Location = new System.Drawing.Point(3, 34);
            this.c0r1.Name = "c0r1";
            this.c0r1.Size = new System.Drawing.Size(34, 29);
            this.c0r1.TabIndex = 74;
            this.c0r1.Visible = false;
            // 
            // c1
            // 
            this.c1.Location = new System.Drawing.Point(39, 3);
            this.c1.Name = "c1";
            this.c1.Size = new System.Drawing.Size(74, 29);
            this.c1.TabIndex = 62;
            this.c1.Visible = false;
            // 
            // c13
            // 
            this.c13.Location = new System.Drawing.Point(1560, 3);
            this.c13.Name = "c13";
            this.c13.Size = new System.Drawing.Size(100, 29);
            this.c13.TabIndex = 73;
            this.c13.Visible = false;
            // 
            // c2
            // 
            this.c2.Location = new System.Drawing.Point(115, 3);
            this.c2.Name = "c2";
            this.c2.Size = new System.Drawing.Size(76, 29);
            this.c2.TabIndex = 63;
            this.c2.Visible = false;
            // 
            // c12
            // 
            this.c12.Location = new System.Drawing.Point(1459, 3);
            this.c12.Name = "c12";
            this.c12.Size = new System.Drawing.Size(100, 29);
            this.c12.TabIndex = 72;
            this.c12.Visible = false;
            // 
            // c3
            // 
            this.c3.Location = new System.Drawing.Point(193, 3);
            this.c3.Name = "c3";
            this.c3.Size = new System.Drawing.Size(445, 29);
            this.c3.TabIndex = 64;
            this.c3.Visible = false;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.c11);
            this.panel11.Location = new System.Drawing.Point(1356, 3);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(101, 29);
            this.panel11.TabIndex = 71;
            this.panel11.Visible = false;
            // 
            // c11
            // 
            this.c11.Location = new System.Drawing.Point(0, 0);
            this.c11.Name = "c11";
            this.c11.Size = new System.Drawing.Size(100, 29);
            this.c11.TabIndex = 69;
            this.c11.Visible = false;
            // 
            // c4
            // 
            this.c4.Location = new System.Drawing.Point(640, 3);
            this.c4.Name = "c4";
            this.c4.Size = new System.Drawing.Size(101, 29);
            this.c4.TabIndex = 62;
            this.c4.Visible = false;
            // 
            // c10
            // 
            this.c10.Location = new System.Drawing.Point(1253, 3);
            this.c10.Name = "c10";
            this.c10.Size = new System.Drawing.Size(101, 29);
            this.c10.TabIndex = 70;
            this.c10.Visible = false;
            // 
            // c5
            // 
            this.c5.Location = new System.Drawing.Point(742, 3);
            this.c5.Name = "c5";
            this.c5.Size = new System.Drawing.Size(100, 29);
            this.c5.TabIndex = 65;
            this.c5.Visible = false;
            // 
            // c9
            // 
            this.c9.Location = new System.Drawing.Point(1151, 3);
            this.c9.Name = "c9";
            this.c9.Size = new System.Drawing.Size(101, 29);
            this.c9.TabIndex = 69;
            this.c9.Visible = false;
            // 
            // c6
            // 
            this.c6.Location = new System.Drawing.Point(844, 3);
            this.c6.Name = "c6";
            this.c6.Size = new System.Drawing.Size(101, 29);
            this.c6.TabIndex = 66;
            this.c6.Visible = false;
            // 
            // c8
            // 
            this.c8.Location = new System.Drawing.Point(1049, 3);
            this.c8.Name = "c8";
            this.c8.Size = new System.Drawing.Size(100, 29);
            this.c8.TabIndex = 68;
            this.c8.Visible = false;
            // 
            // c7
            // 
            this.c7.Location = new System.Drawing.Point(947, 3);
            this.c7.Name = "c7";
            this.c7.Size = new System.Drawing.Size(101, 29);
            this.c7.TabIndex = 67;
            this.c7.Visible = false;
            // 
            // editFolder
            // 
            this.editFolder.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.editFolder.Font = new System.Drawing.Font("굴림", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.editFolder.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.editFolder.Location = new System.Drawing.Point(906, 88);
            this.editFolder.Name = "editFolder";
            this.editFolder.Size = new System.Drawing.Size(296, 12);
            this.editFolder.TabIndex = 58;
            this.editFolder.Text = "0";
            this.editFolder.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // editSaveInterval
            // 
            this.editSaveInterval.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.editSaveInterval.Font = new System.Drawing.Font("굴림", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.editSaveInterval.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.editSaveInterval.Location = new System.Drawing.Point(801, 88);
            this.editSaveInterval.Name = "editSaveInterval";
            this.editSaveInterval.Size = new System.Drawing.Size(26, 12);
            this.editSaveInterval.TabIndex = 57;
            this.editSaveInterval.Text = "0";
            this.editSaveInterval.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // checkSave
            // 
            this.checkSave.BackColor = System.Drawing.Color.Transparent;
            this.checkSave.Image = ((System.Drawing.Image)(resources.GetObject("checkSave.Image")));
            this.checkSave.Location = new System.Drawing.Point(642, 23);
            this.checkSave.Name = "checkSave";
            this.checkSave.Size = new System.Drawing.Size(45, 29);
            this.checkSave.TabIndex = 55;
            this.checkSave.TabStop = false;
            this.checkSave.Visible = false;
            this.checkSave.Click += new System.EventHandler(this.checkSave_Click);
            // 
            // checkOn
            // 
            this.checkOn.BackColor = System.Drawing.Color.Transparent;
            this.checkOn.Image = ((System.Drawing.Image)(resources.GetObject("checkOn.Image")));
            this.checkOn.Location = new System.Drawing.Point(517, 23);
            this.checkOn.Name = "checkOn";
            this.checkOn.Size = new System.Drawing.Size(76, 29);
            this.checkOn.TabIndex = 56;
            this.checkOn.TabStop = false;
            this.checkOn.Visible = false;
            // 
            // editTO
            // 
            this.editTO.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.editTO.Font = new System.Drawing.Font("굴림", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.editTO.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.editTO.Location = new System.Drawing.Point(517, 88);
            this.editTO.Name = "editTO";
            this.editTO.Size = new System.Drawing.Size(36, 12);
            this.editTO.TabIndex = 38;
            this.editTO.Text = "0";
            this.editTO.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // editPort
            // 
            this.editPort.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.editPort.Font = new System.Drawing.Font("굴림", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.editPort.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.editPort.Location = new System.Drawing.Point(374, 88);
            this.editPort.Name = "editPort";
            this.editPort.Size = new System.Drawing.Size(36, 12);
            this.editPort.TabIndex = 37;
            this.editPort.Text = "0";
            this.editPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // editHost
            // 
            this.editHost.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.editHost.Font = new System.Drawing.Font("굴림", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.editHost.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.editHost.Location = new System.Drawing.Point(222, 88);
            this.editHost.Name = "editHost";
            this.editHost.Size = new System.Drawing.Size(114, 12);
            this.editHost.TabIndex = 36;
            this.editHost.Text = "0";
            this.editHost.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // imgBack
            // 
            this.imgBack.Image = ((System.Drawing.Image)(resources.GetObject("imgBack.Image")));
            this.imgBack.Location = new System.Drawing.Point(89, 9);
            this.imgBack.Name = "imgBack";
            this.imgBack.Size = new System.Drawing.Size(60, 23);
            this.imgBack.TabIndex = 35;
            this.imgBack.TabStop = false;
            this.imgBack.Visible = false;
            // 
            // editTemp
            // 
            this.editTemp.Location = new System.Drawing.Point(224, 23);
            this.editTemp.Name = "editTemp";
            this.editTemp.Size = new System.Drawing.Size(112, 21);
            this.editTemp.TabIndex = 31;
            this.editTemp.Visible = false;
            // 
            // _chart
            // 
            this._chart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(243)))), ((int)(((byte)(245)))));
            this._chart.Background = ((Arction.WinForms.Charting.Fill)(resources.GetObject("_chart.Background")));
            this._chart.ChartManager = null;
            this._chart.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this._chart.HorizontalScrollBars = ((System.Collections.Generic.List<Arction.WinForms.Charting.HorizontalScrollBar>)(resources.GetObject("_chart.HorizontalScrollBars")));
            this._chart.Location = new System.Drawing.Point(335, 9);
            this._chart.Margin = new System.Windows.Forms.Padding(3, 8, 3, 8);
            this._chart.MinimumSize = new System.Drawing.Size(110, 219);
            this._chart.Name = "_chart";
            this._chart.Options = ((Arction.WinForms.Charting.ChartOptions)(resources.GetObject("_chart.Options")));
            this._chart.OutputStream = null;
            this._chart.RenderOptions = ((Arction.WinForms.Charting.Views.RenderOptionsCommon)(resources.GetObject("_chart.RenderOptions")));
            this._chart.Size = new System.Drawing.Size(1271, 458);
            this._chart.TabIndex = 32;
            this._chart.Title = ((Arction.WinForms.Charting.Titles.ChartTitle)(resources.GetObject("_chart.Title")));
            this._chart.VerticalScrollBars = ((System.Collections.Generic.List<Arction.WinForms.Charting.VerticalScrollBar>)(resources.GetObject("_chart.VerticalScrollBars")));
            this._chart.View3D = ((Arction.WinForms.Charting.Views.View3D.View3D)(resources.GetObject("_chart.View3D")));
            this._chart.ViewPie3D = ((Arction.WinForms.Charting.Views.ViewPie3D.ViewPie3D)(resources.GetObject("_chart.ViewPie3D")));
            this._chart.ViewPolar = ((Arction.WinForms.Charting.Views.ViewPolar.ViewPolar)(resources.GetObject("_chart.ViewPolar")));
            this._chart.ViewSmith = ((Arction.WinForms.Charting.Views.ViewSmith.ViewSmith)(resources.GetObject("_chart.ViewSmith")));
            this._chart.ViewXY = ((Arction.WinForms.Charting.Views.ViewXY.ViewXY)(resources.GetObject("_chart.ViewXY")));
            // 
            // panelGraphRight
            // 
            this.panelGraphRight.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelGraphRight.BackgroundImage")));
            this.panelGraphRight.Controls.Add(this.comboYMax);
            this.panelGraphRight.Controls.Add(this.comboYMin);
            this.panelGraphRight.Controls.Add(this.editGraphPYMax);
            this.panelGraphRight.Controls.Add(this.editGraphPYMin);
            this.panelGraphRight.Controls.Add(this.buttonGraphPAxisReset);
            this.panelGraphRight.Location = new System.Drawing.Point(1612, 2);
            this.panelGraphRight.Name = "panelGraphRight";
            this.panelGraphRight.Size = new System.Drawing.Size(144, 469);
            this.panelGraphRight.TabIndex = 31;
            // 
            // comboYMax
            // 
            this.comboYMax.BackColor = System.Drawing.Color.Transparent;
            this.comboYMax.Location = new System.Drawing.Point(71, 360);
            this.comboYMax.Name = "comboYMax";
            this.comboYMax.Size = new System.Drawing.Size(30, 30);
            this.comboYMax.TabIndex = 21;
            // 
            // comboYMin
            // 
            this.comboYMin.BackColor = System.Drawing.Color.Transparent;
            this.comboYMin.Location = new System.Drawing.Point(71, 308);
            this.comboYMin.Name = "comboYMin";
            this.comboYMin.Size = new System.Drawing.Size(30, 30);
            this.comboYMin.TabIndex = 20;
            // 
            // editGraphPYMax
            // 
            this.editGraphPYMax.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.editGraphPYMax.Font = new System.Drawing.Font("굴림", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.editGraphPYMax.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.editGraphPYMax.Location = new System.Drawing.Point(33, 370);
            this.editGraphPYMax.Multiline = true;
            this.editGraphPYMax.Name = "editGraphPYMax";
            this.editGraphPYMax.Size = new System.Drawing.Size(38, 17);
            this.editGraphPYMax.TabIndex = 13;
            this.editGraphPYMax.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // editGraphPYMin
            // 
            this.editGraphPYMin.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.editGraphPYMin.Font = new System.Drawing.Font("굴림", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.editGraphPYMin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.editGraphPYMin.Location = new System.Drawing.Point(33, 318);
            this.editGraphPYMin.Multiline = true;
            this.editGraphPYMin.Name = "editGraphPYMin";
            this.editGraphPYMin.Size = new System.Drawing.Size(38, 17);
            this.editGraphPYMin.TabIndex = 12;
            this.editGraphPYMin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonGraphPAxisReset
            // 
            this.buttonGraphPAxisReset.BackColor = System.Drawing.Color.Transparent;
            this.buttonGraphPAxisReset.Location = new System.Drawing.Point(20, 73);
            this.buttonGraphPAxisReset.Name = "buttonGraphPAxisReset";
            this.buttonGraphPAxisReset.Size = new System.Drawing.Size(82, 70);
            this.buttonGraphPAxisReset.TabIndex = 10;
            // 
            // imgCyl12
            // 
            this.imgCyl12.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl12.Location = new System.Drawing.Point(218, 218);
            this.imgCyl12.Name = "imgCyl12";
            this.imgCyl12.Size = new System.Drawing.Size(71, 29);
            this.imgCyl12.TabIndex = 28;
            this.imgCyl12.TabStop = false;
            // 
            // imgCyl11
            // 
            this.imgCyl11.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl11.Location = new System.Drawing.Point(139, 218);
            this.imgCyl11.Name = "imgCyl11";
            this.imgCyl11.Size = new System.Drawing.Size(71, 29);
            this.imgCyl11.TabIndex = 27;
            this.imgCyl11.TabStop = false;
            // 
            // imgCyl10
            // 
            this.imgCyl10.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl10.Location = new System.Drawing.Point(57, 218);
            this.imgCyl10.Name = "imgCyl10";
            this.imgCyl10.Size = new System.Drawing.Size(71, 29);
            this.imgCyl10.TabIndex = 26;
            this.imgCyl10.TabStop = false;
            // 
            // imgCyl1
            // 
            this.imgCyl1.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl1.Image = ((System.Drawing.Image)(resources.GetObject("imgCyl1.Image")));
            this.imgCyl1.Location = new System.Drawing.Point(57, 116);
            this.imgCyl1.Name = "imgCyl1";
            this.imgCyl1.Size = new System.Drawing.Size(71, 29);
            this.imgCyl1.TabIndex = 17;
            this.imgCyl1.TabStop = false;
            // 
            // imgCyl9
            // 
            this.imgCyl9.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl9.Location = new System.Drawing.Point(218, 184);
            this.imgCyl9.Name = "imgCyl9";
            this.imgCyl9.Size = new System.Drawing.Size(71, 29);
            this.imgCyl9.TabIndex = 25;
            this.imgCyl9.TabStop = false;
            // 
            // imgCyl2
            // 
            this.imgCyl2.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl2.Location = new System.Drawing.Point(139, 116);
            this.imgCyl2.Name = "imgCyl2";
            this.imgCyl2.Size = new System.Drawing.Size(71, 29);
            this.imgCyl2.TabIndex = 18;
            this.imgCyl2.TabStop = false;
            // 
            // imgCyl8
            // 
            this.imgCyl8.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl8.Location = new System.Drawing.Point(139, 184);
            this.imgCyl8.Name = "imgCyl8";
            this.imgCyl8.Size = new System.Drawing.Size(71, 29);
            this.imgCyl8.TabIndex = 24;
            this.imgCyl8.TabStop = false;
            // 
            // imgCyl3
            // 
            this.imgCyl3.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl3.Location = new System.Drawing.Point(218, 116);
            this.imgCyl3.Name = "imgCyl3";
            this.imgCyl3.Size = new System.Drawing.Size(71, 29);
            this.imgCyl3.TabIndex = 19;
            this.imgCyl3.TabStop = false;
            // 
            // imgCyl7
            // 
            this.imgCyl7.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl7.Location = new System.Drawing.Point(57, 184);
            this.imgCyl7.Name = "imgCyl7";
            this.imgCyl7.Size = new System.Drawing.Size(71, 29);
            this.imgCyl7.TabIndex = 23;
            this.imgCyl7.TabStop = false;
            // 
            // imgCyl4
            // 
            this.imgCyl4.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl4.Location = new System.Drawing.Point(57, 150);
            this.imgCyl4.Name = "imgCyl4";
            this.imgCyl4.Size = new System.Drawing.Size(71, 29);
            this.imgCyl4.TabIndex = 20;
            this.imgCyl4.TabStop = false;
            // 
            // imgCyl6
            // 
            this.imgCyl6.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl6.Location = new System.Drawing.Point(218, 150);
            this.imgCyl6.Name = "imgCyl6";
            this.imgCyl6.Size = new System.Drawing.Size(71, 29);
            this.imgCyl6.TabIndex = 22;
            this.imgCyl6.TabStop = false;
            // 
            // imgCyl5
            // 
            this.imgCyl5.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl5.Location = new System.Drawing.Point(139, 150);
            this.imgCyl5.Name = "imgCyl5";
            this.imgCyl5.Size = new System.Drawing.Size(71, 29);
            this.imgCyl5.TabIndex = 21;
            this.imgCyl5.TabStop = false;
            // 
            // timer100ms
            // 
            this.timer100ms.Enabled = true;
            this.timer100ms.Tick += new System.EventHandler(this.timer100ms_Tick);
            // 
            // buttonSaveSelect2
            // 
            this.buttonSaveSelect2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(230)))), ((int)(((byte)(232)))));
            this.buttonSaveSelect2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonSaveSelect2.BackgroundImage")));
            this.buttonSaveSelect2.Location = new System.Drawing.Point(5, 5);
            this.buttonSaveSelect2.Name = "buttonSaveSelect2";
            this.buttonSaveSelect2.Size = new System.Drawing.Size(30, 26);
            this.buttonSaveSelect2.TabIndex = 96;
            this.buttonSaveSelect2.Tag = "confirm";
            this.buttonSaveSelect2.Click += new System.EventHandler(this.panelSaveSelect_Click);
            // 
            // buttonApply
            // 
            this.buttonApply.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(230)))), ((int)(((byte)(232)))));
            this.buttonApply.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonApply.BackgroundImage")));
            this.buttonApply.Location = new System.Drawing.Point(1809, 933);
            this.buttonApply.Name = "buttonApply";
            this.buttonApply.Size = new System.Drawing.Size(30, 26);
            this.buttonApply.TabIndex = 97;
            this.buttonApply.Tag = "confirm";
            this.buttonApply.Click += new System.EventHandler(this.buttonApply_Click);
            // 
            // buttonImport
            // 
            this.buttonImport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(243)))), ((int)(((byte)(244)))));
            this.buttonImport.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonImport.BackgroundImage")));
            this.buttonImport.Location = new System.Drawing.Point(63, 13);
            this.buttonImport.Name = "buttonImport";
            this.buttonImport.Size = new System.Drawing.Size(29, 29);
            this.buttonImport.TabIndex = 98;
            this.buttonImport.Tag = "import";
            this.buttonImport.Click += new System.EventHandler(this.buttonImport_Click);
            // 
            // buttonExport
            // 
            this.buttonExport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(243)))), ((int)(((byte)(244)))));
            this.buttonExport.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonExport.BackgroundImage")));
            this.buttonExport.Location = new System.Drawing.Point(106, 12);
            this.buttonExport.Name = "buttonExport";
            this.buttonExport.Size = new System.Drawing.Size(29, 29);
            this.buttonExport.TabIndex = 99;
            this.buttonExport.Tag = "export";
            this.buttonExport.Click += new System.EventHandler(this.buttonExport_Click);
            // 
            // TFormSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1904, 966);
            this.Controls.Add(this.panelForm);
            this.Name = "TFormSetting";
            this.Text = "TFormSetting";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TFormSetting_KeyDown);
            this.panelForm.ResumeLayout(false);
            this.panelForm.PerformLayout();
            this.buttonSaveSelect.ResumeLayout(false);
            this.panelExportBack.ResumeLayout(false);
            this.panelGridContent.ResumeLayout(false);
            this.panelGridContent.PerformLayout();
            this.panel11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkOn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgBack)).EndInit();
            this.panelGraphRight.ResumeLayout(false);
            this.panelGraphRight.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Panel panelForm;
        private System.Windows.Forms.TextBox editTemp;
        public Arction.WinForms.Charting.LightningChartUltimate _chart;
        private System.Windows.Forms.Panel panelGraphRight;
        private System.Windows.Forms.Panel comboYMax;
        private System.Windows.Forms.Panel comboYMin;
        private System.Windows.Forms.TextBox editGraphPYMax;
        private System.Windows.Forms.TextBox editGraphPYMin;
        private System.Windows.Forms.Panel buttonGraphPAxisReset;
        private System.Windows.Forms.PictureBox imgCyl12;
        private System.Windows.Forms.PictureBox imgCyl11;
        private System.Windows.Forms.PictureBox imgCyl10;
        private System.Windows.Forms.PictureBox imgCyl1;
        private System.Windows.Forms.PictureBox imgCyl9;
        private System.Windows.Forms.PictureBox imgCyl2;
        private System.Windows.Forms.PictureBox imgCyl8;
        private System.Windows.Forms.PictureBox imgCyl3;
        private System.Windows.Forms.PictureBox imgCyl7;
        private System.Windows.Forms.PictureBox imgCyl4;
        private System.Windows.Forms.PictureBox imgCyl6;
        private System.Windows.Forms.PictureBox imgCyl5;
        private System.Windows.Forms.PictureBox imgBack;
        private System.Windows.Forms.PictureBox checkSave;
        private System.Windows.Forms.PictureBox checkOn;
        public System.Windows.Forms.TextBox editTO;
        public System.Windows.Forms.TextBox editPort;
        public System.Windows.Forms.TextBox editHost;
        public System.Windows.Forms.TextBox editSaveInterval;
        public System.Windows.Forms.TextBox editFolder;
        private System.Windows.Forms.FolderBrowserDialog DialogFolder;
        private System.Windows.Forms.Panel c3;
        private System.Windows.Forms.Panel c2;
        private System.Windows.Forms.Panel c1;
        private System.Windows.Forms.Panel c0;
        private System.Windows.Forms.Panel c5;
        private System.Windows.Forms.Panel c4;
        private System.Windows.Forms.Panel c12;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel c11;
        private System.Windows.Forms.Panel c10;
        private System.Windows.Forms.Panel c9;
        private System.Windows.Forms.Panel c8;
        private System.Windows.Forms.Panel c7;
        private System.Windows.Forms.Panel c6;
        private System.Windows.Forms.Panel c13;
        private System.Windows.Forms.Panel c0r1;
        private System.Windows.Forms.Panel panelGridContent;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label labelECP;
        private System.Windows.Forms.Label labelEDP;
        public System.Windows.Forms.TextBox editGrid;
        private System.Windows.Forms.Panel panelExportBack;
        private System.Windows.Forms.Panel buttonSaveSelect;
        private System.Windows.Forms.Panel buttonGridScrollUp;
        private System.Windows.Forms.Panel buttonGridScrollDown;
        private System.Windows.Forms.Panel buttonGridScrollLeft;
        private System.Windows.Forms.Panel buttonEditor;
        private System.Windows.Forms.Panel buttonGridScrollRight;
        private System.Windows.Forms.Panel buttonNextx2;
        private System.Windows.Forms.Panel buttonNext;
        private System.Windows.Forms.Panel buttonLast;
        private System.Windows.Forms.Panel buttonPrev2x;
        private System.Windows.Forms.Panel buttonFirst;
        private System.Windows.Forms.Panel buttonPrev;
        private System.Windows.Forms.Panel panelGridNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panelGridScroll;
        private System.Windows.Forms.Timer timer100ms;
        private System.Windows.Forms.Label labelEKP;
        private System.Windows.Forms.Label labelEPTitle;
        private System.Windows.Forms.Label labelBottomMsg;
        private System.Windows.Forms.Panel buttonSaveSelect2;
        private System.Windows.Forms.Panel buttonApply;
        private System.Windows.Forms.Panel buttonExport;
        private System.Windows.Forms.Panel buttonImport;
    }
}
﻿namespace EngineAnalyzer
{
    partial class TFormStatus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.GridStatus = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.TimerTick = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.GridStatus)).BeginInit();
            this.SuspendLayout();
            // 
            // GridStatus
            // 
            this.GridStatus.AllowUserToAddRows = false;
            this.GridStatus.AllowUserToDeleteRows = false;
            this.GridStatus.AllowUserToResizeRows = false;
            this.GridStatus.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.GridStatus.ColumnHeadersHeight = 12;
            this.GridStatus.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GridStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridStatus.Location = new System.Drawing.Point(0, 28);
            this.GridStatus.MultiSelect = false;
            this.GridStatus.Name = "GridStatus";
            this.GridStatus.ReadOnly = true;
            this.GridStatus.RowHeadersVisible = false;
            this.GridStatus.RowTemplate.Height = 16;
            this.GridStatus.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GridStatus.Size = new System.Drawing.Size(330, 426);
            this.GridStatus.TabIndex = 0;
            this.GridStatus.VirtualMode = true;
            this.GridStatus.CellValueNeeded += new System.Windows.Forms.DataGridViewCellValueEventHandler(this.GridStatus_CellValueNeeded);
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(330, 28);
            this.panel1.TabIndex = 1;
            // 
            // TimerTick
            // 
            this.TimerTick.Enabled = true;
            this.TimerTick.Interval = 1000;
            this.TimerTick.Tick += new System.EventHandler(this.TimerTick_Tick);
            // 
            // TFormStatus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(330, 454);
            this.Controls.Add(this.GridStatus);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("맑은 고딕", 8F);
            this.Name = "TFormStatus";
            this.Text = "상태";
            this.Load += new System.EventHandler(this.TFormStatus_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GridStatus)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView GridStatus;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Timer TimerTick;
    }
}
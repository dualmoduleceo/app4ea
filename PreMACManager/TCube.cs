﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection.Emit;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Text;
using System.Collections;
using System.Threading;
using System.Drawing.Text;
using System.Drawing;
using CubeEA;
//using Xamarin.Forms;

namespace DM
{
    public static class TCube
    {
        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVale, int size, string filePath);

        public static Boolean IsTerminate = false;
        
        public static Dictionary<String, TSocketItem> DicSI = new Dictionary<string, TSocketItem>();
        public static String ParamBody = "";

        public static Boolean IsEmul()
        {
            return true;
        }

        public static String GetPathResource()
        {
            return Application.StartupPath + "\\image";
        }

        public static void ParamLoad()
        {
            String documents = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            String sFile = Path.Combine(documents, "cube_param.dat");
            if (File.Exists(sFile))
            {
                ParamBody = File.ReadAllText(sFile);
            }
        }

        public static void ParamSave()
        {
            String documents = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            String sFile = Path.Combine(documents, "cube_param.dat");
            //if (File.Exists(sFile))
            {
                File.WriteAllText(sFile, ParamBody);
            }
        }

        public static String GetParam(String ABody, String AParam, String ADefault = "", Char ADelimiter = '|')
        {
            if (ABody == null)
                return ADefault;
            String[] sa = ABody.Split(ADelimiter);

            int i = 0;
            while (i < sa.Length)
            {
                if (sa[i] == AParam)
                {
                    if (i + 1 < sa.Length)
                    {
                        return sa[i + 1];
                    }
                }
                i = i + 1;
            }
            return ADefault;
        }

        public static int GetParamInt(String ABody, String AParam, int ADefault = 0, Char ADelimiter = '|')
        {
            if (ABody == null)
                return ADefault;
            String[] sa = ABody.Split(ADelimiter);

            int i = 0;
            while (i < sa.Length)
            {
                if (sa[i] == AParam)
                {
                    if (i + 1 < sa.Length)
                    {
                        string s = sa[i + 1];
                        int iResult = 0;

                        if (Int32.TryParse(s, out iResult))
                        {
                            return iResult;
                        }
                        else
                        {
                            return ADefault;
                        }
                    }
                }
                i = i + 1;
            }
            return ADefault;
        }

        public static String SetParam(String ABody, String AParam, String AValue, Char ADelimiter = '|')
        {
            if (ABody == null)
                ABody = "";

            String[] sa = ABody.Split(ADelimiter);

            int i = 0;
            while (i < sa.Length)
            {
                if (sa[i] == AParam)
                {
                    if (i + 1 < sa.Length)
                    {
                        sa[i + 1] = AValue;
                        return String.Join("|", sa);
                    }
                }
                i = i + 1;
            }

            //if (ABody.Length == 0)
            //    ABody = AParam + ADelimiter + AValue;
            //else
            ABody = ABody + ADelimiter + AParam + ADelimiter + AValue;

            return ABody;
        }

        public static void ConfigLoad()
        {
            //int iCount = TCube.GetParamInt(TCube.ParamBody, "connection_count");
            TCube.ParamLoad();
            String sRows = TCube.GetParam(TCube.ParamBody, "connection_list");

            List<String> ListRow = sRows.Split('$').ToList();

            DicSI.Clear();
            foreach (String sRow in ListRow)
            {
                if (sRow == "")
                    continue;
                TSocketItem si = new TSocketItem();

                si.IP = TCube.GetParam(sRow, "ip", "", '*');
                si.Port = TCube.GetParam(sRow, "port", "", '*');
                si.LastConnect = TCube.GetParam(sRow, "lastconnect", "", '*');

                if (TCube.DicSI.ContainsKey(si.Key) == false)
                    TCube.DicSI.Add(si.Key, si);
            }
        }

        public static void ConfigSave()
        {
            List<String> ListRow = new List<string>();
            foreach (TSocketItem si in TCube.DicSI.Values)
            {
                String sParam = "";

                sParam = TCube.SetParam(sParam, "ip", si.IP, '*');
                sParam = TCube.SetParam(sParam, "port", si.Port, '*');
                sParam = TCube.SetParam(sParam, "lastconnect", si.LastConnect, '*');

                ListRow.Add(sParam);
            }

            TCube.ParamBody = TCube.SetParam(TCube.ParamBody, "connection_list", String.Join("$", ListRow.ToArray()));
            TCube.ParamSave();
        }

        public static void SetValue(String AParam, String AValue)
        {
            String sFile = Application.StartupPath + "\\setting.ini";
            WritePrivateProfileString("setting", AParam, AValue, sFile);
        }
        public static String GetValue(String AParam, String AValue)
        {
            String sFile = Application.StartupPath + "\\setting.ini";
            StringBuilder temp = new StringBuilder(255);

            GetPrivateProfileString("setting", AParam, AValue, temp, 255, sFile);
            return temp.ToString();
        }

        public static Boolean GetBit(int AValue, int ABitIndex)
        {
            int data = AValue;

            var bits = new BitArray(new int[] { data });

            return bits.Get(ABitIndex);
        }

        public static int SetBit(int AValue, int ABitIndex, bool ABitValue)
        {
            int data = AValue;

            var bits = new BitArray(new int[] { data });

            bits.Set(ABitIndex, ABitValue);

            if (bits.Length > 32)
                throw new ArgumentException("Argument length shall be at most 32 bits.");

            int[] array = new int[1];
            bits.CopyTo(array, 0);
            return array[0];
        }
    }

    public class TSocketItem
    {
        public String IP;
        public String Port;

        public String LastConnect;

        public TSocketItem()
        {
            IP = "192.168.10.12";
            Port = "502";
        }

        public String Key
        {
            get
            {
                return "IP = " + IP + " / Port = " + Port;
                //return IP + "_" + Port;
            }
        }

        public String Title
        {
            get
            {
                return IP + "/" + Port;
                //return IP + "_" + Port;
            }
        }
    }
    
    public static class TCS
    {
        public static int LogLevel = 0;

        public static List<String> ListLog = new List<string>();
        public static bool IsDebug = false;

        public static Thread ThreadTick = null;
        
        public static TEAPacket EAPAICalLast = null;

        public static TCubeEASocketCmd eaCmd = new TCubeEASocketCmd();

        public static void LogError(Exception E)
        {
            LogMsg(E.Message);
            LogMsg(E.StackTrace);
        }

        public static void ThreadInit()
        {
            ThreadTick = new Thread(new ThreadStart(DoThread));
            ThreadTick.Start();
        }

        public static void LogMsg(String AMsg, int ALevel = 0)
        {
            lock (ListLog)
            {
                String s = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "\t" + AMsg;
                ListLog.Add(s);

                //StreamWriter w = File.AppendText(Application.StartupPath + "\\log\\log_" + DateTime.Now.ToString("yyMMdd") + ".txt");

                //w.WriteLine(s);
            }
        }

        public static void LogDebug(String AMsg, int ALevel = 0)
        {
            if (IsDebug == false)
                return;
            lock (ListLog)
            {
                String s = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "\t" + AMsg;
                ListLog.Add(s);

                //StreamWriter w = File.AppendText(Application.StartupPath + "\\log\\log_" + DateTime.Now.ToString("yyMMdd") + ".txt");

                //w.WriteLine(s);
            }
        }

        public static void ShowMsg(String AMsg)
        {
            MessageBox.Show(AMsg, "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public static void DoThread()
        {
            while (Thread.CurrentThread.IsAlive == true)
            {
                Thread.Sleep(500);

                try
                {
                    //if (CheckLog.Checked == false)
                    //{
                    //    lock (TCS.ListLog)
                    //    {
                    //        TCS.ListLog.Clear();
                    //    }
                    //}
                    //else
                    {
                        if (TCS.ListLog.Count > 0)
                        {
                            List<String> l = null;
                            lock (TCS.ListLog)
                            {
                                l = TCS.ListLog;
                                TCS.ListLog = new List<string>();
                            }

                            String sFile = Application.StartupPath + "\\log\\log_" + DateTime.Now.ToString("yyMMdd") + ".txt";
                            File.AppendAllText(sFile, String.Join("\r\n", l) + "\r\n");

                            //this.Invoke(new Action(delegate () // this == Form 이다. Form이 아닌 컨트롤의 Invoke를 직접호출해도 무방하다.
                            //{
                            //    if (MemoLog.Text.Length > 9048)
                            //        MemoLog.Text = "";

                            //    MemoLog.Text = MemoLog.Text + "\r\n" + String.Join("\r\n", l);
                            //}));
                        }
                    }
                }
                catch (Exception e)
                {

                }
            }
        }
    }

}

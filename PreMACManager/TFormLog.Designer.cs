﻿namespace EngineAnalyzer
{
    partial class TFormLog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.MemoLog = new System.Windows.Forms.RichTextBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.CheckLog = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // MemoLog
            // 
            this.MemoLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MemoLog.Location = new System.Drawing.Point(0, 16);
            this.MemoLog.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.MemoLog.Name = "MemoLog";
            this.MemoLog.Size = new System.Drawing.Size(389, 294);
            this.MemoLog.TabIndex = 0;
            this.MemoLog.Text = "";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // CheckLog
            // 
            this.CheckLog.AutoSize = true;
            this.CheckLog.Dock = System.Windows.Forms.DockStyle.Top;
            this.CheckLog.Location = new System.Drawing.Point(0, 0);
            this.CheckLog.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.CheckLog.Name = "CheckLog";
            this.CheckLog.Size = new System.Drawing.Size(389, 16);
            this.CheckLog.TabIndex = 2;
            this.CheckLog.Text = "Log";
            this.CheckLog.UseVisualStyleBackColor = true;
            // 
            // TFormLog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(389, 310);
            this.Controls.Add(this.MemoLog);
            this.Controls.Add(this.CheckLog);
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "TFormLog";
            this.Text = "TFormLog";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TFormLog_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox MemoLog;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.CheckBox CheckLog;
    }
}
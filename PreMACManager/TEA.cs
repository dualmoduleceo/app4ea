﻿using DM;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EngineAnalyzer
{
    public static class TEA
    {
        public static TFormEA ea = null;

        public static Color ColorTabButtonOnBack = ColorTranslator.FromHtml("#95999b");// Color.FromArgb(149, 153, 155);

        //public Color ColorTabButtonOverBack = Color.FromArgb(149, 153, 155);

        public static Color ColorTabButtonDownBack = ColorTranslator.FromHtml("#95999b");// Color.FromArgb(189, 193, 195);

        public static Color ColorTabButtonOffBack = ColorTranslator.FromHtml("#c4c6c9");// Color.FromArgb(196, 198, 201);

        public static Color ColorTabButtonBorder = Color.FromArgb(187, 191, 193);

        public static Color ColorPanelBorder = Color.FromArgb(207, 207, 212);

        public static Color ColorLabelFont = Color.FromArgb(149, 153, 155);

        public static Color ColorButtonBorder = ColorTranslator.FromHtml("#bbbfc1");  //Color.FromArgb(149, 153, 155);
        
        public static Color ColorGraphCalcData = ColorTranslator.FromHtml("#494b4c");  //Color.FromArgb(149, 153, 155);

        public static Color ColorGridCellFont = ColorTranslator.FromHtml("#494b4c");  //Color.FromArgb(149, 153, 155);

        public static List<Color> ListColorCH = new List<Color>();

        public static PrivateFontCollection privateFonts;

        public static Font fontMd10 ; // = new Font(privateFonts.Families[0], 10, GraphicsUnit.Pixel);
        public static Font fontMd12 ; // = new Font(privateFonts.Families[0], 12, GraphicsUnit.Pixel);
        public static Font fontMd14 ; // = new Font(privateFonts.Families[0], 14, GraphicsUnit.Pixel);
        public static Font fontMd16 ; // = new Font(privateFonts.Families[0], 16, GraphicsUnit.Pixel);
        public static Font fontMdCn20; // = new Font(privateFonts.Families[1], 20, GraphicsUnit.Pixel);
        public static Font fontMdCn22; // = new Font(privateFonts.Families[1], 20, GraphicsUnit.Pixel);
        public static Font fontMdCn40; // = new Font(privateFonts.Families[1], 20, GraphicsUnit.Pixel);
        public static Font fontMdCn14 ; // = new Font(privateFonts.Families[1], 14, GraphicsUnit.Pixel);
        public static Font fontMdCn62 ; // = new Font(privateFonts.Families[1], 62, GraphicsUnit.Pixel);

        public static int CHMaxStatic = 12;
        public static int CHMaxUse = 12;
        public static int RowSizeInRAWFile = 34664;

        public static float[] EmptyData = new float[1440 * 12];

        public static void Init()
        {
            ListColorCH.Add(ColorTranslator.FromHtml("#0059B2"));
            ListColorCH.Add(ColorTranslator.FromHtml("#8CCC00"));
            ListColorCH.Add(ColorTranslator.FromHtml("#158EFC"));
            ListColorCH.Add(ColorTranslator.FromHtml("#FFC545"));
            ListColorCH.Add(ColorTranslator.FromHtml("#78CCFF"));
            ListColorCH.Add(ColorTranslator.FromHtml("#F27421"));
            ListColorCH.Add(ColorTranslator.FromHtml("#E7969F"));
            ListColorCH.Add(ColorTranslator.FromHtml("#7BE6CE"));
            ListColorCH.Add(ColorTranslator.FromHtml("#C495ED"));
            ListColorCH.Add(ColorTranslator.FromHtml("#D63F24"));
            ListColorCH.Add(ColorTranslator.FromHtml("#C9AB8C"));
            ListColorCH.Add(ColorTranslator.FromHtml("#5F6364"));

            privateFonts = new PrivateFontCollection();
            
            //폰트명이 아닌 폰트의 파일명을 적음
            try
            {
                privateFonts.AddFontFile("HelveticaNeueLTPro-Md.ttf");
                privateFonts.AddFontFile("HelveticaNeueLTPro-MdCn.ttf");
            }
            catch (Exception E)
            {

            }

            fontMd10 = new Font(privateFonts.Families[0], 10, GraphicsUnit.Pixel);
            fontMd12 = new Font(privateFonts.Families[0], 12, GraphicsUnit.Pixel);
            fontMd14 = new Font(privateFonts.Families[0], 14, GraphicsUnit.Pixel);
            fontMd16 = new Font(privateFonts.Families[0], 16, GraphicsUnit.Pixel);
            fontMdCn20 = new Font(privateFonts.Families[1], 20, GraphicsUnit.Pixel);
            fontMdCn22 = new Font(privateFonts.Families[1], 22, GraphicsUnit.Pixel);
            fontMdCn40 = new Font(privateFonts.Families[1], 40, GraphicsUnit.Pixel);
            fontMdCn14 = new Font(privateFonts.Families[1], 14, GraphicsUnit.Pixel);
            fontMdCn62 = new Font(privateFonts.Families[1], 62, GraphicsUnit.Pixel);

            for (int i = 0; i < 1440 * 12; i++)
            {
                EmptyData[i] = (float)-9999.0;
            }
        }

        public static Dictionary<Label, String> DicLabel = new Dictionary<Label, String>();

        public static void labelAA_Paint(object sender, PaintEventArgs e)
        {
            Label l = (Label)sender;

            String s = "";
            if (DicLabel.ContainsKey(l) == false)
            {
                DicLabel.Add(l, l.Text);

                l.AutoSize = false;
                l.Text = "";
            }

            if (l.Text != "")
            {
                DicLabel[l] = l.Text;
                l.Text = "";
            }
            s = DicLabel[l];
            //if (l.Text != "")
            //{
            //    l.Tag = l.Text;
            //    l.Text = "";
            //    if (l.AutoSize)
            //        l.AutoSize = false;
            //}

            SolidBrush solidBrush = new SolidBrush(l.ForeColor);
            
            e.Graphics.TextRenderingHint = TextRenderingHint.AntiAlias;
            if (l.TextAlign == ContentAlignment.MiddleCenter)
            {
                StringFormat stringFormat = new StringFormat();
                stringFormat.Alignment = StringAlignment.Center;
                stringFormat.LineAlignment = StringAlignment.Center;
                
                Rectangle r = new Rectangle(new Point(0,0), l.Bounds.Size);

                e.Graphics.DrawString(s, l.Font, solidBrush, r, stringFormat);
            }
            else
            {
                e.Graphics.DrawString(s, l.Font, solidBrush, new PointF(0, 0));
            }
        }

        public static void setBorder(Control ctl, Color col, int width, BorderStyle style)
        {
            if (col == Color.Transparent)
            {
                Panel pan = ctl.Parent as Panel;
                if (pan == null) { throw new Exception("control not in border panel!"); }
                ctl.Location = new Point(pan.Left + width, pan.Top + width);
                ctl.Parent = pan.Parent;
                pan.Dispose();

            }
            else
            {
                Panel pan = new Panel();
                pan.BorderStyle = style;
                pan.Size = new Size(ctl.Width + width * 2, ctl.Height + width * 2);
                pan.Location = new Point(ctl.Left - width, ctl.Top - width);
                pan.BackColor = col;
                pan.Parent = ctl.Parent;
                ctl.Parent = pan;
                ctl.Location = new Point(width, width);
            }
        }

        // cursor
        public static void ControlCursorAdd(Control sender)
        {
            sender.MouseLeave += obj_MouseLeave;
            sender.MouseEnter += obj_MouseEnter;
        }

        public static void MouseHand()
        {
            if (ea != null)
                ea.Cursor = Cursors.Hand;
        }

        public static void MouseNormal()
        {
            if (ea != null)
                ea.Cursor = Cursors.Arrow;
        }

        private static void obj_MouseEnter(object sender, EventArgs e)
        {
            MouseHand();
            try
            {
                if (sender is Panel)
                {
                    Panel p = (Panel)sender;
                    if (p.Tag != null)
                    {
                        String s = p.Tag.ToString();
                        Image img = Image.FromFile(TCube.GetPathResource() + "\\" + s + "_pressed.png");
                        p.BackgroundImage = img;
                    }
                }
                else if (sender is PictureBox)
                {
                    PictureBox p = (PictureBox)sender;
                    if (p.Tag != null)
                    {
                        String s = p.Tag.ToString();
                        Image img = Image.FromFile(TCube.GetPathResource() + "\\" + s + "_pressed.png");
                        p.Image = img;
                    }
                }
            }
            catch
            {

            }
        }

        private static void obj_MouseLeave(object sender, EventArgs e)
        {
            MouseNormal();

            try
            {
                if (sender is Panel)
                {
                    Panel p = (Panel)sender;
                    if (p.Tag != null)
                    {
                        String s = p.Tag.ToString();
                        Image img = Image.FromFile(TCube.GetPathResource() + "\\" + s + "_normal.png");
                        p.BackgroundImage = img;
                    }
                }
                else if (sender is PictureBox)
                {
                    PictureBox p = (PictureBox)sender;
                    if (p.Tag != null)
                    {
                        String s = p.Tag.ToString();
                        Image img = Image.FromFile(TCube.GetPathResource() + "\\" + s + "_normal.png");
                        p.Image = img;
                    }
                }
            }
            catch
            {

            }
        }
    }

    public class TTag
    {
        public DateTime LastChange = DateTime.Now;

        public String Label;
        public int Address;
        public int Default;
        public int RawMin;
        public int RawMax;
        public double ValueMin;
        public double ValueMax;
        public int UnitFactor = 1;
        public double Value
        {
            get
            {
                if (Address == 581)
                {

                }

                double dr = RawMax - RawMin;
                double dv = ValueMax - ValueMin;

                double df = dv / dr;
                double d1 = (FRawValue - RawMin) * df + ValueMin;

                return d1;
            }
        }

        private ushort FRawValue;
        public ushort RawValue
        {
            get
            {
                return FRawValue;
            }

            set
            {
                FRawValue = value;
            }
        }
    }

    public class TCHDataSet
    {
        public TCH Parent;
        public byte[] Raw;
        public int Cycle = 0;
        public int SavedCount = 0;
        public DateTime Time;
        public Boolean IsSaved = false;
    }

    public class TCH
    {
        public int Index;

        public List<TCHDataSet> Raws = new List<TCHDataSet>();
        public List<TCHDataSet> Queue = new List<TCHDataSet>();

        public byte[] FirstRaw()
        {
            if (Raws.Count == 0)
                return null;
            return Raws.First().Raw;
        }

        public TCH(int AIndex)
        {
            Index = AIndex;
        }

        public int RawCount(int AIndexByLast)
        {
            int i = AIndexByLast;
            int iCnt = 0;
            while (i < Raws.Count)
            {
                iCnt += Raws[i].Raw.Length;
                i++;
            }

            return iCnt;
        }
    }

    public class TRaw
    {
        private DateTime time;
        public byte[] Raws = new byte[12 * 2888]; // 
        private ushort[] FCols = new ushort[12]; // 

        public Single[] AngleOffset = new Single[12];

        public int Cycle = 0;
        public int RawsCHCount = 0;
        public Boolean IsEmpty = false;

        public DateTime Time { get { return time; } set { time = value; } }
        public ushort[] Cols
        {
            get
            {
                return FCols;
            }
            set
            {
                FCols = value;
            }
        }

        public ushort CH1 { get { return Cols[0]; } }
        public ushort CH2 { get { return Cols[1]; } }
        public ushort CH3 { get { return Cols[2]; } }
        public ushort CH4 { get { return Cols[3]; } }
        public ushort CH5 { get { return Cols[4]; } }
        public ushort CH6 { get { return Cols[5]; } }
        public ushort CH7 { get { return Cols[6]; } }
        public ushort CH8 { get { return Cols[7]; } }
        public ushort CH9 { get { return Cols[8]; } }
        public ushort CH10 { get { return Cols[9]; } }
        public ushort CH11 { get { return Cols[10]; } }
        public ushort CH12 { get { return Cols[11]; } }

        // 추가
        public byte[] BufCalcData;// = new byte[288 + 8];
        public byte[] BufKnockData;// = new byte[TEA.CHMaxStatic, 512];
    }

    public class TControlEvent
    {
        public String Key;
        public Control Dest;
        public String ID;
        public Rectangle Rect;
        public Rectangle MouseInsideRect;
    }
}

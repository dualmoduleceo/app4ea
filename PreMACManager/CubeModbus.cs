using System;
using System.Collections;
using System.Text;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace CubeModbusTCP
{
    //public static class TCubeGlobal
    //{
    //    public static CubeModbusMaster m = new CubeModbusMaster();
    //}

    public class CubeModbusMaster
    {
        public byte Unit;
        
        private const byte fctReadCoil = 1;
        private const byte fctReadDiscreteInputs = 2;
        private const byte fctReadHoldingRegister = 3;
        private const byte fctReadInputRegister = 4;
        private const byte fctWriteSingleCoil = 5;
        private const byte fctWriteSingleRegister = 6;
        private const byte fctWriteMultipleCoils = 15;
        private const byte fctWriteMultipleRegister = 16;
        private const byte fctReadWriteMultipleRegister = 23;

        private const byte fctFIFO = 24;

        public const byte excIllegalFunction = 1;
        
        public const byte excIllegalDataAdr = 2;
        
        public const byte excIllegalDataVal = 3;
        
        public const byte excSlaveDeviceFailure = 4;
        
        public const byte excAck = 5;
        
        public const byte excSlaveIsBusy = 6;
        
        public const byte excGatePathUnavailable = 10;
        
        public const byte excExceptionNotConnected = 253;
        
        public const byte excExceptionConnectionLost = 254;
        
        public const byte excExceptionTimeout = 255;
        
        private const byte excExceptionOffset = 128;
        
        private const byte excSendFailt = 100;

        
        
        private static ushort _timeout = 2000;
        private static ushort _refresh = 10;
        private static bool _connected = false;

        //private Socket tcpAsyCl;
        //private byte[] tcpAsyClBuffer = new byte[2048];

        private Socket tcpSynCl;
        private byte[] tcpSynClBuffer = new byte[65535];

        public delegate void StateChanged(Boolean AState);

        public event StateChanged OnStateChanged;

        public delegate void ResponseData(ushort id, byte unit, byte function, byte[] data);
        
        public event ResponseData OnResponseData;
        
        public delegate void ExceptionData(ushort id, byte unit, byte function, byte exception);
        
        public event ExceptionData OnException;

        public string IP = "";
        public ushort Port;
        
        public byte[] DataID = new byte[1024];
        public byte[] DataCo = new byte[1024];
        public ushort[] DataIR = new ushort[1024];
        public ushort[] DataHR = new ushort[1024];


        public ushort timeout
        {
            get { return _timeout; }
            set { _timeout = value; }
        }

        
        
        
        public ushort refresh
        {
            get { return _refresh; }
            set { _refresh = value; }
        }

        
        
        public bool connected
        {
            get { return _connected; }
            set
            {
                _connected = value;
            }
        }

        
        
        public CubeModbusMaster()
        {
        }

        
        
        
        
        public CubeModbusMaster(string ip, ushort port)
        {
            connect(ip, port);
        }


        public void connect()
        {
            if (IP.Length == 0)
                return;
            connect(IP, Port);
        }




        public void connect(string ip, ushort port)
        {
            try
            {
                IP = ip;
                Port = port;
                IPAddress _ip;
                if (IPAddress.TryParse(ip, out _ip) == false)
                {
                    IPHostEntry hst = Dns.GetHostEntry(ip);
                    ip = hst.AddressList[0].ToString();
                }
                
                
                //tcpAsyCl = new Socket(
                //    AddressFamily.InterNetwork //IPAddress.Parse(ip).AddressFamily
                //    , SocketType.Stream
                //    , ProtocolType.Tcp
                //);
                //tcpAsyCl.Connect(new IPEndPoint(IPAddress.Parse(ip), port));
                //tcpAsyCl.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.SendTimeout, _timeout);
                //tcpAsyCl.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, _timeout);
                ////tcpAsyCl.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.NoDelay, 1);
                
                
                tcpSynCl = new Socket(
                    AddressFamily.InterNetwork //IPAddress.Parse(ip).AddressFamily
                    , SocketType.Stream
                    , ProtocolType.Tcp
                );
                tcpSynCl.Connect(new IPEndPoint(IPAddress.Parse(ip), port));
                tcpSynCl.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.SendTimeout, _timeout);
                tcpSynCl.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, _timeout);
                //tcpSynCl.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.NoDelay, 1);
                connected = true;

                if (OnStateChanged != null)
                {
                    OnStateChanged(true);
                }
            }
            catch (Exception error)
            //catch (System.IO.IOException error)
            {
                connected = false;

                if (OnStateChanged != null)
                {
                    OnStateChanged(false);
                }
                //throw (error);
            }
        }

        
        
        public void disconnect()
        {
            Dispose();
            connected = false;

            if (OnStateChanged != null)
            {
                OnStateChanged(false);
            }
        }

        
        
        ~CubeModbusMaster()
        {
            Dispose();
        }

        
        
        public void Dispose()
        {
            //if (tcpAsyCl != null)
            //{
            //    if (tcpAsyCl.Connected)
            //    {
            //        try { tcpAsyCl.Shutdown(SocketShutdown.Both); }
            //        catch { }
            //        tcpAsyCl.Close();
            //    }
            //    tcpAsyCl = null;
            //}
            if (tcpSynCl != null)
            {
                if (tcpSynCl.Connected)
                {
                    try { tcpSynCl.Shutdown(SocketShutdown.Both); }
                    catch { }
                    tcpSynCl.Close();
                }
                tcpSynCl = null;
            }
        }

        internal void CallException(ushort id, byte unit, byte function, byte exception)
        {
            if ((tcpSynCl == null)) return;

            //if ((tcpAsyCl == null) || (tcpSynCl == null)) return;
            //if (exception == excExceptionConnectionLost)
            //{
            //    tcpSynCl = null;
            //    tcpAsyCl = null;
            //}

            connected = false;

            if (OnStateChanged != null)
            {
                OnStateChanged(false);
            }

            if (OnException != null) OnException(id, unit, function, exception);
        }

        internal static UInt16 SwapUInt16(UInt16 inValue)
        {
            return (UInt16)(((inValue & 0xff00) >> 8) |
                     ((inValue & 0x00ff) << 8));
        }

        
        
        
        
        
        
        public void ReadCoils(ushort id, byte unit, ushort startAddress, ushort numInputs)
        {
            WriteAsyncData(CreateReadHeader(id, unit, startAddress, numInputs, fctReadCoil), id);
        }

        
        
        
        
        
        
        
        public void ReadCoils(ushort id, byte unit, ushort startAddress, ushort numInputs, ref byte[] values)
        {
            values = WriteSyncData(CreateReadHeader(id, unit, startAddress, numInputs, fctReadCoil), id);
        }

        
        
        
        
        
        
        public void ReadDiscreteInputs(ushort id, byte unit, ushort startAddress, ushort numInputs)
        {
            WriteAsyncData(CreateReadHeader(id, unit, startAddress, numInputs, fctReadDiscreteInputs), id);
        }

        
        
        
        
        
        
        
        public void ReadDiscreteInputs(ushort id, byte unit, ushort startAddress, ushort numInputs, ref byte[] values)
        {
            values = WriteSyncData(CreateReadHeader(id, unit, startAddress, numInputs, fctReadDiscreteInputs), id);
        }

        
        
        
        
        
        
        public void ReadHoldingRegister(ushort id, byte unit, ushort startAddress, ushort numInputs)
        {
            WriteAsyncData(CreateReadHeader(id, unit, startAddress, numInputs, fctReadHoldingRegister), id);
        }

        
        
        
        
        
        
        
        public void ReadHoldingRegister(ushort id, byte unit, ushort startAddress, ushort numInputs, ref byte[] values)
        {
            values = WriteSyncData(CreateReadHeader(id, unit, startAddress, numInputs, fctReadHoldingRegister), id);
        }

        
        
        
        
        
        
        public void ReadInputRegister(ushort id, byte unit, ushort startAddress, ushort numInputs)
        {
            WriteAsyncData(CreateReadHeader(id, unit, startAddress, numInputs, fctReadInputRegister), id);
        }

        
        
        
        
        
        
        
        public void ReadInputRegister(ushort id, byte unit, ushort startAddress, ushort numInputs, ref byte[] values)
        {
            values = WriteSyncData(CreateReadHeader(id, unit, startAddress, numInputs, fctReadInputRegister), id);
        }

        
        
        
        
        
        
        public void WriteSingleCoils(ushort id, byte unit, ushort startAddress, bool OnOff)
        {
            byte[] data;
            data = CreateWriteHeader(id, unit, startAddress, 1, 1, fctWriteSingleCoil);
            if (OnOff == true) data[10] = 255;
            else data[10] = 0;
            WriteAsyncData(data, id);
        }

        
        
        
        
        
        
        
        public void WriteSingleCoils(ushort id, byte unit, ushort startAddress, bool OnOff, ref byte[] result)
        {
            byte[] data;
            data = CreateWriteHeader(id, unit, startAddress, 1, 1, fctWriteSingleCoil);
            if (OnOff == true) data[10] = 255;
            else data[10] = 0;
            result = WriteSyncData(data, id);
        }

        
        
        
        
        
        
        
        public void WriteMultipleCoils(ushort id, byte unit, ushort startAddress, ushort numBits, byte[] values)
        {
            byte numBytes = Convert.ToByte(values.Length);
            byte[] data;
            data = CreateWriteHeader(id, unit, startAddress, numBits, (byte)(numBytes + 2), fctWriteMultipleCoils);
            Array.Copy(values, 0, data, 13, numBytes);
            WriteAsyncData(data, id);
        }

        
        
        
        
        
        
        
        
        public void WriteMultipleCoils(ushort id, byte unit, ushort startAddress, ushort numBits, byte[] values, ref byte[] result)
        {
            byte numBytes = Convert.ToByte(values.Length);
            byte[] data;
            data = CreateWriteHeader(id, unit, startAddress, numBits, (byte)(numBytes + 2), fctWriteMultipleCoils);
            Array.Copy(values, 0, data, 13, numBytes);
            result = WriteSyncData(data, id);
        }

        
        
        
        
        
        
        public void WriteSingleRegister(ushort id, byte unit, ushort startAddress, byte[] values)
        {
            byte[] data;
            data = CreateWriteHeader(id, unit, startAddress, 1, 1, fctWriteSingleRegister);
            data[10] = values[0];
            data[11] = values[1];
            WriteAsyncData(data, id);
        }

        
        
        
        
        
        
        
        public void WriteSingleRegister(ushort id, byte unit, ushort startAddress, byte[] values, ref byte[] result)
        {
            byte[] data;
            data = CreateWriteHeader(id, unit, startAddress, 1, 1, fctWriteSingleRegister);
            data[10] = values[0];
            data[11] = values[1];
            result = WriteSyncData(data, id);
        }

        
        
        
        
        
        
        public void WriteMultipleRegister(ushort id, byte unit, ushort startAddress, byte[] values)
        {
            ushort numBytes = Convert.ToUInt16(values.Length);
            if (numBytes % 2 > 0) numBytes++;
            byte[] data;

            data = CreateWriteHeader(id, unit, startAddress, Convert.ToUInt16(numBytes / 2), Convert.ToUInt16(numBytes + 2), fctWriteMultipleRegister);
            Array.Copy(values, 0, data, 13, values.Length);
            WriteAsyncData(data, id);
        }

        
        
        
        
        
        
        
        public void WriteMultipleRegister(ushort id, byte unit, ushort startAddress, byte[] values, ref byte[] result)
        {
            ushort numBytes = Convert.ToUInt16(values.Length);
            if (numBytes % 2 > 0) numBytes++;
            byte[] data;

            data = CreateWriteHeader(id, unit, startAddress, Convert.ToUInt16(numBytes / 2), Convert.ToUInt16(numBytes + 2), fctWriteMultipleRegister);
            Array.Copy(values, 0, data, 13, values.Length);
            result = WriteSyncData(data, id);
        }



        public void ReadWriteMultipleRegister(ushort id, byte unit, ushort startReadAddress, ushort numInputs, ushort startWriteAddress, byte[] values)
        {
            ushort numBytes = Convert.ToUInt16(values.Length);
            if (numBytes % 2 > 0) numBytes++;
            byte[] data;

            data = CreateReadWriteHeader(id, unit, startReadAddress, numInputs, startWriteAddress, Convert.ToUInt16(numBytes / 2));
            Array.Copy(values, 0, data, 17, values.Length);
            WriteAsyncData(data, id);
        }

        
        
        
        
        
        
        
        
        
        public void ReadWriteMultipleRegister(ushort id, byte unit, ushort startReadAddress, ushort numInputs, ushort startWriteAddress, byte[] values, ref byte[] result)
        {
            ushort numBytes = Convert.ToUInt16(values.Length);
            if (numBytes % 2 > 0) numBytes++;
            byte[] data;

            data = CreateReadWriteHeader(id, unit, startReadAddress, numInputs, startWriteAddress, Convert.ToUInt16(numBytes / 2));
            Array.Copy(values, 0, data, 17, values.Length);
            result = WriteSyncData(data, id);
        }

        
        
        private byte[] CreateReadHeader(ushort id, byte unit, ushort startAddress, ushort length, byte function)
        {
            byte[] data = new byte[12];

            byte[] _id = BitConverter.GetBytes((short)id);
            data[0] = _id[1];			    
            data[1] = _id[0];				
            data[5] = 6;					
            data[6] = unit;					
            data[7] = function;				
            byte[] _adr = BitConverter.GetBytes((short)IPAddress.HostToNetworkOrder((short)startAddress));
            data[8] = _adr[0];				
            data[9] = _adr[1];				
            byte[] _length = BitConverter.GetBytes((short)IPAddress.HostToNetworkOrder((short)length));
            data[10] = _length[0];			
            data[11] = _length[1];			
            return data;
        }

        
        
        private byte[] CreateWriteHeader(ushort id, byte unit, ushort startAddress, ushort numData, ushort numBytes, byte function)
        {
            byte[] data = new byte[numBytes + 11];

            byte[] _id = BitConverter.GetBytes((short)id);
            data[0] = _id[1];				
            data[1] = _id[0];				
            byte[] _size = BitConverter.GetBytes((short)IPAddress.HostToNetworkOrder((short)(5 + numBytes)));
            data[4] = _size[0];				
            data[5] = _size[1];				
            data[6] = unit;					
            data[7] = function;				
            byte[] _adr = BitConverter.GetBytes((short)IPAddress.HostToNetworkOrder((short)startAddress));
            data[8] = _adr[0];				
            data[9] = _adr[1];				
            if (function >= fctWriteMultipleCoils)
            {
                byte[] _cnt = BitConverter.GetBytes((short)IPAddress.HostToNetworkOrder((short)numData));
                data[10] = _cnt[0];			
                data[11] = _cnt[1];			
                data[12] = (byte)(numBytes - 2);
            }
            return data;
        }

        
        
        private byte[] CreateReadWriteHeader(ushort id, byte unit, ushort startReadAddress, ushort numRead, ushort startWriteAddress, ushort numWrite)
        {
            byte[] data = new byte[numWrite * 2 + 17];

            byte[] _id = BitConverter.GetBytes((short)id);
            data[0] = _id[1];						
            data[1] = _id[0];						
            byte[] _size = BitConverter.GetBytes((short)IPAddress.HostToNetworkOrder((short)(11 + numWrite * 2)));
            data[4] = _size[0];						
            data[5] = _size[1];						
            data[6] = unit;							
            data[7] = fctReadWriteMultipleRegister;	
            byte[] _adr_read = BitConverter.GetBytes((short)IPAddress.HostToNetworkOrder((short)startReadAddress));
            data[8] = _adr_read[0];					
            data[9] = _adr_read[1];					
            byte[] _cnt_read = BitConverter.GetBytes((short)IPAddress.HostToNetworkOrder((short)numRead));
            data[10] = _cnt_read[0];				
            data[11] = _cnt_read[1];				
            byte[] _adr_write = BitConverter.GetBytes((short)IPAddress.HostToNetworkOrder((short)startWriteAddress));
            data[12] = _adr_write[0];				
            data[13] = _adr_write[1];				
            byte[] _cnt_write = BitConverter.GetBytes((short)IPAddress.HostToNetworkOrder((short)numWrite));
            data[14] = _cnt_write[0];				
            data[15] = _cnt_write[1];				
            data[16] = (byte)(numWrite * 2);

            return data;
        }

        


        private void WriteAsyncData(byte[] write_data, ushort id)
        {

            if (tcpSynCl.Connected)
            {
                try
                {
                    tcpSynCl.Send(write_data, 0, write_data.Length, SocketFlags.None);
                    int result = tcpSynCl.Receive(tcpSynClBuffer, 0, tcpSynClBuffer.Length, SocketFlags.None);

                    byte unit = tcpSynClBuffer[6];
                    byte function = tcpSynClBuffer[7];
                    byte[] data;

                    if (result == 0) CallException(id, unit, write_data[7], excExceptionConnectionLost);



                    if (function > excExceptionOffset)
                    {
                        function -= excExceptionOffset;
                        CallException(id, unit, function, tcpSynClBuffer[8]);
                        return;
                    }


                    else if ((function >= fctWriteSingleCoil) && (function != fctReadWriteMultipleRegister))
                    {
                        data = new byte[2];
                        Array.Copy(tcpSynClBuffer, 10, data, 0, 2);
                    }


                    else
                    {
                        data = new byte[tcpSynClBuffer[8]];
                        Array.Copy(tcpSynClBuffer, 9, data, 0, tcpSynClBuffer[8]);
                    }
                    return;
                }
                catch (Exception e)
                {
                    CallException(id, write_data[6], write_data[7], excExceptionConnectionLost);
                }
            }
            else CallException(id, write_data[6], write_data[7], excExceptionConnectionLost);
            return;

            //if ((tcpAsyCl != null) && (tcpAsyCl.Connected))
            //{
            //    try
            //    {
            //        tcpAsyCl.BeginSend(write_data, 0, write_data.Length, SocketFlags.None, new AsyncCallback(OnSend), null);
            //        tcpAsyCl.BeginReceive(tcpAsyClBuffer, 0, tcpAsyClBuffer.Length, SocketFlags.None, new AsyncCallback(OnReceive), tcpAsyCl);
            //    }
            //    catch (SystemException)
            //    {
            //        CallException(id, write_data[6], write_data[7], excExceptionConnectionLost);
            //    }
            //}
            //else CallException(id, write_data[6], write_data[7], excExceptionConnectionLost);
        }

        
        
        private void OnSend(System.IAsyncResult result)
        {
            if (result.IsCompleted == false) CallException(0xFFFF, 0xFF, 0xFF, excSendFailt);
        }

        
        
        private void OnReceive(System.IAsyncResult result)
        {
            //if (result.IsCompleted == false) CallException(0xFF, 0xFF, 0xFF,excExceptionConnectionLost);

            //ushort id = SwapUInt16(BitConverter.ToUInt16(tcpAsyClBuffer, 0));
            //byte unit = tcpAsyClBuffer[6];
            //byte function = tcpAsyClBuffer[7];
            //byte[] data;

            
            
            //if ((function >= fctWriteSingleCoil) && (function != fctReadWriteMultipleRegister))
            //{
            //    data = new byte[2];
            //    Array.Copy(tcpAsyClBuffer, 10, data, 0, 2);
            //}
            
            
            //else
            //{
            //    data = new byte[tcpAsyClBuffer[8]];
            //    Array.Copy(tcpAsyClBuffer, 9, data, 0, tcpAsyClBuffer[8]);
            //}
            
            
            //if (function > excExceptionOffset)
            //{
            //    function -= excExceptionOffset;
            //    CallException(id, unit, function, tcpAsyClBuffer[8]);
            //}
            
            
            //else if (OnResponseData != null) OnResponseData(id, unit, function, data);
        }

        
        
        private byte[] WriteSyncData(byte[] write_data, ushort id)
        {
            if (tcpSynCl == null)
                return null;

            if (tcpSynCl.Connected)
            {
                try
                {
                    tcpSynCl.Send(write_data, 0, write_data.Length, SocketFlags.None);
                    int result = tcpSynCl.Receive(tcpSynClBuffer, 0, tcpSynClBuffer.Length, SocketFlags.None);
                    
                    byte unit = tcpSynClBuffer[6];
                    byte function = tcpSynClBuffer[7];
                    byte[] data;

                    if (result == 0) CallException(id, unit, write_data[7], excExceptionConnectionLost);

                    
                    
                    if (function > excExceptionOffset)
                    {
                        function -= excExceptionOffset;
                        CallException(id, unit, function, tcpSynClBuffer[8]);
                        return null;
                    }


                    else if (function == fctFIFO)
                    {
                        int wLength = SwapUInt16(BitConverter.ToUInt16(tcpSynClBuffer, 10));
                        wLength = wLength * 2;
                        data = new byte[wLength];
                        Array.Copy(tcpSynClBuffer, 12, data, 0, wLength);
                    }

                    else if ((function >= fctWriteSingleCoil) && (function != fctReadWriteMultipleRegister))
                    {
                        data = new byte[2];
                        Array.Copy(tcpSynClBuffer, 10, data, 0, 2);
                    }
                    
                    
                    else
                    {
                        data = new byte[tcpSynClBuffer[8]];
                        Array.Copy(tcpSynClBuffer, 9, data, 0, tcpSynClBuffer[8]);
                    }
                    return data;
                }
                catch (SystemException)
                {
                    CallException(id, write_data[6], write_data[7], excExceptionConnectionLost);
                }
            }
            else CallException(id, write_data[6], write_data[7], excExceptionConnectionLost);
            return null;
        }

        // fifo





        private byte[] CreateFIFOHeader(ushort id, byte unit, ushort startAddress)
        {
            byte[] data = new byte[10];

            byte[] _id = BitConverter.GetBytes((short)id);
            data[0] = _id[1];
            data[1] = _id[0];
            data[5] = 6;
            data[6] = unit;
            data[7] = fctFIFO;
            byte[] _adr = BitConverter.GetBytes((short)IPAddress.HostToNetworkOrder((short)startAddress));
            data[8] = _adr[0];
            data[9] = _adr[1];
            return data;
        }
        
        public void ReadFIFO(ushort id, byte unit, ushort startAddress, ref byte[] values)
        {
            values = WriteSyncData(CreateFIFOHeader(id, unit, startAddress), id);
        }

        //public void ReadFIFO(ushort id, byte unit, ushort startReadAddress, ushort numInputs, ushort startWriteAddress, byte[] values)
        //{
        //    numInputs = 1;

        //    ushort numBytes = Convert.ToUInt16(values.Length);
        //    if (numBytes % 2 > 0) numBytes++;
        //    byte[] data;

        //    data = CreateReadWriteHeader(id, unit, startReadAddress, numInputs, startWriteAddress, Convert.ToUInt16(numBytes / 2));
        //    Array.Copy(values, 0, data, 17, values.Length);
        //    WriteAsyncData(data, id);
        //}

    }
}

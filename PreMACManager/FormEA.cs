﻿using Arction.WinForms.Charting;
using Arction.WinForms.Charting.Axes;
using Arction.WinForms.Charting.SeriesXY;
using CubeEA;
using CubeModbusTCP;
using DM;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace EngineAnalyzer
{
    public partial class TFormEA : Form
    {
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        
        public Boolean _bisfrst = true;
        public Boolean _bisfrstKnock = true;

        public int CHMax = 12;
        //public int FieldMax = 12;

        public Dictionary<int, TTag> DicTag = new Dictionary<int, TTag>();

        List<TRaw> ListRaw = new List<TRaw>();

        public Boolean bIsExport = true;

        public int DataType = 0;

        Stopwatch swModbusRetry = new Stopwatch();
        Stopwatch swEARetry = new Stopwatch();

        public int CyclePerSecond = 0;
        public double Frequency = 0;
        public int CycleCount = 0;
        public Stopwatch swCyclePerSecond = new Stopwatch();

        public int CycleSkipTotalCount = 0;
        public int CycleSkipCount = 0;
        public int CycleSkipPerSecond = 0;
        public Stopwatch swCycleSkipPerSecond = new Stopwatch();

        public float[] Data;// = new float[CHMax];
        public ushort[] HR = new ushort[65535];

        public List<TCH> ListCH = new List<TCH>();
        
        public Thread ThreadMain;
        public Thread ThreadExporter;

        public TFormStatus FormStatus;

        public int ii = 0;

        public Stopwatch sw500ms = new Stopwatch(); // 초당 데이터 처리 개수

        public Stopwatch swDPS = new Stopwatch(); // 초당 데이터 처리 개수
        public Stopwatch swPPS = new Stopwatch(); // 초당 데이터 처리 개수

        public double dDPS = 0;

        int iTotal = 0;

        public CubeModbusMaster m = new CubeModbusMaster();
        public TCubeEASocket ea = new TCubeEASocket();
        
        public bool IsStart { get; private set; }
        public bool ModbusConnectFlag { get; private set; }
        
        public List<Label> ListLabelAlarm = new List<Label>();
        public Dictionary<String, List<Label>> DicLabel = new Dictionary<string, List<Label>>();

        public Dictionary<String, Rectangle> DicSeriesEvent = new Dictionary<String, Rectangle>();
        public Dictionary<String, Rectangle> DicSeriesEventDown = new Dictionary<String, Rectangle>();

        public Dictionary<String, TControlEvent> DicEvent = new Dictionary<String, TControlEvent>();
        public Dictionary<String, TControlEvent> DicEventMouseDown = new Dictionary<String, TControlEvent>();

        public double RPMNow = 0;
        public double RPMMin = 0;
        public double RPMMax = 20000;

        public Boolean bIsCalcDataYAuto = true;
        public Boolean bIsCalcDataRPMYAuto = true;

        public int radioCalcData = 0;

        public Form formLogin = null;

        public TFormEA()
        {
            InitializeComponent();

            TEA.ea = this;
        }

        public List<double> ListCHShift = new List<double>();
        //TFormEAViewer FormViewer = null;


        Image imgSettingNormal = Image.FromFile(TCube.GetPathResource() + "\\setting_normal.png");
        Image imgSettingPressed = Image.FromFile(TCube.GetPathResource() + "\\setting_pressed.png");

        // 111111111111111111111111111111111111111111111
        private void FormEA_Load(object sender, EventArgs e)
        {
            FormSetting = new TFormSetting();
            FormSetting.OnSaveChanged += FormSetting_OnSaveChanged;

            //_chart.Left = 126;

            Width = 1920;
            Height = 1080;

            panelTop.BackgroundImage = imgBack.Image;

            //PictureBox b = new PictureBox();
            //Controls.Add(b);
            //b.Image = imgSubmenu;
            //b.Width = panelSubmenu.Width;
            //b.Height = panelSubmenu.Height;
            //b.Location = panelSubmenu.Location;
            //b.BackColor = Color.Transparent;
            //b.BringToFront();

            panelGraph.Location = new Point(80, 524);

            imageConOn.Location = imageConOff.Location;
            imageLoggingOn.Location = imageLoggingOff.Location;

            typeof(Panel).InvokeMember("DoubleBuffered", BindingFlags.SetProperty
               | BindingFlags.Instance | BindingFlags.NonPublic, null,
               panelGraphCalcDataBottom, new object[] { true });

            typeof(Panel).InvokeMember("DoubleBuffered", BindingFlags.SetProperty
               | BindingFlags.Instance | BindingFlags.NonPublic, null,
               panelGraphCalc, new object[] { true });

            typeof(Panel).InvokeMember("DoubleBuffered", BindingFlags.SetProperty
               | BindingFlags.Instance | BindingFlags.NonPublic, null,
               panelTop, new object[] { true });

            typeof(Panel).InvokeMember("DoubleBuffered", BindingFlags.SetProperty
               | BindingFlags.Instance | BindingFlags.NonPublic, null,
               panelGraph, new object[] { true });

            typeof(Panel).InvokeMember("DoubleBuffered", BindingFlags.SetProperty
               | BindingFlags.Instance | BindingFlags.NonPublic, null,
               panelAlarm, new object[] { true });

            typeof(Panel).InvokeMember("DoubleBuffered", BindingFlags.SetProperty
               | BindingFlags.Instance | BindingFlags.NonPublic, null,
               panelRPM, new object[] { true });

            string version = System.Windows.Forms.Application.ProductVersion;

            Text = "Engine Analyzer " + version;

            //----------- event
            //DicEvent.Add("")

            labelRPM.Font = TEA.fontMdCn62;
            labelRPM.Paint += TEA.labelAA_Paint;

            labelBottomMsg.Font = TEA.fontMd14;
            labelMenuSelected.Font = TEA.fontMd16;
            labelRPMTitle.Font = TEA.fontMd16;
            labelAlarm.Font = TEA.fontMd16;
            labelCalcData.Font = TEA.fontMd16;
            
            labelBottomMsg.Paint += TEA.labelAA_Paint;
            labelMenuSelected.Paint += TEA.labelAA_Paint;
            labelRPMTitle.Paint += TEA.labelAA_Paint;
            labelAlarm.Paint += TEA.labelAA_Paint;
            labelCalcData.Paint += TEA.labelAA_Paint;

            labelMenuMonitoring.Paint += TEA.labelAA_Paint;
            labelMenuCal.Paint += TEA.labelAA_Paint;
            labelMenuDiagnosis.Paint += TEA.labelAA_Paint;

            labelMenuMonitoring.Font = TEA.fontMd16;
            labelMenuCal.Font = TEA.fontMd16;
            labelMenuDiagnosis.Font = TEA.fontMd16;

            editGraphPYMin.Font = TEA.fontMd10;
            editGraphPYMax .Font = TEA.fontMd10;
            editCalcDataYMax.Font = TEA.fontMd10;
            editCalcDataYMin.Font = TEA.fontMd10;
            editCalcDataRPMYMax.Font = TEA.fontMd10;
            editCalcDataRPMYMin.Font = TEA.fontMd10;

            comboY.Font = TEA.fontMd10;

            //-------------
            DicLabel.Add("tabAlarm", new List<Label>());

            DicLabel["tabAlarm"].Add(labelAlarmFault);
            DicLabel["tabAlarm"].Add(labelAlarmHKnock);
            DicLabel["tabAlarm"].Add(labelAlarmLKnock);
            DicLabel["tabAlarm"].Add(labelAlarmMisfire);
            DicLabel["tabAlarm"].Add(labelAlarmSys);

            DicLabelSelected.Add(labelAlarmFault, labelAlarmFault);

            //-------------
            DicLabel.Add("tabCalcData", new List<Label>());

            DicLabel["tabCalcData"].Add(labelCalcDataHR);
            DicLabel["tabCalcData"].Add(labelCalcDataIMEP);
            DicLabel["tabCalcData"].Add(labelCalcDataKnockIntst);
            DicLabel["tabCalcData"].Add(labelCalcDataMisfire);
            DicLabel["tabCalcData"].Add(labelCalcDataPegging);
            DicLabel["tabCalcData"].Add(labelCalcDataPmax);

            DicLabelSelected.Add(labelCalcDataHR, labelCalcDataHR);

            //-------------
            DicLabel.Add("tabGraph", new List<Label>());

            DicLabel["tabGraph"].Add(labelGraphCalc);
            DicLabel["tabGraph"].Add(labelGraph);

            DicLabelSelected.Add(labelGraph, labelGraph);

            // ----------
            UIInit();

            //-------------
            foreach (KeyValuePair<String, List<Label>> vp in DicLabel)
            {
                foreach (Label label in vp.Value)
                {
                    label.Tag = vp;
                    label.Paint += label_Paint;
                    label.MouseDown += label_MouseDown;
                    label.MouseUp += label_MouseUp;
                    label.MouseEnter += label_MouseEnter;
                    label.MouseLeave += label_MouseLeave;

                    if (vp.Key.StartsWith("tab"))
                    {
                        label.Font = TEA.fontMd16;
                    }
                }
            }

            // ------------------
            Font f1 = new Font(TEA.privateFonts.Families[0], 10, GraphicsUnit.Pixel);
            labelCalcDataAvg1.Font = f1;
            labelCalcDataAvg1.Paint += TEA.labelAA_Paint;

            f1 = new Font(TEA.privateFonts.Families[1], 20, GraphicsUnit.Pixel);
            labelCalcDataAvg.Font = f1;
            labelCalcDataAvg.Paint += TEA.labelAA_Paint;
            
            for (int i = 0; i < 12; i++)
            {
                ListCHShift.Add(0);
            }

            Data = new float[CHMax];

            InitUI();

            ChartInit();
            //ChartInit(_chartCyl);
            //ChartInit(_chartKnock);

            // view추가
            //FormViewer = new TFormEAViewer();

            //FormViewer.TopLevel = false;
            //FormViewer.FormBorderStyle = FormBorderStyle.None;
            //FormViewer.Dock = DockStyle.Fill;
            //FormViewer.Visible = true;

            //TabHistory.Controls.Add(FormViewer);
            //FormViewer.BringToFront();

            FormSetting.panelForm.Visible = false;
            //f.TopLevel = false;
            Controls.Add(FormSetting.panelForm);
            FormSetting.panelForm.Location = new Point(0, 75);
            
            ThreadMain = new Thread(new ThreadStart(ThreadTimer));
            ThreadMain.Start();
            ThreadExporter = new Thread(new ThreadStart(ThreadExport));
            ThreadExporter.Start();
        }
        
        public void Msg(String AMsg)
        {
            labelBottomMsg.Text = AMsg;
        }

        private void FormSetting_OnSaveChanged(bool AState)
        {
            IsSave = AState;
        }

        private void ChartInitKnock(Chart AChart)
        {
            AChart.EnableZoomAndPanControls(ChartCursorSelected, ChartCursorMoved,
                zoomChanged,
                new ChartOption()
                {
                    ContextMenuAllowToHideSeries = true,
                    XAxisPrecision = 0,
                    YAxisPrecision = 2
                });
            AChart.AxisViewChanging += OnAxisViewChanges;
            AChart.AxisViewChanged += OnAxisViewChanges;

            AChart.SetChartToolState(MSChartExtensionToolState.ZoomX);

            //for (int i = 0; i < 1024; i++)
            //{
            //    AChart.Series[0].Points.AddY(i);
            //}
        }

        private void OnAxisViewChanges(object sender, ViewEventArgs viewEventArgs)
        {
            Debug.Fail("Don't worry, this event is never raised.");
        }

        private void ChartCursorSelected(Chart sender, ChartCursor e)
        {
            //txtChartSelect.Text = e.X.ToString("F4") + ", " + e.Y.ToString("F4");
            //Debug.WriteLine("Cursor Position: " + txtChartSelect.Text + " @ " + e.ChartArea.Name);

            PointF diff = sender.CursorsDiff();
            //txtCursorDelta.Text = diff.X.ToString("F4") + ", " + diff.Y.ToString("F4");
        }

        private void ChartCursorMoved(Chart sender, ChartCursor e)
        {
            //txtChartValue.Text = e.X.ToString("F4") + ", " + e.Y.ToString("F4");
        }

        private void zoomChanged(Chart sender)
        {
        }

        public double GetHR(int AAddr, int ARatio = 10, Boolean ASigned = true)
        {
            double d = 0;

            if (ASigned)
            {
                d = (Int16)HR[AAddr];
                if (ARatio > 1)
                {
                    d = d / ARatio;
                }
            }

            return d;
        }

        private void ButtonRTNew_ItemClick(object sender, EventArgs e)
        {
            //FormRT f = new FormRT();
            //f.TopLevel = false;
            //f.Dock = DockStyle.Fill;
            //f.FormBorderStyle = FormBorderStyle.None;

            //PanelMain.Controls.Add(f);
            //f.Visible = true;

            //f.Show();
        }

        void setBorder(Control ctl, Color col, int width, BorderStyle style)
        {
            if (col == Color.Transparent)
            {
                Panel pan = ctl.Parent as Panel;
                if (pan == null) { throw new Exception("control not in border panel!"); }
                ctl.Location = new Point(pan.Left + width, pan.Top + width);
                ctl.Parent = pan.Parent;
                pan.Dispose();

            }
            else
            {
                Panel pan = new Panel();
                pan.BorderStyle = style;
                pan.Size = new Size(ctl.Width + width * 2, ctl.Height + width * 2);
                pan.Location = new Point(ctl.Left - width, ctl.Top - width);
                pan.BackColor = col;
                pan.Parent = ctl.Parent;
                ctl.Parent = pan;
                ctl.Location = new Point(width, width);
            }
        }

        public void SettingLoad()
        {
            FormSetting.editFolder.Text = TCube.GetValue("exportfolder", Application.StartupPath + "\\raw\\");
            FormSetting.editHost.Text = TCube.GetValue("EditEAIP", "192.168.10.15");
            FormSetting.editPort.Text = TCube.GetValue("EditEAPort", "503");
            FormSetting.editSaveInterval.Text = TCube.GetValue("editSaveInterval", "10");
            FormSetting.editTO.Text = TCube.GetValue("editTO", "1000");
            EditCHMax.Text = TCube.GetValue("EditCHMax", "12");
            EditIP.Text = TCube.GetValue("EditIP", "192.168.10.15");
            EditPort.Text = TCube.GetValue("EditPort", "502");

            PanelData.Width = (Convert.ToInt32(TCube.GetValue("PanelData.Width", "272")));
            try
            {
                ea.UseCHMax = Convert.ToInt32(EditCHMax.Text);
                TEA.CHMaxUse = ea.UseCHMax;
            }
            catch (Exception E)
            {
                TCS.LogError(E);
            }

            try
            {
                Directory.CreateDirectory(FormSetting.editFolder.Text);
            }
            catch(Exception e)
            {

            }

            EditDelayForRecv.Text = TCube.GetValue("DelayForRecv", "0");
            try
            {
                ea.DelayForRecv = Convert.ToInt32(EditDelayForRecv.Text);
            }
            catch (Exception E)
            {
                TCS.LogError(E);
            }

            CheckYAuto.Checked = TCube.GetValue("CheckYAuto", "1") == "1";

            FormSetting.CheckExportChecked = TCube.GetValue("CheckExport", "1") == "1";

            EditXCount.Text = TCube.GetValue("EditXCount", "1440");

            comboDataType.Text = TCube.GetValue("comboDataType", "INT16");
            String sDataType = comboDataType.Text;
            if (sDataType == "INT16")
                DataType = 0;
            else if (sDataType == "UINT16")
                DataType = 1;

            SettingApply();
        }

        TFormLog FormLog = null;

        public void ControlCursorAdd(Control sender)
        {
            sender.MouseLeave += obj_MouseLeave;
            sender.MouseEnter += obj_MouseEnter;
        }

        public void comboInit(Panel sender)
        {
            sender.Click += combo_Click;
        }

        public void InitUI()
        {
            FormLog = new TFormLog();

            FormLog.Show();

            FormStatus = new TFormStatus(this);
            FormStatus.TopLevel = false;
            FormStatus.FormBorderStyle = FormBorderStyle.None;
            PanelData.Controls.Add(FormStatus);
            FormStatus.Dock = DockStyle.Fill;
            FormStatus.Show();

            // combo
            comboY.SelectedIndexChanged += comboY_SelectedIndexChanged;
            setBorder(comboY, TEA.ColorButtonBorder, 1, BorderStyle.FixedSingle);

            ControlCursorAdd(comboYMax);
            ControlCursorAdd(comboYMin);
            ControlCursorAdd(comboY);

            comboInit(comboYMax);
            comboInit(comboYMin);

            TagLoad();

            //EditFolder.Text = Application.StartupPath + "\\raw\\";

            ControlCursorAdd(buttonGraphPAxisReset);
            ControlCursorAdd(panelCalcDataRPMYReset);
            ControlCursorAdd(panelCalcDataYReset);

            ControlCursorAdd(panelMenuCalibration);
            ControlCursorAdd(panelMenuDignosis);
            ControlCursorAdd(panelMenuCylKnock);
            ControlCursorAdd(panelMenuCylTDC);
            ControlCursorAdd(panelMonitoring);


            ControlCursorAdd(labelMenuMonitoring);
            ControlCursorAdd(labelMenuDiagnosis);
            ControlCursorAdd(labelMenuCal);

            m.OnStateChanged += OnStateChanged;
            ea.OnStateChanged += OnEAStateChanged;
            
            //RibbonMain.SelectedPage = PageRT;

            CheckListCH.Tag = 1;
            for (int i = 0; i < CHMax; i++)
            {
                ListCH.Add(new TCH(i));
                CheckListCH.Items.Add("CH" + (i + 1).ToString(), true);
            }
            CheckListCH.Tag = null;

            ea.OnResponseCHData += OnResponseData;
            ea.OnResponseCalcData += OnResponseCalcData;
            ea.OnResponseKnockData += OnResponseKnockData;

            SettingLoad();

            chartPMax.Series[0].SetCustomProperty("PixelPointWidth", "40");

            for (int i = 1; i <= 12; i++)
            {
                chartPMax.ChartAreas["ChartArea1"].AxisX.CustomLabels.Add(i - 0.5, i + 0.5, "cyl." + i.ToString());

                //chartPMax.Series[1].Points.AddXY(i - 0.5, 0);
            }
            chartPMax.Series[1].Points.AddXY(0.7, 0);
            chartPMax.Series[1].Points.AddXY(12.3, 0);
            //chartPMax.ChartAreas[0].Position.X = 4;
            //chartPMax.ChartAreas[0].Position.Width = 92;
            //Chart1.ChartAreas("ChartArea1").AxisX.CustomLabels.Add(1.5, 2.5, "yr2")
            //Chart1.ChartAreas("ChartArea1").AxisX.CustomLabels.Add(2.5, 3.5, "yr3")

            chartPMax.ChartAreas[0].AxisY.LabelStyle.Format = "#,##0";
            _chartCalcData.ChartAreas[0].AxisY.LabelStyle.Format = "#,##0";
            _chartCalcDataRPM.ChartAreas[0].AxisY.LabelStyle.Format = "#,##0";

            chartPMax.ChartAreas[0].AxisY.LabelStyle.Font = TEA.fontMd10;
            _chartCalcData.ChartAreas[0].AxisY.LabelStyle.Font = TEA.fontMd10;
            _chartCalcDataRPM.ChartAreas[0].AxisY.LabelStyle.Font = TEA.fontMd10;

            chartPMax.ChartAreas[0].AxisX.LabelStyle.Font = TEA.fontMd10;
            _chartCalcData.ChartAreas[0].AxisX.LabelStyle.Font = TEA.fontMd10;
            _chartCalcDataRPM.ChartAreas[0].AxisX.LabelStyle.Font = TEA.fontMd10;
        }

        public Boolean IsSave
        {
            get
            {
                return imageLoggingOn.Visible;
            }
            set
            {
                if (value)
                {
                    imageLoggingOn.Visible = true;
                }
                else
                {
                    imageLoggingOn.Visible = false;
                }
            }
        }

        public float[] ConvertByteToFloat(byte[] array)
        {
            float[] floatArr = new float[array.Length / 4];
            for (int i = 0; i < floatArr.Length; i++)
            {
                if (BitConverter.IsLittleEndian)
                {
                    Array.Reverse(array, i * 4, 4);
                }
                floatArr[i] = BitConverter.ToSingle(array, i * 4);
            }
            return floatArr;
        }

        public void CHAdd(ref float[] AData, int ACycle = 0)
        {
            String CurrentTime;
            Random Rand_Num = new Random();

            CurrentTime = DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString();

            int nAmountToUpdate = AData.Length / CHMax;
        }

        // 111111111111111111111111111111111111
        public void ThreadTimer()
        {
            swModbusRetry.Reset();
            swModbusRetry.Start();
            swEARetry.Reset();
            swEARetry.Start();

            Stopwatch swLimit = new Stopwatch();
            swLimit.Start();

            while (Thread.CurrentThread.ThreadState != System.Threading.ThreadState.AbortRequested)
            {
                Thread.Sleep(1);

                if (ModbusConnectFlag)
                {
                    TCS.LogMsg("----- MODBUS Connect Try -----");
                    ModbusConnectFlag = false;
                    this.Invoke(new Action(delegate () // this == Form 이다. Form이 아닌 컨트롤의 Invoke를 직접호출해도 무방하다.
                    {
                        m.IP = EditIP.Text;
                        m.Port = Convert.ToUInt16(EditPort.Text);
                    }));
                    m.disconnect();
                    m.connect();
                }

                //if (m.connected == false)
                //{
                //    if (swModbusRetry.IsRunning)
                //    {
                //        if (swModbusRetry.ElapsedMilliseconds >= 5000)
                //        {
                //            ModbusConnectFlag = true;

                //            swModbusRetry.Stop();
                //        }
                //    }
                //}


                if (sw500ms.IsRunning == false)
                {
                    sw500ms.Start();
                }

                if (sw500ms.ElapsedMilliseconds >= 500)
                {
                    TCS.eaCmd.FlagAICalData = IsCalTDC;
                    if (IsCalTDC)
                    {
                        FormCalTDC.TimerTick();
                    }
                }
                if (swDPS.IsRunning == false)
                    swDPS.Start();

                if (swDPS.ElapsedMilliseconds >= 1000)
                {
                    swDPS.Restart();

                    dDPS = iTotal;
                    iTotal = 0;
                }

                if (swCycleSkipPerSecond.IsRunning == false)
                    swCycleSkipPerSecond.Start();

                if (swCycleSkipPerSecond.ElapsedMilliseconds >= 1000)
                {
                    swCycleSkipPerSecond.Restart();

                    CycleSkipPerSecond = CycleSkipCount;
                    CycleSkipCount = 0;
                }

                if (swCyclePerSecond.IsRunning == false)
                    swCyclePerSecond.Start();

                if (swCyclePerSecond.ElapsedMilliseconds >= 1000)
                {
                    swCyclePerSecond.Restart();

                    CyclePerSecond = CycleCount;
                    Frequency = CycleCount;
                    CycleCount = 0;
                }

                //if (swPPS.IsRunning == false)
                //    swPPS.Start();

                //if (swPPS.ElapsedMilliseconds >= 1000)
                //{
                //    swPPS = Stopwatch.StartNew();

                //    dDPS = iTotal;
                //    iTotal = 0;
                //}

                if (TCube.IsTerminate)
                {
                    break;
                }
                if (m.connected)
                {
                    GetStatus();
                    //if (IsStart)
                    //{
                    //    GetFIFO();
                    //}
                }
            }
        }

        public void GetStatus()
        {
            for (int i = 0; i < CHMax; i++)
            {
                byte[] bb = new byte[65535];
                m.ReadHoldingRegister(1, 1, 500, 100, ref bb); // 500은 0부터 시작하는 주소

                if (bb != null)
                {
                    if (bb.Length > 1)
                    {
                        int iAddrBase = 501; // 실제 HR에서 다루는 주소 체계. 1부터 시작
                        for (int iCnt = 0; iCnt < 100; iCnt++)
                        {
                            int iAddr = iAddrBase + iCnt;

                            UInt16 w = (UInt16)(BitConverter.ToUInt16(bb, iCnt * 2));
                            w = SwapBytes(w);

                            HR[iAddr] = w;

                            if (DicTag.ContainsKey(iAddr))
                            {
                                DicTag[iAddr].RawValue = w;
                            }
                        }
                    }
                }
            }
        }


        // 22222222222222222222222222222
        public void ThreadExport()
        {
            while (Thread.CurrentThread.ThreadState != System.Threading.ThreadState.AbortRequested)
            {
                Thread.Sleep(1);

                if (TCube.IsTerminate)
                {
                    break;
                }

                int iCycleCount = 1;
                Int32.TryParse(EditCycleCount.Text, out iCycleCount);

                if (ListRaw.Count < iCycleCount)
                    continue;

                if (ListRaw.Count > 0)
                {
                    List<TRaw> l = null;

                    lock (ListRaw)
                    {
                        l = ListRaw;
                        ListRaw = new List<TRaw>();
                    }

                    try
                    {
                        ThreadRawsToGraph(-2, iCycleCount, l);

                        ThreadHRToShow();

                        CalcDataToUI();

                        //foreach (TRaw r in l)
                        //{
                        //// 모든 CH 받기
                        //float[] f = new float[1440 * 12];
                        //if (r.IsEmpty == false)
                        //{
                        //    int ich = 0;
                        //    while (ich < 12)
                        //    {
                        //        int iIdx = ich * 2888 + 8;
                        //        int iArray = ich * 1440;
                        //        int iCnt = 0;
                        //        while (iCnt < 1440)
                        //        {
                        //            UInt16 w = (UInt16)(BitConverter.ToUInt16(r.Raws, iIdx));
                        //            f[iArray] = w / 10;

                        //            iIdx += 2;
                        //            iArray++;
                        //            iCnt++;
                        //        }
                        //        ich++;
                        //    }
                        //}
                        //this.Invoke(new Action(delegate () // this == Form 이다. Form이 아닌 컨트롤의 Invoke를 직접호출해도 무방하다.
                        //{
                        //    if (r.IsEmpty)
                        //    {
                        //        if (CheckGraphSkip.Checked == false)
                        //        {
                        //            CHAdd(ref EA.EmptyData, r.Cycle);
                        //        }
                        //    }
                        //    else
                        //        CHAdd(ref f, r.Cycle);
                        //}));

                        //this.Invoke(new Action(delegate () // this == Form 이다. Form이 아닌 컨트롤의 Invoke를 직접호출해도 무방하다.
                        //{
                        //}));
                        //}
                    }
                    catch (Exception E)
                    {
                        TCS.LogError(E);
                    }

                    if (FormSetting.CheckExportChecked)
                    {
                        Export(l);
                    }
                    l.Clear();
                }        
            }
        }

        private void barButtonItem2_ItemClick(object sender, EventArgs e)
        {
        }

        private void barEditItem1_ItemClick(object sender, EventArgs e)
        {

        }

        private void FormMain_FormClosed(object sender, FormClosedEventArgs e)
        {

        }

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show(this, "Exit Application?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
            {
                e.Cancel = true;
                return;
            }

            TCube.IsTerminate = true;

            ThreadMain.Abort();
            ThreadExporter.Abort();

            System.Diagnostics.Process.GetCurrentProcess().Kill();
            
            //using (WaitDialogForm dlg = new WaitDialogForm(
            //        "Terminating...", "Terminate Application", new Size(200, 50), this))
            //{
            while (ThreadMain.IsAlive)
                {
                    Application.DoEvents();
                }
            //}
        }

        private void ButtonConnect_ItemClick(object sender, EventArgs e)
        {
            try
            {
                TCube.SetValue("EditIP", EditIP.Text);
                TCube.SetValue("EditPort", EditPort.Text);

                ModbusConnectFlag = true;
            }
            catch (Exception E)
            {
                TCS.LogError(E);
            }
        }

        private void ButtonStart_ItemClick(object sender, EventArgs e)
        {
            IsStart = true;
            ea.DoSendStart();
        }

        private void TimerTick_Tick(object sender, EventArgs e)
        {
            String s1 = "";
            String s2 = "";

            if (m.connected)
                s1 = "MODBUS OPEN";
            else 
                s1 = "MODBUS CLOSE";
            if (ea.connected)
                s2 = "SIGNAL OPEN";
            else
                s2 = "SIGNAL CLOSE";

            EditStatus.Text = s1 + "\r\n" + s2;

            //if (IsStart)
            //{
            //    EditStatus.Text = "Running";
            //}
            //else
            //{
            //    EditStatus.Text = "Stopped";
            //}

            LabelDPS.Text = "Data Per Second : " + (dDPS / CHMax).ToString("0");
            int i = ListCH[0].Queue.Count;
            if (i == 0)
            {
                LabelExport.Text = "Write Queue is Empty";
            }
            else
            {
                LabelExport.Text = "Write Overhead : " + ListCH[0].Queue.Count;
            }

            for (int k = 0; k < CHMax; k++)
            {
                CheckListCH.Items[k] = "CH" + (k + 1).ToString() + "=" + Data[k].ToString("0.0");

            }

            if (ea.connected == true)
            {
                if (LabelConnection.Text != "Connected")
                {
                    LabelConnection.Text = "Connected";
                    imageConOn.Visible = true;
                    Msg("Connected");
                }
            }
            else
            {
                if (LabelConnection.Text != "Disconnected")
                {
                    LabelConnection.Text = "Disconnected";
                    imageConOn.Visible = false;
                    Msg("Disconnected");
                }
            }

            LabelReq.Text = "(" + ea.PacketReqCount1M.ToString() + "M)" + ea.PacketReqCount.ToString();
            LabelCHGetCnt.Text = ea.GetCHCnt.ToString();
            LabelCycle.Text = ea.LastCycle.ToString();

            LabelRawQueue.Text = ListCH[0].Raws.Count.ToString();
            LabelSaveQueue.Text = ListRaw.Count.ToString();

            LabelCPS.Text = CyclePerSecond.ToString();

            if (CheckGraphSkip.Checked)
            {

                LabelCSPS.Text = "(OK)";
                LabelCSC.Text = "(OK)";
            }
            else
            {

                LabelCSPS.Text = CycleSkipPerSecond.ToString();
                LabelCSC.Text = CycleSkipTotalCount.ToString();
            }

            // RPM
            RPMNow = GetHR(506, 10);

            panelRPM.Invalidate();

            // Alarm

            panelAlarm.Invalidate();

            FormStatus.UpdateStatus();

            panelTop.Invalidate();
        }

        public ushort SwapBytes(ushort x)
        {
            return (ushort)((ushort)((x & 0xff) << 8) | ((x >> 8) & 0xff));
        }

        public uint SwapBytes(uint x)
        {
            return ((x & 0x000000ff) << 24) +
                   ((x & 0x0000ff00) << 8) +
                   ((x & 0x00ff0000) >> 8) +
                   ((x & 0xff000000) >> 24);
        }

        public UInt32 ReverseBytes(UInt32 value)
        {
            return (value & 0x000000FFU) << 24 | (value & 0x0000FF00U) << 8 |
                (value & 0x00FF0000U) >> 8 | (value & 0xFF000000U) >> 24;
        }


        public void GetRAW()
        {

        }

        public void GetFIFO()
        {
            for (int i = 0; i < CHMax; i++)
            {
                byte[] bb = new byte[65535];
                m.ReadFIFO(1, 1, Convert.ToUInt16(1000 + i), ref bb);

                //for (int k = 0; k < bb.Length; k += 2)
                //{
                //    bb[k] = (byte)i;
                //    bb[k + 1] = 0;
                //}

                if (bb != null)
                {
                    if (bb.Length > 1)
                    {
                        // 마지막 데이터
                        UInt16 w = (UInt16)(BitConverter.ToUInt16(bb, 0));
                        w = SwapBytes(w);

                        //Data[i] = w;
                        ////////////////

                        lock (ListCH[i].Raws)
                        {
                            ListCH[i].Raws.Add(new TCHDataSet { Parent = ListCH[i], Raw = bb, Time = DateTime.Now });
                        }
                        //lock (ListCH[i].Queue)
                        //{
                        //    ListCH[i].Queue.Add(new TCHDataSet { Parent = ListCH[i], Raw = bb, Time = DateTime.Now });
                        //}
                    }
                }                
            }

            ProcessRAW();
        }

        public void ProcessRAW()
        { 
            int iMax = 0;
            for (int i = 0; i < CHMax; i++)
            {
                if (ListCH[i].FirstRaw() != null)
                    if (ListCH[i].FirstRaw().Length > iMax)
                        iMax = ListCH[i].FirstRaw().Length;
            }

            iMax = iMax / 2;

            //if (Convert.ToInt32(EditXCount.Text) < iMax)
            //{
            //    this.Invoke(new Action(delegate () // this == Form 이다. Form이 아닌 컨트롤의 Invoke를 직접호출해도 무방하다.
            //    {
            //        if (PanelXCountAlarm.Visible == false)
            //            PanelXCountAlarm.Visible = true;
            //    }));
            //}
            //else
            //{
            //    this.Invoke(new Action(delegate () // this == Form 이다. Form이 아닌 컨트롤의 Invoke를 직접호출해도 무방하다.
            //    {
            //        if (PanelXCountAlarm.Visible == true)
            //            PanelXCountAlarm.Visible = false;
            //    }));
            //}
            int iCycle = 0;
            if (iMax > 0)
            {
                float[] d = new float[CHMax * iMax];
                for (int i = 0; i < CHMax; i++)
                {
                    if (ListCH[i].FirstRaw() != null)
                    {
                        iCycle = ListCH[i].Raws[0].Cycle;
                        String sDebug = "";

                        int j = 0;
                        int iCnt = 0;
                        while (j < ListCH[i].FirstRaw().Length - 1)
                        {
                            int idx = i * iMax + iCnt;

                            UInt16 w = (UInt16)(BitConverter.ToUInt16(ListCH[i].FirstRaw(), j));

                            //w = SwapBytes(w);

                            d[idx] = w / 10;
                            if (j == 0)
                                Data[i] = d[idx];

                            //sDebug = sDebug + "[" + iCnt.ToString() + "] = " + w.ToString() + "\r\n";

                            j = j + 2;
                            iCnt = iCnt + 1;

                            iTotal++;
                        }

                        Console.WriteLine("1:" + d[0].ToString() + "/" + d[1439].ToString());
                        //TCS.LogMsg(sDebug);

                        lock (ListCH[i].Raws)
                        {
                            ListCH[i].Raws.RemoveAt(0);
                        }
                    }
                }

                if (ea.LastCycle == -1)
                {
                    ea.LastCycle = iCycle;
                }
                while (ea.LastCycle + 1 < iCycle)
                {
                    TCS.LogMsg("### Cycle Skipped ---> UI = " + ea.LastCycle.ToString() + " / Packet = " + iCycle.ToString());
                    CHAdd(ref TEA.EmptyData, ea.LastCycle);

                    CycleSkipTotalCount += iCycle - ea.LastCycle - 1;
                    CycleSkipCount += iCycle - ea.LastCycle;

                    if (CycleSkipTotalCount > 65535)
                        CycleSkipTotalCount = 0;

                    ea.LastCycle = iCycle;
                    //ea.LastCycle++;
                }

                this.Invoke(new Action(delegate () // this == Form 이다. Form이 아닌 컨트롤의 Invoke를 직접호출해도 무방하다.
                {
                        CHAdd(ref d, iCycle);
                }));
            }
        }
        
        private void ButtonSetting_ItemClick(object sender, EventArgs e)
        {

        }

        private void ButtonYAxisSet_ItemClick(object sender, EventArgs e)
        {
            try
            {
                //_chart.ViewXY.YAxes[0].SetRange(-10, 100); //Y축 범위 (-10~100) 정하기
                //_chart.ViewXY.YAxes[0].MajorDiv = Convert.ToInt32(ComboPhaseShiftCH.Text); //Y축의 나타낼 숫자 개수 
                //_chart.ViewXY.YAxes[0].MajorDivCount = Convert.ToInt32(EditPSAngle.Text); //Y축에 숫자 간의 값차이(Gap)?


                _chart.ViewXY.YAxes[0].SetRange(Convert.ToDouble(editGraphPYMin.Text), Convert.ToDouble(editGraphPYMax.Text));
            }
            catch (Exception E)
            {
                TCS.LogError(E);
            }
        }

        private void EditXCount_TextChanged(object sender, EventArgs e)
        {
            int i = 0;
            if (Int32.TryParse(EditXCount.Text, out i))
            {
                if (i > 0)
                {
                    _chart.ViewXY.XAxes[0].SetRange(0, i);
                }
            }
        }

        private void editGraphPYMin_ItemClick(object sender, EventArgs e)
        {

        }

        private void editGraphPYMax_TextChanged(object sender, EventArgs e)
        {
            //try
            //{
            //    double d = Convert.ToDouble(editGraphPYMax.Text);
            //    if (d > PegoRT.PeGrid.Configure.ManualMinY)
            //    {
            //        PegoRT.PeGrid.Configure.ManualMaxY = d;
            //    }
            //    
            //}
            //catch (Exception E)
            //{
            //    //TCS.LogError(E);
            //}
        }
        
        public void OnStateChanged(Boolean AState)
        {
            //if (AState)
            //    ButtonConnect.Text = "Disconnect";
            //else
            //    ButtonConnect.Text = "Connect";
        
            if (AState == false)
            {
                if (swModbusRetry.IsRunning == false)
                {
                    swModbusRetry.Reset();
                    swModbusRetry.Start();
                }
            }
        }

        public void OnEAStateChanged(Boolean AState)
        {
            this.Invoke(new Action(delegate () // this == Form 이다. Form이 아닌 컨트롤의 Invoke를 직접호출해도 무방하다.
            {
                if (AState)
                {
                    //ButtonConnectToggle.Text = "Disconnect";

                    //PanelDisconnect.Visible = false;
                    //ButtonStart.Enabled = true;
                    //ButtonStop.Enabled = true;

                    if (IsStart)
                    {
                        ea.DoSendStart();
                    }
                }
                else
                {
                    //ButtonConnectToggle.Text = "Connect";

                    //PanelDisconnect.Visible = true;
                    //ButtonStart.Enabled = false;
                    //ButtonStop.Enabled = false;

                    if (IsStart)
                    {
                        IsStart = false;
                    }
                }

                if (AState)
                    TCS.LogMsg("----- Connect OK -----");
                else
                    TCS.LogMsg("----- Disconnect OK -----");

                if (AState == false)
                {
                    if (swEARetry.IsRunning == false)
                    {
                        swEARetry.Reset();
                        swEARetry.Start();
                    }
                }
            }));
        }

        private void dockPanel3_Click(object sender, EventArgs e)
        {

        }

        public void LogLoad(String AFile)
        {
            //using (WaitDialogForm dlg = new WaitDialogForm(
            //        "다시 그리는중...", "그래프 확대/축소", new Size(200, 50), this))
            //{
            //    //foreach (TDCPCol dc in ListDCPCol)
            //    //{
            //    //    String sLog = DrawPoints(
            //    //        dt1,
            //    //        dt2,
            //    //        dc.Line,
            //    //        dc.DateTimes,
            //    //        dc.Values
            //    //    );
            //    //    TCS.LogMsg(sLog);
            //    //}
            //}
        }

        private void CheckListCH_CheckMemberChanged(object sender, EventArgs e)
        {
        }

        private void CheckCHAllVIsible_CheckedChanged(object sender, EventArgs e)
        {
            CheckListCH.Tag = 1;
            for (int i = 0; i < CHMax; i++)
            {
                CheckListCH.SetItemChecked(i, CheckCHAllVIsible.Checked);
            }
            CheckListCH.Tag = null;
            
            for (int i = 0; i < CHMax; i++)
            {
                Boolean b = CheckCHAllVIsible.Checked;
                _chart.ViewXY.SampleDataSeries[i].Visible = b;
            }
            
        }
        
        private void ButtonStop_ItemClick(object sender, EventArgs e)
        {
            IsStart = false;
            ea.DoSendStop();
        }

        public byte[] GetBytesAlt(UInt16[] values)
        {
            var result = new byte[values.Length * sizeof(UInt16)];
            Buffer.BlockCopy(values, 0, result, 0, result.Length);
            return result;
        }

        public byte[] BufCalcData = new Byte[296];

        public Boolean bNewFile = true;
        public int lastIdx = 0;
        public String lastTime = "";
        public Boolean Export(List<TRaw> AList)
        {
            try
            {
                // 1 = ch
                // 4 = length
                // 4 = 
                byte[] baFF = new byte[2];
                baFF[0] = 0xFF;
                baFF[1] = 0xFF;

                String sTime = DateTime.Now.ToString("yyMMdd_HHm");
                if (sTime != lastTime)
                {
                    lastTime = sTime;
                    lastIdx = 0;
                }

                String sFileBase =
                    FormSetting.editFolder.Text
                    + "\\RAW_"
                    + DateTime.Now.ToString("yyMMdd_HHmm");

                String sFile = sFileBase.Substring(0, sFileBase.Length - 1) + "0_" + lastIdx.ToString() + ".dat";

                if (bNewFile)
                {
                    bNewFile = false;
                    while (File.Exists(sFile))
                    {
                        lastIdx++;
                        sFile = sFileBase.Substring(0, sFileBase.Length - 1) + "0_" + lastIdx.ToString() + ".dat";
                    }
                }

                FileStream fs = new FileStream(sFile, FileMode.Append, FileAccess.Write);

                //// 2 = length
                //byte[] intBytes = BitConverter.GetBytes(AData.Raw.Length);
                //Array.Reverse(intBytes);
                //byte[] result = intBytes;
                //fs.Write(result, 0, result.Length);

                int iRow = 0;
                while (iRow < AList.Count)
                {
                    // 0. RAW 종류 분기용 코드 = 시간(8)+채널별OFFSET(2x12)+나머지 34568 제외 영역
                    {
                        int iOffset = 0;//2880*12+8

                        // 0-1. Time
                        byte[] Bits = BitConverter.GetBytes(AList[iRow].Time.ToBinary());
                        fs.Write(Bits, 0, Bits.Length);
                        iOffset += 8;

                        // 0-2. INFO CODE
                        fs.Write(baFF, 0, baFF.Length);
                        iOffset += 2;

                        fs.WriteByte(Convert.ToByte(ea.UseCHMax));
                        iOffset += 1;

                        fs.WriteByte(0);
                        iOffset += 1;

                        // 0-3. CH's Offset 
                        Int16 iCHinOffset = 0;
                        while (iCHinOffset < 12)
                        {
                            Int16 iCHOffset = 0;
                            if (iCHinOffset < ListCHShift.Count)
                                iCHOffset = Convert.ToInt16(ListCHShift[iCHinOffset] * 10);
                            Bits = BitConverter.GetBytes(iCHOffset);
                            fs.Write(Bits, 0, Bits.Length);

                            iCHinOffset++;
                            iOffset += 2;
                        }

                        // 1 Row = 시간 + 정보 + (34656 + 8의 나머지)
                        // offset = 28부터 시작

                        // 0-4. Calc Data = 288 + 8
                        lock (BufCalcData)
                        {
                            fs.Write(BufCalcData, 0, BufCalcData.Length);
                            iOffset += BufCalcData.Length;
                        }

                        // 0-5. Knock Data = 512 + 8 * 12
                        lock (BufKnockData)
                        {
                            fs.Write(BufKnockData, 0, BufKnockData.Length);
                            iOffset += BufKnockData.Length;
                        }

                        // 0-6. Remain
                        Bits = new byte[(34656 + 8) - iOffset];
                        fs.Write(Bits, 0, Bits.Length);
                    }

                    // 1. time=8
                    {
                        byte[] Bits = BitConverter.GetBytes(AList[iRow].Time.ToBinary());
                        fs.Write(Bits, 0, Bits.Length);
                    }
                    // 2. data=12+12
                    //byte[] b = GetBytesAlt(AList[iRow].Cols);
                    //fs.Write(b, 0, b.Length);
                    fs.Write(AList[iRow].Raws, 0, AList[iRow].Raws.Length); // (2880 + 8 ) * 12

                    // 3. 501~600 = 1200byte
                    //for (int i = 501; i <= 600; i++)
                    //{
                    //    ushort w = HR[0];

                    //    byte[] target = new byte[600 * 2];
                    //    Buffer.BlockCopy(HR, 501, target, 0, 600 * 2);

                    //    fs.Write(target, 0, 600 * 2);
                    //}

                    iRow++;
                }

                fs.Close();

                return true;
            }
            catch (Exception E)
            {
                TCS.LogError(E);
                return true; // 임시 true. false로 하면 무한 반복 예상됨
            }
        }

        private void ButtonRAWImport_ItemClick(object sender, EventArgs e)
        {
            
        }
        
        private void TabControlMain_Click(object sender, EventArgs e)
        {

        }

        private void ButtonHistoryCancel_Click(object sender, EventArgs e)
        {
            
        }
        
        public void TagLoad()
        {
            try
            {
                String sFile = Application.StartupPath + "\\setting\\modbus_map.csv";
                String sContent = File.ReadAllText(sFile, Encoding.UTF8);

                List<String> lRows = sContent.Split('\n').ToList();

                int iRow = 0;
                foreach (String s in lRows)
                {
                    String sRow = s.Replace("\r", "");

                    List<String> sCols = sRow.Split(',').ToList();

                    if (sCols.Count >= 8)
                    {
                        TTag t = new TTag();

                        t.Label = sCols[0];
                        Int32.TryParse(sCols[1], out t.Address);
                        Int32.TryParse(sCols[2], out t.Default);
                        Int32.TryParse(sCols[3], out t.RawMin);
                        Int32.TryParse(sCols[4], out t.RawMax);
                        Double.TryParse(sCols[5], out t.ValueMin);
                        Double.TryParse(sCols[6], out t.ValueMax);
                        Int32.TryParse(sCols[7], out t.UnitFactor);

                        if (DicTag.ContainsKey(t.Address) == false)
                            DicTag.Add(t.Address, t);
                    }
                    iRow++;
                }

                FormStatus.UpdateStatus();
            }
            catch(Exception e)
            {

            }
        }

        private void CheckListCH_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (CheckListCH.Tag != null)
                if (CheckListCH.Tag.ToString() == "1")
                    return;
            //if (e.State == CheckState.Checked)
            //    PegoRT.PeData.SubsetsToShow[e.Index] = 1;
            //else
            //    PegoRT.PeData.SubsetsToShow[e.Index] = 0;

            for (int i = 0; i < CHMax; i++)
            {
                //int i = CheckListCH.SelectedIndex;
                if (i < CheckListCH.Items.Count)
                {
                    Boolean b = CheckListCH.GetItemCheckState(i) == CheckState.Checked;
                    if (i == CheckListCH.SelectedIndex)
                        b = !b;
                    _chart.ViewXY.SampleDataSeries[i].Visible = b;
                }
            }
            
        }

        private void ButtonDisconnect_Click(object sender, EventArgs e)
        {
            try
            {
                m.disconnect();
            }
            catch (Exception E)
            {
                TCS.LogError(E);
            }
        }
        
        // 111111111111111111111111111111
        public void OnResponseData(ushort ACH, ushort ACycle, ref byte[] ABuffer, int BufferIndex, int BufferCount)
        {
            try
            {
                if (ACH < 0xFFFF)
                {
                    byte[] bb = new byte[BufferCount];

                    Array.Copy(ABuffer, BufferIndex, bb, 0, BufferCount);

                    //// 마지막 데이터
                    //UInt16 w = (UInt16)(BitConverter.ToUInt16(ABuffer, BufferCount - 2));
                    ////w = SwapBytes(w);

                    //Data[ACH] = w;
                    ////////////////

                    lock (ListCH[ACH].Raws)
                    {
                        ListCH[ACH].Raws.Add(new TCHDataSet { Parent = ListCH[ACH], Raw = bb, Time = DateTime.Now, Cycle = ACycle });
                    }
                    //lock (ListCH[ACH].Queue)
                    //{
                    //    ListCH[ACH].Queue.Add(new TCHDataSet { Parent = ListCH[ACH], Raw = bb, Time = DateTime.Now });
                    //}
                }
                else
                {
                    CycleCount++;

                    int iCycle = (UInt16)(BitConverter.ToUInt16(ABuffer, 6));

                    this.Invoke(new Action(delegate () // this == Form 이다. Form이 아닌 컨트롤의 Invoke를 직접호출해도 무방하다.
                    {
                        if (ea.LastCycle == -1)
                        {
                            ea.LastCycle = iCycle;
                        }
                        
                        //Random rnd = new Random();
                        //if (rnd.Next(100) > 90)
                        //{
                        //    TRaw r = new TRaw();
                        //    r.Time = DateTime.Now;
                        //    r.Cycle = iCycle;
                        //    r.IsEmpty = true;
                        //    lock (ListRaw)
                        //    {
                        //        ListRaw.Add(r);
                        //    }

                        //}
                        
                        while (ea.LastCycle + 1 < iCycle)
                        {
                            if (CheckGraphSkip.Checked == false)
                            {
                                TCS.LogMsg("### Cycle Skipped ---> UI = " + ea.LastCycle.ToString() + " / Packet = " + iCycle.ToString());
                                TRaw r = new TRaw();
                                r.Time = DateTime.Now;
                                r.Cycle = iCycle;
                                r.IsEmpty = true;
                                lock (ListRaw)
                                {
                                    ListRaw.Add(r);
                                }
                            }

                            CycleSkipTotalCount += iCycle - ea.LastCycle - 1;
                            CycleSkipCount += iCycle - ea.LastCycle;

                            if (CycleSkipTotalCount > 65535)
                                CycleSkipTotalCount = 0;

                            ea.LastCycle = iCycle;
                            //ea.LastCycle++;
                        }
                    }));

                    ea.LastCycle = iCycle;

                    //if (CheckExport.Checked)
                    {
                        // 기록용 RAW 추가
                        TRaw r = new TRaw();
                        r.Time = DateTime.Now;
                        r.Cycle = iCycle;
                        r.RawsCHCount = BufferCount / 2888;
                        Array.Copy(ABuffer, 0, r.Raws, 0, BufferCount);
                        lock (ListRaw)
                        {
                            ListRaw.Add(r);
                        }
                    }
                }
            }
            catch (Exception E)
            {
                TCS.LogError(E);
            }
        }

        public void OnResponseCalcData(ushort ACH, ushort ACycle, ref byte[] ABuffer, int BufferIndex, int BufferCount)
        {
            try
            {
                if (ACH == 0xFFFF) // calc data ch은 0xfff로 수신됨
                {
                    // 총 288 바이트 = 72 WORD
                    // MODBUS HR 501부터 매칭
                    int i = 8; // 앞에 8은 헤더
                    int iHR = 501;
					lock(BufCalcData)
                    {
                        Buffer.BlockCopy(ABuffer, 0, BufCalcData, 0, Math.Max(BufferCount, BufCalcData.Length));
                    }			 
                    while (i < BufferCount)
                    {
                        UInt16 w = (UInt16)(BitConverter.ToUInt16(ABuffer, BufferIndex + i));
                        //w = SwapBytes(w);
                        HR[iHR] = w;

                        TTag t = null;
                        if (DicTag.TryGetValue(iHR, out t))
                        {
                            t.RawValue = w;
                        }
                        else
                        {

                        }

                        i = i + 2;
                        iHR++;
                    }

                    CalcDataToList();
                }
            }
            catch (Exception E)
            {
                TCS.LogError(E);
            }
        }

		public byte[] BufKnockData = new Byte[TEA.CHMaxStatic * (512 + 8)];
														   
        public void OnResponseKnockData(ushort ACH, ushort ACycle, ref byte[] ABuffer, int BufferIndex, int BufferCount)
        {
            try
            {
                // 0x13 Knock Intensity
                // 512 Byte
                
			lock (BufKnockData)
                {
                    Buffer.BlockCopy(ABuffer, 0, BufKnockData, (512 + 8) * ACH, BufferCount);
                }
                int i = 8; // 앞에 8은 헤더
                int iXPos = 0;

                int iCount = (BufferCount - 8) / 2;

                if ((arrKnockX[ACH] == null) || (arrKnockX[ACH].Length != iCount))
                {
                    arrKnockX[ACH] = new float[iCount];
                    Array.Clear(arrKnockX[ACH], 0, arrKnockX[ACH].Length);
                }

                lock (arrKnockX[ACH])
                {
                    while (i < BufferCount)
                    {
                        UInt16 w = (UInt16)(BitConverter.ToUInt16(ABuffer, BufferIndex + i));
                                              
                        arrKnockX[ACH][iXPos] = w;

                        iXPos++;                            
                        i = i + 2;
                    }
                }
            }
            catch (Exception E)
            {
                TCS.LogError(E);
            }
        }

        public double[,,] darrCalcDataQueue = new double[7, 12, 1000 * 60];
        public double[,,] darrCalcData = null;

        int CalcDataCntQueue = 0;
        int CalcDataCnt = 0;

        //public Dictionary<String, List<double>> DicCalcDataLogginQueue = new Dictionary<string, List<double>>();
        //public Dictionary<String, List<double>> DicCalcDataLoggin = new Dictionary<string, List<double>>();

        public void CalcDataToList()
        {
            //if (DicCalcDataLoggin.Count == 0)
            //{
            //    DicCalcDataLoggin.Add("pmax", new List<double>());
            //    DicCalcDataLoggin.Add("imep", new List<double>());
            //    DicCalcDataLoggin.Add("hr", new List<double>());
            //    DicCalcDataLoggin.Add("misfire", new List<double>());
            //    DicCalcDataLoggin.Add("pegging", new List<double>());
            //    DicCalcDataLoggin.Add("knockinst", new List<double>());
            //}

            //if (DicCalcDataLogginQueue.Count == 0)
            //{
            //    DicCalcDataLogginQueue.Add("pmax", new List<double>());
            //    DicCalcDataLogginQueue.Add("imep", new List<double>());
            //    DicCalcDataLogginQueue.Add("hr", new List<double>());
            //    DicCalcDataLogginQueue.Add("misfire", new List<double>());
            //    DicCalcDataLogginQueue.Add("pegging", new List<double>());
            //    DicCalcDataLogginQueue.Add("knockinst", new List<double>());
            //}

            //foreach (KeyValuePair<String, List<double>> kvp in DicCalcDataLogginQueue)
            //{
            //    int iBase = CalcDataBaseAddr(kvp.Key);
            //    int iCH = 0;
            //    while (iCH < 12)
            //    {
            //        lock (kvp.Value)
            //        {
            //            kvp.Value.Add(GetHR(iBase + iCH, 10));
            //        }
            //        iCH++;
            //    }
            //}

            //if (darrCalcDataQueue == null)
            //{
            //    darrCalcDataQueue = new double[7, 12, 1000 * 60];
            //    CalcDataCntQueue = 0;
            //}

            lock (darrCalcDataQueue)
            {
                int iKind = radioCalcData;
                while (iKind < 7)
                {
                    int iCH = 0;
                    while (iCH < 12)
                    {
                        if (iKind == 6)
                        {
                            if (iCH > 0) // RPM = CH1 
                            {
                                break;
                            }
                        }

                        int iBase = CalcDataBaseAddr(iKind);

                        darrCalcDataQueue[iKind, iCH, CalcDataCntQueue] = GetHR(iBase + iCH, 10);

                        iCH++;
                    }

                    iKind++;
                }
            }

            CalcDataCntQueue++;
        }

        public void MouseHand()
        {
            Cursor = Cursors.Hand;
        }

        public void MouseNormal()
        {
            Cursor = Cursors.Arrow;
        }
        
        public void CursorChanged(object sender, EventArgs e)
        {
            //TEA.
            //Cursor = c;
        }

        public void CalcDataToUI()
        {
            // queue -> real
            lock (darrCalcDataQueue)
            {
                darrCalcData = darrCalcDataQueue;
                CalcDataCnt = CalcDataCntQueue;

                darrCalcDataQueue = new double[7, 12, 1000 * 60];
                CalcDataCntQueue = 0;
            }

            double d = 0;

            this.Invoke(new Action(delegate () // this == Form 이다. Form이 아닌 컨트롤의 Invoke를 직접호출해도 무방하다.
            {
                //if (_chartCalcDataRPM.ChartAreas[0].AxisY.auto!= bIsCalcDataRPMYAuto)
                //{

                //}

                int iCHNew = 0;
                while (_chartCalcData.Series.Count < ea.UseCHMax)
                {
                    Series s = new Series();

                    //s.Name = "TEMP_STACK_BOILER_1";
                    if (iCHNew < _chart.ViewXY.SampleDataSeries.Count)
                    {
                        s.Enabled = _chart.ViewXY.SampleDataSeries[iCHNew].Visible;
                        s.ChartType = SeriesChartType.FastLine;
                        s.XValueType = ChartValueType.DateTime;
                        s.YValueType = ChartValueType.Double;
                        s.Color = TEA.ListColorCH[iCHNew];
                        _chartCalcData.Series.Add(s);
                    }
                    iCHNew++;
                }

                _chartCalcDataRPM.Series.SuspendUpdates();
                try
                {
                    int i = 0;
                    while (i < CalcDataCnt)
                    {
                        if (_chartCalcDataRPM.Series[0].Points.Count > 100)
                            _chartCalcDataRPM.Series[0].Points.RemoveAt(0);

                        // RPM
                        d = darrCalcData[6, 0, i];
                        _chartCalcDataRPM.Series[0].Points.Add(d);

                        i++;
                    }
                }
                finally
                {
                    _chartCalcDataRPM.Series.ResumeUpdates();
                    _chartCalcDataRPM.Series.Invalidate();
                    //_chartCalcDataRPM.Series.SuspendUpdates();

                    if (editCalcDataRPMYMax.Focused == false)
                    {
                        editCalcDataRPMYMax.Tag = 1;
                        editCalcDataRPMYMax.Text = _chartCalcDataRPM.ChartAreas[0].AxisY.Maximum.ToString("0");
                        editCalcDataRPMYMax.Tag = null;
                    }
                    if (editCalcDataRPMYMin.Focused == false)
                    {
                        editCalcDataRPMYMin.Tag = 1;
                        editCalcDataRPMYMin.Text = _chartCalcDataRPM.ChartAreas[0].AxisY.Minimum.ToString("0");
                        editCalcDataRPMYMin.Tag = null;
                    }
                }

                _chartCalcData.Series.SuspendUpdates();
                try
                {
                    int iKind = radioCalcData;
                    while (iKind < 6)
                    {
                        int iCH = 0;
                        while (iCH < ea.UseCHMax)
                        {
                            int i = 0;
                            while (i < CalcDataCnt)
                            {
                                if (_chartCalcData.Series[iCH].Points.Count > 100)
                                    _chartCalcData.Series[iCH].Points.RemoveAt(0);

                                d = darrCalcData[iKind, iCH, i];
                                _chartCalcData.Series[iCH].Points.Add(d);

                                i++;
                            }
                            iCH++;
                        }
                        iKind++;
                    }
                }
                finally
                {
                    _chartCalcData.Series.ResumeUpdates();
                    _chartCalcData.Series.Invalidate();
                    //_chartCalcData.Series.SuspendUpdates();

                    if (editCalcDataYMax.Focused == false)
                    {
                        editCalcDataYMax.Tag = 1;
                        editCalcDataYMax.Text = _chartCalcData.ChartAreas[0].AxisY.Maximum.ToString("0");
                        editCalcDataYMax.Tag = null;
                    }
                    if (editCalcDataYMin.Focused == false)
                    {
                        editCalcDataYMin.Tag = 1;
                        editCalcDataYMin.Text = _chartCalcData.ChartAreas[0].AxisY.Minimum.ToString("0");
                        editCalcDataYMin.Tag = null;
                    }
                }

            }));
        }

        private void ButtonEAConnect_Click(object sender, EventArgs e)
        {
            try
            {
                TCube.SetValue("EditEAIP", FormSetting.editHost.Text);
                TCube.SetValue("EditEAPort", FormSetting.editPort.Text);

                TCS.LogMsg("----- Pressure Data Connect Try -----");
                this.Invoke(new Action(delegate () // this == Form 이다. Form이 아닌 컨트롤의 Invoke를 직접호출해도 무방하다.
                {
                    ea.IP = FormSetting.editHost.Text;
                    ea.Port = Convert.ToUInt16(FormSetting.editPort.Text);
                }));
                //ea.DoConnect();
                
                TCS.LogMsg("----- Cmd Connect Try -----");
                this.Invoke(new Action(delegate () // this == Form 이다. Form이 아닌 컨트롤의 Invoke를 직접호출해도 무방하다.
                {
                    TCS.eaCmd.IP = FormSetting.editHost.Text;
                    TCS.eaCmd.Port = 504;
                }));
                TCS.eaCmd.DoConnect();


            }
            catch (Exception E)
            {
                TCS.LogError(E);
            }
        }

        private void ButtonEADisconnect_Click(object sender, EventArgs e)
        {
            try
            {
                ea.disconnect();
                TCS.eaCmd.disconnect();
            }
            catch (Exception E)
            {
                TCS.LogError(E);
            }
        }

        private void toolStripStatusLabel1_Click(object sender, EventArgs e)
        {

        }

        private void statusStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void ButtonReq_Click(object sender, EventArgs e)
        {
            ea.DoSendReq();
        }

        private void ButtonDelaySet_Click(object sender, EventArgs e)
        {
            try
            {
                ea.DelayForRecv = Convert.ToInt32(EditDelayForRecv.Text);
                TCube.SetValue("DelayForRecv", EditDelayForRecv.Text);
            }
            catch(Exception E)
            {

            }
        }

        private void CheckYAuto_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                labelymin.Enabled = !CheckYAuto.Checked;
                labelymax.Enabled = !CheckYAuto.Checked;
                editGraphPYMax.Enabled = !CheckYAuto.Checked;
                editGraphPYMin.Enabled = !CheckYAuto.Checked;
                ButtonYAxisSet.Enabled = !CheckYAuto.Checked;
            }
            catch (Exception E)
            {
                TCS.LogError(E);
            }
        }

        private void toolStripStatusLabel12_Click(object sender, EventArgs e)
        {

        }

        private void ButtonRAWImport_Click(object sender, EventArgs e)
        {
        }

        private void GridData_CellValueNeeded(object sender, DataGridViewCellValueEventArgs e)
        {
        }

        public void ChartRecalc(LightningChartUltimate AChart)
        {
            try
            {
                AChart.ViewXY.ZoomToFit();
                //Boolean b;
                //_chart.ViewXY.YAxes[0].Fit(10, out b, false, false);
                //_chart.ViewXY.XAxes[0].Fit(out b, false);
            }
            catch (Exception E)
            {

            }
        }

        private void ButtonYAxis_Click(object sender, EventArgs e)
        {
            //CheckListCH.Tag = 1;
            //for (int ii = 0; ii < CHMax; ii++)
            //{
            //    CheckListCH.SetItemChecked(ii, CheckCHAllVIsible.Checked);
            //}
            //CheckListCH.Tag = null;
            ChartRecalc(_chart);
            

            GraphToUI();
        }

        public void ChartInit()
        {
            //LightningChartUltimate.SetDeploymentKey("lgCAAIT+iZ0B1tIBJABVcGRhdGVhYmxlVGlsbD0yMDE4LTA1LTI3I1JldmlzaW9uPTABfy0eAiydkK3kV/TcEfM6MRNGTGlNLKyCubEiAF9Bh3yUFUjIJF/df4WNkqQ2ME6NmLYOCBfq4kNBtn4cqbLwuA5cElUyTQZOfC4Tn6UzgVbMRuBeZJNvxQC1xSLBvDC138xq1knu3lZ6LHBXTptKH0SMmYmO3JKmXiA22kTI99aRXllgEzzDWh8UadBo3jh95lHbkjOp740Xf7XtJnWD/3uTsQgLz3N+N3Ez9Ou5tStJ6hqnNpqaUJhK4cdqERBXFo4s3GnQSI+e4InvDY5W6dFwzrZvUScDQ2gs5jU+/MhL6IczORqepQcRDZolO3ztjMw+H6cfueUCZWugDl+WihxgZiA9WzXRvi/c0trz1xuM4YLz92dwubFs41PhPxeRGs6juGT8MdDfyaWoeWuwf5C4f+C0reUfDvGUqSayXiPPxCvNsjLpzii6iSk5pL54Sf3QYp//VDTfQJ7eBmFnDt6Pm2yS5PkzuumW75aeIekaXIeB5N4MreIDSjdIfkp96Q==");
            LightningChartUltimate.SetDeploymentKey("lgCAAIT + iZ0B1tIBJABVcGRhdGVhYmxlVGlsbD0yMDE4LTA1LTI3I1JldmlzaW9uPTABfy0eAiydkK3kV / TcEfM6MRNGTGlNLKyCubEiAF9Bh3yUFUjIJF / df4WNkqQ2ME6NmLYOCBfq4kNBtn4cqbLwuA5cElUyTQZOfC4Tn6UzgVbMRuBeZJNvxQC1xSLBvDC138xq1knu3lZ6LHBXTptKH0SMmYmO3JKmXiA22kTI99aRXllgEzzDWh8UadBo3jh95lHbkjOp740Xf7XtJnWD / 3uTsQgLz3N + N3Ez9Ou5tStJ6hqnNpqaUJhK4cdqERBXFo4s3GnQSI + e4InvDY5W6dFwzrZvUScDQ2gs5jU +/ MhL6IczORqepQcRDZolO3ztjMw + H6cfueUCZWugDl + WihxgZiA9WzXRvi / c0trz1xuM4YLz92dwubFs41PhPxeRGs6juGT8MdDfyaWoeWuwf5C4f + C0reUfDvGUqSayXiPPxCvNsjLpzii6iSk5pL54Sf3QYp//VDTfQJ7eBmFnDt6Pm2yS5PkzuumW75aeIekaXIeB5N4MreIDSjdIfkp96Q==");

            //Disable rendering, strongly recommended before updating chart properties
            _chart.BeginUpdate();

            //Set active view as xy
            _chart.ActiveView = ActiveView.ViewXY;

            //Chart name
            _chart.Name = "Data chart";



            //Y-axis are stacked and has common x-axis drawn
            _chart.ViewXY.AxisLayout.YAxesLayout = YAxesLayout.Stacked;

            //_chart.ViewXY.LegendBoxes[0].Position = LegendBoxPositionXY.TopCenter - 24;//9999999999999999999999

            //_chart.ViewXY.LegendBoxes[0].Offset.Y = 0;//-(_chart.Height- _chart.ViewXY.LegendBoxes[0].Height);//탑메뉴 위치를 지정한다.
            //_chart.ViewXY.LegendBoxes[0].Width = _chart.Width;//탑메뉴 길이를 늘린다.
            //_chart.ViewXY.LegendBoxes[0].Height = 40;

            //_chart.ViewXY.LegendBoxes[0].Visible = true;

            //_chart.ViewXY.AxisLayout.AutoAdjustMargins = false; //차트 수동변경을 위한 설정 셋팅
            //_chart.ViewXY.Margins = new Padding(53, 50, 12, 58);
            
            _chart.Update();

            //Don't show x-axis title
            _chart.ViewXY.XAxes[0].Title.Visible = false;
            _chart.ViewXY.XAxes[0].ScrollMode = XAxisScrollMode.None;

            _chart.ViewXY.XAxes[0].ValueType = AxisValueType.Number;
            _chart.ViewXY.XAxes[0].Title.Text = "Period(ms)";

            _chart.ViewXY.XAxes[0].LabelsFont = TEA.fontMd10;

            //Allow chart rendering
            _chart.EndUpdate();


            GenerateData(_chart, ea.UseCHMax, 1440, 1);

            //CreateSpectrumChart();
        }

        public void ChartInit(LightningChartUltimate AChart)
        {
            //Disable rendering, strongly recommended before updating chart properties
            AChart.BeginUpdate();

            //Set active view as xy
            AChart.ActiveView = ActiveView.ViewXY;

            //Chart name
            AChart.Name = "Cylinder";

            //Y-axis are stacked and has common x-axis drawn
            AChart.ViewXY.AxisLayout.YAxesLayout = YAxesLayout.Stacked;

            //Don't show legend box
            //AChart.ViewXY.LegendBoxes[0].Visible = true;
            //AChart.ViewXY.LegendBoxes[0].Position = LegendBoxPositionXY.SegmentTopCenter;

            AChart.ViewXY.LegendBoxes[0].Position = LegendBoxPositionXY.TopCenter - 24;//9999999999999999999999

            AChart.ViewXY.LegendBoxes[0].Offset.Y = 0;//-(AChart.Height- AChart.ViewXY.LegendBoxes[0].Height);//탑메뉴 위치를 지정한다.
            AChart.ViewXY.LegendBoxes[0].Width = AChart.Width;//탑메뉴 길이를 늘린다.
            AChart.ViewXY.LegendBoxes[0].Height = 40;

            AChart.ViewXY.AxisLayout.AutoAdjustMargins = false; //차트 수동변경을 위한 설정 셋팅
            AChart.ViewXY.Margins = new Padding(53, 50, 12, 58);

            //AChart.ViewXY.YAxes[0].SetRange(-10, 5); //= .Maximum = 30;
            //AChart.ViewXY.YAxes[0].Minimum

            AChart.Update();

            AChart.ViewXY.LegendBoxes[0].Visible = true;

            //Don't show x-axis title
            AChart.ViewXY.XAxes[0].Title.Visible = false;
            AChart.ViewXY.XAxes[0].ScrollMode = XAxisScrollMode.None;

            AChart.ViewXY.XAxes[0].ValueType = AxisValueType.Number;
            AChart.ViewXY.XAxes[0].Title.Text = "Period(ms)";

            AChart.ViewXY.XAxes[0].LabelsFont = TEA.fontMd10;

            //AChart.ViewXY.YAxes.

            //Set custom ticks for X axis, show only dates having a data point 
            //for (int i = 0; i < 1440; i++)
            //{
            //    AChart.ViewXY.XAxes[0].CustomTicks.Add(
            //        new CustomAxisTick(
            //            AChart.ViewXY.XAxes[0], i, "A", 
            //            10, 
            //            true, 
            //            Color.FromArgb(40, Color.White), 
            //            CustomTickStyle.TickAndGrid)
            //        );
            //}

            //AChart.ViewXY.XAxes[0].Maximum

            // Setup custom style.
            //ExampleUtils.SetDarkFlatStyle(AChart);

            //Allow chart rendering
            AChart.EndUpdate();


            GenerateData(AChart, 1, 1440, 1);

            //CreateSpectrumChart();
        }

        public void ChartInitKnock(LightningChartUltimate AChart)
        {
            //Disable rendering, strongly recommended before updating chart properties
            AChart.BeginUpdate();

            //Set active view as xy
            AChart.ActiveView = ActiveView.ViewXY;

            //Chart name
            AChart.Name = "Knock";

            //Y-axis are stacked and has common x-axis drawn
            AChart.ViewXY.AxisLayout.YAxesLayout = YAxesLayout.Stacked;

            //Don't show legend box
            //AChart.ViewXY.LegendBoxes[0].Visible = true;
            //AChart.ViewXY.LegendBoxes[0].Position = LegendBoxPositionXY.SegmentTopCenter;

            AChart.ViewXY.LegendBoxes[0].Position = LegendBoxPositionXY.TopCenter - 24;//9999999999999999999999

            AChart.ViewXY.LegendBoxes[0].Offset.Y = 0;//-(AChart.Height- AChart.ViewXY.LegendBoxes[0].Height);//탑메뉴 위치를 지정한다.
            AChart.ViewXY.LegendBoxes[0].Width = AChart.Width;//탑메뉴 길이를 늘린다.
            AChart.ViewXY.LegendBoxes[0].Height = 40;

            AChart.ViewXY.AxisLayout.AutoAdjustMargins = false; //차트 수동변경을 위한 설정 셋팅
            AChart.ViewXY.Margins = new Padding(53, 50, 12, 58);

            //AChart.ViewXY.YAxes[0].SetRange(-10, 5); //= .Maximum = 30;
            //AChart.ViewXY.YAxes[0].Minimum

            AChart.Update();

            AChart.ViewXY.LegendBoxes[0].Visible = true;

            //Don't show x-axis title
            AChart.ViewXY.XAxes[0].Title.Visible = false;
            AChart.ViewXY.XAxes[0].ScrollMode = XAxisScrollMode.None;

            AChart.ViewXY.XAxes[0].ValueType = AxisValueType.Number;
            AChart.ViewXY.XAxes[0].Title.Text = "Index";



            //Set custom ticks for X axis, show only dates having a data point 
            //for (int i = 0; i < 1440; i++)
            //{
            //    AChart.ViewXY.XAxes[0].CustomTicks.Add(
            //        new CustomAxisTick(
            //            AChart.ViewXY.XAxes[0], i, "A", 
            //            10, 
            //            true, 
            //            Color.FromArgb(40, Color.White), 
            //            CustomTickStyle.TickAndGrid)
            //        );
            //}

            //AChart.ViewXY.XAxes[0].Maximum

            // Setup custom style.
            //ExampleUtils.SetDarkFlatStyle(AChart);

            //Allow chart rendering
            AChart.EndUpdate();


            //InitKnock(AChart, 256);

            //CreateSpectrumChart();
        }

        //public void InitKnock(LightningChartUltimate AChart, int AXCount)
        //{
        //    //Cursor currentCursor = Cursor;
        //    //Cursor = Cursors.WaitCursor; //Change cursor to wait
            
        //    //Disable rendering
        //    AChart.BeginUpdate();

        //    //Remove existing series
        //    ExampleUtils.DisposeAllAndClear(AChart.ViewXY.SampleDataSeries);
        //    //Remove existing y-axes
        //    ExampleUtils.DisposeAllAndClear(AChart.ViewXY.YAxes);

        //    AxisY yAxis = new AxisY(AChart.ViewXY);
        //    yAxis.AllowAutoYFit = true;
        //    //yAxis.SetRange(-15, 15);
        //    yAxis.Units.Visible = false;
        //    yAxis.Title.Text = "";

        //    AChart.ViewXY.YAxes.Add(yAxis);
        //    for (int channelIndex = 0; channelIndex < 1; channelIndex++)
        //    {
        //        SampleDataSeries sds = new SampleDataSeries();
        //        sds.SampleFormat = SampleFormat.SingleFloat;
        //        //sds.FirstSampleTimeStamp = 0; // 1.0 / frequency;
        //        //sds.SamplingFrequency = 1;// frequency;

        //        sds.MouseInteraction = false;
        //        sds.LineStyle.Width = 1;
        //        sds.LineStyle.AntiAliasing = LineAntialias.None;
        //        sds.LineStyle.Color = DefaultColors.SeriesForBlackBackground[channelIndex % DefaultColors.SeriesForBlackBackground.Length];

        //        sds.Title.Text = "Knock Intensity Data";

        //        sds.DataBreaking.Enabled = true;
        //        sds.DataBreaking.Value = float.NaN; // set data gap defining value (default = NaN)


        //        AChart.ViewXY.SampleDataSeries.Add(sds);
        //    }

        //    //Update x-axis range
        //    AChart.ViewXY.XAxes[0].SetRange(0, AXCount);
        //    AChart.ViewXY.XAxes[0].AutoDivSpacing = false;
        //    AChart.ViewXY.XAxes[0].MajorDiv = 5;

        //    //Allow rendering
        //    AChart.EndUpdate();
            

            

        //}
        AxisY yAxis = null;

        private void GenerateData(LightningChartUltimate AChart, int channelCount, double frequency, double length)
        {
            //Cursor currentCursor = Cursor;
            //Cursor = Cursors.WaitCursor; //Change cursor to wait
            
            int sampleCount = (int)(frequency * length); //Samples points

            //Disable rendering
            AChart.BeginUpdate();

            //Remove existing series
            ExampleUtils.DisposeAllAndClear(AChart.ViewXY.SampleDataSeries);
            //Remove existing y-axes

        
            if (yAxis == null)
            {
                ExampleUtils.DisposeAllAndClear(AChart.ViewXY.YAxes);
                yAxis = new AxisY(AChart.ViewXY);
                //yAxis.AllowAutoYFit = true;
                //yAxis.SetRange(-15, 15);
                yAxis.Units.Visible = false;
                yAxis.Title.Text = "";
                yAxis.MajorDivTickStyle.Visible = false;
                yAxis.MinorDivTickStyle.Visible = false;
                yAxis.AxisColor = Color.FromArgb(196, 198, 201);
                yAxis.AxisThickness = 1;
                yAxis.LabelsVisible = true;
                yAxis.LabelsColor = ColorTranslator.FromHtml("#95999B");
                yAxis.ScaleNibs.Color = Color.FromArgb(196, 198, 201);
                yAxis.AutoFormatLabels = false;
                yAxis.LabelsNumberFormat = "#,##0";
                yAxis.LabelsFont = TEA.fontMd10;
                AChart.ViewXY.YAxes.Add(yAxis);
            }
            for (int channelIndex = 0; channelIndex < channelCount; channelIndex++)
            {
                SampleDataSeries sds = new SampleDataSeries();
                sds.SampleFormat = SampleFormat.SingleFloat;
                sds.FirstSampleTimeStamp = -360; // 1.0 / frequency;
                sds.SamplingFrequency = 2;// frequency;

                sds.MouseInteraction = false;
                sds.LineStyle.Width = 2;
                sds.LineStyle.AntiAliasing = LineAntialias.Normal;
                sds.LineStyle.Color = TEA.ListColorCH[channelIndex];// DefaultColors.SeriesForBlackBackground[channelIndex % DefaultColors.SeriesForBlackBackground.Length];

                if (channelCount == 1)
                    sds.Title.Text = "Cylinder Pressure";
                else
                    sds.Title.Text = "CH" + (channelIndex + 1).ToString();
                sds.DataBreaking.Enabled = true;
                sds.DataBreaking.Value = float.NaN; // set data gap defining value (default = NaN)

                AChart.ViewXY.SampleDataSeries.Add(sds);
            }

            //Update x-axis range
            AChart.ViewXY.XAxes[0].SetRange(0, (double)sampleCount / frequency);
            AChart.ViewXY.XAxes[0].AutoDivSpacing = false;
            AChart.ViewXY.XAxes[0].MajorDiv = 5;

            //Allow rendering
            AChart.EndUpdate();

            //Cursor = currentCursor;
        }

        public int CalcDataBaseAddr(String AKind)
        {
            if (AKind == "pmax")
                return 537;
            else if (AKind == "imep")
                return 561;
            else if (AKind == "hr")
                return 585;
            else if (AKind == "misfire")
                return 609;
            else if (AKind == "pegging")
                return 621;
            else if (AKind == "knockinst")
                return 633;
            else
                return 0;
        }

        public int CalcDataBaseAddr(int AKind)
        {
            if (AKind == 0)
                return 537;
            else if (AKind == 1)
                return 561;
            else if (AKind == 2)
                return 585;
            else if (AKind == 3)
                return 609;
            else if (AKind == 4)
                return 621;
            else if (AKind == 5)
                return 633;
            else if (AKind == 6)
                return 506;
            else
                return 0;
        }

        // 333333333333333333333333
        public void ThreadHRToShow()
        {
            this.Invoke(new Action(delegate () // this == Form 이다. Form이 아닌 컨트롤의 Invoke를 직접호출해도 무방하다.
            {
                labelRPM.Text = String.Format("{0:#,##0}", GetHR(506, 10));
                
                while (chartPMax.Series[0].Points.Count < ea.UseCHMax + 2)
                {
                    chartPMax.Series[0].Points.AddXY(chartPMax.Series[0].Points.Count + 0, 0);
                    //chartPMax.Series[1].Points.AddXY(chartPMax.Series[0].Points.Count + 0, 0);
                }
                while (chartPMax.Series[0].Points.Count > ea.UseCHMax + 2)
                {
                    chartPMax.Series[0].Points.RemoveAt(chartPMax.Series[0].Points.Count - 1);
                    //chartPMax.Series[1].Points.RemoveAt(chartPMax.Series[0].Points.Count - 1);
                }

                int iBase = 0;

                if (tabSelected(labelCalcDataPmax))
                    iBase = 537;
                else if (tabSelected(labelCalcDataIMEP))
                    iBase = 561;
                else if (tabSelected(labelCalcDataHR))
                    iBase = 585;
                else if (tabSelected(labelCalcDataMisfire))
                    iBase = 609;
                else if (tabSelected(labelCalcDataPegging))
                    iBase = 621;
                else if (tabSelected(labelCalcDataKnockIntst))
                    iBase = 633;
                            
                int i = 0;
                List<double> lValues = new List<double>();
                while (i < ea.UseCHMax)
                {
                    double iValue = GetHR(iBase + i, 10);
                    lValues.Add(iValue);
                    chartPMax.Series[0].Points[i + 1].YValues.SetValue(iValue, 0);
                    i++;
                }

                double dAvg = lValues.Average();

                //chartPMax.Series[1].Points.Clear();
                //chartPMax.Series[1].Points.AddXY(0.7, dAvg);
                //chartPMax.Series[1].Points.AddXY(ea.UseCHMax + 0.3, dAvg);
                //for (int si = 0; si < chartPMax.Series[1].Points.Count; si++)
                //{
                //    chartPMax.Series[1].Points[si].YValues.SetValue(dAvg, 0);
                chartPMax.Series[1].Points[0].YValues.SetValue(dAvg, 0);
                chartPMax.Series[1].Points[1].YValues.SetValue(dAvg, 0);
                //}
                //chartPMax.Series[0].Points[0].YValues.SetValue(0, 0);
                //chartPMax.Series[0].Points[13].YValues.SetValue(0, 0);

                try
                {
                    double d = lValues.Max();
                    if (d > 0)
                        d = d * 1.2;
                    else if (d < 0)
                        d = d * 0.8;
                    if (d > chartPMax.ChartAreas[0].AxisY.Minimum)
                        chartPMax.ChartAreas[0].AxisY.Maximum = d;
                    else
                    {

                    }
                }
                catch
                {

                }
                chartPMax.Invalidate();
                panelGraphCalcDataBottom.Invalidate();
                panelAlarm.Invalidate();
                labelCalcDataAvg.Text = String.Format("{0:#,##0.0}", dAvg);
            }));
        }
        
        float[][] arrKnockX = new float[TEA.CHMaxStatic][];

        float[][] arrX = new float[TEA.CHMaxStatic][];
        float[] arrXPS;// = new float[1440];
        float[] arrXPSORG;// = new float[1440];

        public void ThreadRawsToGraph(int AFrom = -1, int ACount = -1, List<TRaw> AList = null)
        {
            if (AList == null)
                return;
            
            int iCHPS = -1;
            int iPSOffset = -1;

            if (AList.Count > 0)
            {
                //this.Invoke(new Action(delegate () // this == Form 이다. Form이 아닌 컨트롤의 Invoke를 직접호출해도 무방하다.
                //{
                //}));
                try
                {
                    if (AFrom == -1)
                        AFrom = 0;
                    else if (AFrom == -2)
                        AFrom = AList.Count - ACount;

                    if (AFrom < 0)
                        AFrom = 0;

                    if (ACount == -1)
                        ACount = AList.Count;
                    // CH당 200만 포인트 허용 ===> 1000 Count
                    if (ACount > 1000)
                        ACount = 1000;

                    int iStepCH = ACount * 1440;

                    for (int iCHTemp = 0; iCHTemp < TEA.CHMaxStatic; iCHTemp++)
                    {
                        arrX[iCHTemp] = new float[iStepCH];
                        Array.Clear(arrX[iCHTemp], 0, arrX[iCHTemp].Length);
                    }

                    if (IsCalTDC)
                    { 
                        //this.Invoke(new Action(delegate () // this == Form 이다. Form이 아닌 컨트롤의 Invoke를 직접호출해도 무방하다.
                        //{
                        arrXPS = new float[iStepCH];
                        arrXPSORG = new float[iStepCH];

                        iCHPS = FormCalTDC.CylSelected;

                        iPSOffset = FormCalTDC.TDCOffset;

                            //if (Int32.TryParse(EditPhaseShiftOffset.Text, out iPSOffset) == false)
                            //{
                            //    iPSOffset = 0;
                            //}
                        //}));
                    }

                    TRaw r = null;
                    //foreach (TRaw r in ListRawTemp)
                    for (int iRow = AFrom; iRow < AFrom + ACount; iRow++)
                    {
                        r = AList[iRow]; 

                        {
                            int iCH = 0;
                            //while (iCH < CHMax)
                            while (iCH < r.RawsCHCount)
                            {
                                int iIdx = iCH * 2888 + 8; // 
                                int iArray;
                                int iCnt = 0;
                                while (iCnt < 1440)
                                {
                                    iArray = ((iRow - AFrom) * 1440) + iCnt;
                                    if (r.IsEmpty)
                                    {
                                        arrX[iCH][iArray] = float.NaN;
                                    }
                                    else
                                    {
                                        float f = 0;
                                        if (DataType == 0)
                                        {
                                            f = (Int16)(BitConverter.ToInt16(r.Raws, iIdx));
                                        }
                                        else if (DataType == 1)
                                        {
                                            f = (UInt16)(BitConverter.ToUInt16(r.Raws, iIdx));
                                        }                                        
                                        arrX[iCH][iArray] = f;
                                    }

                                    iIdx += 2;
                                    iCnt++;
                                }
                                
                                if (iCHPS != -1)
                                {
                                    if (iCH == iCHPS)
                                    {
                                        iIdx = iCH * 2888 + 8;
                                        iCnt = 0;
                                        while (iCnt < 1440)
                                        {
                                            // PHASE SHIFT 
                                            // iPSOffset = -1440 ~ 1439 기본=0 
                                            int iCntPS = iCnt - iPSOffset;
                                            if (iCntPS < 0)
                                                iCntPS = 1440 + iCntPS;
                                            if (iCntPS >= 1440)
                                            {
                                                iCntPS = iCntPS - 1440;
                                            }
                                           
                                            iArray = ((iRow - AFrom) * 1440) + iCntPS;

                                            int iArrayPS = arrXPS.Length - iArray - 1;
                                            int iArrayPSORG = arrXPS.Length - (iArray - iCntPS + iCnt) - 1;
                                            if (r.IsEmpty)
                                            {
                                                arrXPS[iArrayPS] = float.NaN;
                                                arrXPSORG[iArray] = float.NaN;
                                            }
                                            else
                                            {
                                                float f = 0;
                                                if (DataType == 0)
                                                {
                                                    f = (Int16)(BitConverter.ToInt16(r.Raws, iIdx));
                                                }
                                                else if (DataType == 1)
                                                {
                                                    f = (UInt16)(BitConverter.ToUInt16(r.Raws, iIdx));
                                                }

                                                arrXPS[iArrayPS] = f;
                                                arrXPSORG[iArray] = f;
                                            }

                                            iIdx += 2;
                                            iCnt++;
                                        }
                                    }
                                }
                                
                                // 3333333333333333333333333333
                                if (ListCHShift[iCH] != 0)
                                {
                                    iIdx = iCH * 2888 + 8;
                                    iCnt = 0;
                                    while (iCnt < 1440)
                                    {
                                        // PHASE SHIFT 
                                        // iPSOffset = -1440 ~ 1439 기본=0 
                                        double dr = 1440 / 720;
                                        double dd = dr * ListCHShift[iCH];

                                        iPSOffset = Convert.ToInt32(dd); //ListCHShift[iCH];

                                        int iCntPS = iCnt - iPSOffset;
                                        if (iCntPS < 0)
                                            iCntPS = 1440 + iCntPS;
                                        if (iCntPS >= 1440)
                                        {
                                            iCntPS = iCntPS - 1440;
                                        }

                                        iArray = ((iRow - AFrom) * 1440) + iCntPS;

                                        int iArrayPS = arrX[iCH].Length - (iArray - iCntPS + iCnt) - 1;
                                        if (r.IsEmpty)
                                        {
                                            arrX[iCH][iArray] = float.NaN;
                                        }
                                        else
                                        {
                                            float f = 0;
                                            if (DataType == 0)
                                            {
                                                f = (Int16)(BitConverter.ToInt16(r.Raws, iIdx));
                                            }
                                            else if (DataType == 1)
                                            {
                                                f = (UInt16)(BitConverter.ToUInt16(r.Raws, iIdx));
                                            }
                                            arrX[iCH][iArray] = f;
                                        }

                                        iIdx += 2;
                                        iCnt++;
                                    }
                                }

                                iCH++;
                            }
                        }
                    }
                }
                catch (Exception E)
                {
                    TCS.LogError(E);
                }
                
                this.Invoke(new Action(delegate () // this == Form 이다. Form이 아닌 컨트롤의 Invoke를 직접호출해도 무방하다.
                {
                    if (IsCalTDC == false)
                    {
                        // 일반 Monitoring
                        if (_chart.ViewXY.SampleDataSeries.Count != ea.UseCHMax)
                        {
                            if (swGraph.IsRunning == false)
                                swGraph.Start();

                            if (swGraph.ElapsedMilliseconds >= 1000)
                            {
                                swGraph.Restart();
                                GenerateData(_chart, ea.UseCHMax, 1440, 1);
                            }
                        }

                        for (int channelIndex = 0; channelIndex < ea.UseCHMax; channelIndex++)
                        {
                            _chart.ViewXY.SampleDataSeries[channelIndex].SamplesSingle = arrX[channelIndex];
                        }
                        if (_bisfrst)
                        {
                            _bisfrst = false;
                            Boolean b;

                            //if (CheckYAuto.Checked)
                            {
                                _chart.ViewXY.YAxes[0].Fit(10, out b, false, false);
                                _chart.ViewXY.YAxes[0].Fit(10, out b, false, false);
                            }

                            ChartRecalc(_chart);
                            GraphToUI();
                        }
                    }
                    else
                    {
                        // Cal - TDC
                        if (iCHPS >= 0)
                        {
                            FormCalTDC._chart.ViewXY.SampleDataSeries[0].SamplesSingle = arrXPSORG;
                            FormCalTDC._chart.ViewXY.SampleDataSeries[1].SamplesSingle = arrXPS;
                        }

                        if (FormCalTDC._bisfrst)
                        {
                            FormCalTDC._bisfrst = false;
                            Boolean b;

                            {
                                FormCalTDC._chart.ViewXY.YAxes[0].Fit(10, out b, false, false);
                                FormCalTDC._chart.ViewXY.YAxes[0].Fit(10, out b, false, false);
                            }

                            ChartRecalc(FormCalTDC._chart);

                            FormCalTDC.GraphToUI();
                        }
                    }


                    if (IsCalKnock)
                    {
                        int iCH = FormCalKnock.CylSelected;

                        if (iCH >= 0)
                        {
                            FormCalKnock.chartCyl.Series[0].Points.DataBindY(arrX[iCH]);
                            if (arrKnockX[iCH] != null)
                            {
                                FormCalKnock.KnockSet(arrKnockX[iCH]);
                                FormCalKnock.Calc();
                                //FormCalKnock.chartKnock.Series[0].Points.DataBindY(arrKnockX[iCH]);
                            }
                        }
                        if (_bisfrstKnock)
                        {
                            _bisfrstKnock = false;
                            //Boolean b;

                            //_chartCyl.ViewXY.YAxes[0].Fit(10, out b, false, false);
                            //_chartKnock.ViewXY.YAxes[0].Fit(10, out b, false, false);

                            //ChartRecalc(_chartCyl);
                            //ChartRecalc(_chartKnock);
                        }
                    }
                }));
                
                //// 준비 : 주파수, 
                //// fft
                //float[][] fft = new float[EA.CHMax][];
                //double samplingFrequency = Frequency;

                //for (int channelIndex = 0; channelIndex < EA.CHMax; channelIndex++)
                //{
                //    int processedSampleCount = 0;
                //    bool calculated = _spectrumCalculator.PowerSpectrumOverlapped(arrX[channelIndex], 1440, 0, out fft[channelIndex], out processedSampleCount);
                //    if (calculated)
                //    {
                //        // Fill spectrum chart.
                //        FillSpectrumChart(channelIndex, fft[channelIndex], samplingFrequency);
                //    }
                //    else
                //    {
                //        //MessageBox.Show("Spectrum calculation failed");
                //    }
                //}
            }
        }

        public Stopwatch swGraph = new Stopwatch();

        public void RPMToChart(Chart AChart = null)
        {
            if (AChart == null)
                AChart = _chartCalcDataRPM;

            while (AChart.Series.Count < 12)
            {
                Series s = new Series();
                
                //s.Name = "TEMP_STACK_BOILER_1";
                s.ChartType = SeriesChartType.FastLine;
                s.XValueType = ChartValueType.DateTime;
                s.YValueType = ChartValueType.Double;
                AChart.Series.Add(s);
            }
            
            AChart.Series[0].Points.AddXY(chartPMax.Series[0].Points.Count + 0, 0);          


        }

        private void ButtonConnectToggle_Click(object sender, EventArgs e)
        {
            if (m.connected == true)
            {
                ButtonDisconnect_Click(null, null);
            }
            else
            {
                ButtonConnect_ItemClick(null, null);
            }

            if (ea.connected)
            {
                ButtonEADisconnect_Click(null, null);
            }
            else
            {
                //ButtonConnectToggle.Text = "Connecting...";
                ButtonEAConnect_Click(null, null);
            }            
        }

        private void ButtonCycleCountSet_Click(object sender, EventArgs e)
        {
            ChartRecalc(_chart);
        }

        private void TabControlGraph_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (TabControlGraph.SelectedIndex == 0)
            //{
            //    _chart.Visible= true;
            //    _chartSpectrum.Visible = true;
            //    _chartPS.Visible = false;

            //}
            //else
            //{
            //    _chart.Visible = false;
            //    _chartSpectrum.Visible = false;
            //    _chartPS.Visible = true;
            //}
        }
        
        private void ButtonCHMaxSet_Click(object sender, EventArgs e)
        {
            try
            {
                int i = Convert.ToInt32(EditCHMax.Text);

                if (i < 2)
                {
                    TCS.ShowMsg("Can't setup value under 2");
                    EditCHMax.Text = "2";
                }

                if ((i > 0) && (i <= 12))
                {
                    ea.UseCHMax = i;
                    TEA.CHMaxUse = i;
                    TCube.SetValue("EditCHMax", EditCHMax.Text);
                }
            }
            catch (Exception E)
            {
                TCS.LogError(E);
            }
        }
        
        private void PanelData_SizeChanged(object sender, EventArgs e)
        {
            TCube.SetValue("PanelData.Width", PanelData.Width.ToString());
        }

        private void ComboPhaseShiftCH_SelectedIndexChanged(object sender, EventArgs e)
        {
            _bisfrst = true;
        }

        private void TabEA_TabIndexChanged(object sender, EventArgs e)
        {
            if (TabEA.SelectedTab == tabSetting)
            {
                GraphToUI();

            }
        }

        public void GraphToUI()
        {
            if (editGraphPYMax.Focused == false)
                editGraphPYMax.Text = Convert.ToDecimal(_chart.ViewXY.YAxes[0].Maximum).ToString("0");

            if (editGraphPYMin.Focused == false)
                editGraphPYMin.Text = Convert.ToDecimal(_chart.ViewXY.YAxes[0].Minimum).ToString("0");
        }

        public void SettingApply()
        {
            try
            {
                for (int i = 0; i < ea.UseCHMax; i++)
                {
                    ListCHShift[i] = Convert.ToDouble(TCube.GetValue("CHShift" + (i + 1).ToString(), "0"));
                }
            }
            catch (Exception E)
            {
                TCS.LogError(E);
            }
        }

        public void buttonCHSetFinish_Click(object sender, EventArgs e)
        {
            try
            {
                if (FormCHSetup != null)
                {
                    foreach (TFormCHCell fc in FormCHSetup.ListCH)
                    {
                        int i = Convert.ToInt32(fc.editValue.Tag) + 1;

                        try
                        {
                            //int iShift = Convert.ToInt32(fc.editValue.Text);
                            TCube.SetValue("CHShift" + i.ToString(), fc.editValue.Text);
                        }
                        catch (Exception E)
                        {
                        }
                    }

                    SettingApply();
                }
            }
            catch (Exception E)
            {
                TCS.LogError(E);
            }
        }
        TFormCH FormCHSetup = null;

        private void buttonCHSet_Click(object sender, EventArgs e)
        {
            try
            {
                if (FormCHSetup == null)
                {
                    FormCHSetup = new TFormCH();

                    FormCHSetup.SetInput("Cylinder Shift Setup");

                    FormCHSetup.buttonOK.Click += buttonCHSetFinish_Click;
                }

                FormCHSetup.Init(ea.UseCHMax);
                foreach (TFormCHCell fc in FormCHSetup.ListCH)
                {
                    int i = Convert.ToInt32(fc.editValue.Tag) + 1;

                    
                    fc.editValue.Text = TCube.GetValue("CHShift" + i.ToString(), "0");
                }
                FormCHSetup.Show();
                FormCHSetup.BringToFront();
            }
            catch (Exception E)
            {
                TCS.LogError(E);
            }
        }

        private void checkLog_CheckedChanged(object sender, EventArgs e)
        {
            if (checkLog.Checked)
            {
                FormLog.Show();
            }
            else
            {
                FormLog.Hide();
            }
        }

        private void buttonDataTypeSet_Click(object sender, EventArgs e)
        {
            TCube.SetValue("comboDataType", comboDataType.Text);
            String sDataType = comboDataType.Text;
            if (sDataType == "INT16")
                DataType = 0;
            else if (sDataType == "UINT16")
                DataType = 1;
            //FormViewer.DataType = DataType;
        }

        private void comboKnockCH_SelectedIndexChanged(object sender, EventArgs e)
        {
            _bisfrstKnock = true;
        }

        // uuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu

        public Boolean tabSelected(Label ALabel)
        {
            return DicLabelSelected.ContainsKey(ALabel);

            //if (ALabel.BackColor == TEA.ColorTabButtonOnBack)
            //{
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}
        }

        private void imageExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void imageConOn_Click(object sender, EventArgs e)
        {
            if (m.connected == true)
            {
                ButtonDisconnect_Click(null, null);
            }
            else
            {
                ButtonConnect_ItemClick(null, null);
            }

            if (ea.connected)
            {
                ButtonEADisconnect_Click(null, null);
            }
            else
            {
                //ButtonConnectToggle.Text = "Connecting...";
                ButtonEAConnect_Click(null, null);
            }
        }
        
        private void imageTop_MouseDown(object sender, MouseEventArgs e)
        {
           
        }

        private void label_Paint(object sender, PaintEventArgs e)
        {
            //e.Graphics.TextRenderingHint = TextRenderingHint.AntiAlias;
            //base.OnPaint(e);

            //SolidBrush b = new SolidBrush(((Label)sender).BackColor);
            //e.Graphics.FillRectangle(b, ((Label)sender).);

            Pen p = new Pen(TEA.ColorTabButtonBorder, 1);
            e.Graphics.DrawRectangle(p, new Rectangle(0, 0, ((Control)sender).Width, ((Control)sender).Height));

            //ControlPaint.DrawBorder(e.Graphics, ((Label)sender).DisplayRectangle
            //    , TEA.ColorTabButtonBorder, 1, ButtonBorderStyle.Solid
            //    , TEA.ColorTabButtonBorder, 1, ButtonBorderStyle.Solid
            //    , TEA.ColorTabButtonBorder, 1, ButtonBorderStyle.Solid
            //    , TEA.ColorTabButtonBorder, 1, ButtonBorderStyle.Solid
            //    );
            TEA.labelAA_Paint(sender, e);
        }

        private void panel_Paint(object sender, PaintEventArgs e)
        {
            Pen p = new Pen(TEA.ColorTabButtonBorder, 1);
            e.Graphics.DrawRectangle(p, new Rectangle(0, 0, ((Control)sender).Width, ((Control)sender).Height));

            //ControlPaint.DrawBorder(e.Graphics, ((Panel)sender).DisplayRectangle, TEA.ColorPanelBorder, ButtonBorderStyle.Solid);
        }

        Dictionary<Label, Label> DicLabelSelected = new Dictionary<Label, Label>();

        private void label_MouseDown(object sender, MouseEventArgs e)
        {
            Label l = (Label)sender;
            KeyValuePair<String, List<Label>> vp = (KeyValuePair<String, List<Label>>)l.Tag;

            foreach (Label l1 in vp.Value)
            {
                if (l1 == l)
                {
                    l1.BackColor = TEA.ColorTabButtonDownBack;
                }
                else
                {
                    l1.BackColor = TEA.ColorTabButtonOffBack;
                }
            }
        }

        private void label_MouseEnter(object sender, EventArgs e)
        {
            TEA.MouseHand();
            Label l = (Label)sender;
            KeyValuePair<String, List<Label>> vp = (KeyValuePair<String, List<Label>>)l.Tag;

            foreach (Label l1 in vp.Value)
            {
                if (l1 == l)
                {
                    l1.BringToFront();
                    l1.BackColor = TEA.ColorTabButtonDownBack;
                }
                else
                {
                    if (tabSelected(l1))
                        l1.BackColor = TEA.ColorTabButtonDownBack;
                    else
                        l1.BackColor = TEA.ColorTabButtonOffBack;
                }
            }
        }

        private void label_MouseLeave(object sender, EventArgs e)
        {
            MouseNormal();
            Label l = (Label)sender;
            KeyValuePair<String, List<Label>> vp = (KeyValuePair<String, List<Label>>)l.Tag;

            foreach (Label l1 in vp.Value)
            {
                if (tabSelected(l1))
                {
                    l1.BackColor = TEA.ColorTabButtonOnBack;
                }
                else
                {
                    l1.BackColor = TEA.ColorTabButtonOffBack;
                }
            }
        }

        private void label_MouseUp(object sender, MouseEventArgs e)
        {
            Label l = (Label)sender;
            KeyValuePair<String, List<Label>> vp = (KeyValuePair <String, List<Label>>)l.Tag;

            foreach (Label l1 in vp.Value)
            {
                if (l1 == l)
                {
                    if (DicLabelSelected.ContainsKey(l1) == false)
                        DicLabelSelected.Add(l1, l1);
                    l1.BackColor = TEA.ColorTabButtonDownBack;
                }
                else
                {
                    if (DicLabelSelected.ContainsKey(l1))
                        DicLabelSelected.Remove(l1);
                    l1.BackColor = TEA.ColorTabButtonOffBack;
                }
            }

            if (tabSelected(labelGraph))
            {
                panelGraph.Visible = true;
                panelGraphCalc.Visible = false;
            }
            else if (tabSelected(labelGraphCalc))
            {
                panelGraph.Visible = false;
                panelGraphCalc.Visible = true;
            }

            UIInvalidate();
        }

        public void UIInvalidate()
        {
            panelAlarm.Invalidate();
            panelGraph.Invalidate();
            panelCalcData.Invalidate();
            panelGraphCalcDataBottom.Invalidate();
        }

        // gggggggggggggggggggggggg
        private void panelGraph_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
            
            Pen ptemp = new Pen(TEA.ColorTabButtonBorder, 1);
            e.Graphics.DrawRectangle(ptemp, new Rectangle(0, 0, ((Control)sender).Width, ((Control)sender).Height));
            //ControlPaint.DrawBorder(g, ((Panel)sender).DisplayRectangle, TEA.ColorPanelBorder, ButtonBorderStyle.Solid);
            
            //if (tabSelected(labelGraphp) || tabSelected(labelGraphCalc))
            {
                int y = 6;
                int x = 31;
                int i = 0;

                Pen pen = new Pen(TEA.ColorPanelBorder);

                while (i < 12)
                {
                    y += 35;

                    // circle
                    Rectangle rectCircle = new Rectangle(x, y, 10, 10);
                    Boolean bb = false;
                    if (i < _chart.ViewXY.SampleDataSeries.Count)
                    {
                        if (_chart.ViewXY.SampleDataSeries[i].Visible)
                        {
                            SolidBrush b = new SolidBrush(TEA.ListColorCH[i]);
                            g.FillEllipse(b, rectCircle);

                            bb = true;
                        }
                    }

                    if (bb == false)
                    {
                        Pen p = new Pen(TEA.ColorPanelBorder);
                        g.DrawEllipse(p, rectCircle);
                    }
                    
                    // label
                    SolidBrush solidBrush = new SolidBrush(TEA.ColorLabelFont);
                    string s = "Cyl. " + (i + 1).ToString();

                    g.TextRenderingHint = TextRenderingHint.AntiAlias;
                    g.DrawString(s, TEA.fontMd14, solidBrush, new PointF(x + 16, y - 3));

                    // line
                    if (i < 11)
                        g.DrawLine(pen, x - 6, y + 5 + 16, x - 6 + 75, y + 5 + 16);

                    //
                    String sKey = "checkCHVisible" + i.ToString();
                    if (DicSeriesEvent.ContainsKey(sKey) == false)
                    {
                        DicSeriesEvent.Add(sKey, new Rectangle(x, y - 3, 75, 16));
                    }

                    i++;
                }
            }
            //catch
            //{

            //}
        }
        
        private void panelGraph_MouseMove(object sender, MouseEventArgs e)
        {
            Rectangle r = new Rectangle(0, 0, 120, 475);

            if (r.IntersectsWith(new Rectangle(e.Location, new Size(1, 1))))
            {
                Boolean bIsFind = false;
                foreach (KeyValuePair<String, Rectangle> vp in DicSeriesEvent)
                {
                    if (vp.Value.IntersectsWith(new Rectangle(e.Location, new Size(1, 1))))
                    {
                        bIsFind = true;
                        break;
                    }
                }
                if (bIsFind)
                {
                    MouseHand();
                }
                else
                {
                    MouseNormal();
                }
            }
        }

        private void panelGraph_MouseLeave(object sender, EventArgs e)
        {

        }

        private void panelGraph_MouseEnter(object sender, EventArgs e)
        {
        }

        private void panelGraph_MouseDown(object sender, MouseEventArgs e)
        {
            foreach (KeyValuePair<String, Rectangle> vp in DicSeriesEvent)
            {
                if (vp.Value.IntersectsWith(new Rectangle(e.Location, new Size(1, 1))))
                {
                    if (DicSeriesEventDown.ContainsKey(vp.Key) == false)
                        DicSeriesEventDown.Add(vp.Key, vp.Value);
                }
            }
        }

        private void panelGraph_MouseUp(object sender, MouseEventArgs e)
        {
            foreach(KeyValuePair<String, Rectangle> vp in DicSeriesEvent)
            {
                if (DicSeriesEventDown.ContainsKey(vp.Key))
                {
                    if (vp.Value.IntersectsWith(new Rectangle(e.Location, new Size(1, 1))))
                    {
                        Boolean bIsCtrl = false;
                        if (e.Button == MouseButtons.Left && (ModifierKeys & Keys.Control) == Keys.Control)
                        { 
                            bIsCtrl = true;
                        }
                        EventToggleCH(vp.Key, bIsCtrl);
                    }
                }
            }
            DicSeriesEventDown.Clear();
        }

        public void EventToggleCH(String AParam, Boolean ACtrl = false)
        {
            try
            {
                if (AParam.StartsWith("checkCHVisible"))
                {
                    if (ACtrl)
                    {
                        int iCHFind = Convert.ToInt32(AParam.Substring(14));

                        Boolean b = !_chart.ViewXY.SampleDataSeries[iCHFind].Visible;

                        int iCH = 0;
                        while (iCH < 12)
                        {
                            _chart.ViewXY.SampleDataSeries[iCH].Visible = b;
                            if (iCH < _chartCalcData.Series.Count)
                                _chartCalcData.Series[iCH].Enabled = b;
                            iCH++;
                        }
                    }
                    else
                    {
                        int iCH = Convert.ToInt32(AParam.Substring(14));

                        _chart.ViewXY.SampleDataSeries[iCH].Visible = !_chart.ViewXY.SampleDataSeries[iCH].Visible;

                        if (iCH < _chartCalcData.Series.Count)
                            _chartCalcData.Series[iCH].Enabled = _chart.ViewXY.SampleDataSeries[iCH].Visible;
                    }
                }
            }
            catch
            {

            }

            if(panelGraph.Visible)
                panelGraph.Invalidate();
            if (panelGraphCalc.Visible)
                panelGraphCalc.Invalidate();
        }

        private void buttonGraphPAxisReset_Paint(object sender, PaintEventArgs e)
        {
        }

        private void panelGraphRight_Paint(object sender, PaintEventArgs e)
        {
            //Graphics g = e.Graphics;
            //g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            //g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;

            //ControlPaint.DrawBorder(g, ((Panel)sender).DisplayRectangle, TEA.ColorPanelBorder, ButtonBorderStyle.Solid);
            
            //int y = 6;
            //int x = 31;
            //int i = 0;

            //Pen pen = new Pen(TEA.ColorPanelBorder);

            //while (i < 12)
            //{
            //    y += 33;

            //    // circle
            //    Rectangle rectCircle = new Rectangle(x, y, 10, 10);
            //    if (_chart.ViewXY.SampleDataSeries[i].Visible)
            //    {
            //        SolidBrush b = new SolidBrush(TEA.ListColorCH[i]);
            //        g.FillEllipse(b, rectCircle);
            //    }
            //    else
            //    {
            //        Pen p = new Pen(TEA.ColorPanelBorder);
            //        g.DrawEllipse(p, rectCircle);
            //    }

            //    // label
            //    SolidBrush solidBrush = new SolidBrush(TEA.ColorLabelFont);
            //    string s = "Cyl. " + (i + 1).ToString();

            //    g.TextRenderingHint = TextRenderingHint.AntiAlias;
            //    g.DrawString(s, EA.fontMd14, solidBrush, new PointF(x + 16, y - 3));

            //    // line
            //    g.DrawLine(pen, x - 6, y + 5 + 16, x - 6 + 75, y + 5 + 16);

            //    //
            //    String sKey = "checkCHVisible" + i.ToString();
            //    if (DicEvent.ContainsKey(sKey) == false)
            //    {
            //        DicEvent.Add(sKey, new Rectangle(x, y - 3, 75, 16));
            //    }

            //    i++;
            //}
        }

        private void buttonGraphPAxisReset_Click(object sender, EventArgs e)
        {
            EditIP.Focus();

            ChartRecalc(_chart);

            GraphToUI();
        }

        private void editGraphPYMin_TextChanged(object sender, EventArgs e)
        {
            int i = 0;
            TextBox t = (TextBox)sender;
            if (Int32.TryParse(t.Text, out i))
            {
                String[] sa = t.Text.Split('\r');
                if (sa.Length > 0)
                {
                    t.Text = sa[0];
                }

                try
                {
                    _chart.ViewXY.YAxes[0].SetRange(Convert.ToDouble(editGraphPYMin.Text), Convert.ToDouble(editGraphPYMax.Text));
                }
                catch
                {

                }

                //ChartRecalc(_chart);
            }
            else
            {
                //t.Text = "0";
            }
        }

        private void panelCalcData_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
            g.TextRenderingHint = TextRenderingHint.AntiAlias;

            ControlPaint.DrawBorder(g, ((Panel)sender).DisplayRectangle, TEA.ColorPanelBorder, ButtonBorderStyle.Solid);            
        }

        private void panelGraphCalcDataBottom_Paint(object sender, PaintEventArgs e)
        {

            Graphics g = e.Graphics;
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
            g.TextRenderingHint = TextRenderingHint.AntiAlias;
            
            int iCHCnt = 12;
            int i = 0;
            int y = panelGraphCalcDataBottom.Height - 25 - 20;
            int w = 880 - 42 - 25;

            int iXStep = (int)(w / iCHCnt + 1);
            int x = 42 + ((iXStep / 4) * 3);

            w = 880 - 42 - 25 - (((iXStep / 4) * 3) * 1);
            iXStep = (int)(w / iCHCnt);

            //chartPMax.ChartAreas[0].Position.X = 4;
            //chartPMax.ChartAreas[0].Position.Width = 92;

            if (chartPMax.Series[0].Points.Count == iCHCnt + 2)
            {
                while (i < iCHCnt)
                {
                    // label
                    SolidBrush solidBrush = new SolidBrush(TEA.ColorLabelFont);
    
                    string s = String.Format("{0:#,##0.0}", chartPMax.Series[0].Points[i + 1].YValues[0]);
                    SizeF sz = g.MeasureString(s, TEA.fontMdCn20);
                    g.DrawString(s, TEA.fontMdCn20, solidBrush, new PointF(x - (sz.Width / 2), y));

                    s = "cyl." + (i + 1).ToString();
                    sz = g.MeasureString(s, TEA.fontMd12);
                    g.DrawString(s, TEA.fontMd12, solidBrush, new PointF(x - (sz.Width / 2), y - 14));

                    i++;
                    x += iXStep;
                }
            }
        }

        private void buttonSettingClose_Click(object sender, EventArgs e)
        {
            
        }

        private void panelTop_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
            panelGraph.Invalidate();
            //panelCalcData.Invalidate();
            panelGraphCalcDataBottom.Invalidate();
        }

        private void panelSetting_Paint(object sender, PaintEventArgs e)
        {
        }

        private void buttonSetting_MouseClick(object sender, MouseEventArgs e)
        {
            UISetting();

            //panelSetting.Top = 112;
            //panelSetting.Visible = !panelSetting.Visible;
            //panelSetting.BringToFront();
        }

        private void panelAlarm_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;

            ControlPaint.DrawBorder(g, ((Panel)sender).DisplayRectangle, TEA.ColorPanelBorder, ButtonBorderStyle.Solid);

            //try
            {
                Pen pen = new Pen(TEA.ColorPanelBorder);

                if (tabSelected(labelAlarmSys))
                {
                    int y = 16;
                    int x = 35;
                    int i = 0;

                    String[] sLabel = new string[]{"SYSTEM State Flag"
                    , "PICKUP SENSING #1 fault 1"
                    , "PICKUP SENSING #1 fault 2"
                    , "PICKUP SENSING #2 fault 1"
                    , "PICKUP SENSING #2 fault 2"
                    , "Missing tooth fault"
                    , "Pressure Sensor Fail" };

                    while (i < 7)
                    {
                        // circle
                        Rectangle rectCircle = new Rectangle(x + 390, y, 18, 18);
                        if (TCube.GetBit((int)GetHR(501), i))
                        {
                            SolidBrush b = new SolidBrush(ColorTranslator.FromHtml("#d64027")); // a6372a
                            g.FillEllipse(b, rectCircle);
                        }
                        else
                        {
                            SolidBrush b = new SolidBrush(ColorTranslator.FromHtml("#e2e6e9")); // a6372a
                            g.FillEllipse(b, rectCircle);
                        }

                        // label
                        SolidBrush solidBrush = new SolidBrush(TEA.ColorLabelFont);
                        string s = sLabel[i];

                        g.TextRenderingHint = TextRenderingHint.AntiAlias;
                        g.DrawString(s, TEA.fontMd14, solidBrush, new PointF(x, y - 0));

                        // line
                        if (i < 6)
                            g.DrawLine(pen, 25, y + 5 + 18, 25 + 450, y + 5 + 18);

                        y += 30;
                        i++;
                    }
                }
                else
                {
                    int iTabID = 0;
                    if (tabSelected(labelAlarmFault)) iTabID = 0;
                    else if (tabSelected(labelAlarmMisfire)) iTabID = 1;
                    else if (tabSelected(labelAlarmLKnock)) iTabID = 2;
                    else if (tabSelected(labelAlarmHKnock)) iTabID = 3;

                    String[] sLabel = new string[]{"Pressure Sensor Fault"
                    , "Misfire"
                    , "L.Knock"
                    , "H.Knock" };
                    int[] iAddr = new[] { 502, 503, 504, 505 };

                    int y = 16 + 15;
                    int x = 35;
                    int i = 0;
                    
                    {
                        // label
                        SolidBrush solidBrush = new SolidBrush(TEA.ColorLabelFont);
                        string s = sLabel[iTabID];

                        g.TextRenderingHint = TextRenderingHint.AntiAlias;
                        g.DrawString(s, TEA.fontMd14, solidBrush, new PointF(x, y - 0));
                    }

                    x = 202;// 180;

                    while (i < 12)
                    {
                        if (i == 6)
                        {
                            y = 16 + 15;
                            x = 370; //350;
                        }

                        // circle 250,400
                        Rectangle rectCircle = new Rectangle(x + 85, y, 18, 18);
                        
                        if (TCube.GetBit((int)GetHR(iAddr[iTabID]), i))
                        {
                            SolidBrush b = new SolidBrush(ColorTranslator.FromHtml("#d64027"));
                            g.FillEllipse(b, rectCircle);
                        }
                        else
                        {
                            SolidBrush b = new SolidBrush(ColorTranslator.FromHtml("#e2e6e9")); 
                            g.FillEllipse(b, rectCircle);
                        }

                        // label
                        SolidBrush solidBrush = new SolidBrush(TEA.ColorLabelFont);
                        string s = "Cyl. " + (i + 1).ToString();

                        g.TextRenderingHint = TextRenderingHint.AntiAlias;
                        g.DrawString(s, TEA.fontMd14, solidBrush, new PointF(x, y - 0));

                        // line
                        if (i < 6)
                            g.DrawLine(pen, 25, y + 5 + 18, 25 + 450, y + 5 + 18);

                        y += 30;
                        i++;
                    }
                }
            }
        }

        private void panelRPM_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;

            ControlPaint.DrawBorder(g, ((Panel)sender).DisplayRectangle, TEA.ColorPanelBorder, ButtonBorderStyle.Solid);

            // 300 x 269
            // | 36   center   36 |

            int iAngleValue = (int)((RPMNow / RPMMax) * 300);
            int iAngleEmpty = 300 - iAngleValue;

            {
                var center = new Point(300 / 2, 269 / 2);
                var innerR = 95;
                var thickness = 20;
                var startAngle = 120;
                var arcLength = iAngleValue;// 300;
                var outerR = innerR + thickness;
                var outerRect = new Rectangle
                                (center.X - outerR, center.Y - outerR, 2 * outerR, 2 * outerR);
                var innerRect = new Rectangle
                                (center.X - innerR, center.Y - innerR, 2 * innerR, 2 * innerR);

                SolidBrush b = new SolidBrush(ColorTranslator.FromHtml("#0059b2")); // a6372a

                using (var p = new GraphicsPath())
                {
                    p.AddArc(outerRect, startAngle, arcLength);
                    p.AddArc(innerRect, startAngle + arcLength, -arcLength);
                    p.CloseFigure();
                    e.Graphics.FillPath(b, p);
                    //e.Graphics.DrawPath(Pens.Black, p);
                }
            }
            {
                var center = new Point(300 / 2, 269 / 2);
                var innerR = 95;
                var thickness = 20;
                var startAngle = 120 + iAngleValue;
                var arcLength = iAngleEmpty;
                var outerR = innerR + thickness;
                var outerRect = new Rectangle
                                (center.X - outerR, center.Y - outerR, 2 * outerR, 2 * outerR);
                var innerRect = new Rectangle
                                (center.X - innerR, center.Y - innerR, 2 * innerR, 2 * innerR);

                SolidBrush b = new SolidBrush(Color.White); // a6372a

                using (var p = new GraphicsPath())
                {
                    p.AddArc(outerRect, startAngle, arcLength);
                    p.AddArc(innerRect, startAngle + arcLength, -arcLength);
                    p.CloseFigure();
                    e.Graphics.FillPath(b, p);
                    //e.Graphics.DrawPath(Pens.Black, p);
                }
            }
        }

        private void labelCalcDataAvg_Click(object sender, EventArgs e)
        {

        }

        private void panelTop_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;

            // time
            SolidBrush solidBrush = new SolidBrush(ColorTranslator.FromHtml("#6C6F70"));
            string s = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            g.TextRenderingHint = TextRenderingHint.AntiAlias;
            g.DrawString(s, TEA.fontMdCn14, solidBrush, new PointF(1745, 20));

            g.DrawString(s, TEA.fontMdCn14, solidBrush, new PointF(1745, 38));
        }

        private void panelExit_MouseClick(object sender, MouseEventArgs e)
        {
            Close();
        }

        // uuuuuuuuuuuuuuuu
        public void UIInit()
        {
            int ix = 260;

            Rectangle r = new Rectangle(ix, 150, ix + 500 + 190, 22 + 80);

            EventAdd(panelGraphCalc, "radiopmax", new Rectangle(ix, 190, 90, 22), r);
            ix += 100;
            EventAdd(panelGraphCalc, "radioimep", new Rectangle(ix, 190, 90, 22), r);
            ix += 100;
            EventAdd(panelGraphCalc, "radiohr", new Rectangle(ix, 190, 90, 22), r);
            ix += 100;
            EventAdd(panelGraphCalc, "radiomisfire", new Rectangle(ix, 190, 90, 22), r);
            ix += 100;
            EventAdd(panelGraphCalc, "radiopegging", new Rectangle(ix, 190, 90, 22), r);
            ix += 100;
            EventAdd(panelGraphCalc, "radioknockinst", new Rectangle(ix, 190, 90, 22), r);
            ix += 100;
        }

        // cccccccccccccccccccccccccccccccccccc
        private void panelGraphCalc_Paint(object sender, PaintEventArgs e)
        {
            if (panelGraphCalc.Visible == false)
                return;

            panelGraph_Paint(sender, e);

            Graphics g = e.Graphics;
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;

            ControlPaint.DrawBorder(g, ((Panel)sender).DisplayRectangle, TEA.ColorPanelBorder, ButtonBorderStyle.Solid);

            //
            label(g, 122, 25, "RPM", TEA.fontMd16);// "Md16");
            label(g, 122, 190, "Calculation Data", TEA.fontMd16);// "Md16");

            int iX = 260;
            radio(g, iX, 190, "Pmax", TEA.fontMd12, radioCalcData == 0); iX += 100;
            radio(g, iX, 190, "IMEP", TEA.fontMd12, radioCalcData == 1); iX += 100;
            radio(g, iX, 190, "HR", TEA.fontMd12, radioCalcData == 2); iX += 100;
            radio(g, iX, 190, "Misfire", TEA.fontMd12, radioCalcData == 3); iX += 100;
            radio(g, iX, 190, "Pegging", TEA.fontMd12, radioCalcData == 4); iX += 100;
            radio(g, iX, 190, "Knock Inst.", TEA.fontMd12, radioCalcData == 5); iX += 100;
        }

        public void EventMouseMove(object ASender, Point ALocation)
        {
            Control ctrl = (Control)ASender;

            int iFind = -1;

            foreach (KeyValuePair<String, TControlEvent> kvp in DicEvent)
            {
                if (kvp.Key.Split(':').ToArray()[0] == ctrl.Name)
                {
                    if (kvp.Value.MouseInsideRect.IntersectsWith(new Rectangle(ALocation, new Size(1, 1))))
                    {
                        if (iFind == -1)
                            iFind = 0;
                        if (kvp.Value.Rect.IntersectsWith(new Rectangle(ALocation, new Size(1, 1))))
                        {
                            iFind++;
                        }
                    }
                }
            }
            if (iFind > 0)
            {
                MouseHand();
            }
            else if (iFind == 0)
            {
                MouseNormal();
            }
            else if (iFind == -1)
            {
            }
        }

        public void EventMouseDown(object ASender, Point ALocation)
        {
            Control ctrl = (Control)ASender;

            foreach (KeyValuePair<String, TControlEvent> kvp in DicEvent)
            {
                if (kvp.Key.Split(':').ToArray()[0] == ctrl.Name)
                {
                    if(kvp.Value.Rect.IntersectsWith(new Rectangle(ALocation, new Size(1, 1))))
                    {
                        if (DicEventMouseDown.ContainsKey(kvp.Key) == false)
                            DicEventMouseDown.Add(kvp.Key, kvp.Value);
                    }
                }
            }
        }

        public void EventMouseUp(object ASender, Point ALocation)
        {
            Control ctrl = (Control)ASender;

            foreach (KeyValuePair<String, TControlEvent> kvp in DicEvent)
            {
                if (kvp.Key.Split(':').ToArray()[0] == ctrl.Name)
                {
                    foreach (TControlEvent ce in DicEventMouseDown.Values)
                    {
                        if (ce.Rect.IntersectsWith(new Rectangle(ALocation, new Size(1, 1))))
                        {
                            EventDo(ce.Key);
                        }
                    }
                }
            }
            
            DicEventMouseDown.Clear();
        }

        public void EventDo(String AKey)
        {
            TControlEvent ce = null;

            if (DicEvent.TryGetValue(AKey, out ce) == false)
                return;

            Boolean bisUpdate = false;

            { 
                // radiocalcdata
                int radioCalcDataLast = radioCalcData;

                if (ce.ID == "radiopmax")
                    radioCalcData = 0;
                else if (ce.ID == "radioimep")
                    radioCalcData = 1;
                else if (ce.ID == "radiohr")
                    radioCalcData = 2;
                else if (ce.ID == "radiomisfire")
                    radioCalcData = 3;
                else if (ce.ID == "radiopegging")
                    radioCalcData = 4;
                else if (ce.ID == "radioknockinst")
                    radioCalcData = 5;

                if (radioCalcData != radioCalcDataLast)
                {
                    bisUpdate = true;

                    _chartCalcData.Series.Clear();
                }
            }

            if (bisUpdate)
            {
                ce.Dest.Invalidate();
            }
        }

        public void EventAdd(Control AControl, String AID, Rectangle ARect, Rectangle AMouseInsdeRect)
        {
            String sKey = AControl.Name + ":" + AID;
            if (DicEvent.ContainsKey(sKey) == false)
            {
                TControlEvent ce = new TControlEvent
                {
                    Dest = AControl,
                    Key = sKey,
                    ID = AID,
                    Rect = ARect,
                    MouseInsideRect = AMouseInsdeRect
                };
                DicEvent.Add(sKey, ce);
            }
        }

        public void label(Graphics AG, int AX, int AY, String AText,Font AFont)
        {
            SolidBrush solidBrush = new SolidBrush(ColorTranslator.FromHtml("#6C6F70"));
            
            //if (AFont == "Md12")
            //    f = EA.fontMd12;
            //else if (AFont == "Md14")
            //    f = EA.fontMd14;
            //else if (AFont == "Md16")
            //    f = EA.fontMd16;
            //else if (AFont == "MdCn14")
            //    f = EA.fontMdCn14;
            //else if (AFont == "MdCn20")
            //    f = EA.fontMdCn20;

            AG.TextRenderingHint = TextRenderingHint.AntiAlias;
            AG.DrawString(AText, AFont, solidBrush, new PointF(AX, AY));


        }

        public void radio(Graphics AG, int AX, int AY, String AText, Font AFont, Boolean AChecked)
        {
            Rectangle rectCircle = new Rectangle(AX, AY, 22, 22);
            Rectangle rectCheck  = new Rectangle(AX + 6, AY + 6, 10, 10);

            SolidBrush b = new SolidBrush(ColorTranslator.FromHtml("#FFFFFF"));
            AG.FillEllipse(b, rectCircle);

            Pen p = new Pen(ColorTranslator.FromHtml("#CFCFD4"));
            AG.DrawEllipse(p, rectCircle);

            if (AChecked)
            {
                b = new SolidBrush(ColorTranslator.FromHtml("#95999B"));
                AG.FillEllipse(b, rectCheck);
            }

            // label
            SolidBrush solidBrush = new SolidBrush(ColorTranslator.FromHtml("#6C6F70"));
            
            AG.TextRenderingHint = TextRenderingHint.AntiAlias;
            AG.DrawString(AText, AFont, solidBrush, new PointF(AX + 27, AY + 5));
        }

        private void panelGraphCalc_MouseDown(object sender, MouseEventArgs e)
        {
            panelGraph_MouseDown(sender, e);

            EventMouseDown(sender, e.Location);
        }

        private void panelGraphCalc_MouseUp(object sender, MouseEventArgs e)
        {
            panelGraph_MouseUp(sender, e);

            EventMouseUp(sender, e.Location);
        }

        private void editCalcDataRPMYMin_TextChanged(object sender, EventArgs e)
        {
            if (editCalcDataRPMYMin.Tag != null || editCalcDataRPMYMax.Tag != null)
                return;

            int i = 0;
            TextBox t = (TextBox)sender;
            if (Int32.TryParse(t.Text, out i))
            {
                String[] sa = t.Text.Split('\r');
                if (sa.Length > 0)
                {
                    t.Text = sa[0];
                }

                try
                {
                    double dMin = Convert.ToDouble(editCalcDataRPMYMin.Text);
                    double dMax = Convert.ToDouble(editCalcDataRPMYMax.Text);

                    if (dMin < dMax)
                    {
                        if (dMin < _chartCalcDataRPM.ChartAreas[0].AxisY.Maximum)
                        {
                            _chartCalcDataRPM.ChartAreas[0].AxisY.Minimum = dMin;
                            _chartCalcDataRPM.ChartAreas[0].AxisY.Maximum = dMax;
                        }
                        else
                        {
                            _chartCalcDataRPM.ChartAreas[0].AxisY.Maximum = dMax;
                            _chartCalcDataRPM.ChartAreas[0].AxisY.Minimum = dMin;
                        }
                    }
                }
                catch
                {

                }

                //ChartRecalc(_chart);
            }
            else
            {
                //t.Text = "0";
            }
        }

        private void editCalcDataYMin_TextChanged(object sender, EventArgs e)
        {
            if (editCalcDataYMin.Tag != null || editCalcDataYMax.Tag != null)
                return;

            int i = 0;
            TextBox t = (TextBox)sender;
            if (Int32.TryParse(t.Text, out i))
            {
                String[] sa = t.Text.Split('\r');
                if (sa.Length > 0)
                {
                    t.Text = sa[0];
                }

                try
                {
                    double dMin = Convert.ToDouble(editCalcDataYMin.Text);
                    double dMax = Convert.ToDouble(editCalcDataYMax.Text);

                    if (dMin < dMax)
                    {
                        if (dMin < _chartCalcData.ChartAreas[0].AxisY.Maximum)
                        {
                            _chartCalcData.ChartAreas[0].AxisY.Minimum = dMin;
                            _chartCalcData.ChartAreas[0].AxisY.Maximum = dMax;
                        }
                        else
                        {
                            _chartCalcData.ChartAreas[0].AxisY.Maximum = dMax;
                            _chartCalcData.ChartAreas[0].AxisY.Minimum = dMin;
                        }
                    }
                }
                catch
                {

                }

                //ChartRecalc(_chart);
            }
            else
            {
                //t.Text = "0";
            }
        }

        private void panelCalcDataRPMYReset_MouseClick(object sender, MouseEventArgs e)
        {
            bIsCalcDataRPMYAuto = true;

            try
            {
                _chartCalcDataRPM.ChartAreas[0].AxisY.Maximum = Double.NaN;
                _chartCalcDataRPM.ChartAreas[0].AxisY.Minimum = Double.NaN;
            }
            catch
            {

            }
            EditIP.Focus();
        }

        private void panelCalcDataYReset_MouseClick(object sender, MouseEventArgs e)
        {
            bIsCalcDataYAuto = true;

            try
            {
                _chartCalcData.ChartAreas[0].AxisY.Maximum = Double.NaN;
                _chartCalcData.ChartAreas[0].AxisY.Minimum = Double.NaN;
            }
            catch
            {

            }
            EditIP.Focus();
        }

        private void panelMonitoring_MouseClick(object sender, MouseEventArgs e)
        {
            
        }

        private void obj_MouseEnter(object sender, EventArgs e)
        {
            MouseHand();
            try
            {
                Panel p = (Panel)sender;
                if (p.Tag != null)
                {
                    String s = p.Tag.ToString();
                    Image img = Image.FromFile(TCube.GetPathResource() + "\\" + s + "_pressed.png");
                    p.BackgroundImage = img;
                }
            }
            catch
            {

            }
        }

        private void obj_MouseLeave(object sender, EventArgs e)
        {
            MouseNormal();

            try
            {
                Panel p = (Panel)sender;
                if (p.Tag != null)
                {
                    String s = p.Tag.ToString();
                    Image img = Image.FromFile(TCube.GetPathResource() + "\\" + s + "_normal.png");
                    p.BackgroundImage = img;
                }
            }
            catch
            {

            }
        }

        private void imageLogginOn_Click(object sender, EventArgs e)
        {
            IsSave = false;
            FormSetting.CheckExportChecked = false;
        }

        private void imageLogginOff_Click(object sender, EventArgs e)
        {
            IsSave = true;
            FormSetting.CheckExportChecked = true;
        }

        private void comboOutside_Click(object sender, EventArgs e)
        {
            comboHide();
        }

        private void combo_Click(object sender, EventArgs e)
        {
            Panel p = (Panel)sender;

            if (comboY.Visible == true)
            {
                comboHide();
                return;
            }

            if (p == comboCalcDataRPMMax || p == comboCalcDataYMax || p == comboYMax)
            {
                comboY.Items.Clear();
                comboY.Items.Add("50");
                comboY.Items.Add("100");
                comboY.Items.Add("150");
            }
            else
            {
                comboY.Items.Clear();
                comboY.Items.Add("-10");
                comboY.Items.Add("0");
                comboY.Items.Add("10");
            }

            if (p == comboCalcDataRPMMax)
                editComboDest = editCalcDataRPMYMax;
            else if (p == comboCalcDataYMax)
                editComboDest = editCalcDataYMax;
            else if (p == comboYMax)
                editComboDest = editGraphPYMax;
            else if (p == comboYMin)
                editComboDest = editGraphPYMin;
            else if (p == comboCalcDataRPMYMin)
                editComboDest = editCalcDataRPMYMin;
            else if (p == comboCalcDataYMin)
                editComboDest = editCalcDataYMin;
            else if (p == comboYMin)
                editComboDest = editGraphPYMin;

            if (editComboDest != null)
            {
                ComboShow(p);
            }
        }

        TextBox editComboDest = null;

        public void comboHide()
        {
            comboY.Visible = false;
         
            editComboDest.Parent.Click -= comboOutside_Click;
            editComboDest.Parent.Controls.Remove(comboY);
        }

        public void ComboShow(Panel ADest)
        {            
            if (editComboDest.Parent != null)
            {
                editComboDest.Parent.Controls.Add(comboY);
                editComboDest.Parent.Click += comboOutside_Click;
            }
            comboY.Top = ADest.Top + 29;
            comboY.Left = ADest.Left - 39;
            comboY.Visible = true;
            comboY.BringToFront();
        }

        private void comboY_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (editComboDest != null)
            {
                editComboDest.Text = comboY.SelectedItem.ToString();
            }
            comboHide();
        }

        private void panelGraphCalc_MouseMove(object sender, MouseEventArgs e)
        {
            panelGraph_MouseMove(sender, e);

            EventMouseMove(sender, e.Location);
        }

        private void panelMenuCalibration_Click(object sender, EventArgs e)
        {
            panelMenuCal.Visible = !panelMenuCal.Visible;
            if (panelMenuCal.Visible)
            {
                panelMenuCal.BringToFront();
            }


            labelMenuMonitoring.ForeColor = ColorTranslator.FromHtml("#95999B");
            labelMenuCal.ForeColor = ColorTranslator.FromHtml("#FFFFFF");
            labelMenuDiagnosis.ForeColor = ColorTranslator.FromHtml("#95999B");

            labelMenuMonitoring.Invalidate();
            labelMenuCal.Invalidate();
            labelMenuDiagnosis.Invalidate();

            panelMonitoring.Invalidate();

            //panelSubmenu.Visible = !panelSubmenu.Visible;
        }

        private void TFormEA_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;

            //Rectangle destRect = new Rectangle(panelSubmenu.Left, panelSubmenu.Top, panelSubmenu.Width, panelSubmenu.Height);
            //Rectangle sourceRect = new Rectangle(0, 0, imgSubmenu.Width, imgSubmenu.Height);

            //g.DrawImage(imgSubmenu, destRect, sourceRect, GraphicsUnit.Pixel);
        }
        
        private void buttonLogout_Click(object sender, EventArgs e)
        {
            if (formLogin != null)
                formLogin.Show();
            Hide();
        }

        public void UIMonitoring()
        {
            TCS.eaCmd.FlagAICalData = false;
            TCS.eaCmd.FlagCmdParameter = 0;


            labelMenuMonitoring.ForeColor = ColorTranslator.FromHtml("#FFFFFF");
            labelMenuCal.ForeColor = ColorTranslator.FromHtml("#95999B");
            labelMenuDiagnosis.ForeColor = ColorTranslator.FromHtml("#95999B");

            labelMenuMonitoring.Invalidate();
            labelMenuCal.Invalidate();
            labelMenuDiagnosis.Invalidate();

            panelMonitoring.Invalidate();

            if (panelMenuCal.Visible)
                panelMenuCal.Visible = false;

            if (FormCalKnock != null)
            {
                if (FormCalKnock.panelForm.Visible)
                {
                    FormCalKnock.panelForm.Visible = false;
                }
            }
            if (FormCalTDC != null)
            {
                if (FormCalTDC.panelForm.Visible)
                {
                    FormCalTDC.panelForm.Visible = false;
                }
            }
            if (FormDignosis != null)
            {
                if (FormDignosis.panelForm.Visible)
                {
                    FormDignosis.panelForm.Visible = false;
                }
            }
            if (FormSetting.panelForm.Visible)
                FormSetting.panelForm.Visible = false;

            panelForm.Visible = true;
        }

        TFormSetting FormSetting;// = new TFormSetting();
        public void UISetting()
        {
            TCS.eaCmd.FlagAICalData = false;
            TCS.eaCmd.FlagCmdParameter = 0;

            labelMenuMonitoring.ForeColor = ColorTranslator.FromHtml("#95999B");
            labelMenuCal.ForeColor = ColorTranslator.FromHtml("#95999B");
            labelMenuDiagnosis.ForeColor = ColorTranslator.FromHtml("#95999B");

            labelMenuMonitoring.Invalidate();
            labelMenuCal.Invalidate();
            labelMenuDiagnosis.Invalidate();

            if (panelMenuCal.Visible)
                panelMenuCal.Visible = false;

            if (panelForm.Visible)
            {
                panelForm.Visible = false;
            }
            if (FormCalKnock != null)
            {
                if (FormCalKnock.panelForm.Visible)
                {
                    FormCalKnock.panelForm.Visible = false;
                }
            }
            if (FormCalTDC != null)
            {
                if (FormCalTDC.panelForm.Visible)
                {
                    FormCalTDC.panelForm.Visible = false;
                }
            }
            if (FormDignosis != null)
            {
                if (FormDignosis.panelForm.Visible)
                {
                    FormDignosis.panelForm.Visible = false;
                }
            }

            FormSetting.panelForm.Visible = true;
            FormSetting.UIToFlagCmdParameter();
            //eaCmd.FlagCmdParameter = 1;
        }

        TFormDignosis FormDignosis;// = new TFormSetting();
        public void UIDignosis()
        {
            TCS.eaCmd.FlagAICalData = false;
            TCS.eaCmd.FlagCmdParameter = 0;


            labelMenuMonitoring.ForeColor = ColorTranslator.FromHtml("#95999B");
            labelMenuCal.ForeColor = ColorTranslator.FromHtml("#95999B");
            labelMenuDiagnosis.ForeColor = ColorTranslator.FromHtml("#FFFFFF");

            labelMenuMonitoring.Invalidate();
            labelMenuCal.Invalidate();
            labelMenuDiagnosis.Invalidate();

            panelMenuDignosis.Invalidate();

            if (panelMenuCal.Visible)
                panelMenuCal.Visible = false;

            if (panelForm.Visible)
            {
                panelForm.Visible = false;
            }
            if (FormCalKnock != null)
            {
                if (FormCalKnock.panelForm.Visible)
                {
                    FormCalKnock.panelForm.Visible = false;
                }
            }
            if (FormCalTDC != null)
            {
                if (FormCalTDC.panelForm.Visible)
                {
                    FormCalTDC.panelForm.Visible = false;
                }
            }
            if (FormSetting.panelForm.Visible)
                FormSetting.panelForm.Visible = false;

            if (FormDignosis == null)
            {
                FormDignosis = new TFormDignosis();
                FormDignosis.TopLevel = false;
                tabSetting.Controls.Add(FormDignosis);
                FormDignosis.Show();

                FormDignosis.panelForm.Visible = false;
                //f.TopLevel = false;
                Controls.Add(FormDignosis.panelForm);
                FormDignosis.panelForm.Location = new Point(0, 75);

                FormDignosis.Show();

                //ChartInitKnock(FormCalKnock.chartCyl);
                //ChartInitKnock(FormCalKnock.chartKnock);
            }
            FormDignosis.panelForm.Visible = true;
            FormDignosis.panelForm.BringToFront();
        }

        TFormCalKnock FormCalKnock;
        public Boolean IsCalKnock
        {
            get
            {
                if (FormCalKnock == null)
                    return false;
                return FormCalKnock.panelForm.Visible;
            }
        }
        public void UICalKnock()
        {
            TCS.eaCmd.FlagAICalData = false;
            TCS.eaCmd.FlagCmdParameter = 0;


            labelMenuMonitoring.ForeColor = ColorTranslator.FromHtml("#95999B");
            labelMenuCal.ForeColor = ColorTranslator.FromHtml("#FFFFFF");
            labelMenuDiagnosis.ForeColor = ColorTranslator.FromHtml("#95999B");

            labelMenuMonitoring.Invalidate();
            labelMenuCal.Invalidate();
            labelMenuDiagnosis.Invalidate();

            if (panelMenuCal.Visible)
                panelMenuCal.Visible = false;

            if (panelForm.Visible)
            {
                panelForm.Visible = false;
            }
            if (FormSetting.panelForm.Visible)
            {
                FormSetting.panelForm.Visible = false;
            }
            if (FormCalTDC != null)
            {
                if (FormCalTDC.panelForm.Visible)
                {
                    FormCalTDC.panelForm.Visible = false;
                }
            }
            if (FormDignosis != null)
            {
                if (FormDignosis.panelForm.Visible)
                {
                    FormDignosis.panelForm.Visible = false;
                }
            }
            if (FormCalKnock == null)
            {
                FormCalKnock = new TFormCalKnock();
                FormCalKnock.panelForm.Visible = false;
                //f.TopLevel = false;
                Controls.Add(FormCalKnock.panelForm);
                FormCalKnock.panelForm.Location = new Point(0, 75);

                ChartInitKnock(FormCalKnock.chartCyl);
                ChartInitKnock(FormCalKnock.chartKnock);
            }
            FormCalKnock.panelForm.Visible = true;
            FormCalKnock.panelForm.BringToFront();
        }

        TFormCylTDC FormCalTDC;
        public Boolean IsCalTDC
        {
            get
            {
                if (FormCalTDC == null)
                    return false;
                return FormCalTDC.panelForm.Visible;
            }
        }
        public void UICalTDC()
        {
            TCS.eaCmd.FlagAICalData = false;
            TCS.eaCmd.FlagCmdParameter = 0;


            labelMenuMonitoring.ForeColor = ColorTranslator.FromHtml("#95999B");
            labelMenuCal.ForeColor = ColorTranslator.FromHtml("#FFFFFF");
            labelMenuDiagnosis.ForeColor = ColorTranslator.FromHtml("#95999B");

            if (panelMenuCal.Visible)
                panelMenuCal.Visible = false;

            if (FormCalKnock != null)
            {
                if (FormCalKnock.panelForm.Visible)
                {
                    FormCalKnock.panelForm.Visible = false;
                }
            }
            if (FormSetting.panelForm.Visible)
            {
                FormSetting.panelForm.Visible = false;
            }
            if (panelForm.Visible)
            {
                panelForm.Visible = false;
            }
            if (FormDignosis != null)
            {
                if (FormDignosis.panelForm.Visible)
                {
                    FormDignosis.panelForm.Visible = false;
                }
            }
            if (FormCalTDC == null)
            {
                FormCalTDC = new TFormCylTDC();
                FormCalTDC.panelForm.Visible = false;
                //f.TopLevel = false;
                Controls.Add(FormCalTDC.panelForm);
                FormCalTDC.panelForm.Location = new Point(0, 75);

                GenerateData(FormCalTDC._chart, 2, 1440, 1);

                FormCalTDC._chart.ViewXY.SampleDataSeries[0].Visible = true;
                FormCalTDC._chart.ViewXY.SampleDataSeries[1].Visible = true;

                FormCalTDC._chart.ViewXY.SampleDataSeries[0].Title.Text = "Original";
                FormCalTDC._chart.ViewXY.SampleDataSeries[1].Title.Text = "Shift";

                FormCalTDC._bisfrst = true;

                TCS.eaCmd.FlagAICalData = true;
            }
            FormCalTDC.panelForm.Visible = true;
            FormCalTDC.panelForm.BringToFront();
        }

        private void panelMenuCylTDC_MouseEnter(object sender, EventArgs e)
        {
            panelMenuCylTDC.BackgroundImage = imgCylTDC.Image;
        }

        private void panelMenuCylTDC_MouseLeave(object sender, EventArgs e)
        {
            panelMenuCylTDC.BackgroundImage = null;
        }

        private void panelMenuCylKnock_MouseEnter(object sender, EventArgs e)
        {
            panelMenuCylKnock.BackgroundImage = imgCylKnock.Image;
        }

        private void panelMenuCylKnock_MouseLeave(object sender, EventArgs e)
        {
            panelMenuCylKnock.BackgroundImage = null;
        }

        private void panelMenuCylTDC_Click(object sender, EventArgs e)
        {
            panelMenuCal.Visible = false;
            UICalTDC();
        }

        private void panelMenuCylKnock_Click(object sender, EventArgs e)
        {
            panelMenuCal.Visible = false;
            UICalKnock();
        }

        private void panelMenuDignosis_Click(object sender, EventArgs e)
        {
            panelMenuCal.Visible = false;
            UIDignosis();
        }

        private void panelMonitoring_MouseEnter(object sender, EventArgs e)
        {

        }

        private void panelMonitoring_Click(object sender, EventArgs e)
        {
            UIMonitoring();
        }
    }
}

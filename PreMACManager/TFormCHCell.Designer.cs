﻿namespace EngineAnalyzer
{
    partial class TFormCHCell
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelCHCell = new System.Windows.Forms.Panel();
            this.editValue = new System.Windows.Forms.TextBox();
            this.labelCH = new System.Windows.Forms.Label();
            this.panelCHCell.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelCHCell
            // 
            this.panelCHCell.Controls.Add(this.editValue);
            this.panelCHCell.Controls.Add(this.labelCH);
            this.panelCHCell.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelCHCell.Location = new System.Drawing.Point(0, 0);
            this.panelCHCell.Name = "panelCHCell";
            this.panelCHCell.Size = new System.Drawing.Size(310, 21);
            this.panelCHCell.TabIndex = 0;
            // 
            // editValue
            // 
            this.editValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editValue.Location = new System.Drawing.Point(60, 0);
            this.editValue.Name = "editValue";
            this.editValue.Size = new System.Drawing.Size(250, 21);
            this.editValue.TabIndex = 1;
            this.editValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelCH
            // 
            this.labelCH.Dock = System.Windows.Forms.DockStyle.Left;
            this.labelCH.Location = new System.Drawing.Point(0, 0);
            this.labelCH.Name = "labelCH";
            this.labelCH.Size = new System.Drawing.Size(60, 21);
            this.labelCH.TabIndex = 0;
            this.labelCH.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TFormCHCell
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(310, 196);
            this.Controls.Add(this.panelCHCell);
            this.Name = "TFormCHCell";
            this.Text = "TFormCHCell";
            this.panelCHCell.ResumeLayout(false);
            this.panelCHCell.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        public System.Windows.Forms.Panel panelCHCell;
        public System.Windows.Forms.TextBox editValue;
        public System.Windows.Forms.Label labelCH;
    }
}
﻿using DM;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// label
// 프로퍼티
// 소스 : 크기, 색상, 폰트

namespace EngineAnalyzer
{
    public partial class TFormCalKnock : Form
    {
        public float[] arrKnockX;
        public float[] arrKnockCalc;

        public Dictionary<int, PictureBox> DicW = new Dictionary<int, PictureBox>();

        public TFormCalKnock()
        {
            InitializeComponent();

            panelForm.BackgroundImage = imgBack.Image;

            InitCyl();

            InitWindow();

            TEA.ControlCursorAdd(panelOK);

            chartCyl.ChartAreas[0].AxisY.LabelStyle.Format = "#,##0";
            chartKnock.ChartAreas[0].AxisY.LabelStyle.Format = "#,##0";
            chartCyl.ChartAreas[0].AxisY.LabelStyle.Font = TEA.fontMd10;
            chartKnock.ChartAreas[0].AxisY.LabelStyle.Font = TEA.fontMd10;
            chartCyl.ChartAreas[0].AxisX.LabelStyle.Font = TEA.fontMd10;
            chartKnock.ChartAreas[0].AxisX.LabelStyle.Font = TEA.fontMd10;

            // window 값을 모두 합침
            labelAV.Font = TEA.fontMdCn40;
            labelAV.Paint += TEA.labelAA_Paint;
            
            EditLinkUpDown(editAvgCycleCnt, upAvgCycleCnt, downAvgCycleCnt);
            EditLinkUpDown(editHeavyKnock, upHeavyKnock, downHeavyKnock);
            EditLinkUpDown(editLightKnock, upLightKnock, downLightKnock);

            editAvgCycleCnt.Text = TCube.GetValue("editAvgCycleCnt", "1");
            editHeavyKnock.Text = TCube.GetValue("editHeavyKnock", "256");
            editLightKnock.Text = TCube.GetValue("editLightKnock", "1");
        }

        public void KnockSet(float[] AData)
        {
            arrKnockX = AData;
        }

        int CalcSumCnt = 0;
        public void Calc()
        {
            try
            {
                int iMax = Math.Min(arrKnockX.Length, Convert.ToInt32(editHeavyKnock.Text)) - 1;
                int iMin = Convert.ToInt32(editLightKnock.Text) - 1;

                if (iMax < iMin)
                {
                    int iTemp = iMax;
                    iMax = iMin;
                    iMin = iTemp;
                }

                int iSize = iMax - iMin + 1;

                int iSumCnt = Convert.ToInt32(editAvgCycleCnt.Text);

                if (arrKnockCalc != null)
                {
                    if (arrKnockCalc.Length != iSize)
                    {
                        arrKnockCalc = null;
                        CalcSumCnt = 0;
                    }
                }

                if (CalcSumCnt == 0 || arrKnockCalc == null)
                {
                    arrKnockCalc = new float[iSize];
                }
                                
                int i = iMin;
                int j = 0;
                while (i <= iMax)
                {
                    if (i < arrKnockX.Length)
                    {
                        if (CalcSumCnt < iSumCnt)
                        {
                            arrKnockCalc[j] += arrKnockX[i];
                        }
                    }
                    i++;
                    j++;
                }
                CalcSumCnt++;

                if (CalcSumCnt >= iSumCnt)
                {
                    long iSum = 0;
                    int iCnt = 0;
                    foreach (TCubeWindow w in DicWindow.Values)
                    {
                        if (w.Enabled)
                        {
                            iCnt++;

                            int iw = w.Start;
                            while (iw <= w.End)
                            {
                                iSum += (int)arrKnockX[iw];
                                iw++;
                            }
                        }
                    }
                    if (iCnt == 0)
                    {
                        iSum = (long)arrKnockCalc.Sum();
                    }
                    labelAV.Text = string.Format("{0:#,##0}", iSum);

                    chartKnock.Series[0].Points.DataBindY(arrKnockCalc);
                    CalcSumCnt = 0;
                }
            }
            catch (Exception E)
            {

            }
        }

        // Cyl
        public Dictionary<PictureBox, int> DicCyl = new Dictionary<PictureBox, int>();
        public void InitCyl()
        {
            DicCyl.Add(imgCyl1, 0);
            DicCyl.Add(imgCyl2, 1);
            DicCyl.Add(imgCyl3, 2);
            DicCyl.Add(imgCyl4, 3);
            DicCyl.Add(imgCyl5, 4);
            DicCyl.Add(imgCyl6, 5);
            DicCyl.Add(imgCyl7, 6);
            DicCyl.Add(imgCyl8, 7);
            DicCyl.Add(imgCyl9, 8);
            DicCyl.Add(imgCyl10, 9);
            DicCyl.Add(imgCyl11, 10);
            DicCyl.Add(imgCyl12, 11);

            int i = 0;
            foreach (PictureBox p in DicCyl.Keys)
            {
                p.Click += imgCylOn_Click;
                p.Tag = i;
                i++;

                TEA.ControlCursorAdd(p);
            }
            CylSelect(0);
        }

        public int CylSelected
        {
            get
            {
                foreach (PictureBox p in DicCyl.Keys)
                {
                    if (p.Image != null)
                        return (int)p.Tag;
                }
                return 0;
            }
        }

        public void CylSelect(int i)
        {
            foreach(PictureBox p in DicCyl.Keys)
            {
                if ((int)p.Tag == i)
                {
                    p.Image = imgCylOn.Image;
                }
                else
                {
                    if (p.Image != null)
                        p.Image = null;
                }
            }
        }

        private void imgCylOn_Click(object sender, EventArgs e)
        {
            try
            {
                PictureBox p = (PictureBox)sender;
                int i = DicCyl[p];

                CylSelect(i);
            }
            catch (Exception E)
            {

            }
        }

        // Window
        public class TCubeWindow
        {
            public PictureBox CheckOn;

            public TCubeWindow()
            {
                
            }

            public void Toggle()
            {
                Enabled = !Enabled;
            }

            public Boolean Enabled
            {
                get
                {
                    return Check.Image != null;
                }
                set
                {
                    if (value)
                        Check.Image = CheckOn.Image;
                    else
                        Check.Image = null;
                }
            }
            public int Start
            {
                get
                {
                    int i = 0;
                    Int32.TryParse(EditStart.Text, out i);
                    return i;
                }
            }
            public int End
            {
                get
                {
                    int i = 0;
                    Int32.TryParse(EditEnd.Text, out i);
                    return i;
                }
            }

            public PictureBox FCheck;
            public PictureBox Check
            {
                get
                {
                    return FCheck;
                }
                set
                {
                    FCheck = value;
                    String s = TCube.GetValue("Window" + (Index + 1).ToString() + "_Enabled", "0");
                    Enabled = s == "1";

                    TEA.ControlCursorAdd(FCheck);
                }
            }

            public TextBox FEditStart;
            public TextBox EditStart
            {
                get
                {
                    return FEditStart;
                }
                set
                {                    
                    FEditStart = value;
                    FEditStart.Tag = this;
                    FEditStart.Text = TCube.GetValue("Window" + (Index + 1).ToString() + "_Start", "1");
                }
            }
            public TextBox FEditEnd;
            public TextBox EditEnd
            {
                get
                {
                    return FEditEnd;
                }
                set
                {
                    FEditEnd = value;
                    FEditEnd.Tag = this;
                    FEditEnd.Text = TCube.GetValue("Window" + (Index + 1).ToString() + "_End", "256");
                }
            }
            public int Index; // 0~
        }

        Dictionary<int, TCubeWindow> DicWindow = new Dictionary<int, TCubeWindow>();

        public void InitWindow()
        {
            TCubeWindow w;
            DicWindow.Add(0, new TCubeWindow
            {
                Index = 0,
                CheckOn = checkOn,
                Check = checkW1,
                EditStart = editWS1,
                EditEnd = editWE1,
            });
            DicWindow.Add(1, new TCubeWindow
            {
                Index = 1,
                CheckOn = checkOn,
                Check = checkW2,
                EditStart = editWS2,
                EditEnd = editWE2,
            });
            DicWindow.Add(2, new TCubeWindow
            {
                Index = 2,
                CheckOn = checkOn,
                Check = checkW3,
                EditStart = editWS3,
                EditEnd = editWE3,
            });
            DicWindow.Add(3, new TCubeWindow
            {
                Index = 3,
                CheckOn = checkOn,
                Check = checkW4,
                EditStart = editWS4,
                EditEnd = editWE4,
            });
            DicWindow.Add(4, new TCubeWindow
            {
                Index = 4,
                CheckOn = checkOn,
                Check = checkW5,
                EditStart = editWS5,
                EditEnd = editWE5,
            });

            int i = 0;
            foreach (TCubeWindow ww in DicWindow.Values)
            {
                TEA.ControlCursorAdd(ww.Check);
                ww.Check.Click += imgW_Click;
                ww.Check.Tag = ww;
                i++;
            }

            EditLinkUpDown(editWS1, upWS1, downWS1);
            EditLinkUpDown(editWS2, upWS2, downWS2);
            EditLinkUpDown(editWS3, upWS3, downWS3);
            EditLinkUpDown(editWS4, upWS4, downWS4);
            EditLinkUpDown(editWS5, upWS5, downWS5);

            EditLinkUpDown(editWE1, upWE1, downWE1);
            EditLinkUpDown(editWE2, upWE2, downWE2);
            EditLinkUpDown(editWE3, upWE3, downWE3);
            EditLinkUpDown(editWE4, upWE4, downWE4);
            EditLinkUpDown(editWE5, upWE5, downWE5);
        }

        private void imgW_Click(object sender, EventArgs e)
        {
            try
            {
                PictureBox p = (PictureBox)sender;
                TCubeWindow ww = (TCubeWindow)p.Tag;
                ww.Toggle();

                swSave.Restart();
            }
            catch (Exception E)
            {

            }
        }
        
        public void UIToW()
        {
            
        }

        public void EditLinkUpDown(TextBox AEdit, Panel AUp, Panel ADown)
        {
            AUp.Tag = AEdit;
            AUp.Click += AUp_Click;
            ADown.Tag = AEdit;
            ADown.Click += ADown_Click;

            AEdit.TextChanged += AEdit_TextChanged;

            TEA.ControlCursorAdd(AUp);
            TEA.ControlCursorAdd(ADown);
        }

        private void AEdit_TextChanged(object sender, EventArgs e)
        {
            try
            {
                TextBox t = (TextBox)sender;
                if (t.Tag != null)
                {
                    TCubeWindow ww = (TCubeWindow)t.Tag;
                }

                swSave.Restart();

                //if (ww.EditStart == t)
                //    TCube.SetValue("Window" + (ww.Index + 1).ToString() + "_Start", t.Text);
                //else if (ww.EditEnd == t)
                //    TCube.SetValue("Window" + (ww.Index + 1).ToString() + "_End", t.Text);
            }
            catch (Exception E)
            {

            }
        }

        private void AUp_Click(object sender, EventArgs e)
        {
            try
            {
                Panel p = (Panel)sender;
                TextBox t = (TextBox)p.Tag;

                int i = Convert.ToInt32(t.Text);
                i++;
                t.Text = i.ToString();
            }
            catch
            {

            }
        }

        private void ADown_Click(object sender, EventArgs e)
        {
            try
            {
                Panel p = (Panel)sender;
                TextBox t = (TextBox)p.Tag;

                int i = Convert.ToInt32(t.Text);
                if (i > 1)
                {
                    i--;
                    t.Text = i.ToString();
                }
            }
            catch
            {

            }
        }

        public void DicWindowSave()
        {
            foreach (TCubeWindow ww in DicWindow.Values)
            {
                if (ww.Enabled)
                    TCube.SetValue("Window" + (ww.Index + 1).ToString() + "_Enabled", "1");
                else
                    TCube.SetValue("Window" + (ww.Index + 1).ToString() + "_Enabled", "0");
                TCube.SetValue("Window" + (ww.Index + 1).ToString() + "_Start", ww.Start.ToString());
                TCube.SetValue("Window" + (ww.Index + 1).ToString() + "_End", ww.End.ToString());
            }
        }
        
        RectangleF _rect;
        int _x = -1;
        private void chartKnock_PostPaint(object sender, System.Windows.Forms.DataVisualization.Charting.ChartPaintEventArgs e)
        {
            object o = e.ChartElement;

            if (o.GetType().Equals(typeof(System.Windows.Forms.DataVisualization.Charting.Series)))
            {
                try
                {
                    foreach (TCubeWindow w in DicWindow.Values)
                    {
                        if ((w.Enabled) && (w.Start < w.End))
                        {
                            double y2 = chartKnock.ChartAreas[0].AxisY.Maximum;
                            double y1 = chartKnock.ChartAreas[0].AxisY.Minimum;

                            float xVal = (float)e.ChartGraphics.GetPositionFromAxis(e.Chart.ChartAreas[0].Name, System.Windows.Forms.DataVisualization.Charting.AxisName.X, w.Start);
                            float xVal2 = (float)e.ChartGraphics.GetPositionFromAxis(e.Chart.ChartAreas[0].Name, System.Windows.Forms.DataVisualization.Charting.AxisName.X, w.End);
                            float yVal = (float)e.ChartGraphics.GetPositionFromAxis(e.Chart.ChartAreas[0].Name, System.Windows.Forms.DataVisualization.Charting.AxisName.Y, y1);
                            float yVal2 = (float)e.ChartGraphics.GetPositionFromAxis(e.Chart.ChartAreas[0].Name, System.Windows.Forms.DataVisualization.Charting.AxisName.Y, y2);
                            //yVal2 += 10;

                            //float fx1 = (float)chartKnock.ChartAreas[0].AxisX.ValueToPixelPosition(40);
                            //float fx2 = (float)chartKnock.ChartAreas[0].AxisX.ValueToPixelPosition(80);
                            //float fy1 = (float)chartKnock.ChartAreas[0].AxisY.ValueToPixelPosition(y1);
                            //float fy2 = (float)chartKnock.ChartAreas[0].AxisY.ValueToPixelPosition(y2);

                            //_rect = e.ChartGraphics.GetAbsoluteRectangle(new RectangleF(new PointF(fx1, fy2), new SizeF(fx2 - fx1, Math.Abs(fy2 - fy1))));
                            _rect = e.ChartGraphics.GetAbsoluteRectangle(new RectangleF(new PointF(xVal, yVal2), new SizeF(Math.Abs(xVal - xVal2), Math.Abs(yVal - yVal2))));
                            _rect.Y += 10;
                            _rect.Height -= 10;

                            //_rect = new RectangleF(new PointF(fx1, fy2), new SizeF(fx2 - fx1, Math.Abs(fy2 - fy1)));
                            using (SolidBrush sb = new SolidBrush(Color.FromArgb(64, 177, 183, 185)))
                                e.ChartGraphics.Graphics.FillRectangle(sb, Rectangle.Round(_rect));
                            
                            // label
                            RectangleF _rectfont = e.ChartGraphics.GetAbsoluteRectangle(new RectangleF(new PointF(xVal, yVal2), new SizeF(Math.Abs(xVal - xVal2), Math.Abs(yVal2 - yVal2))));

                            // x < 37
                            // x > 1337

                            if (_rectfont.Width < 60)
                            {
                                int iDiff = 60 - (int)_rectfont.Width;
                                _rectfont.Width += iDiff;
                                _rectfont.X -= (int)(iDiff / 2);
                            }

                            if (_rectfont.Left < 35)
                            {
                                float ixdiff = 35 - _rectfont.Left;
                                _rectfont.X = _rectfont.X + ixdiff;
                            }
                            else if (_rectfont.Left >  1337)
                            {
                                float ixdiff = _rectfont.Left - 1337;
                                _rectfont.X = _rectfont.X - ixdiff;
                            }
                            _rectfont.Height = 10;

                            SolidBrush solidBrush = new SolidBrush(TEA.ColorLabelFont);
                            string s = "Window " + (w.Index + 1).ToString();

                            StringFormat stringFormat = new StringFormat();
                            stringFormat.Alignment = StringAlignment.Center;
                            stringFormat.LineAlignment = StringAlignment.Center;

                            e.ChartGraphics.Graphics.TextRenderingHint = TextRenderingHint.AntiAlias;
                            e.ChartGraphics.Graphics.DrawString(s, TEA.fontMd10, solidBrush, _rectfont, stringFormat);
                        }
                    }
                }
                catch
                {

                }
            }
        }

        public Stopwatch swSave = new Stopwatch();
        private void timerTick_Tick(object sender, EventArgs e)
        {
            if (swSave.IsRunning)
                if (swSave.ElapsedMilliseconds >= 5000)
                {
                    DicWindowSave();

                    TCube.SetValue("editAvgCycleCnt", editAvgCycleCnt.Text);
                    TCube.SetValue("editHeavyKnock", editHeavyKnock.Text);
                    TCube.SetValue("editLightKnock", editLightKnock.Text);
                }
        }

        private void panelOK_Click(object sender, EventArgs e)
        {
            swSave.Restart();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EngineAnalyzer
{
    public partial class TFormCH : Form
    {
        public List<TFormCHCell> ListCH = new List<TFormCHCell>();

        public TFormCH()
        {
            InitializeComponent();
        }

        public void Init(int ACHMax)
        {
            int iMax = ACHMax;

            panelCH.Controls.Clear();
            ListCH.Clear();
            for (int i = 1; i <= iMax; i++)
            {
                TFormCHCell f = new TFormCHCell();

                panelCH.Controls.Add(f.panelCHCell);
                f.panelCHCell.BringToFront();

                f.labelCH.Text = "CH" + i.ToString();
                f.editValue.Tag = i - 1;
                f.editValue.TextChanged += edit_TextChanged;

                ListCH.Add(f);
            }
        }

        public void SetInput(String AText)
        {
            Text = AText;

            labelText.Text = AText;
        }

        private void edit_TextChanged(object sender, EventArgs e)
        {

        }

        private void TFormCH_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();
        }
    }
}

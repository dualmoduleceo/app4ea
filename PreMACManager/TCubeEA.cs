using System;
using System.Collections;
using System.Text;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using DM;
using EngineAnalyzer;
using System.Diagnostics;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace System.Net.Sockets
{
    public static class SocketExtensions
    {
        private const int BytesPerLong = 4; // 32 / 8
        private const int BitsPerByte = 8;

        /// <summary>
        /// Sets the keep-alive interval for the socket.
        /// </summary>
        /// <param name="socket">The socket.</param>
        /// <param name="time">Time between two keep alive "pings".</param>
        /// <param name="interval">Time between two keep alive "pings" when first one fails.</param>
        /// <returns>If the keep alive infos were succefully modified.</returns>
        public static bool SetKeepAlive(this Socket socket, ulong time, ulong interval)
        {
            try
            {
                // Array to hold input values.
                var input = new[]
                {
                    (time == 0 || interval == 0) ? 0UL : 1UL, // on or off
					time,
                    interval
                };

                // Pack input into byte struct.
                byte[] inValue = new byte[3 * BytesPerLong];
                for (int i = 0; i < input.Length; i++)
                {
                    inValue[i * BytesPerLong + 3] = (byte)(input[i] >> ((BytesPerLong - 1) * BitsPerByte) & 0xff);
                    inValue[i * BytesPerLong + 2] = (byte)(input[i] >> ((BytesPerLong - 2) * BitsPerByte) & 0xff);
                    inValue[i * BytesPerLong + 1] = (byte)(input[i] >> ((BytesPerLong - 3) * BitsPerByte) & 0xff);
                    inValue[i * BytesPerLong + 0] = (byte)(input[i] >> ((BytesPerLong - 4) * BitsPerByte) & 0xff);
                }

                // Create bytestruct for result (bytes pending on server socket).
                byte[] outValue = BitConverter.GetBytes(0);

                // Write SIO_VALS to Socket IOControl.
                socket.SetSocketOption(SocketOptionLevel.Tcp, SocketOptionName.KeepAlive, true);
                socket.IOControl(IOControlCode.KeepAliveValues, inValue, outValue);
            }
            catch (SocketException e)
            {
                Console.WriteLine("Failed to set keep-alive: {0} {1}", e.ErrorCode, e);
                return false;
            }

            return true;
        }
    }
}
namespace CubeEA
{
    public class TCubeEASocket
    {
        public Stopwatch swRx = new Stopwatch();

        private static int _FLAG_START = 1;
        private static int _FLAG_REQ = 3;
        private static int _FLAG_STOP = 2;
        private static int _FLAG_NONE = 0;

        private static int _MODE_NONE = 0;
        private static int _MODE_RECV = 1;

        private static Byte _CMD_START = 0x01;
        private static Byte _CMD_STOP = 0x02;
        private static Byte _CMD_REQ = 0x03;

        private static Byte _CMD_RAW = 0x10;
        private static Byte _CMD_DATA = 0x11;

        private static ushort _timeout = 2000;
        private static ushort _refresh = 10;
        private static bool _connected = false;

        public int UseCHMax = 12;

        public Socket Socket;
        public byte[] Buffer = new byte[65535];

        public byte[] BufCycle;
        public byte[] BufRaw = new Byte[2888 * 12]; // CH 꽉 채우면 이벤트 발생
        public int BufRawPos = -1;

        public delegate void StateChanged(Boolean AState);

        public event StateChanged OnStateChanged;

        public delegate void ResponseData(ushort ACH, ushort ACycle, ref byte[] ABuffer, int BufferIndex, int BufferCount);
        public event ResponseData OnResponseCHData;
        public event ResponseData OnResponseCalcData;
        public event ResponseData OnResponseKnockData;

        public delegate void ExceptionData(ushort id, byte unit, byte function, byte exception);

        public event ExceptionData OnException;

        public string IP = "";
        public ushort Port;

        public int LastCycle = -1;

        public int GetCHCnt = 0;
        public List<int> ListGetCH = new List<int>();

        public int SocketMode = 0; // 1= udp       /         0 = tcp

        public MemoryStream streamCommErr = new MemoryStream();

        public ushort timeout
        {
            get { return _timeout; }
            set { _timeout = value; }
        }




        public ushort refresh
        {
            get { return _refresh; }
            set { _refresh = value; }
        }



        public bool connected
        {
            get { return _connected; }
            set
            {
                _connected = value;
            }
        }

        public Thread ThreadTick { get; private set; }
        public Thread ThreadTickTiny { get; private set; }
        
        public int Flag { get; private set; }
        public int Mode { get; private set; }
        public int DelayForRecv = 0;

        public TCubeEASocket()
        {
            //SocketMode = 0;

            int i = -1;
            i++; bPacketReq[i] = 0x02;
            i++; bPacketReq[i] = _CMD_REQ;
            i++; bPacketReq[i] = 0x00; // length
            i++; bPacketReq[i] = 0x00; // length


            MakeThreadTick();
            //MakeThreadTickTiny();
        }

        

        public Boolean FlagConnect = false;
        public void DoConnect()
        {
            FlagConnect = true;
        }

        public void connect()
        {
            if (IP.Length == 0)
                return;
            connect(IP, Port);
            swRx.Restart();
        }


        UdpClient udpClient;
        IPEndPoint RemoteIpEndPoint;

        public void connectUDP(string ip, ushort port)
        {
            try
            {
                if (udpClient != null)
                {
                    udpClient.Close();
                    udpClient = null;
                }
                udpClient = new UdpClient(port);

                //udpClient.Connect(ip, port);

                //RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, 0);
                connected = true;

                if (OnStateChanged != null)
                {
                    OnStateChanged(true);
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void connect(string ip, ushort port)
        {
            if (SocketMode == 1)
            {
                connectUDP(ip, port);
                return;
            }

            try
            {
                IP = ip;
                Port = port;
                IPAddress _ip;
                if (IPAddress.TryParse(ip, out _ip) == false)
                {
                    IPHostEntry hst = Dns.GetHostEntry(ip);
                    ip = hst.AddressList[0].ToString();
                }


                //tcpAsyCl = new Socket(
                //    AddressFamily.InterNetwork //IPAddress.Parse(ip).AddressFamily
                //    , SocketType.Stream
                //    , ProtocolType.Tcp
                //);
                //tcpAsyCl.Connect(new IPEndPoint(IPAddress.Parse(ip), port));
                //tcpAsyCl.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.SendTimeout, _timeout);
                //tcpAsyCl.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, _timeout);
                ////tcpAsyCl.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.NoDelay, 1);


                Socket = new Socket(
                    AddressFamily.InterNetwork //IPAddress.Parse(ip).AddressFamily
                    , SocketType.Stream
                    , ProtocolType.Tcp
                );
                
                Socket.Connect(new IPEndPoint(IPAddress.Parse(ip), port));
                Socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.SendTimeout, _timeout);
                Socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, _timeout);
                //tcpSynCl.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.NoDelay, 1);
                connected = true;

                if (OnStateChanged != null)
                {
                    OnStateChanged(true);
                }
            }
            catch (Exception error)
            //catch (System.IO.IOException error)
            {
                connected = false;

                if (OnStateChanged != null)
                {
                    OnStateChanged(false);
                }
                //throw (error);
            }
        }



        public void disconnect()
        {
            Dispose();
            connected = false;

            if (OnStateChanged != null)
            {
                OnStateChanged(false);
            }
        }



        ~TCubeEASocket()
        {
            Dispose();
        }



        public void Dispose()
        {
            //if (tcpAsyCl != null)
            //{
            //    if (tcpAsyCl.Connected)
            //    {
            //        try { tcpAsyCl.Shutdown(SocketShutdown.Both); }
            //        catch { }
            //        tcpAsyCl.Close();
            //    }
            //    tcpAsyCl = null;
            //}
            if (Socket != null)
            {
                if (connected)
                {
                    try { Socket.Shutdown(SocketShutdown.Both); }
                    catch { }
                    Socket.Close();
                }
                Socket = null;
            }
        }

        internal void CallException(ushort id, byte unit, byte function, byte exception)
        {
            if ((Socket == null)) return;

            //if ((tcpAsyCl == null) || (tcpSynCl == null)) return;
            //if (exception == excExceptionConnectionLost)
            //{
            //    tcpSynCl = null;
            //    tcpAsyCl = null;
            //}

            connected = false;

            if (OnStateChanged != null)
            {
                OnStateChanged(false);
            }

            if (OnException != null) OnException(id, unit, function, exception);
        }

        internal static UInt16 SwapUInt16(UInt16 inValue)
        {
            return (UInt16)(((inValue & 0xff00) >> 8) |
                     ((inValue & 0x00ff) << 8));
        }


        private byte[] CreateReadHeader(ushort id, byte unit, ushort startAddress, ushort length, byte function)
        {
            byte[] data = new byte[12];

            byte[] _id = BitConverter.GetBytes((short)id);
            data[0] = _id[1];
            data[1] = _id[0];
            data[5] = 6;
            data[6] = unit;
            data[7] = function;
            byte[] _adr = BitConverter.GetBytes((short)IPAddress.HostToNetworkOrder((short)startAddress));
            data[8] = _adr[0];
            data[9] = _adr[1];
            byte[] _length = BitConverter.GetBytes((short)IPAddress.HostToNetworkOrder((short)length));
            data[10] = _length[0];
            data[11] = _length[1];
            return data;
        }

        private void OnSend(System.IAsyncResult result)
        {
            //if (result.IsCompleted == false) CallException(0xFFFF, 0xFF, 0xFF, excSendFailt);
        }



        private void OnReceive(System.IAsyncResult result)
        {
            //if (result.IsCompleted == false) CallException(0xFF, 0xFF, 0xFF,excExceptionConnectionLost);

            //ushort id = SwapUInt16(BitConverter.ToUInt16(tcpAsyClBuffer, 0));
            //byte unit = tcpAsyClBuffer[6];
            //byte function = tcpAsyClBuffer[7];
            //byte[] data;



            //if ((function >= fctWriteSingleCoil) && (function != fctReadWriteMultipleRegister))
            //{
            //    data = new byte[2];
            //    Array.Copy(tcpAsyClBuffer, 10, data, 0, 2);
            //}


            //else
            //{
            //    data = new byte[tcpAsyClBuffer[8]];
            //    Array.Copy(tcpAsyClBuffer, 9, data, 0, tcpAsyClBuffer[8]);
            //}


            //if (function > excExceptionOffset)
            //{
            //    function -= excExceptionOffset;
            //    CallException(id, unit, function, tcpAsyClBuffer[8]);
            //}


            //else if (OnResponseData != null) OnResponseData(id, unit, function, data);
        }
        
        protected byte[] Write(byte[] write_data, int ACount, Boolean ARecv = false)
        {
            if (Socket == null)
                return null;

            if (connected)
            {
                try
                {
                    Socket.Send(write_data, 0, ACount, SocketFlags.None);
                    if (ARecv)
                    {
                        int result = Socket.Receive(Buffer, 0, Buffer.Length, SocketFlags.None);

                        byte[] data = new byte[result];
                        Array.Copy(Buffer, 0, data, 0, result);

                        return data;
                    }
                }
                catch (Exception e)
                {
                    //CallException(id, write_data[6], write_data[7], excExceptionConnectionLost);
                }
            }
            {
                //else CallException(id, write_data[6], write_data[7], excExceptionConnectionLost);
            }
            return null;
        }

        protected Boolean DoWrite(byte[] write_data, int ACount)
        {
            if (Socket == null)
                return false;

            if (connected)
            {
                Socket.Send(write_data, 0, ACount, SocketFlags.None);
            }
            return true;
        }

        public void DoSendStart()
        {
            Flag = _FLAG_START;
        }

        public void DoSendReq()
        {
            //Flag = _FLAG_REQ;
            //Mode = _MODE_NONE;
        }

        public void DoSendStop()
        {
            Flag = _FLAG_STOP;
        }

        public Boolean SendStart()
        {
            LastCycle = -1;

            byte[] data = new byte[12];

            int i = -1;
            i++; data[i] = 0x02;
            i++; data[i] = _CMD_START;
            i++; data[i] = 0x00; // length
            i++; data[i] = 0x00; // length
            //i++; data[i] = 0xCC; // crc
            //i++; data[i] = 0x03;

            TCS.LogMsg("TX TRY >> 4 BYTE === START CMD");
            byte[] b = Write(data, 4);

            //if (b != null)
            //{
            //    // stx(1) + cmd(1) + len(2) + code(1)
            //    if (b.Length == 8)
            //    {
            //        ushort wCode = BitConverter.ToUInt16(data, 4);

            //        if (wCode == 0)
            //        {
            //            return true;
            //        }
            //    }
            //}



            return true;
            //return false;
        }

        public int PacketReqCount1M = 0;
        public int PacketReqCount = 0;
        byte[] bPacketReq = new byte[4];
        public Boolean SendReq()
        {
            GetCHCnt = 0;
            ListGetCH.Clear();

            TCS.LogMsg("TX TRY >> 4 BYTE === REQ CMD");
            byte[] b = Write(bPacketReq, 4);
            PacketReqCount++;
            if (PacketReqCount > 999999)
            {
                PacketReqCount = 0;
                PacketReqCount1M++;
            }

            return true;
        }

        public Boolean SendStop()
        {
            byte[] data = new byte[12];

            int i = -1;
            i++; data[i] = 0x02;
            i++; data[i] = _CMD_STOP;
            i++; data[i] = 0x00; // length
            i++; data[i] = 0x00; // length
            //i++; data[i] = 0xCC; // crc
            //i++; data[i] = 0x03;

            byte[] b = Write(data, 4);

            return true;
            //if (b != null)
            //{
            //    // stx(1) + cmd(1) + len(2) + code(1) + crc(2) + etx(1)
            //    if (b.Length == 8)
            //    {
            //        ushort wCode = BitConverter.ToUInt16(data, 4);

            //        if (wCode == 0)
            //        {
            //            return true;
            //        }
            //    }
            //}
            //return false;
        }

        public void MakeThreadTickTiny()
        {
            ThreadTickTiny = new Thread(new ThreadStart(TickTiny));
            ThreadTickTiny.Start();
        }

        public void TickTiny()
        {
            Stopwatch swTick = new Stopwatch();
            swTick.Start();

            while (Thread.CurrentThread.ThreadState != System.Threading.ThreadState.AbortRequested)
            {
                if (swTick.ElapsedMilliseconds >= 100)
                {
                    swTick.Restart();
                    Thread.Sleep(1);
                }

                if (Socket == null)
                    continue;

                if (connected)
                {
                    SendReq();
                }
            }
        }

        public void MakeThreadTick()
        {
            ThreadTick = new Thread(new ThreadStart(Tick));
            ThreadTick.Start();
        }

        public Boolean bIsAutoConnect = false;

        // 1111111111111111111111111111111
        public virtual void Tick()
        {
            Mode = _MODE_NONE;
            Stopwatch swTick = new Stopwatch();
            swTick.Start();

            while (Thread.CurrentThread.ThreadState != System.Threading.ThreadState.AbortRequested)
            {
                if (swTick.ElapsedMilliseconds >= 100)
                {
                    swTick.Restart();
                    Thread.Sleep(1);
                }

                try
                { 
                    if (FlagConnect)
                    {
                        FlagConnect = false;
                        try
                        {
                            disconnect();
                        }
                        catch (Exception E)
                        {
                            TCS.LogError(E);
                        }
                        connect();

                        Flag = _FLAG_STOP;

                        if (SocketMode == 0)
                        {
                            int size = sizeof(UInt32);
                            UInt32 on = 1;
                            UInt32 keepAliveInterval = 250;   // Send a packet once every 10 seconds.
                            UInt32 retryInterval = 3000;        // If no response, resend every second.
                            byte[] inArray = new byte[size * 3];
                            Array.Copy(BitConverter.GetBytes(on), 0, inArray, 0, size);
                            Array.Copy(BitConverter.GetBytes(keepAliveInterval), 0, inArray, size, size);
                            Array.Copy(BitConverter.GetBytes(retryInterval), 0, inArray, size * 2, size);

                            Socket.IOControl(IOControlCode.KeepAliveValues, inArray, null);
                        }
                    }
                    if (SocketMode == 0)
                    {
                        if (Socket == null)
                        {
                            if (bIsAutoConnect)
                            {
                                Thread.Sleep(2000);
                                DoConnect();
                            }
                            continue;
                        }
                    }

                    if (connected == false)
                    {
                        if (bIsAutoConnect)
                        {
                            Thread.Sleep(2000);
                            DoConnect();
                        }
                        continue;
                    }
                    else
                    {
                        GetRAW();
                    }

                }
                catch(Exception e)
                {
                    TCS.LogMsg(e.Message);
                    TCS.LogMsg(e.StackTrace);
                }
            }
        }

        public void Flush()
        {
            return;
            Stopwatch swFlush = new Stopwatch();

            swFlush.Start();
            int iCntTotal = 0;
            while (swFlush.ElapsedMilliseconds <= 1000)
            {
                byte[] b = new byte[1];

                int icnt = Socket.Receive(b, 0, 1, SocketFlags.None);
                if (icnt > 0)
                    swFlush.Restart();
                iCntTotal += icnt;
            }

            TCS.LogMsg("BUFFER FLUSH = " + iCntTotal.ToString());
        }

        public Boolean CheckReq()
        {
            //if (result > 0)
            {
                if (Buffer[0] == 0x02)
                {
                    // req 응답
                    if (Buffer[1] == _CMD_REQ)
                    {
                        if (Buffer[3] == 0x01) // length
                        {
                            if (Buffer[4] == 0x00)
                            {
                                // 데이터 얻기
                                return true;
                            }
                            else if (Buffer[5] == 0x01)
                            {
                                return false;
                            }
                        }
                    }
                }
            }
            return false;
        }

        public Boolean IsFindSTX = false;
        public MemoryStream msHead = new MemoryStream();

        public Boolean bIsStartRaw = false;
        public int LastRawCH = 0;

        public virtual Boolean GetRAW()
        {
            try
            {
                Stopwatch swTO = new Stopwatch();

                byte[] bBuf = new byte[4096];

                int iLen;
                int wLenTotal;
                int iCycle = 0;
                int iCH;

                Boolean bIsNewCycle = false;
                
                // msHead에 6만 읽도록 구성
                // msHead 검사 후 0x02까지만 남기고 또 다음에 6까지 수신 반복

                // 1. 수신 버퍼 읽어서 msHead를 6까지 만듬
                swTO.Restart();
                int wLen = 6;
                int iGet = 0;
                while (iGet < wLen)
                {
                    if (swTO.ElapsedMilliseconds >= 1000)
                    {
                        TCS.LogMsg("TIMEOUT HEADER");   
                        //Flush();
                        return false;
                    }
                    int iRemain = wLen - iGet;
                    if (iRemain <= 0)
                        break;
                    int icnt = Socket.Receive(bBuf, iGet, iRemain, SocketFlags.None);
                    iGet += icnt;
                }

                // 2. 임시버퍼 -> 스트림헤더
                msHead.Write(bBuf, 0, iGet);

                // 3. 스트림에서 처음 0x02 찾기
                

                // 3. 스트림 -> 버퍼 이후 헤더 찾기
                Byte[] bTemp = msHead.ToArray();


                //if (msHead.Length < 6) // 논리적으로 불가
                //{
                //    // ERROR : 수신 데이터 없음
                //    return false;
                //}

                //msHead.Read(bBuf, 0, (int)msHead.Length);

                //Boolean bIsOK = false;

                if (bBuf[0] == 0x02)
                {
                    // req 응답
                    if (bBuf[1] == _CMD_REQ)
                    {
                        if (bIsStartRaw == false)
                        {
                            bIsStartRaw = true;
                        }

                        iLen = BitConverter.ToUInt16(bBuf, 2);
                        iCH = BitConverter.ToUInt16(bBuf, 4);

                        if (LastRawCH > 0)
                        {
                            if (iCH < LastRawCH)
                            {
                                TCS.LogMsg(String.Format("Last CH Saved    {0}", LastRawCH + 1));
                                TCS.LogMsg(String.Format("New  CH Detected {0}", iCH + 1));
                                
                                if (OnResponseCHData != null)
                                {
                                    OnResponseCHData(0xFFFF, 0xFFFF, ref BufRaw, 0, BufRaw.Length);
                                }

                                LastRawCH = 0;
                                BufRaw = new byte[2888 * 12];
                                Array.Clear(BufRaw, 0, BufRaw.Length);
                                bIsStartRaw = false;
                            }
                        }
                        LastRawCH = iCH;
                        
                        if (iLen == 1)
                        {
                            if (iCH == 0)
                            {
                                // go getraw
                                return true;
                            }
                            else if (iCH == 1)
                            {
                                DoSendReq();
                                return true;
                            }
                        }
                        else
                        {
                            BufRawPos = iCH * 2888;

                            // stx1 + cmd1 + len2 + ch2 + cycle2 

                            // len = 
                            wLenTotal = iLen + 4;

                            //BufCycle = new byte[wLenTotal];

                            // 이미 받은것 채우기
                            Array.Copy(bBuf, 0, BufRaw, BufRawPos, 6);

                            // RAW 모두 받기
                            swTO.Restart();
                            int iRemainRx = 0;
                            int iNowcnt = 0;
                            iGet = 6; // 이미 6바이트 받음
                            while (iGet < wLenTotal)
                            {
                                //if (swTO.ElapsedMilliseconds >= 5000)
                                //{
                                //    TCS.LogMsg("TIMEOUT DATA");
                                //    //disconnect();
                                //    return false;
                                //}
                                iRemainRx = wLenTotal - iGet;
                                if (iRemainRx <= 0)
                                {
                                    TCS.LogMsg(String.Format("{0} {1} {2}", wLenTotal, iGet, iRemainRx));
                                    break;
                                }

                                iNowcnt = Socket.Receive(BufRaw, BufRawPos + iGet, iRemainRx, SocketFlags.None);
                                if (iNowcnt > 0)
                                {
                                    iGet += iNowcnt;

                                    swTO.Restart();
                                }

                                iCycle = (UInt16)(BitConverter.ToUInt16(BufRaw, BufRawPos + 6));
                            }

                            TCS.LogMsg("RX >> " + iGet.ToString() + " BYTE");

                            //if (OnResponseKnockData != null)
                            //{
                            //    OnResponseKnockData((ushort)iCH, (ushort)iCycle, ref BufRaw, 0, wLenTotal);
                            //}

                            swRx.Restart();

                            return true;
                        }
                    }
                    else
                    {
                        if (bIsStartRaw == true)
                        {
                            TCS.LogMsg(String.Format("Last CH Saved    {0}", LastRawCH + 1));
                            TCS.LogMsg("Last CH Ended");

                            if (OnResponseCHData != null)
                            {
                                OnResponseCHData(0xFFFF, 0xFFFF, ref BufRaw, 0, BufRaw.Length);
                            }

                            LastRawCH = 0;
                            BufRaw = new byte[2888 * 12];
                            Array.Clear(BufRaw, 0, BufRaw.Length);
                            bIsStartRaw = false;
                        }
                        
                        //else if (bBuf[1] == _CMD_REQ)
                        //{
                        //    iLen = BitConverter.ToUInt16(bBuf, 2);
                        //    iCH = BitConverter.ToUInt16(bBuf, 4);

                        //    if (iLen == 1)
                        //    {
                        //        if (iCH == 0)
                        //        {
                        //            // go getraw
                        //            return true;
                        //        }
                        //        else if (iCH == 1)
                        //        {
                        //            DoSendReq();
                        //            return true;
                        //        }
                        //    }
                        //    else
                        //    {
                        //        // stx + cmd + len2 + ch2 + cycle2 

                        //        // 모든 채널 받기

                        //        wLen = 2888 * UseCHMax;
                        //        wLenTotal = 2888 * 12;

                        //        BufCycle = new byte[wLenTotal];

                        //        // 이미 받은것 채우기
                        //        Array.Copy(bBuf, 0, BufCycle, 0, 6);

                        //        // RAW 모두 받기
                        //        swTO.Restart();
                        //        int iRemainRx = 0;
                        //        int iNowcnt = 0;
                        //        iGet = 6;
                        //        while (iGet < wLen)
                        //        {
                        //            if (swTO.ElapsedMilliseconds >= 5000)
                        //            {
                        //                TCS.LogMsg("TIMEOUT RAW");
                        //                //disconnect();
                        //                return false;
                        //            }
                        //            iRemainRx = wLen - iGet;
                        //            if (iRemainRx <= 0)
                        //            {
                        //                TCS.LogMsg(String.Format("{0} {1} {2}", wLen, iGet, iRemainRx));
                        //                break;
                        //            }

                        //            iNowcnt = Socket.Receive(BufCycle, iGet, iRemainRx, SocketFlags.None);
                        //            if (iNowcnt > 0)
                        //            {
                        //                iGet += iNowcnt;

                        //                swTO.Restart();
                        //            }
                        //        }

                        //        TCS.LogMsg("RX >> " + iGet.ToString() + " BYTE");

                        //        if (OnResponseCHData != null)
                        //        {
                        //            OnResponseCHData(0xFFFF, 0xFFFF, ref BufCycle, 0, wLen);
                        //        }

                        //        swRx.Restart();

                        //        DoSendReq();

                        //        return true;
                        //    }
                        //}
                        if (bBuf[1] == 0x13)
                        {
                            iLen = BitConverter.ToUInt16(bBuf, 2); // 288+4 = 292 = 0x0124
                            iCH = BitConverter.ToUInt16(bBuf, 4);

                            if (iLen > 0)
                            {
                                // stx1 + cmd1 + len2 + ch2 + cycle2 

                                // len = 292수신
                                wLenTotal = iLen + 4;

                                BufCycle = new byte[wLenTotal];

                                // 이미 받은것 채우기
                                Array.Copy(bBuf, 0, BufCycle, 0, 6);

                                // RAW 모두 받기
                                swTO.Restart();
                                int iRemainRx = 0;
                                int iNowcnt = 0;
                                iGet = 6; // 이미 6바이트 받음
                                while (iGet < wLenTotal)
                                {
                                    //if (swTO.ElapsedMilliseconds >= 5000)
                                    //{
                                    //    TCS.LogMsg("TIMEOUT DATA");
                                    //    //disconnect();
                                    //    return false;
                                    //}
                                    iRemainRx = wLenTotal - iGet;
                                    if (iRemainRx <= 0)
                                    {
                                        TCS.LogMsg(String.Format("{0} {1} {2}", wLenTotal, iGet, iRemainRx));
                                        break;
                                    }

                                    iNowcnt = Socket.Receive(BufCycle, iGet, iRemainRx, SocketFlags.None);
                                    if (iNowcnt > 0)
                                    {
                                        iGet += iNowcnt;

                                        swTO.Restart();
                                    }

                                    iCycle = (UInt16)(BitConverter.ToUInt16(BufCycle, 6));
                                }

                                TCS.LogMsg("RX >> " + iGet.ToString() + " BYTE");

                                if (OnResponseKnockData != null)
                                {
                                    OnResponseKnockData((ushort)iCH, (ushort)iCycle, ref BufCycle, 0, wLenTotal);
                                }

                                swRx.Restart();

                                return true;
                            }
                            else
                            {
                                // calculation packet 오류

                            }
                        }
                        else if (bBuf[1] == 0x11)
                        {
                            iLen = BitConverter.ToUInt16(bBuf, 2); // 288+4 = 292 = 0x0124
                            iCH = BitConverter.ToUInt16(bBuf, 4);

                            if (iLen > 0)
                            {
                                // stx + cmd + len2 + ch2 + cycle2 

                                // len = 292수신
                                wLenTotal = iLen + 4;

                                BufCycle = new byte[wLenTotal];

                                // 이미 받은것 채우기
                                Array.Copy(bBuf, 0, BufCycle, 0, 6);

                                // RAW 모두 받기
                                swTO.Restart();
                                int iRemainRx = 0;
                                int iNowcnt = 0;
                                iGet = 6; // 이미 6바이트 받음
                                while (iGet < wLenTotal)
                                {
                                    //if (swTO.ElapsedMilliseconds >= 5000)
                                    //{
                                    //    TCS.LogMsg("TIMEOUT DATA");
                                    //    //disconnect();
                                    //    return false;
                                    //}
                                    iRemainRx = wLenTotal - iGet;
                                    if (iRemainRx <= 0)
                                    {
                                        TCS.LogMsg(String.Format("{0} {1} {2}", wLenTotal, iGet, iRemainRx));
                                        break;
                                    }

                                    iNowcnt = Socket.Receive(BufCycle, iGet, iRemainRx, SocketFlags.None);
                                    if (iNowcnt > 0)
                                    {
                                        iGet += iNowcnt;

                                        swTO.Restart();
                                    }
                                }

                                TCS.LogMsg("RX >> " + iGet.ToString() + " BYTE");

                                if (OnResponseCalcData != null)
                                {
                                    OnResponseCalcData(0xFFFF, 0xFFFF, ref BufCycle, 0, wLenTotal);
                                }

                                swRx.Restart();

                                return true;
                            }
                            else
                            {
                                // calculation packet 오류

                            }
                        }
                    }                    
                }
                else
                {

                }

                //if (bIsOK == false)
                //{
                //    // msHead에서 0x02까지 남김
                //    int i1 = 0;
                //    int iHeadLen = (int)msHead.Length;
                //    msHead.Position = 0;
                //    while (i1 < iHeadLen)
                //    {
                //        if (msHead.ReadByte() == 0x02)
                //        {
                //            Byte[] bTemp = msHead.ToArray();

                //            msHead.SetLength(0);
                //            msHead.Write(bTemp, i1, iHeadLen - i1);
                //            bIsOK = true;
                //            break;
                //        }

                //        i1++;
                //    }
                //}

                //if (bIsOK == false)
                //{
                //    msHead.SetLength(0);
                //}


                
                    IsFindSTX = true;

                    streamCommErr.Write(bBuf, 0, 6);

                    if (streamCommErr.Length > 1024 * 1000)
                    {
                        try
                        {
                            FileStream swCommErr = new System.IO.FileStream(Application.StartupPath + "\\log\\commerr_" + DateTime.Now.ToString("yyMMdd") + ".bin", FileMode.Append);
                            streamCommErr.CopyTo(swCommErr);
                            streamCommErr.Position = 0;
                        }
                        catch
                        {

                        }
                    }

                    TCS.LogMsg("RX ERROR \t STX NOT FIND IN 1 BYTE");
                    //Flush();
                    DoSendReq();
            }
            catch (Exception e)
            {
                var st = new StackTrace(e, true);
                // Get the top stack frame
                var frame = st.GetFrame(0);
                // Get the line number from the stack frame
                var line = frame.GetFileLineNumber();

                TCS.LogMsg(line.ToString() + " = " + e.Message);
                TCS.LogMsg(e.StackTrace);
                disconnect();
            }
            return true;
        }
        public Boolean GetRAW2()
        {
            int wLen = 8;
            int iGet = 0;
            int icnt = 0;
            int iRemainRx = 0;
            int iNowcnt = 0;
            try
            {
                Stopwatch swTO = new Stopwatch();

                byte[] bBuf = new byte[4096];

                int iLen;
                int iCycle = 0;
                int iCH;

                // stx(1) + cmd(1) + ch(1) + len(2) 

                //if (DelayForRecv > 0)
                //{
                //    Thread.Sleep(DelayForRecv);
                //}
                // make header
                swTO.Restart();
                while (iGet < wLen)
                {
                    if (swTO.ElapsedMilliseconds >= 1000)
                    {
                        TCS.LogMsg("TIMEOUT HEADER");
                        //Flush();
                        return false;
                    }
                    int iRemain = wLen - iGet;
                    if (iRemain <= 0)
                        break;

                    //int icnt = Socket.Receive(bBuf, iGet, iRemain, SocketFlags.None);

                    RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, 503);
                    bBuf = udpClient.Receive(ref RemoteIpEndPoint);
                    icnt = bBuf.Length;

                    iGet += icnt;

                    //SendReq();
                }


                //int ii = 0;
                //while (true)
                //{
                //    byte[] rbbb = new byte[255];
                //    ii = Socket.Receive(bbb, 0, 1, SocketFlags.None);
                //    if (ii == 0)
                //        break;
                //}
                //TCS.LogMsg("RX >> " + result.ToString() + " BYTE");

                //if (result > 0)
                {
                    if (bBuf[0] == 0x02)
                    {
                        // req 응답
                        if (bBuf[1] == _CMD_REQ)
                        {
                            iLen = BitConverter.ToUInt16(bBuf, 2);
                            iCH = BitConverter.ToUInt16(bBuf, 4);

                            if (iLen == 1)
                            {
                                if (iCH == 0)
                                {
                                    // go getraw

                                }
                                else if (iCH == 1)
                                {
                                    
                                    DoSendReq();
                                }
                            }
                            else if (iCH == 0)
                            {
                                // stx + cmd + len2 + ch2 + cycle2 

                                // 모든 채널 받기
                                wLen = 2888 * 12;

                                BufCycle = new byte[wLen];

                                // 이미 받은것 채우기
                                Array.Copy(bBuf, 0, BufCycle, 0, bBuf.Length);

                                // RAW 모두 받기
                                swTO.Restart();
                                //iGet = 6;
                                while (iGet < wLen)
                                {
                                    if (swTO.ElapsedMilliseconds >= 5000)
                                    {
                                        TCS.LogMsg("TIMEOUT RAW");
                                        //disconnect();
                                        return false;
                                    }
                                    iRemainRx = wLen - iGet;
                                    if (iRemainRx <= 0)
                                    {
                                        TCS.LogMsg(String.Format("{0} {1} {2}", wLen, iGet, iRemainRx));
                                        break;
                                    }

                                    bBuf = new byte[40960];
                                    //RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, 503);
                                    bBuf = udpClient.Receive(ref RemoteIpEndPoint);
                                    iNowcnt = bBuf.Length;

                                    Array.Copy(bBuf, 0, BufCycle, iGet, iNowcnt);

                                    //iNowcnt = Socket.Receive(BufCycle, iGet, iRemainRx, SocketFlags.None);
                                    if (iNowcnt > 0)
                                    {
                                        iGet += iNowcnt;

                                        swTO.Restart();
                                    }
                                }

                                TCS.LogMsg("RX >> " + iGet.ToString() + " BYTE");

                                if (OnResponseCHData != null)
                                {
                                    OnResponseCHData(0xFFFF, 0xFFFF, ref BufCycle, 0, wLen);
                                }

                                swRx.Restart();

                                DoSendReq();

                                return true;
                            }
                        }
                        else
                        {

                        }
                    }
                    else
                    {
                        //TCS.LogMsg("RX ERROR \t STX NOT FIND IN 1 BYTE");
                        //Flush();
                        //DoSendReq();
                    }
                }
            }
            catch (Exception e)
            {
                TCS.LogMsg(e.Message);
                TCS.LogMsg(e.StackTrace);
                //disconnect();
            }
            return true;
        }
    }
}

﻿namespace EngineAnalyzer
{
    partial class TFormCalKnock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TFormCalKnock));
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.panelForm = new System.Windows.Forms.Panel();
            this.imgBack = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.chartKnock = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chartCyl = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panelOK = new System.Windows.Forms.Panel();
            this.labelAV = new System.Windows.Forms.Label();
            this.checkW5 = new System.Windows.Forms.PictureBox();
            this.checkW4 = new System.Windows.Forms.PictureBox();
            this.checkW3 = new System.Windows.Forms.PictureBox();
            this.checkW2 = new System.Windows.Forms.PictureBox();
            this.checkW1 = new System.Windows.Forms.PictureBox();
            this.checkOn = new System.Windows.Forms.PictureBox();
            this.downAvgCycleCnt = new System.Windows.Forms.Panel();
            this.downHeavyKnock = new System.Windows.Forms.Panel();
            this.upAvgCycleCnt = new System.Windows.Forms.Panel();
            this.upHeavyKnock = new System.Windows.Forms.Panel();
            this.downLightKnock = new System.Windows.Forms.Panel();
            this.upLightKnock = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.downWE5 = new System.Windows.Forms.Panel();
            this.upWE5 = new System.Windows.Forms.Panel();
            this.downWS5 = new System.Windows.Forms.Panel();
            this.upWS5 = new System.Windows.Forms.Panel();
            this.editWE5 = new System.Windows.Forms.TextBox();
            this.editWS5 = new System.Windows.Forms.TextBox();
            this.panel22 = new System.Windows.Forms.Panel();
            this.downWE4 = new System.Windows.Forms.Panel();
            this.upWE4 = new System.Windows.Forms.Panel();
            this.downWS4 = new System.Windows.Forms.Panel();
            this.upWS4 = new System.Windows.Forms.Panel();
            this.editWE4 = new System.Windows.Forms.TextBox();
            this.editWS4 = new System.Windows.Forms.TextBox();
            this.panel12 = new System.Windows.Forms.Panel();
            this.downWE3 = new System.Windows.Forms.Panel();
            this.upWE3 = new System.Windows.Forms.Panel();
            this.downWS3 = new System.Windows.Forms.Panel();
            this.upWS3 = new System.Windows.Forms.Panel();
            this.editWE3 = new System.Windows.Forms.TextBox();
            this.editWS3 = new System.Windows.Forms.TextBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.downWE2 = new System.Windows.Forms.Panel();
            this.upWE2 = new System.Windows.Forms.Panel();
            this.downWS2 = new System.Windows.Forms.Panel();
            this.upWS2 = new System.Windows.Forms.Panel();
            this.editWE2 = new System.Windows.Forms.TextBox();
            this.editWS2 = new System.Windows.Forms.TextBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.downWE1 = new System.Windows.Forms.Panel();
            this.upWE1 = new System.Windows.Forms.Panel();
            this.downWS1 = new System.Windows.Forms.Panel();
            this.upWS1 = new System.Windows.Forms.Panel();
            this.editWE1 = new System.Windows.Forms.TextBox();
            this.editWS1 = new System.Windows.Forms.TextBox();
            this.editAvgCycleCnt = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.editHeavyKnock = new System.Windows.Forms.TextBox();
            this.editLightKnock = new System.Windows.Forms.TextBox();
            this.imgCyl12 = new System.Windows.Forms.PictureBox();
            this.imgCyl11 = new System.Windows.Forms.PictureBox();
            this.imgCyl10 = new System.Windows.Forms.PictureBox();
            this.imgCyl1 = new System.Windows.Forms.PictureBox();
            this.imgCyl9 = new System.Windows.Forms.PictureBox();
            this.imgCyl2 = new System.Windows.Forms.PictureBox();
            this.imgCyl8 = new System.Windows.Forms.PictureBox();
            this.imgCyl3 = new System.Windows.Forms.PictureBox();
            this.imgCyl7 = new System.Windows.Forms.PictureBox();
            this.imgCyl4 = new System.Windows.Forms.PictureBox();
            this.imgCyl6 = new System.Windows.Forms.PictureBox();
            this.imgCyl5 = new System.Windows.Forms.PictureBox();
            this.imgCylOn = new System.Windows.Forms.PictureBox();
            this.timerTick = new System.Windows.Forms.Timer(this.components);
            this.labelBottomMsg = new System.Windows.Forms.Label();
            this.panelForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgBack)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartKnock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartCyl)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkW5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkW4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkW3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkW2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkW1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkOn)).BeginInit();
            this.panel17.SuspendLayout();
            this.panel22.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCylOn)).BeginInit();
            this.SuspendLayout();
            // 
            // panelForm
            // 
            this.panelForm.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panelForm.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelForm.BackgroundImage")));
            this.panelForm.Controls.Add(this.labelBottomMsg);
            this.panelForm.Controls.Add(this.imgBack);
            this.panelForm.Controls.Add(this.panel2);
            this.panelForm.Controls.Add(this.panel1);
            this.panelForm.Controls.Add(this.imgCylOn);
            this.panelForm.Location = new System.Drawing.Point(0, 0);
            this.panelForm.Name = "panelForm";
            this.panelForm.Size = new System.Drawing.Size(1920, 1005);
            this.panelForm.TabIndex = 0;
            this.panelForm.Visible = false;
            // 
            // imgBack
            // 
            this.imgBack.Image = ((System.Drawing.Image)(resources.GetObject("imgBack.Image")));
            this.imgBack.Location = new System.Drawing.Point(93, 8);
            this.imgBack.Name = "imgBack";
            this.imgBack.Size = new System.Drawing.Size(60, 23);
            this.imgBack.TabIndex = 33;
            this.imgBack.TabStop = false;
            this.imgBack.Visible = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.chartKnock);
            this.panel2.Controls.Add(this.chartCyl);
            this.panel2.Location = new System.Drawing.Point(403, 101);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1438, 824);
            this.panel2.TabIndex = 32;
            // 
            // chartKnock
            // 
            this.chartKnock.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(243)))), ((int)(((byte)(245)))));
            this.chartKnock.BorderlineWidth = 0;
            this.chartKnock.BorderSkin.BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            this.chartKnock.BorderSkin.PageColor = System.Drawing.Color.Empty;
            chartArea1.AxisX.IsLabelAutoFit = false;
            chartArea1.AxisX.IsMarginVisible = false;
            chartArea1.AxisX.LabelAutoFitMaxFontSize = 12;
            chartArea1.AxisX.LabelAutoFitMinFontSize = 12;
            chartArea1.AxisX.LabelStyle.Enabled = false;
            chartArea1.AxisX.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            chartArea1.AxisX.LabelStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            chartArea1.AxisX.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(191)))), ((int)(((byte)(193)))));
            chartArea1.AxisX.MajorGrid.Enabled = false;
            chartArea1.AxisX.MajorTickMark.Enabled = false;
            chartArea1.AxisX.TitleForeColor = System.Drawing.Color.DimGray;
            chartArea1.AxisX2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(191)))), ((int)(((byte)(193)))));
            chartArea1.AxisY.IsLabelAutoFit = false;
            chartArea1.AxisY.LabelStyle.Font = new System.Drawing.Font("HelveticaNeueLT Pro 67 MdCn", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            chartArea1.AxisY.LabelStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            chartArea1.AxisY.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(191)))), ((int)(((byte)(193)))));
            chartArea1.AxisY.MajorGrid.Enabled = false;
            chartArea1.AxisY.MajorTickMark.Enabled = false;
            chartArea1.AxisY2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(191)))), ((int)(((byte)(193)))));
            chartArea1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(243)))), ((int)(((byte)(245)))));
            chartArea1.InnerPlotPosition.Auto = false;
            chartArea1.InnerPlotPosition.Height = 98F;
            chartArea1.InnerPlotPosition.Width = 98F;
            chartArea1.InnerPlotPosition.X = 2F;
            chartArea1.InnerPlotPosition.Y = 2F;
            chartArea1.Name = "ChartArea1";
            chartArea1.Position.Auto = false;
            chartArea1.Position.Height = 94F;
            chartArea1.Position.Width = 99.5F;
            chartArea1.Position.X = 0.5F;
            chartArea1.Position.Y = 3F;
            this.chartKnock.ChartAreas.Add(chartArea1);
            this.chartKnock.Location = new System.Drawing.Point(18, 459);
            this.chartKnock.Margin = new System.Windows.Forms.Padding(0);
            this.chartKnock.Name = "chartKnock";
            this.chartKnock.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            series1.ChartArea = "ChartArea1";
            series1.Name = "SeriesRPMLogging";
            this.chartKnock.Series.Add(series1);
            this.chartKnock.Size = new System.Drawing.Size(1395, 350);
            this.chartKnock.TabIndex = 5;
            this.chartKnock.PostPaint += new System.EventHandler<System.Windows.Forms.DataVisualization.Charting.ChartPaintEventArgs>(this.chartKnock_PostPaint);
            // 
            // chartCyl
            // 
            this.chartCyl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(243)))), ((int)(((byte)(245)))));
            this.chartCyl.BorderlineWidth = 0;
            this.chartCyl.BorderSkin.BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            this.chartCyl.BorderSkin.PageColor = System.Drawing.Color.Empty;
            chartArea2.AxisX.IsLabelAutoFit = false;
            chartArea2.AxisX.IsMarginVisible = false;
            chartArea2.AxisX.LabelAutoFitMaxFontSize = 12;
            chartArea2.AxisX.LabelAutoFitMinFontSize = 12;
            chartArea2.AxisX.LabelStyle.Enabled = false;
            chartArea2.AxisX.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            chartArea2.AxisX.LabelStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            chartArea2.AxisX.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(191)))), ((int)(((byte)(193)))));
            chartArea2.AxisX.MajorGrid.Enabled = false;
            chartArea2.AxisX.MajorTickMark.Enabled = false;
            chartArea2.AxisX.TitleForeColor = System.Drawing.Color.DimGray;
            chartArea2.AxisX2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(191)))), ((int)(((byte)(193)))));
            chartArea2.AxisY.IsLabelAutoFit = false;
            chartArea2.AxisY.LabelStyle.Font = new System.Drawing.Font("HelveticaNeueLT Pro 67 MdCn", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            chartArea2.AxisY.LabelStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            chartArea2.AxisY.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(191)))), ((int)(((byte)(193)))));
            chartArea2.AxisY.MajorGrid.Enabled = false;
            chartArea2.AxisY.MajorTickMark.Enabled = false;
            chartArea2.AxisY2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(191)))), ((int)(((byte)(193)))));
            chartArea2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(243)))), ((int)(((byte)(245)))));
            chartArea2.InnerPlotPosition.Auto = false;
            chartArea2.InnerPlotPosition.Height = 98F;
            chartArea2.InnerPlotPosition.Width = 98F;
            chartArea2.InnerPlotPosition.X = 2F;
            chartArea2.InnerPlotPosition.Y = 2F;
            chartArea2.Name = "ChartArea1";
            chartArea2.Position.Auto = false;
            chartArea2.Position.Height = 94F;
            chartArea2.Position.Width = 99.5F;
            chartArea2.Position.X = 0.5F;
            chartArea2.Position.Y = 3F;
            this.chartCyl.ChartAreas.Add(chartArea2);
            this.chartCyl.Location = new System.Drawing.Point(18, 46);
            this.chartCyl.Margin = new System.Windows.Forms.Padding(0);
            this.chartCyl.Name = "chartCyl";
            this.chartCyl.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            series2.Name = "SeriesRPMLogging";
            this.chartCyl.Series.Add(series2);
            this.chartCyl.Size = new System.Drawing.Size(1395, 305);
            this.chartCyl.TabIndex = 4;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.panelOK);
            this.panel1.Controls.Add(this.labelAV);
            this.panel1.Controls.Add(this.checkW5);
            this.panel1.Controls.Add(this.checkW4);
            this.panel1.Controls.Add(this.checkW3);
            this.panel1.Controls.Add(this.checkW2);
            this.panel1.Controls.Add(this.checkW1);
            this.panel1.Controls.Add(this.checkOn);
            this.panel1.Controls.Add(this.downAvgCycleCnt);
            this.panel1.Controls.Add(this.downHeavyKnock);
            this.panel1.Controls.Add(this.upAvgCycleCnt);
            this.panel1.Controls.Add(this.upHeavyKnock);
            this.panel1.Controls.Add(this.downLightKnock);
            this.panel1.Controls.Add(this.upLightKnock);
            this.panel1.Controls.Add(this.panel17);
            this.panel1.Controls.Add(this.panel22);
            this.panel1.Controls.Add(this.panel12);
            this.panel1.Controls.Add(this.panel7);
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.editAvgCycleCnt);
            this.panel1.Controls.Add(this.textBox2);
            this.panel1.Controls.Add(this.editHeavyKnock);
            this.panel1.Controls.Add(this.editLightKnock);
            this.panel1.Controls.Add(this.imgCyl12);
            this.panel1.Controls.Add(this.imgCyl11);
            this.panel1.Controls.Add(this.imgCyl10);
            this.panel1.Controls.Add(this.imgCyl1);
            this.panel1.Controls.Add(this.imgCyl9);
            this.panel1.Controls.Add(this.imgCyl2);
            this.panel1.Controls.Add(this.imgCyl8);
            this.panel1.Controls.Add(this.imgCyl3);
            this.panel1.Controls.Add(this.imgCyl7);
            this.panel1.Controls.Add(this.imgCyl4);
            this.panel1.Controls.Add(this.imgCyl6);
            this.panel1.Controls.Add(this.imgCyl5);
            this.panel1.Location = new System.Drawing.Point(80, 101);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(283, 824);
            this.panel1.TabIndex = 30;
            // 
            // panelOK
            // 
            this.panelOK.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelOK.BackgroundImage")));
            this.panelOK.Location = new System.Drawing.Point(228, 237);
            this.panelOK.Name = "panelOK";
            this.panelOK.Size = new System.Drawing.Size(30, 26);
            this.panelOK.TabIndex = 56;
            this.panelOK.Tag = "confirm";
            this.panelOK.Click += new System.EventHandler(this.panelOK_Click);
            // 
            // labelAV
            // 
            this.labelAV.BackColor = System.Drawing.Color.Transparent;
            this.labelAV.Font = new System.Drawing.Font("맑은 고딕", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelAV.Location = new System.Drawing.Point(25, 215);
            this.labelAV.Name = "labelAV";
            this.labelAV.Size = new System.Drawing.Size(228, 50);
            this.labelAV.TabIndex = 55;
            this.labelAV.Text = "0";
            this.labelAV.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // checkW5
            // 
            this.checkW5.BackColor = System.Drawing.Color.Transparent;
            this.checkW5.Location = new System.Drawing.Point(22, 772);
            this.checkW5.Name = "checkW5";
            this.checkW5.Size = new System.Drawing.Size(76, 29);
            this.checkW5.TabIndex = 53;
            this.checkW5.TabStop = false;
            // 
            // checkW4
            // 
            this.checkW4.BackColor = System.Drawing.Color.Transparent;
            this.checkW4.Location = new System.Drawing.Point(21, 700);
            this.checkW4.Name = "checkW4";
            this.checkW4.Size = new System.Drawing.Size(76, 29);
            this.checkW4.TabIndex = 52;
            this.checkW4.TabStop = false;
            // 
            // checkW3
            // 
            this.checkW3.BackColor = System.Drawing.Color.Transparent;
            this.checkW3.Location = new System.Drawing.Point(21, 628);
            this.checkW3.Name = "checkW3";
            this.checkW3.Size = new System.Drawing.Size(76, 29);
            this.checkW3.TabIndex = 51;
            this.checkW3.TabStop = false;
            // 
            // checkW2
            // 
            this.checkW2.BackColor = System.Drawing.Color.Transparent;
            this.checkW2.Location = new System.Drawing.Point(22, 557);
            this.checkW2.Name = "checkW2";
            this.checkW2.Size = new System.Drawing.Size(76, 29);
            this.checkW2.TabIndex = 50;
            this.checkW2.TabStop = false;
            // 
            // checkW1
            // 
            this.checkW1.BackColor = System.Drawing.Color.Transparent;
            this.checkW1.Location = new System.Drawing.Point(22, 485);
            this.checkW1.Name = "checkW1";
            this.checkW1.Size = new System.Drawing.Size(76, 29);
            this.checkW1.TabIndex = 49;
            this.checkW1.TabStop = false;
            // 
            // checkOn
            // 
            this.checkOn.BackColor = System.Drawing.Color.Transparent;
            this.checkOn.Image = ((System.Drawing.Image)(resources.GetObject("checkOn.Image")));
            this.checkOn.Location = new System.Drawing.Point(103, 398);
            this.checkOn.Name = "checkOn";
            this.checkOn.Size = new System.Drawing.Size(76, 29);
            this.checkOn.TabIndex = 54;
            this.checkOn.TabStop = false;
            this.checkOn.Visible = false;
            // 
            // downAvgCycleCnt
            // 
            this.downAvgCycleCnt.Location = new System.Drawing.Point(227, 391);
            this.downAvgCycleCnt.Name = "downAvgCycleCnt";
            this.downAvgCycleCnt.Size = new System.Drawing.Size(30, 15);
            this.downAvgCycleCnt.TabIndex = 48;
            // 
            // downHeavyKnock
            // 
            this.downHeavyKnock.Location = new System.Drawing.Point(227, 351);
            this.downHeavyKnock.Name = "downHeavyKnock";
            this.downHeavyKnock.Size = new System.Drawing.Size(30, 15);
            this.downHeavyKnock.TabIndex = 48;
            // 
            // upAvgCycleCnt
            // 
            this.upAvgCycleCnt.Location = new System.Drawing.Point(227, 377);
            this.upAvgCycleCnt.Name = "upAvgCycleCnt";
            this.upAvgCycleCnt.Size = new System.Drawing.Size(30, 15);
            this.upAvgCycleCnt.TabIndex = 47;
            // 
            // upHeavyKnock
            // 
            this.upHeavyKnock.Location = new System.Drawing.Point(227, 337);
            this.upHeavyKnock.Name = "upHeavyKnock";
            this.upHeavyKnock.Size = new System.Drawing.Size(30, 15);
            this.upHeavyKnock.TabIndex = 47;
            // 
            // downLightKnock
            // 
            this.downLightKnock.Location = new System.Drawing.Point(227, 311);
            this.downLightKnock.Name = "downLightKnock";
            this.downLightKnock.Size = new System.Drawing.Size(30, 15);
            this.downLightKnock.TabIndex = 46;
            // 
            // upLightKnock
            // 
            this.upLightKnock.Location = new System.Drawing.Point(227, 297);
            this.upLightKnock.Name = "upLightKnock";
            this.upLightKnock.Size = new System.Drawing.Size(30, 15);
            this.upLightKnock.TabIndex = 45;
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.downWE5);
            this.panel17.Controls.Add(this.upWE5);
            this.panel17.Controls.Add(this.downWS5);
            this.panel17.Controls.Add(this.upWS5);
            this.panel17.Controls.Add(this.editWE5);
            this.panel17.Controls.Add(this.editWS5);
            this.panel17.Location = new System.Drawing.Point(103, 759);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(165, 50);
            this.panel17.TabIndex = 44;
            // 
            // downWE5
            // 
            this.downWE5.Location = new System.Drawing.Point(124, 25);
            this.downWE5.Name = "downWE5";
            this.downWE5.Size = new System.Drawing.Size(30, 15);
            this.downWE5.TabIndex = 39;
            // 
            // upWE5
            // 
            this.upWE5.Location = new System.Drawing.Point(124, 11);
            this.upWE5.Name = "upWE5";
            this.upWE5.Size = new System.Drawing.Size(30, 15);
            this.upWE5.TabIndex = 38;
            // 
            // downWS5
            // 
            this.downWS5.Location = new System.Drawing.Point(50, 25);
            this.downWS5.Name = "downWS5";
            this.downWS5.Size = new System.Drawing.Size(30, 15);
            this.downWS5.TabIndex = 37;
            // 
            // upWS5
            // 
            this.upWS5.Location = new System.Drawing.Point(50, 11);
            this.upWS5.Name = "upWS5";
            this.upWS5.Size = new System.Drawing.Size(30, 15);
            this.upWS5.TabIndex = 36;
            // 
            // editWE5
            // 
            this.editWE5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.editWE5.Font = new System.Drawing.Font("굴림", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.editWE5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.editWE5.Location = new System.Drawing.Point(86, 21);
            this.editWE5.Multiline = true;
            this.editWE5.Name = "editWE5";
            this.editWE5.Size = new System.Drawing.Size(38, 17);
            this.editWE5.TabIndex = 35;
            this.editWE5.Text = "0";
            this.editWE5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // editWS5
            // 
            this.editWS5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.editWS5.Font = new System.Drawing.Font("굴림", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.editWS5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.editWS5.Location = new System.Drawing.Point(12, 20);
            this.editWS5.Multiline = true;
            this.editWS5.Name = "editWS5";
            this.editWS5.Size = new System.Drawing.Size(38, 17);
            this.editWS5.TabIndex = 34;
            this.editWS5.Text = "0";
            this.editWS5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel22
            // 
            this.panel22.Controls.Add(this.downWE4);
            this.panel22.Controls.Add(this.upWE4);
            this.panel22.Controls.Add(this.downWS4);
            this.panel22.Controls.Add(this.upWS4);
            this.panel22.Controls.Add(this.editWE4);
            this.panel22.Controls.Add(this.editWS4);
            this.panel22.Location = new System.Drawing.Point(103, 687);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(165, 50);
            this.panel22.TabIndex = 43;
            // 
            // downWE4
            // 
            this.downWE4.Location = new System.Drawing.Point(124, 25);
            this.downWE4.Name = "downWE4";
            this.downWE4.Size = new System.Drawing.Size(30, 15);
            this.downWE4.TabIndex = 39;
            // 
            // upWE4
            // 
            this.upWE4.Location = new System.Drawing.Point(124, 11);
            this.upWE4.Name = "upWE4";
            this.upWE4.Size = new System.Drawing.Size(30, 15);
            this.upWE4.TabIndex = 38;
            // 
            // downWS4
            // 
            this.downWS4.Location = new System.Drawing.Point(50, 25);
            this.downWS4.Name = "downWS4";
            this.downWS4.Size = new System.Drawing.Size(30, 15);
            this.downWS4.TabIndex = 37;
            // 
            // upWS4
            // 
            this.upWS4.Location = new System.Drawing.Point(50, 11);
            this.upWS4.Name = "upWS4";
            this.upWS4.Size = new System.Drawing.Size(30, 15);
            this.upWS4.TabIndex = 36;
            // 
            // editWE4
            // 
            this.editWE4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.editWE4.Font = new System.Drawing.Font("굴림", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.editWE4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.editWE4.Location = new System.Drawing.Point(86, 21);
            this.editWE4.Multiline = true;
            this.editWE4.Name = "editWE4";
            this.editWE4.Size = new System.Drawing.Size(38, 17);
            this.editWE4.TabIndex = 35;
            this.editWE4.Text = "0";
            this.editWE4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // editWS4
            // 
            this.editWS4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.editWS4.Font = new System.Drawing.Font("굴림", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.editWS4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.editWS4.Location = new System.Drawing.Point(12, 20);
            this.editWS4.Multiline = true;
            this.editWS4.Name = "editWS4";
            this.editWS4.Size = new System.Drawing.Size(38, 17);
            this.editWS4.TabIndex = 34;
            this.editWS4.Text = "0";
            this.editWS4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.downWE3);
            this.panel12.Controls.Add(this.upWE3);
            this.panel12.Controls.Add(this.downWS3);
            this.panel12.Controls.Add(this.upWS3);
            this.panel12.Controls.Add(this.editWE3);
            this.panel12.Controls.Add(this.editWS3);
            this.panel12.Location = new System.Drawing.Point(103, 615);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(165, 50);
            this.panel12.TabIndex = 42;
            // 
            // downWE3
            // 
            this.downWE3.Location = new System.Drawing.Point(124, 25);
            this.downWE3.Name = "downWE3";
            this.downWE3.Size = new System.Drawing.Size(30, 15);
            this.downWE3.TabIndex = 39;
            // 
            // upWE3
            // 
            this.upWE3.Location = new System.Drawing.Point(124, 11);
            this.upWE3.Name = "upWE3";
            this.upWE3.Size = new System.Drawing.Size(30, 15);
            this.upWE3.TabIndex = 38;
            // 
            // downWS3
            // 
            this.downWS3.Location = new System.Drawing.Point(50, 25);
            this.downWS3.Name = "downWS3";
            this.downWS3.Size = new System.Drawing.Size(30, 15);
            this.downWS3.TabIndex = 37;
            // 
            // upWS3
            // 
            this.upWS3.Location = new System.Drawing.Point(50, 11);
            this.upWS3.Name = "upWS3";
            this.upWS3.Size = new System.Drawing.Size(30, 15);
            this.upWS3.TabIndex = 36;
            // 
            // editWE3
            // 
            this.editWE3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.editWE3.Font = new System.Drawing.Font("굴림", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.editWE3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.editWE3.Location = new System.Drawing.Point(86, 21);
            this.editWE3.Multiline = true;
            this.editWE3.Name = "editWE3";
            this.editWE3.Size = new System.Drawing.Size(38, 17);
            this.editWE3.TabIndex = 35;
            this.editWE3.Text = "0";
            this.editWE3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // editWS3
            // 
            this.editWS3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.editWS3.Font = new System.Drawing.Font("굴림", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.editWS3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.editWS3.Location = new System.Drawing.Point(12, 20);
            this.editWS3.Multiline = true;
            this.editWS3.Name = "editWS3";
            this.editWS3.Size = new System.Drawing.Size(38, 17);
            this.editWS3.TabIndex = 34;
            this.editWS3.Text = "0";
            this.editWS3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.downWE2);
            this.panel7.Controls.Add(this.upWE2);
            this.panel7.Controls.Add(this.downWS2);
            this.panel7.Controls.Add(this.upWS2);
            this.panel7.Controls.Add(this.editWE2);
            this.panel7.Controls.Add(this.editWS2);
            this.panel7.Location = new System.Drawing.Point(103, 543);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(165, 50);
            this.panel7.TabIndex = 41;
            // 
            // downWE2
            // 
            this.downWE2.Location = new System.Drawing.Point(124, 25);
            this.downWE2.Name = "downWE2";
            this.downWE2.Size = new System.Drawing.Size(30, 15);
            this.downWE2.TabIndex = 39;
            // 
            // upWE2
            // 
            this.upWE2.Location = new System.Drawing.Point(124, 11);
            this.upWE2.Name = "upWE2";
            this.upWE2.Size = new System.Drawing.Size(30, 15);
            this.upWE2.TabIndex = 38;
            // 
            // downWS2
            // 
            this.downWS2.Location = new System.Drawing.Point(50, 25);
            this.downWS2.Name = "downWS2";
            this.downWS2.Size = new System.Drawing.Size(30, 15);
            this.downWS2.TabIndex = 37;
            // 
            // upWS2
            // 
            this.upWS2.Location = new System.Drawing.Point(50, 11);
            this.upWS2.Name = "upWS2";
            this.upWS2.Size = new System.Drawing.Size(30, 15);
            this.upWS2.TabIndex = 36;
            // 
            // editWE2
            // 
            this.editWE2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.editWE2.Font = new System.Drawing.Font("굴림", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.editWE2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.editWE2.Location = new System.Drawing.Point(86, 21);
            this.editWE2.Multiline = true;
            this.editWE2.Name = "editWE2";
            this.editWE2.Size = new System.Drawing.Size(38, 17);
            this.editWE2.TabIndex = 35;
            this.editWE2.Text = "0";
            this.editWE2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // editWS2
            // 
            this.editWS2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.editWS2.Font = new System.Drawing.Font("굴림", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.editWS2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.editWS2.Location = new System.Drawing.Point(12, 20);
            this.editWS2.Multiline = true;
            this.editWS2.Name = "editWS2";
            this.editWS2.Size = new System.Drawing.Size(38, 17);
            this.editWS2.TabIndex = 34;
            this.editWS2.Text = "0";
            this.editWS2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.downWE1);
            this.panel6.Controls.Add(this.upWE1);
            this.panel6.Controls.Add(this.downWS1);
            this.panel6.Controls.Add(this.upWS1);
            this.panel6.Controls.Add(this.editWE1);
            this.panel6.Controls.Add(this.editWS1);
            this.panel6.Location = new System.Drawing.Point(103, 471);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(165, 50);
            this.panel6.TabIndex = 40;
            // 
            // downWE1
            // 
            this.downWE1.Location = new System.Drawing.Point(124, 25);
            this.downWE1.Name = "downWE1";
            this.downWE1.Size = new System.Drawing.Size(30, 15);
            this.downWE1.TabIndex = 39;
            // 
            // upWE1
            // 
            this.upWE1.Location = new System.Drawing.Point(124, 11);
            this.upWE1.Name = "upWE1";
            this.upWE1.Size = new System.Drawing.Size(30, 15);
            this.upWE1.TabIndex = 38;
            // 
            // downWS1
            // 
            this.downWS1.Location = new System.Drawing.Point(50, 25);
            this.downWS1.Name = "downWS1";
            this.downWS1.Size = new System.Drawing.Size(30, 15);
            this.downWS1.TabIndex = 37;
            // 
            // upWS1
            // 
            this.upWS1.Location = new System.Drawing.Point(50, 11);
            this.upWS1.Name = "upWS1";
            this.upWS1.Size = new System.Drawing.Size(30, 15);
            this.upWS1.TabIndex = 36;
            // 
            // editWE1
            // 
            this.editWE1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.editWE1.Font = new System.Drawing.Font("굴림", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.editWE1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.editWE1.Location = new System.Drawing.Point(86, 21);
            this.editWE1.Multiline = true;
            this.editWE1.Name = "editWE1";
            this.editWE1.Size = new System.Drawing.Size(38, 17);
            this.editWE1.TabIndex = 35;
            this.editWE1.Text = "0";
            this.editWE1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // editWS1
            // 
            this.editWS1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.editWS1.Font = new System.Drawing.Font("굴림", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.editWS1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.editWS1.Location = new System.Drawing.Point(12, 20);
            this.editWS1.Multiline = true;
            this.editWS1.Name = "editWS1";
            this.editWS1.Size = new System.Drawing.Size(38, 17);
            this.editWS1.TabIndex = 34;
            this.editWS1.Text = "0";
            this.editWS1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // editAvgCycleCnt
            // 
            this.editAvgCycleCnt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.editAvgCycleCnt.Font = new System.Drawing.Font("굴림", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.editAvgCycleCnt.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.editAvgCycleCnt.Location = new System.Drawing.Point(189, 387);
            this.editAvgCycleCnt.Multiline = true;
            this.editAvgCycleCnt.Name = "editAvgCycleCnt";
            this.editAvgCycleCnt.Size = new System.Drawing.Size(38, 17);
            this.editAvgCycleCnt.TabIndex = 33;
            this.editAvgCycleCnt.Text = "1";
            this.editAvgCycleCnt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox2
            // 
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox2.Font = new System.Drawing.Font("굴림", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.textBox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.textBox2.Location = new System.Drawing.Point(189, 387);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(38, 17);
            this.textBox2.TabIndex = 32;
            this.textBox2.Text = "0";
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // editHeavyKnock
            // 
            this.editHeavyKnock.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.editHeavyKnock.Font = new System.Drawing.Font("굴림", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.editHeavyKnock.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.editHeavyKnock.Location = new System.Drawing.Point(189, 346);
            this.editHeavyKnock.Multiline = true;
            this.editHeavyKnock.Name = "editHeavyKnock";
            this.editHeavyKnock.Size = new System.Drawing.Size(38, 17);
            this.editHeavyKnock.TabIndex = 31;
            this.editHeavyKnock.Text = "256";
            this.editHeavyKnock.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // editLightKnock
            // 
            this.editLightKnock.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.editLightKnock.Font = new System.Drawing.Font("굴림", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.editLightKnock.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.editLightKnock.Location = new System.Drawing.Point(189, 307);
            this.editLightKnock.Multiline = true;
            this.editLightKnock.Name = "editLightKnock";
            this.editLightKnock.Size = new System.Drawing.Size(38, 17);
            this.editLightKnock.TabIndex = 29;
            this.editLightKnock.Text = "1";
            this.editLightKnock.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // imgCyl12
            // 
            this.imgCyl12.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl12.Location = new System.Drawing.Point(182, 149);
            this.imgCyl12.Name = "imgCyl12";
            this.imgCyl12.Size = new System.Drawing.Size(71, 29);
            this.imgCyl12.TabIndex = 28;
            this.imgCyl12.TabStop = false;
            // 
            // imgCyl11
            // 
            this.imgCyl11.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl11.Location = new System.Drawing.Point(103, 149);
            this.imgCyl11.Name = "imgCyl11";
            this.imgCyl11.Size = new System.Drawing.Size(71, 29);
            this.imgCyl11.TabIndex = 27;
            this.imgCyl11.TabStop = false;
            // 
            // imgCyl10
            // 
            this.imgCyl10.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl10.Location = new System.Drawing.Point(21, 149);
            this.imgCyl10.Name = "imgCyl10";
            this.imgCyl10.Size = new System.Drawing.Size(71, 29);
            this.imgCyl10.TabIndex = 26;
            this.imgCyl10.TabStop = false;
            // 
            // imgCyl1
            // 
            this.imgCyl1.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl1.Location = new System.Drawing.Point(21, 47);
            this.imgCyl1.Name = "imgCyl1";
            this.imgCyl1.Size = new System.Drawing.Size(71, 29);
            this.imgCyl1.TabIndex = 17;
            this.imgCyl1.TabStop = false;
            // 
            // imgCyl9
            // 
            this.imgCyl9.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl9.Location = new System.Drawing.Point(182, 115);
            this.imgCyl9.Name = "imgCyl9";
            this.imgCyl9.Size = new System.Drawing.Size(71, 29);
            this.imgCyl9.TabIndex = 25;
            this.imgCyl9.TabStop = false;
            // 
            // imgCyl2
            // 
            this.imgCyl2.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl2.Location = new System.Drawing.Point(103, 47);
            this.imgCyl2.Name = "imgCyl2";
            this.imgCyl2.Size = new System.Drawing.Size(71, 29);
            this.imgCyl2.TabIndex = 18;
            this.imgCyl2.TabStop = false;
            // 
            // imgCyl8
            // 
            this.imgCyl8.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl8.Location = new System.Drawing.Point(103, 115);
            this.imgCyl8.Name = "imgCyl8";
            this.imgCyl8.Size = new System.Drawing.Size(71, 29);
            this.imgCyl8.TabIndex = 24;
            this.imgCyl8.TabStop = false;
            // 
            // imgCyl3
            // 
            this.imgCyl3.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl3.Location = new System.Drawing.Point(182, 47);
            this.imgCyl3.Name = "imgCyl3";
            this.imgCyl3.Size = new System.Drawing.Size(71, 29);
            this.imgCyl3.TabIndex = 19;
            this.imgCyl3.TabStop = false;
            // 
            // imgCyl7
            // 
            this.imgCyl7.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl7.Location = new System.Drawing.Point(21, 115);
            this.imgCyl7.Name = "imgCyl7";
            this.imgCyl7.Size = new System.Drawing.Size(71, 29);
            this.imgCyl7.TabIndex = 23;
            this.imgCyl7.TabStop = false;
            // 
            // imgCyl4
            // 
            this.imgCyl4.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl4.Location = new System.Drawing.Point(21, 81);
            this.imgCyl4.Name = "imgCyl4";
            this.imgCyl4.Size = new System.Drawing.Size(71, 29);
            this.imgCyl4.TabIndex = 20;
            this.imgCyl4.TabStop = false;
            // 
            // imgCyl6
            // 
            this.imgCyl6.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl6.Location = new System.Drawing.Point(182, 81);
            this.imgCyl6.Name = "imgCyl6";
            this.imgCyl6.Size = new System.Drawing.Size(71, 29);
            this.imgCyl6.TabIndex = 22;
            this.imgCyl6.TabStop = false;
            // 
            // imgCyl5
            // 
            this.imgCyl5.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl5.Location = new System.Drawing.Point(103, 81);
            this.imgCyl5.Name = "imgCyl5";
            this.imgCyl5.Size = new System.Drawing.Size(71, 29);
            this.imgCyl5.TabIndex = 21;
            this.imgCyl5.TabStop = false;
            // 
            // imgCylOn
            // 
            this.imgCylOn.BackColor = System.Drawing.Color.Transparent;
            this.imgCylOn.Image = ((System.Drawing.Image)(resources.GetObject("imgCylOn.Image")));
            this.imgCylOn.Location = new System.Drawing.Point(12, 3);
            this.imgCylOn.Name = "imgCylOn";
            this.imgCylOn.Size = new System.Drawing.Size(71, 29);
            this.imgCylOn.TabIndex = 24;
            this.imgCylOn.TabStop = false;
            this.imgCylOn.Visible = false;
            this.imgCylOn.Click += new System.EventHandler(this.imgCylOn_Click);
            // 
            // timerTick
            // 
            this.timerTick.Enabled = true;
            this.timerTick.Interval = 1000;
            this.timerTick.Tick += new System.EventHandler(this.timerTick_Tick);
            // 
            // labelBottomMsg
            // 
            this.labelBottomMsg.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(230)))), ((int)(((byte)(232)))));
            this.labelBottomMsg.Font = new System.Drawing.Font("맑은 고딕", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelBottomMsg.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(63)))), ((int)(((byte)(36)))));
            this.labelBottomMsg.Location = new System.Drawing.Point(77, 933);
            this.labelBottomMsg.Name = "labelBottomMsg";
            this.labelBottomMsg.Size = new System.Drawing.Size(1707, 21);
            this.labelBottomMsg.TabIndex = 96;
            // 
            // TFormCalKnock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1904, 1041);
            this.Controls.Add(this.panelForm);
            this.Name = "TFormCalKnock";
            this.Text = "TFormCal";
            this.panelForm.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imgBack)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartKnock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartCyl)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkW5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkW4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkW3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkW2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkW1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkOn)).EndInit();
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            this.panel22.ResumeLayout(false);
            this.panel22.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCylOn)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Panel panelForm;
        private System.Windows.Forms.PictureBox imgCyl12;
        private System.Windows.Forms.PictureBox imgCyl11;
        private System.Windows.Forms.PictureBox imgCyl10;
        private System.Windows.Forms.PictureBox imgCyl9;
        private System.Windows.Forms.PictureBox imgCyl8;
        private System.Windows.Forms.PictureBox imgCyl7;
        private System.Windows.Forms.PictureBox imgCyl6;
        private System.Windows.Forms.PictureBox imgCyl5;
        private System.Windows.Forms.PictureBox imgCyl4;
        private System.Windows.Forms.PictureBox imgCyl3;
        private System.Windows.Forms.PictureBox imgCyl2;
        private System.Windows.Forms.PictureBox imgCyl1;
        private System.Windows.Forms.PictureBox imgCylOn;
        private System.Windows.Forms.TextBox editLightKnock;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox editAvgCycleCnt;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox editHeavyKnock;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Panel downWE5;
        private System.Windows.Forms.Panel upWE5;
        private System.Windows.Forms.Panel downWS5;
        private System.Windows.Forms.Panel upWS5;
        private System.Windows.Forms.TextBox editWE5;
        private System.Windows.Forms.TextBox editWS5;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Panel downWE4;
        private System.Windows.Forms.Panel upWE4;
        private System.Windows.Forms.Panel downWS4;
        private System.Windows.Forms.Panel upWS4;
        private System.Windows.Forms.TextBox editWE4;
        private System.Windows.Forms.TextBox editWS4;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel downWE3;
        private System.Windows.Forms.Panel upWE3;
        private System.Windows.Forms.Panel downWS3;
        private System.Windows.Forms.Panel upWS3;
        private System.Windows.Forms.TextBox editWE3;
        private System.Windows.Forms.TextBox editWS3;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel downWE2;
        private System.Windows.Forms.Panel upWE2;
        private System.Windows.Forms.Panel downWS2;
        private System.Windows.Forms.Panel upWS2;
        private System.Windows.Forms.TextBox editWE2;
        private System.Windows.Forms.TextBox editWS2;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel downWE1;
        private System.Windows.Forms.Panel upWE1;
        private System.Windows.Forms.Panel downWS1;
        private System.Windows.Forms.Panel upWS1;
        private System.Windows.Forms.TextBox editWE1;
        private System.Windows.Forms.TextBox editWS1;
        private System.Windows.Forms.Panel downAvgCycleCnt;
        private System.Windows.Forms.Panel downHeavyKnock;
        private System.Windows.Forms.Panel upAvgCycleCnt;
        private System.Windows.Forms.Panel upHeavyKnock;
        private System.Windows.Forms.Panel downLightKnock;
        private System.Windows.Forms.Panel upLightKnock;
        private System.Windows.Forms.PictureBox checkW1;
        private System.Windows.Forms.PictureBox checkW5;
        private System.Windows.Forms.PictureBox checkW4;
        private System.Windows.Forms.PictureBox checkW3;
        private System.Windows.Forms.PictureBox checkW2;
        private System.Windows.Forms.PictureBox checkOn;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox imgBack;
        public System.Windows.Forms.DataVisualization.Charting.Chart chartCyl;
        public System.Windows.Forms.DataVisualization.Charting.Chart chartKnock;
        private System.Windows.Forms.Label labelAV;
        private System.Windows.Forms.Timer timerTick;
        private System.Windows.Forms.Panel panelOK;
        private System.Windows.Forms.Label labelBottomMsg;
    }
}
﻿namespace EngineAnalyzer
{
    partial class TFormCH
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelCH = new System.Windows.Forms.Panel();
            this.labelText = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonOK = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelCH
            // 
            this.panelCH.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelCH.Location = new System.Drawing.Point(0, 24);
            this.panelCH.Name = "panelCH";
            this.panelCH.Size = new System.Drawing.Size(313, 488);
            this.panelCH.TabIndex = 0;
            // 
            // labelText
            // 
            this.labelText.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelText.Location = new System.Drawing.Point(0, 0);
            this.labelText.Name = "labelText";
            this.labelText.Size = new System.Drawing.Size(313, 24);
            this.labelText.TabIndex = 1;
            this.labelText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonOK);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(205, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(108, 488);
            this.panel1.TabIndex = 2;
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(3, 3);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(102, 29);
            this.buttonOK.TabIndex = 0;
            this.buttonOK.Text = "확인";
            this.buttonOK.UseVisualStyleBackColor = true;
            // 
            // TFormCH
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(313, 512);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panelCH);
            this.Controls.Add(this.labelText);
            this.Name = "TFormCH";
            this.Text = "실린더 설정";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TFormCH_FormClosing);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelCH;
        private System.Windows.Forms.Label labelText;
        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.Button buttonOK;
    }
}
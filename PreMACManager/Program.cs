﻿using DM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace EngineAnalyzer
{
    static class Program
    {
        /// <summary>
        /// 해당 응용 프로그램의 주 진입점입니다.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            TEA.Init();

            TCS.ThreadInit();
            TCS.LogMsg("----- APPLICATION START -----");

            Application.EnableVisualStyles();
            Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Application_ThreadException);
            Application.SetCompatibleTextRenderingDefault(false);

            if (args.Length > 0)
            {
                if (args[0] == "-view")
                {
                    //Application.Run(new TFormEAViewer());
                }
                else if (args[0] == "-debug")
                {
                    TCS.IsDebug = true;

                    Application.Run(new TFormEA());
                }
            }
            else
            {
                TCS.LogDebug("BEFORE  Application.Run(new TFormLogin());");
                Application.Run(new TFormLogin());
                TCS.LogDebug("AFTER   Application.Run(new TFormLogin());");

                //Application.Run(new TFormEA());
            }
        }

        static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            TCS.LogMsg(e.Exception.Message);
            TCS.LogMsg(e.Exception.StackTrace);
            MessageBox.Show(e.Exception.Message, "EA Exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}


﻿namespace EngineAnalyzer
{
    partial class TFormLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TFormLogin));
            this.panelLogin = new System.Windows.Forms.Panel();
            this.editID = new ExTextBox.ExTextBox();
            this.editPW = new ExTextBox.ExTextBox();
            this.panelExit = new System.Windows.Forms.Panel();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // panelLogin
            // 
            this.panelLogin.BackColor = System.Drawing.Color.Transparent;
            this.panelLogin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panelLogin.Location = new System.Drawing.Point(1680, 494);
            this.panelLogin.Name = "panelLogin";
            this.panelLogin.Size = new System.Drawing.Size(25, 31);
            this.panelLogin.TabIndex = 11;
            this.panelLogin.Tag = "login";
            this.panelLogin.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panelLogin_MouseClick);
            this.panelLogin.MouseEnter += new System.EventHandler(this.panelLogin_MouseEnter);
            this.panelLogin.MouseLeave += new System.EventHandler(this.panelLogin_MouseLeave);
            // 
            // editID
            // 
            this.editID.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.editID.Hint = "ID";
            this.editID.Location = new System.Drawing.Point(1477, 470);
            this.editID.Name = "editID";
            this.editID.Size = new System.Drawing.Size(185, 14);
            this.editID.TabIndex = 12;
            this.editID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.editID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.editID_KeyPress);
            // 
            // editPW
            // 
            this.editPW.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.editPW.Hint = "Password";
            this.editPW.Location = new System.Drawing.Point(1477, 500);
            this.editPW.Name = "editPW";
            this.editPW.PasswordChar = '*';
            this.editPW.Size = new System.Drawing.Size(185, 14);
            this.editPW.TabIndex = 13;
            this.editPW.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.editPW.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.editPW_KeyPress);
            // 
            // panelExit
            // 
            this.panelExit.BackColor = System.Drawing.Color.Transparent;
            this.panelExit.Location = new System.Drawing.Point(1778, 12);
            this.panelExit.Name = "panelExit";
            this.panelExit.Size = new System.Drawing.Size(39, 30);
            this.panelExit.TabIndex = 14;
            this.panelExit.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panelExit_MouseClick);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(-200, 0);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(122, 21);
            this.textBox1.TabIndex = 0;
            // 
            // TFormLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1904, 1041);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.panelExit);
            this.Controls.Add(this.editPW);
            this.Controls.Add(this.editID);
            this.Controls.Add(this.panelLogin);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "TFormLogin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.TFormLogin_Load);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.TFormLogin_MouseClick);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.imageLoginWall_MouseDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panelLogin;
        private ExTextBox.ExTextBox editID;
        private ExTextBox.ExTextBox editPW;
        private System.Windows.Forms.Panel panelExit;
        private System.Windows.Forms.TextBox textBox1;
    }
}
﻿using DM;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;

namespace CubeEA
{
    public class TEAPacket
    {
        public byte CMD;
        public UInt16 UID;
        public UInt16 LEN;
        public byte[] Data;

        public MemoryStream DataStream = new MemoryStream();

        public int Timeout = 1000;

        public int Get(int AIdx)
        {
            int iAddr = AIdx;

            int iValue = BitConverter.ToUInt16(Data, iAddr * 2);

            return iValue;
        }
    }

    public class TCubeEASocketCmd : TCubeEASocket
    {
        public TCubeEASocketCmd()
        {

        }

        public Boolean DoReqCmd(byte ACMD, byte AUID, out TEAPacket AResult)
        {
            AResult = null;
            
            int PacketReqCount1M = 0;
            int PacketReqCount = 0;
            byte[] bPacketReq = new byte[4];
            int i = -1;

            i++; bPacketReq[i] = 0x04;
            i++; bPacketReq[i] = ACMD;
            i++; bPacketReq[i] = AUID;
            i++; bPacketReq[i] = 0x00;

            TCS.LogMsg("Tx -----> 4 Byte");
            TCS.LogMsg(String.Format("CMD={0:D}", ACMD));
            TCS.LogMsg(String.Format("UID={0:D}", AUID));
            TCS.LogMsg(String.Format("LEN={0:D}", 1));
            if (DoWrite(bPacketReq, 4))
            {
                if (DoRecv(out AResult))
                {
                    return true;
                }
            }
            PacketReqCount++;
            if (PacketReqCount > 999999)
            {
                PacketReqCount = 0;
                PacketReqCount1M++;
            }
            return false;
        }

        public Boolean DoRecv(out TEAPacket AResult)
        {
            TEAPacket eap = new TEAPacket();
            AResult = eap;

            try
            {
                byte[] bBuf = new byte[4096];
                int wLen = 6;
                int iGet = 0;
                {
                    Stopwatch swTO = new Stopwatch();
                    swTO.Restart();
                    while (iGet < wLen)
                    {
                        if (swTO.ElapsedMilliseconds >= 1000)
                        {
                            TCS.LogMsg("TIMEOUT HEADER");
                            return false;
                        }
                        int iRemain = wLen - iGet;
                        if (iRemain <= 0)
                            break;
                        int icnt = Socket.Receive(bBuf, iGet, iRemain, SocketFlags.None);
                        iGet += icnt;
                    }
                }

                if (iGet == wLen)
                {
                    //if (bBuf[0] == 0x01)
                    {
                        byte[] bBufData = new byte[4096];

                        eap.CMD = bBuf[1];
                        eap.UID = BitConverter.ToUInt16(bBuf, 2); // bBuf[2];

                        // 임시
                        //byte bTemp = bBuf[4];
                        //bBuf[4] = bBuf[5];
                        //bBuf[5] = bTemp;

                        eap.LEN = BitConverter.ToUInt16(bBuf, 4);

                        if (eap.LEN > 0)
                        {
                            wLen = eap.LEN;
                            iGet = 0;
                            {
                                Stopwatch swTO = new Stopwatch();
                                swTO.Restart();
                                while (iGet < wLen)
                                {
                                    if (swTO.ElapsedMilliseconds >= 1000)
                                    {
                                        TCS.LogMsg("TIMEOUT HEADER");
                                        return false;
                                    }
                                    int iRemain = wLen - iGet;
                                    if (iRemain <= 0)
                                        break;
                                    int icnt = Socket.Receive(bBufData, iGet, iRemain, SocketFlags.None);
                                    iGet += icnt;
                                }
                            }

                            if (eap.LEN == iGet)
                            {
                                eap.DataStream.Write(bBufData, 0, iGet);
                                eap.Data = eap.DataStream.ToArray();

                                return true;
                            }
                        }
                    }
                }
            }
            catch
            {

            }
            return false;
        }

        public Stopwatch sw500ms = new Stopwatch();

        //int iUIDIdx = 1;

        public Boolean FlagAICalData = false;
        public int FlagCmdParameter = 0;

        public override Boolean GetRAW()
        {
            Boolean bResult = false;

            if (sw500ms.IsRunning == false)
                sw500ms.Restart();

            try
            {
                if (sw500ms.ElapsedMilliseconds >= 500)
                {
                    sw500ms.Restart();

                    if (FlagAICalData)
                    {
                        TEAPacket eap;
                        if (DoReqCmd(1, 4, out eap)) // AI Data
                        {
                            TCS.EAPAICalLast = eap;

                            TCS.LogMsg("Rx <----- " + (6 + eap.LEN).ToString());
                            TCS.LogMsg(String.Format("CMD={0:D}", eap.CMD));
                            TCS.LogMsg(String.Format("UID={0:D}", eap.UID));
                            TCS.LogMsg(String.Format("LEN={0:D}", eap.LEN));
                        }
                    }
                    else if (FlagCmdParameter > 0)
                    {
                        TEAPacket eap;
                        if (DoReqCmd(1, (byte)FlagCmdParameter, out eap)) // AI Data
                        {
                            TCS.EAPAICalLast = eap;

                            TCS.LogMsg("Rx <----- " + (6 + eap.LEN).ToString());
                            TCS.LogMsg(String.Format("CMD={0:D}", eap.CMD));
                            TCS.LogMsg(String.Format("UID={0:D}", eap.UID));
                            TCS.LogMsg(String.Format("LEN={0:D}", eap.LEN));
                        }
                    }
                    //// 1,2,3
                    //iUIDIdx++;
                    //if (iUIDIdx >= 4)
                    //    iUIDIdx = 1;
                }
            }
            catch (Exception e)
            {
                var st = new StackTrace(e, true);
                // Get the top stack frame
                var frame = st.GetFrame(0);
                // Get the line number from the stack frame
                var line = frame.GetFileLineNumber();

                TCS.LogMsg(line.ToString() + " = " + e.Message);
                TCS.LogMsg(e.StackTrace);
                disconnect();
            }

            return bResult;
        }
    }
}

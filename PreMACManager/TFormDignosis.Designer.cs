﻿namespace EngineAnalyzer
{
    partial class TFormDignosis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TFormDignosis));
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.panelForm = new System.Windows.Forms.Panel();
            this.ProgressHistory = new System.Windows.Forms.ProgressBar();
            this.buttonExport = new System.Windows.Forms.Panel();
            this.buttonClear = new System.Windows.Forms.Panel();
            this.buttonExpression = new System.Windows.Forms.Panel();
            this.editAlarmFilter = new System.Windows.Forms.TextBox();
            this.checkAlarm = new System.Windows.Forms.PictureBox();
            this.checkOn = new System.Windows.Forms.PictureBox();
            this.panelGraphTDC = new System.Windows.Forms.Panel();
            this.panelGraphKnock = new System.Windows.Forms.Panel();
            this.chartKnock = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panelGraph = new System.Windows.Forms.Panel();
            this.panelGraphRight = new System.Windows.Forms.Panel();
            this.comboYMax = new System.Windows.Forms.Panel();
            this.comboYMin = new System.Windows.Forms.Panel();
            this.editGraphPYMax = new System.Windows.Forms.TextBox();
            this.editGraphPYMin = new System.Windows.Forms.TextBox();
            this.buttonGraphPAxisReset = new System.Windows.Forms.Panel();
            this._chart = new Arction.WinForms.Charting.LightningChartUltimate();
            this.panelPreview = new System.Windows.Forms.Panel();
            this.chartPreview = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.labelKnock = new System.Windows.Forms.Label();
            this.labelTDC = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panelGraphCalc = new System.Windows.Forms.Panel();
            this.panelGraphCalcLogginData = new System.Windows.Forms.Panel();
            this.comboCalcDataYMax = new System.Windows.Forms.Panel();
            this.comboCalcDataYMin = new System.Windows.Forms.Panel();
            this.comboCalcDataRPMMax = new System.Windows.Forms.Panel();
            this.comboCalcDataRPMYMin = new System.Windows.Forms.Panel();
            this.editCalcDataRPMYMax = new System.Windows.Forms.TextBox();
            this.editCalcDataRPMYMin = new System.Windows.Forms.TextBox();
            this.panelCalcDataYReset = new System.Windows.Forms.Panel();
            this.editCalcDataYMax = new System.Windows.Forms.TextBox();
            this.editCalcDataYMin = new System.Windows.Forms.TextBox();
            this.panelCalcDataRPMYReset = new System.Windows.Forms.Panel();
            this._chartCalcData = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this._chartCalcDataRPM = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.labelGraphCalc = new System.Windows.Forms.Label();
            this.labelGraph = new System.Windows.Forms.Label();
            this.buttonLoad = new System.Windows.Forms.Panel();
            this.editTimeEnd = new System.Windows.Forms.TextBox();
            this.editTimeBegin = new System.Windows.Forms.TextBox();
            this.imgBack = new System.Windows.Forms.PictureBox();
            this.comboY = new System.Windows.Forms.ListBox();
            this.editIP = new System.Windows.Forms.TextBox();
            this.imgCyl12 = new System.Windows.Forms.PictureBox();
            this.imgCyl11 = new System.Windows.Forms.PictureBox();
            this.imgCyl10 = new System.Windows.Forms.PictureBox();
            this.imgCyl1 = new System.Windows.Forms.PictureBox();
            this.imgCyl9 = new System.Windows.Forms.PictureBox();
            this.imgCyl2 = new System.Windows.Forms.PictureBox();
            this.imgCyl8 = new System.Windows.Forms.PictureBox();
            this.imgCyl3 = new System.Windows.Forms.PictureBox();
            this.imgCyl7 = new System.Windows.Forms.PictureBox();
            this.imgCyl4 = new System.Windows.Forms.PictureBox();
            this.imgCyl6 = new System.Windows.Forms.PictureBox();
            this.imgCyl5 = new System.Windows.Forms.PictureBox();
            this.lightningChartUltimate1 = new Arction.WinForms.Charting.LightningChartUltimate();
            this.DialogOpen = new System.Windows.Forms.OpenFileDialog();
            this.DialogSave = new System.Windows.Forms.SaveFileDialog();
            this.labelBottomMsg = new System.Windows.Forms.Label();
            this.labelAA1 = new System.Windows.Forms.Label();
            this.panelForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkAlarm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkOn)).BeginInit();
            this.panelGraphKnock.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartKnock)).BeginInit();
            this.panelGraph.SuspendLayout();
            this.panelGraphRight.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartPreview)).BeginInit();
            this.panelGraphCalc.SuspendLayout();
            this.panelGraphCalcLogginData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._chartCalcData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._chartCalcDataRPM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl5)).BeginInit();
            this.SuspendLayout();
            // 
            // panelForm
            // 
            this.panelForm.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panelForm.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelForm.BackgroundImage")));
            this.panelForm.Controls.Add(this.labelAA1);
            this.panelForm.Controls.Add(this.labelBottomMsg);
            this.panelForm.Controls.Add(this.ProgressHistory);
            this.panelForm.Controls.Add(this.buttonExport);
            this.panelForm.Controls.Add(this.buttonClear);
            this.panelForm.Controls.Add(this.buttonExpression);
            this.panelForm.Controls.Add(this.editAlarmFilter);
            this.panelForm.Controls.Add(this.checkAlarm);
            this.panelForm.Controls.Add(this.checkOn);
            this.panelForm.Controls.Add(this.panelGraphTDC);
            this.panelForm.Controls.Add(this.panelGraphKnock);
            this.panelForm.Controls.Add(this.panelGraph);
            this.panelForm.Controls.Add(this.panelPreview);
            this.panelForm.Controls.Add(this.chartPreview);
            this.panelForm.Controls.Add(this.labelKnock);
            this.panelForm.Controls.Add(this.labelTDC);
            this.panelForm.Controls.Add(this.label1);
            this.panelForm.Controls.Add(this.panelGraphCalc);
            this.panelForm.Controls.Add(this.labelGraphCalc);
            this.panelForm.Controls.Add(this.labelGraph);
            this.panelForm.Controls.Add(this.buttonLoad);
            this.panelForm.Controls.Add(this.editTimeEnd);
            this.panelForm.Controls.Add(this.editTimeBegin);
            this.panelForm.Controls.Add(this.imgBack);
            this.panelForm.Controls.Add(this.comboY);
            this.panelForm.Controls.Add(this.editIP);
            this.panelForm.Location = new System.Drawing.Point(0, 0);
            this.panelForm.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panelForm.Name = "panelForm";
            this.panelForm.Size = new System.Drawing.Size(1920, 1256);
            this.panelForm.TabIndex = 2;
            this.panelForm.Visible = false;
            // 
            // ProgressHistory
            // 
            this.ProgressHistory.Location = new System.Drawing.Point(1661, 397);
            this.ProgressHistory.Name = "ProgressHistory";
            this.ProgressHistory.Size = new System.Drawing.Size(179, 12);
            this.ProgressHistory.TabIndex = 61;
            this.ProgressHistory.Visible = false;
            // 
            // buttonExport
            // 
            this.buttonExport.BackColor = System.Drawing.Color.Transparent;
            this.buttonExport.Location = new System.Drawing.Point(1661, 409);
            this.buttonExport.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonExport.Name = "buttonExport";
            this.buttonExport.Size = new System.Drawing.Size(179, 39);
            this.buttonExport.TabIndex = 60;
            this.buttonExport.Click += new System.EventHandler(this.buttonExport_Click);
            // 
            // buttonClear
            // 
            this.buttonClear.BackColor = System.Drawing.Color.Transparent;
            this.buttonClear.Location = new System.Drawing.Point(1075, 349);
            this.buttonClear.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(120, 39);
            this.buttonClear.TabIndex = 59;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // buttonExpression
            // 
            this.buttonExpression.BackColor = System.Drawing.Color.Transparent;
            this.buttonExpression.Location = new System.Drawing.Point(953, 349);
            this.buttonExpression.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonExpression.Name = "buttonExpression";
            this.buttonExpression.Size = new System.Drawing.Size(120, 39);
            this.buttonExpression.TabIndex = 58;
            this.buttonExpression.Click += new System.EventHandler(this.buttonExpression_Click);
            // 
            // editAlarmFilter
            // 
            this.editAlarmFilter.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.editAlarmFilter.Font = new System.Drawing.Font("굴림", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.editAlarmFilter.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.editAlarmFilter.Location = new System.Drawing.Point(455, 364);
            this.editAlarmFilter.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.editAlarmFilter.Multiline = true;
            this.editAlarmFilter.Name = "editAlarmFilter";
            this.editAlarmFilter.Size = new System.Drawing.Size(488, 19);
            this.editAlarmFilter.TabIndex = 57;
            // 
            // checkAlarm
            // 
            this.checkAlarm.BackColor = System.Drawing.Color.Transparent;
            this.checkAlarm.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.checkAlarm.ErrorImage = null;
            this.checkAlarm.InitialImage = null;
            this.checkAlarm.Location = new System.Drawing.Point(298, 358);
            this.checkAlarm.Name = "checkAlarm";
            this.checkAlarm.Size = new System.Drawing.Size(87, 30);
            this.checkAlarm.TabIndex = 55;
            this.checkAlarm.TabStop = false;
            this.checkAlarm.Click += new System.EventHandler(this.checkAlarm_Click);
            // 
            // checkOn
            // 
            this.checkOn.BackColor = System.Drawing.Color.Transparent;
            this.checkOn.Image = ((System.Drawing.Image)(resources.GetObject("checkOn.Image")));
            this.checkOn.Location = new System.Drawing.Point(542, 51);
            this.checkOn.Name = "checkOn";
            this.checkOn.Size = new System.Drawing.Size(76, 29);
            this.checkOn.TabIndex = 56;
            this.checkOn.TabStop = false;
            this.checkOn.Visible = false;
            // 
            // panelGraphTDC
            // 
            this.panelGraphTDC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(244)))));
            this.panelGraphTDC.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelGraphTDC.BackgroundImage")));
            this.panelGraphTDC.Location = new System.Drawing.Point(80, 1452);
            this.panelGraphTDC.Name = "panelGraphTDC";
            this.panelGraphTDC.Size = new System.Drawing.Size(1761, 476);
            this.panelGraphTDC.TabIndex = 49;
            this.panelGraphTDC.Visible = false;
            this.panelGraphTDC.Paint += new System.Windows.Forms.PaintEventHandler(this.panelGraphTDC_Paint);
            // 
            // panelGraphKnock
            // 
            this.panelGraphKnock.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(244)))));
            this.panelGraphKnock.Controls.Add(this.chartKnock);
            this.panelGraphKnock.Location = new System.Drawing.Point(80, 452);
            this.panelGraphKnock.Name = "panelGraphKnock";
            this.panelGraphKnock.Size = new System.Drawing.Size(1761, 476);
            this.panelGraphKnock.TabIndex = 48;
            this.panelGraphKnock.Visible = false;
            this.panelGraphKnock.Paint += new System.Windows.Forms.PaintEventHandler(this.panelGraphKnock_Paint);
            this.panelGraphKnock.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panelGraphKnock_MouseDown);
            this.panelGraphKnock.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panelGraphKnock_MouseMove);
            this.panelGraphKnock.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panelGraphKnock_MouseUp);
            // 
            // chartKnock
            // 
            this.chartKnock.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(243)))), ((int)(((byte)(245)))));
            this.chartKnock.BorderlineWidth = 0;
            this.chartKnock.BorderSkin.BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            this.chartKnock.BorderSkin.PageColor = System.Drawing.Color.Empty;
            chartArea1.AxisX.IsLabelAutoFit = false;
            chartArea1.AxisX.IsMarginVisible = false;
            chartArea1.AxisX.LabelAutoFitMaxFontSize = 12;
            chartArea1.AxisX.LabelAutoFitMinFontSize = 12;
            chartArea1.AxisX.LabelStyle.Enabled = false;
            chartArea1.AxisX.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            chartArea1.AxisX.LabelStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            chartArea1.AxisX.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(191)))), ((int)(((byte)(193)))));
            chartArea1.AxisX.MajorGrid.Enabled = false;
            chartArea1.AxisX.MajorTickMark.Enabled = false;
            chartArea1.AxisX.TitleForeColor = System.Drawing.Color.DimGray;
            chartArea1.AxisX2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(191)))), ((int)(((byte)(193)))));
            chartArea1.AxisY.IsLabelAutoFit = false;
            chartArea1.AxisY.LabelStyle.Font = new System.Drawing.Font("HelveticaNeueLT Pro 67 MdCn", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            chartArea1.AxisY.LabelStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            chartArea1.AxisY.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(191)))), ((int)(((byte)(193)))));
            chartArea1.AxisY.MajorGrid.Enabled = false;
            chartArea1.AxisY.MajorTickMark.Enabled = false;
            chartArea1.AxisY2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(191)))), ((int)(((byte)(193)))));
            chartArea1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(243)))), ((int)(((byte)(245)))));
            chartArea1.InnerPlotPosition.Auto = false;
            chartArea1.InnerPlotPosition.Height = 98F;
            chartArea1.InnerPlotPosition.Width = 98F;
            chartArea1.InnerPlotPosition.X = 2F;
            chartArea1.InnerPlotPosition.Y = 2F;
            chartArea1.Name = "ChartArea1";
            chartArea1.Position.Auto = false;
            chartArea1.Position.Height = 94F;
            chartArea1.Position.Width = 96.5F;
            chartArea1.Position.X = 0.5F;
            chartArea1.Position.Y = 3F;
            this.chartKnock.ChartAreas.Add(chartArea1);
            this.chartKnock.Location = new System.Drawing.Point(122, 10);
            this.chartKnock.Margin = new System.Windows.Forms.Padding(0);
            this.chartKnock.Name = "chartKnock";
            this.chartKnock.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            series1.ChartArea = "ChartArea1";
            series1.Name = "SeriesRPMLogging";
            this.chartKnock.Series.Add(series1);
            this.chartKnock.Size = new System.Drawing.Size(1628, 456);
            this.chartKnock.TabIndex = 14;
            // 
            // panelGraph
            // 
            this.panelGraph.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(244)))));
            this.panelGraph.Controls.Add(this.panelGraphRight);
            this.panelGraph.Controls.Add(this._chart);
            this.panelGraph.ForeColor = System.Drawing.Color.Red;
            this.panelGraph.Location = new System.Drawing.Point(80, 1452);
            this.panelGraph.Name = "panelGraph";
            this.panelGraph.Size = new System.Drawing.Size(1761, 476);
            this.panelGraph.TabIndex = 40;
            this.panelGraph.Paint += new System.Windows.Forms.PaintEventHandler(this.panelGraph_Paint);
            this.panelGraph.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panelGraph_MouseDown);
            this.panelGraph.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panelGraph_MouseMove);
            this.panelGraph.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panelGraph_MouseUp);
            // 
            // panelGraphRight
            // 
            this.panelGraphRight.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelGraphRight.BackgroundImage")));
            this.panelGraphRight.Controls.Add(this.comboYMax);
            this.panelGraphRight.Controls.Add(this.comboYMin);
            this.panelGraphRight.Controls.Add(this.editGraphPYMax);
            this.panelGraphRight.Controls.Add(this.editGraphPYMin);
            this.panelGraphRight.Controls.Add(this.buttonGraphPAxisReset);
            this.panelGraphRight.Location = new System.Drawing.Point(1611, 3);
            this.panelGraphRight.Name = "panelGraphRight";
            this.panelGraphRight.Size = new System.Drawing.Size(144, 469);
            this.panelGraphRight.TabIndex = 12;
            // 
            // comboYMax
            // 
            this.comboYMax.BackColor = System.Drawing.Color.Transparent;
            this.comboYMax.Location = new System.Drawing.Point(71, 360);
            this.comboYMax.Name = "comboYMax";
            this.comboYMax.Size = new System.Drawing.Size(30, 30);
            this.comboYMax.TabIndex = 21;
            this.comboYMax.Click += new System.EventHandler(this.combo_Click);
            this.comboYMax.MouseEnter += new System.EventHandler(this.obj_MouseEnter);
            this.comboYMax.MouseLeave += new System.EventHandler(this.obj_MouseLeave);
            // 
            // comboYMin
            // 
            this.comboYMin.BackColor = System.Drawing.Color.Transparent;
            this.comboYMin.Location = new System.Drawing.Point(71, 308);
            this.comboYMin.Name = "comboYMin";
            this.comboYMin.Size = new System.Drawing.Size(30, 30);
            this.comboYMin.TabIndex = 20;
            this.comboYMin.Click += new System.EventHandler(this.combo_Click);
            this.comboYMin.MouseEnter += new System.EventHandler(this.obj_MouseEnter);
            this.comboYMin.MouseLeave += new System.EventHandler(this.obj_MouseLeave);
            // 
            // editGraphPYMax
            // 
            this.editGraphPYMax.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.editGraphPYMax.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.editGraphPYMax.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.editGraphPYMax.Location = new System.Drawing.Point(33, 371);
            this.editGraphPYMax.Multiline = true;
            this.editGraphPYMax.Name = "editGraphPYMax";
            this.editGraphPYMax.Size = new System.Drawing.Size(38, 17);
            this.editGraphPYMax.TabIndex = 13;
            this.editGraphPYMax.Text = "0";
            this.editGraphPYMax.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.editGraphPYMax.TextChanged += new System.EventHandler(this.editGraphPYMin_TextChanged);
            // 
            // editGraphPYMin
            // 
            this.editGraphPYMin.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.editGraphPYMin.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.editGraphPYMin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.editGraphPYMin.Location = new System.Drawing.Point(33, 319);
            this.editGraphPYMin.Multiline = true;
            this.editGraphPYMin.Name = "editGraphPYMin";
            this.editGraphPYMin.Size = new System.Drawing.Size(38, 17);
            this.editGraphPYMin.TabIndex = 12;
            this.editGraphPYMin.Text = "0";
            this.editGraphPYMin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.editGraphPYMin.TextChanged += new System.EventHandler(this.editGraphPYMin_TextChanged);
            // 
            // buttonGraphPAxisReset
            // 
            this.buttonGraphPAxisReset.BackColor = System.Drawing.Color.Transparent;
            this.buttonGraphPAxisReset.Location = new System.Drawing.Point(20, 73);
            this.buttonGraphPAxisReset.Name = "buttonGraphPAxisReset";
            this.buttonGraphPAxisReset.Size = new System.Drawing.Size(82, 70);
            this.buttonGraphPAxisReset.TabIndex = 10;
            this.buttonGraphPAxisReset.Click += new System.EventHandler(this.panelGraphPressReset_Click);
            // 
            // _chart
            // 
            this._chart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(243)))), ((int)(((byte)(245)))));
            this._chart.Background = ((Arction.WinForms.Charting.Fill)(resources.GetObject("_chart.Background")));
            this._chart.ChartManager = null;
            this._chart.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this._chart.HorizontalScrollBars = ((System.Collections.Generic.List<Arction.WinForms.Charting.HorizontalScrollBar>)(resources.GetObject("_chart.HorizontalScrollBars")));
            this._chart.Location = new System.Drawing.Point(100, 8);
            this._chart.Margin = new System.Windows.Forms.Padding(3, 8, 3, 8);
            this._chart.MinimumSize = new System.Drawing.Size(60, 79);
            this._chart.Name = "_chart";
            this._chart.Options = ((Arction.WinForms.Charting.ChartOptions)(resources.GetObject("_chart.Options")));
            this._chart.OutputStream = null;
            this._chart.RenderOptions = ((Arction.WinForms.Charting.Views.RenderOptionsCommon)(resources.GetObject("_chart.RenderOptions")));
            this._chart.Size = new System.Drawing.Size(1504, 458);
            this._chart.TabIndex = 8;
            this._chart.Title = ((Arction.WinForms.Charting.Titles.ChartTitle)(resources.GetObject("_chart.Title")));
            this._chart.VerticalScrollBars = ((System.Collections.Generic.List<Arction.WinForms.Charting.VerticalScrollBar>)(resources.GetObject("_chart.VerticalScrollBars")));
            this._chart.View3D = ((Arction.WinForms.Charting.Views.View3D.View3D)(resources.GetObject("_chart.View3D")));
            this._chart.ViewPie3D = ((Arction.WinForms.Charting.Views.ViewPie3D.ViewPie3D)(resources.GetObject("_chart.ViewPie3D")));
            this._chart.ViewPolar = ((Arction.WinForms.Charting.Views.ViewPolar.ViewPolar)(resources.GetObject("_chart.ViewPolar")));
            this._chart.ViewSmith = ((Arction.WinForms.Charting.Views.ViewSmith.ViewSmith)(resources.GetObject("_chart.ViewSmith")));
            this._chart.ViewXY = ((Arction.WinForms.Charting.Views.ViewXY.ViewXY)(resources.GetObject("_chart.ViewXY")));
            // 
            // panelPreview
            // 
            this.panelPreview.BackColor = System.Drawing.Color.Transparent;
            this.panelPreview.Location = new System.Drawing.Point(82, 294);
            this.panelPreview.Name = "panelPreview";
            this.panelPreview.Size = new System.Drawing.Size(1758, 36);
            this.panelPreview.TabIndex = 46;
            this.panelPreview.Paint += new System.Windows.Forms.PaintEventHandler(this.panelPreview_Paint);
            // 
            // chartPreview
            // 
            this.chartPreview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(243)))), ((int)(((byte)(245)))));
            this.chartPreview.BorderlineWidth = 0;
            this.chartPreview.BorderSkin.BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            this.chartPreview.BorderSkin.PageColor = System.Drawing.Color.Empty;
            chartArea2.AxisX.IsLabelAutoFit = false;
            chartArea2.AxisX.IsMarginVisible = false;
            chartArea2.AxisX.LabelAutoFitMaxFontSize = 12;
            chartArea2.AxisX.LabelAutoFitMinFontSize = 12;
            chartArea2.AxisX.LabelStyle.Enabled = false;
            chartArea2.AxisX.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            chartArea2.AxisX.LabelStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            chartArea2.AxisX.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(191)))), ((int)(((byte)(193)))));
            chartArea2.AxisX.MajorGrid.Enabled = false;
            chartArea2.AxisX.MajorTickMark.Enabled = false;
            chartArea2.AxisX.TitleForeColor = System.Drawing.Color.DimGray;
            chartArea2.AxisX2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(191)))), ((int)(((byte)(193)))));
            chartArea2.AxisY.IsLabelAutoFit = false;
            chartArea2.AxisY.LabelStyle.Font = new System.Drawing.Font("HelveticaNeueLT Pro 67 MdCn", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            chartArea2.AxisY.LabelStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            chartArea2.AxisY.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(191)))), ((int)(((byte)(193)))));
            chartArea2.AxisY.MajorGrid.Enabled = false;
            chartArea2.AxisY.MajorTickMark.Enabled = false;
            chartArea2.AxisY2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(191)))), ((int)(((byte)(193)))));
            chartArea2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(243)))), ((int)(((byte)(245)))));
            chartArea2.InnerPlotPosition.Auto = false;
            chartArea2.InnerPlotPosition.Height = 98F;
            chartArea2.InnerPlotPosition.Width = 98F;
            chartArea2.InnerPlotPosition.X = 2F;
            chartArea2.InnerPlotPosition.Y = 2F;
            chartArea2.Name = "ChartArea1";
            chartArea2.Position.Auto = false;
            chartArea2.Position.Height = 94F;
            chartArea2.Position.Width = 96.5F;
            chartArea2.Position.X = 0.5F;
            chartArea2.Position.Y = 3F;
            this.chartPreview.ChartAreas.Add(chartArea2);
            this.chartPreview.Location = new System.Drawing.Point(82, 136);
            this.chartPreview.Margin = new System.Windows.Forms.Padding(0);
            this.chartPreview.Name = "chartPreview";
            this.chartPreview.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            this.chartPreview.Size = new System.Drawing.Size(1755, 155);
            this.chartPreview.TabIndex = 45;
            this.chartPreview.Paint += new System.Windows.Forms.PaintEventHandler(this.chartPreview_Paint);
            this.chartPreview.MouseDown += new System.Windows.Forms.MouseEventHandler(this.chartPreview_MouseDown);
            this.chartPreview.MouseLeave += new System.EventHandler(this.chartPreview_MouseLeave);
            this.chartPreview.MouseMove += new System.Windows.Forms.MouseEventHandler(this.chartPreview_MouseMove);
            this.chartPreview.MouseUp += new System.Windows.Forms.MouseEventHandler(this.chartPreview_MouseUp);
            // 
            // labelKnock
            // 
            this.labelKnock.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(198)))), ((int)(((byte)(201)))));
            this.labelKnock.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.labelKnock.ForeColor = System.Drawing.Color.White;
            this.labelKnock.Location = new System.Drawing.Point(739, 412);
            this.labelKnock.Name = "labelKnock";
            this.labelKnock.Size = new System.Drawing.Size(220, 39);
            this.labelKnock.TabIndex = 44;
            this.labelKnock.Text = "Knock Calibration";
            this.labelKnock.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelKnock.MouseDown += new System.Windows.Forms.MouseEventHandler(this.label_MouseDown);
            this.labelKnock.MouseEnter += new System.EventHandler(this.label_MouseEnter);
            this.labelKnock.MouseLeave += new System.EventHandler(this.label_MouseLeave);
            this.labelKnock.MouseUp += new System.Windows.Forms.MouseEventHandler(this.label_MouseUp);
            // 
            // labelTDC
            // 
            this.labelTDC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(198)))), ((int)(((byte)(201)))));
            this.labelTDC.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.labelTDC.ForeColor = System.Drawing.Color.White;
            this.labelTDC.Location = new System.Drawing.Point(519, 412);
            this.labelTDC.Name = "labelTDC";
            this.labelTDC.Size = new System.Drawing.Size(220, 39);
            this.labelTDC.TabIndex = 43;
            this.labelTDC.Text = "TDC Calibration";
            this.labelTDC.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelTDC.MouseDown += new System.Windows.Forms.MouseEventHandler(this.label_MouseDown);
            this.labelTDC.MouseEnter += new System.EventHandler(this.label_MouseEnter);
            this.labelTDC.MouseLeave += new System.EventHandler(this.label_MouseLeave);
            this.labelTDC.MouseUp += new System.Windows.Forms.MouseEventHandler(this.label_MouseUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 386);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 15);
            this.label1.TabIndex = 42;
            this.label1.Text = "80,452";
            this.label1.Visible = false;
            // 
            // panelGraphCalc
            // 
            this.panelGraphCalc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(244)))));
            this.panelGraphCalc.Controls.Add(this.panelGraphCalcLogginData);
            this.panelGraphCalc.Controls.Add(this._chartCalcData);
            this.panelGraphCalc.Controls.Add(this._chartCalcDataRPM);
            this.panelGraphCalc.Location = new System.Drawing.Point(80, 452);
            this.panelGraphCalc.Name = "panelGraphCalc";
            this.panelGraphCalc.Size = new System.Drawing.Size(1761, 476);
            this.panelGraphCalc.TabIndex = 41;
            this.panelGraphCalc.Visible = false;
            this.panelGraphCalc.Paint += new System.Windows.Forms.PaintEventHandler(this.panelGraphCalc_Paint);
            this.panelGraphCalc.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panelGraphCalc_MouseDown);
            this.panelGraphCalc.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panelGraphCalc_MouseMove);
            this.panelGraphCalc.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panelGraphCalc_MouseUp);
            // 
            // panelGraphCalcLogginData
            // 
            this.panelGraphCalcLogginData.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelGraphCalcLogginData.BackgroundImage")));
            this.panelGraphCalcLogginData.Controls.Add(this.comboCalcDataYMax);
            this.panelGraphCalcLogginData.Controls.Add(this.comboCalcDataYMin);
            this.panelGraphCalcLogginData.Controls.Add(this.comboCalcDataRPMMax);
            this.panelGraphCalcLogginData.Controls.Add(this.comboCalcDataRPMYMin);
            this.panelGraphCalcLogginData.Controls.Add(this.editCalcDataRPMYMax);
            this.panelGraphCalcLogginData.Controls.Add(this.editCalcDataRPMYMin);
            this.panelGraphCalcLogginData.Controls.Add(this.panelCalcDataYReset);
            this.panelGraphCalcLogginData.Controls.Add(this.editCalcDataYMax);
            this.panelGraphCalcLogginData.Controls.Add(this.editCalcDataYMin);
            this.panelGraphCalcLogginData.Controls.Add(this.panelCalcDataRPMYReset);
            this.panelGraphCalcLogginData.Location = new System.Drawing.Point(1611, 3);
            this.panelGraphCalcLogginData.Name = "panelGraphCalcLogginData";
            this.panelGraphCalcLogginData.Size = new System.Drawing.Size(144, 469);
            this.panelGraphCalcLogginData.TabIndex = 12;
            // 
            // comboCalcDataYMax
            // 
            this.comboCalcDataYMax.BackColor = System.Drawing.Color.Transparent;
            this.comboCalcDataYMax.Location = new System.Drawing.Point(90, 400);
            this.comboCalcDataYMax.Name = "comboCalcDataYMax";
            this.comboCalcDataYMax.Size = new System.Drawing.Size(30, 30);
            this.comboCalcDataYMax.TabIndex = 19;
            this.comboCalcDataYMax.Click += new System.EventHandler(this.combo_Click);
            this.comboCalcDataYMax.MouseEnter += new System.EventHandler(this.obj_MouseEnter);
            this.comboCalcDataYMax.MouseLeave += new System.EventHandler(this.obj_MouseLeave);
            // 
            // comboCalcDataYMin
            // 
            this.comboCalcDataYMin.BackColor = System.Drawing.Color.Transparent;
            this.comboCalcDataYMin.Location = new System.Drawing.Point(90, 348);
            this.comboCalcDataYMin.Name = "comboCalcDataYMin";
            this.comboCalcDataYMin.Size = new System.Drawing.Size(30, 30);
            this.comboCalcDataYMin.TabIndex = 18;
            this.comboCalcDataYMin.Click += new System.EventHandler(this.combo_Click);
            this.comboCalcDataYMin.MouseEnter += new System.EventHandler(this.obj_MouseEnter);
            this.comboCalcDataYMin.MouseLeave += new System.EventHandler(this.obj_MouseLeave);
            // 
            // comboCalcDataRPMMax
            // 
            this.comboCalcDataRPMMax.BackColor = System.Drawing.Color.Transparent;
            this.comboCalcDataRPMMax.Location = new System.Drawing.Point(90, 113);
            this.comboCalcDataRPMMax.Name = "comboCalcDataRPMMax";
            this.comboCalcDataRPMMax.Size = new System.Drawing.Size(30, 30);
            this.comboCalcDataRPMMax.TabIndex = 17;
            this.comboCalcDataRPMMax.Click += new System.EventHandler(this.combo_Click);
            this.comboCalcDataRPMMax.MouseEnter += new System.EventHandler(this.obj_MouseEnter);
            this.comboCalcDataRPMMax.MouseLeave += new System.EventHandler(this.obj_MouseLeave);
            // 
            // comboCalcDataRPMYMin
            // 
            this.comboCalcDataRPMYMin.BackColor = System.Drawing.Color.Transparent;
            this.comboCalcDataRPMYMin.Location = new System.Drawing.Point(90, 61);
            this.comboCalcDataRPMYMin.Name = "comboCalcDataRPMYMin";
            this.comboCalcDataRPMYMin.Size = new System.Drawing.Size(30, 30);
            this.comboCalcDataRPMYMin.TabIndex = 16;
            this.comboCalcDataRPMYMin.Click += new System.EventHandler(this.combo_Click);
            this.comboCalcDataRPMYMin.MouseEnter += new System.EventHandler(this.obj_MouseEnter);
            this.comboCalcDataRPMYMin.MouseLeave += new System.EventHandler(this.obj_MouseLeave);
            // 
            // editCalcDataRPMYMax
            // 
            this.editCalcDataRPMYMax.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.editCalcDataRPMYMax.Font = new System.Drawing.Font("맑은 고딕", 10F);
            this.editCalcDataRPMYMax.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.editCalcDataRPMYMax.Location = new System.Drawing.Point(52, 120);
            this.editCalcDataRPMYMax.Multiline = true;
            this.editCalcDataRPMYMax.Name = "editCalcDataRPMYMax";
            this.editCalcDataRPMYMax.Size = new System.Drawing.Size(38, 17);
            this.editCalcDataRPMYMax.TabIndex = 15;
            this.editCalcDataRPMYMax.Text = "0";
            this.editCalcDataRPMYMax.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.editCalcDataRPMYMax.TextChanged += new System.EventHandler(this.editCalcDataRPMYMin_TextChanged);
            // 
            // editCalcDataRPMYMin
            // 
            this.editCalcDataRPMYMin.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.editCalcDataRPMYMin.Font = new System.Drawing.Font("맑은 고딕", 10F);
            this.editCalcDataRPMYMin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.editCalcDataRPMYMin.Location = new System.Drawing.Point(52, 68);
            this.editCalcDataRPMYMin.Multiline = true;
            this.editCalcDataRPMYMin.Name = "editCalcDataRPMYMin";
            this.editCalcDataRPMYMin.Size = new System.Drawing.Size(38, 17);
            this.editCalcDataRPMYMin.TabIndex = 14;
            this.editCalcDataRPMYMin.Text = "0";
            this.editCalcDataRPMYMin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.editCalcDataRPMYMin.TextChanged += new System.EventHandler(this.editCalcDataRPMYMin_TextChanged);
            // 
            // panelCalcDataYReset
            // 
            this.panelCalcDataYReset.BackColor = System.Drawing.Color.Transparent;
            this.panelCalcDataYReset.Location = new System.Drawing.Point(1, 306);
            this.panelCalcDataYReset.Name = "panelCalcDataYReset";
            this.panelCalcDataYReset.Size = new System.Drawing.Size(45, 81);
            this.panelCalcDataYReset.TabIndex = 11;
            this.panelCalcDataYReset.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panelCalcDataYReset_MouseClick);
            // 
            // editCalcDataYMax
            // 
            this.editCalcDataYMax.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.editCalcDataYMax.Font = new System.Drawing.Font("맑은 고딕", 10F);
            this.editCalcDataYMax.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.editCalcDataYMax.Location = new System.Drawing.Point(52, 407);
            this.editCalcDataYMax.Multiline = true;
            this.editCalcDataYMax.Name = "editCalcDataYMax";
            this.editCalcDataYMax.Size = new System.Drawing.Size(38, 17);
            this.editCalcDataYMax.TabIndex = 13;
            this.editCalcDataYMax.Text = "0";
            this.editCalcDataYMax.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.editCalcDataYMax.TextChanged += new System.EventHandler(this.editCalcDataYMin_TextChanged);
            // 
            // editCalcDataYMin
            // 
            this.editCalcDataYMin.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.editCalcDataYMin.Font = new System.Drawing.Font("맑은 고딕", 10F);
            this.editCalcDataYMin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.editCalcDataYMin.Location = new System.Drawing.Point(52, 355);
            this.editCalcDataYMin.Multiline = true;
            this.editCalcDataYMin.Name = "editCalcDataYMin";
            this.editCalcDataYMin.Size = new System.Drawing.Size(38, 17);
            this.editCalcDataYMin.TabIndex = 12;
            this.editCalcDataYMin.Text = "0";
            this.editCalcDataYMin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.editCalcDataYMin.TextChanged += new System.EventHandler(this.editCalcDataYMin_TextChanged);
            // 
            // panelCalcDataRPMYReset
            // 
            this.panelCalcDataRPMYReset.BackColor = System.Drawing.Color.Transparent;
            this.panelCalcDataRPMYReset.Location = new System.Drawing.Point(0, 19);
            this.panelCalcDataRPMYReset.Name = "panelCalcDataRPMYReset";
            this.panelCalcDataRPMYReset.Size = new System.Drawing.Size(45, 81);
            this.panelCalcDataRPMYReset.TabIndex = 10;
            this.panelCalcDataRPMYReset.Click += new System.EventHandler(this.panelCalcDataRPMYReset_Click);
            // 
            // _chartCalcData
            // 
            this._chartCalcData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(243)))), ((int)(((byte)(245)))));
            this._chartCalcData.BorderlineWidth = 0;
            this._chartCalcData.BorderSkin.BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            this._chartCalcData.BorderSkin.PageColor = System.Drawing.Color.Empty;
            chartArea3.AxisX.IsLabelAutoFit = false;
            chartArea3.AxisX.IsMarginVisible = false;
            chartArea3.AxisX.LabelAutoFitMaxFontSize = 12;
            chartArea3.AxisX.LabelAutoFitMinFontSize = 12;
            chartArea3.AxisX.LabelStyle.Enabled = false;
            chartArea3.AxisX.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            chartArea3.AxisX.LabelStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            chartArea3.AxisX.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(191)))), ((int)(((byte)(193)))));
            chartArea3.AxisX.MajorGrid.Enabled = false;
            chartArea3.AxisX.MajorTickMark.Enabled = false;
            chartArea3.AxisX.TitleForeColor = System.Drawing.Color.DimGray;
            chartArea3.AxisX2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(191)))), ((int)(((byte)(193)))));
            chartArea3.AxisY.IsLabelAutoFit = false;
            chartArea3.AxisY.LabelStyle.Font = new System.Drawing.Font("HelveticaNeueLT Pro 67 MdCn", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            chartArea3.AxisY.LabelStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            chartArea3.AxisY.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(191)))), ((int)(((byte)(193)))));
            chartArea3.AxisY.MajorGrid.Enabled = false;
            chartArea3.AxisY.MajorTickMark.Enabled = false;
            chartArea3.AxisY2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(191)))), ((int)(((byte)(193)))));
            chartArea3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(243)))), ((int)(((byte)(245)))));
            chartArea3.InnerPlotPosition.Auto = false;
            chartArea3.InnerPlotPosition.Height = 98F;
            chartArea3.InnerPlotPosition.Width = 98F;
            chartArea3.InnerPlotPosition.X = 2F;
            chartArea3.InnerPlotPosition.Y = 2F;
            chartArea3.Name = "ChartArea1";
            chartArea3.Position.Auto = false;
            chartArea3.Position.Height = 94F;
            chartArea3.Position.Width = 96.5F;
            chartArea3.Position.X = 0.5F;
            chartArea3.Position.Y = 3F;
            this._chartCalcData.ChartAreas.Add(chartArea3);
            this._chartCalcData.Location = new System.Drawing.Point(122, 215);
            this._chartCalcData.Margin = new System.Windows.Forms.Padding(0);
            this._chartCalcData.Name = "_chartCalcData";
            this._chartCalcData.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            this._chartCalcData.Size = new System.Drawing.Size(1515, 226);
            this._chartCalcData.TabIndex = 13;
            // 
            // _chartCalcDataRPM
            // 
            this._chartCalcDataRPM.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(243)))), ((int)(((byte)(245)))));
            this._chartCalcDataRPM.BorderlineWidth = 0;
            this._chartCalcDataRPM.BorderSkin.BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            this._chartCalcDataRPM.BorderSkin.PageColor = System.Drawing.Color.Empty;
            chartArea4.AxisX.IsLabelAutoFit = false;
            chartArea4.AxisX.IsMarginVisible = false;
            chartArea4.AxisX.LabelAutoFitMaxFontSize = 12;
            chartArea4.AxisX.LabelAutoFitMinFontSize = 12;
            chartArea4.AxisX.LabelStyle.Enabled = false;
            chartArea4.AxisX.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            chartArea4.AxisX.LabelStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            chartArea4.AxisX.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(191)))), ((int)(((byte)(193)))));
            chartArea4.AxisX.MajorGrid.Enabled = false;
            chartArea4.AxisX.MajorTickMark.Enabled = false;
            chartArea4.AxisX.TitleForeColor = System.Drawing.Color.DimGray;
            chartArea4.AxisX2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(191)))), ((int)(((byte)(193)))));
            chartArea4.AxisY.IsLabelAutoFit = false;
            chartArea4.AxisY.LabelStyle.Font = new System.Drawing.Font("HelveticaNeueLT Pro 67 MdCn", 10F);
            chartArea4.AxisY.LabelStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            chartArea4.AxisY.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(191)))), ((int)(((byte)(193)))));
            chartArea4.AxisY.MajorGrid.Enabled = false;
            chartArea4.AxisY.MajorTickMark.Enabled = false;
            chartArea4.AxisY2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(191)))), ((int)(((byte)(193)))));
            chartArea4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(243)))), ((int)(((byte)(245)))));
            chartArea4.InnerPlotPosition.Auto = false;
            chartArea4.InnerPlotPosition.Height = 98F;
            chartArea4.InnerPlotPosition.Width = 98F;
            chartArea4.InnerPlotPosition.X = 2F;
            chartArea4.InnerPlotPosition.Y = 2F;
            chartArea4.Name = "ChartArea1";
            chartArea4.Position.Auto = false;
            chartArea4.Position.Height = 94F;
            chartArea4.Position.Width = 96.5F;
            chartArea4.Position.X = 0.5F;
            chartArea4.Position.Y = 3F;
            this._chartCalcDataRPM.ChartAreas.Add(chartArea4);
            this._chartCalcDataRPM.Location = new System.Drawing.Point(122, 50);
            this._chartCalcDataRPM.Margin = new System.Windows.Forms.Padding(0);
            this._chartCalcDataRPM.Name = "_chartCalcDataRPM";
            this._chartCalcDataRPM.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            series2.Name = "SeriesRPMLogging";
            this._chartCalcDataRPM.Series.Add(series2);
            this._chartCalcDataRPM.Size = new System.Drawing.Size(1515, 106);
            this._chartCalcDataRPM.TabIndex = 3;
            // 
            // labelGraphCalc
            // 
            this.labelGraphCalc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(198)))), ((int)(((byte)(201)))));
            this.labelGraphCalc.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.labelGraphCalc.ForeColor = System.Drawing.Color.White;
            this.labelGraphCalc.Location = new System.Drawing.Point(299, 412);
            this.labelGraphCalc.Name = "labelGraphCalc";
            this.labelGraphCalc.Size = new System.Drawing.Size(220, 39);
            this.labelGraphCalc.TabIndex = 39;
            this.labelGraphCalc.Text = "Calculated Logging Data";
            this.labelGraphCalc.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelGraphCalc.MouseDown += new System.Windows.Forms.MouseEventHandler(this.label_MouseDown);
            this.labelGraphCalc.MouseEnter += new System.EventHandler(this.label_MouseEnter);
            this.labelGraphCalc.MouseLeave += new System.EventHandler(this.label_MouseLeave);
            this.labelGraphCalc.MouseUp += new System.Windows.Forms.MouseEventHandler(this.label_MouseUp);
            // 
            // labelGraph
            // 
            this.labelGraph.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.labelGraph.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.labelGraph.ForeColor = System.Drawing.Color.White;
            this.labelGraph.Location = new System.Drawing.Point(79, 412);
            this.labelGraph.Name = "labelGraph";
            this.labelGraph.Size = new System.Drawing.Size(220, 39);
            this.labelGraph.TabIndex = 38;
            this.labelGraph.Text = "Cyl. Pressure Data";
            this.labelGraph.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelGraph.MouseDown += new System.Windows.Forms.MouseEventHandler(this.label_MouseDown);
            this.labelGraph.MouseEnter += new System.EventHandler(this.label_MouseEnter);
            this.labelGraph.MouseLeave += new System.EventHandler(this.label_MouseLeave);
            this.labelGraph.MouseUp += new System.Windows.Forms.MouseEventHandler(this.label_MouseUp);
            // 
            // buttonLoad
            // 
            this.buttonLoad.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(230)))), ((int)(((byte)(233)))));
            this.buttonLoad.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonLoad.BackgroundImage")));
            this.buttonLoad.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.buttonLoad.Location = new System.Drawing.Point(436, 65);
            this.buttonLoad.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonLoad.Name = "buttonLoad";
            this.buttonLoad.Size = new System.Drawing.Size(52, 59);
            this.buttonLoad.TabIndex = 37;
            this.buttonLoad.Tag = "confirm";
            this.buttonLoad.Click += new System.EventHandler(this.buttonLoad_Click);
            // 
            // editTimeEnd
            // 
            this.editTimeEnd.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.editTimeEnd.Font = new System.Drawing.Font("굴림", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.editTimeEnd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.editTimeEnd.Location = new System.Drawing.Point(322, 91);
            this.editTimeEnd.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.editTimeEnd.Multiline = true;
            this.editTimeEnd.Name = "editTimeEnd";
            this.editTimeEnd.Size = new System.Drawing.Size(108, 19);
            this.editTimeEnd.TabIndex = 36;
            this.editTimeEnd.Text = "2018-01-01 00:00";
            this.editTimeEnd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.editTimeEnd.Click += new System.EventHandler(this.editTimeEnd_Click);
            // 
            // editTimeBegin
            // 
            this.editTimeBegin.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.editTimeBegin.Font = new System.Drawing.Font("굴림", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.editTimeBegin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.editTimeBegin.Location = new System.Drawing.Point(182, 91);
            this.editTimeBegin.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.editTimeBegin.Multiline = true;
            this.editTimeBegin.Name = "editTimeBegin";
            this.editTimeBegin.Size = new System.Drawing.Size(108, 19);
            this.editTimeBegin.TabIndex = 35;
            this.editTimeBegin.Text = "2018-01-01 00:00";
            this.editTimeBegin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.editTimeBegin.Click += new System.EventHandler(this.editTimeBegin_Click);
            // 
            // imgBack
            // 
            this.imgBack.Image = ((System.Drawing.Image)(resources.GetObject("imgBack.Image")));
            this.imgBack.Location = new System.Drawing.Point(27, 15);
            this.imgBack.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.imgBack.Name = "imgBack";
            this.imgBack.Size = new System.Drawing.Size(60, 29);
            this.imgBack.TabIndex = 34;
            this.imgBack.TabStop = false;
            this.imgBack.Visible = false;
            // 
            // comboY
            // 
            this.comboY.FormattingEnabled = true;
            this.comboY.ItemHeight = 15;
            this.comboY.Items.AddRange(new object[] {
            "1",
            "2",
            "3"});
            this.comboY.Location = new System.Drawing.Point(542, 142);
            this.comboY.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.comboY.Name = "comboY";
            this.comboY.Size = new System.Drawing.Size(69, 49);
            this.comboY.TabIndex = 33;
            this.comboY.Visible = false;
            // 
            // editIP
            // 
            this.editIP.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.editIP.Font = new System.Drawing.Font("굴림", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.editIP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.editIP.Location = new System.Drawing.Point(542, 159);
            this.editIP.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.editIP.Multiline = true;
            this.editIP.Name = "editIP";
            this.editIP.Size = new System.Drawing.Size(108, 19);
            this.editIP.TabIndex = 47;
            this.editIP.Text = "2018-01-01 00:00";
            this.editIP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // imgCyl12
            // 
            this.imgCyl12.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl12.Location = new System.Drawing.Point(218, 218);
            this.imgCyl12.Name = "imgCyl12";
            this.imgCyl12.Size = new System.Drawing.Size(71, 29);
            this.imgCyl12.TabIndex = 28;
            this.imgCyl12.TabStop = false;
            // 
            // imgCyl11
            // 
            this.imgCyl11.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl11.Location = new System.Drawing.Point(139, 218);
            this.imgCyl11.Name = "imgCyl11";
            this.imgCyl11.Size = new System.Drawing.Size(71, 29);
            this.imgCyl11.TabIndex = 27;
            this.imgCyl11.TabStop = false;
            // 
            // imgCyl10
            // 
            this.imgCyl10.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl10.Location = new System.Drawing.Point(57, 218);
            this.imgCyl10.Name = "imgCyl10";
            this.imgCyl10.Size = new System.Drawing.Size(71, 29);
            this.imgCyl10.TabIndex = 26;
            this.imgCyl10.TabStop = false;
            // 
            // imgCyl1
            // 
            this.imgCyl1.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl1.Image = ((System.Drawing.Image)(resources.GetObject("imgCyl1.Image")));
            this.imgCyl1.Location = new System.Drawing.Point(57, 116);
            this.imgCyl1.Name = "imgCyl1";
            this.imgCyl1.Size = new System.Drawing.Size(71, 29);
            this.imgCyl1.TabIndex = 17;
            this.imgCyl1.TabStop = false;
            // 
            // imgCyl9
            // 
            this.imgCyl9.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl9.Location = new System.Drawing.Point(218, 184);
            this.imgCyl9.Name = "imgCyl9";
            this.imgCyl9.Size = new System.Drawing.Size(71, 29);
            this.imgCyl9.TabIndex = 25;
            this.imgCyl9.TabStop = false;
            // 
            // imgCyl2
            // 
            this.imgCyl2.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl2.Location = new System.Drawing.Point(139, 116);
            this.imgCyl2.Name = "imgCyl2";
            this.imgCyl2.Size = new System.Drawing.Size(71, 29);
            this.imgCyl2.TabIndex = 18;
            this.imgCyl2.TabStop = false;
            // 
            // imgCyl8
            // 
            this.imgCyl8.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl8.Location = new System.Drawing.Point(139, 184);
            this.imgCyl8.Name = "imgCyl8";
            this.imgCyl8.Size = new System.Drawing.Size(71, 29);
            this.imgCyl8.TabIndex = 24;
            this.imgCyl8.TabStop = false;
            // 
            // imgCyl3
            // 
            this.imgCyl3.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl3.Location = new System.Drawing.Point(218, 116);
            this.imgCyl3.Name = "imgCyl3";
            this.imgCyl3.Size = new System.Drawing.Size(71, 29);
            this.imgCyl3.TabIndex = 19;
            this.imgCyl3.TabStop = false;
            // 
            // imgCyl7
            // 
            this.imgCyl7.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl7.Location = new System.Drawing.Point(57, 184);
            this.imgCyl7.Name = "imgCyl7";
            this.imgCyl7.Size = new System.Drawing.Size(71, 29);
            this.imgCyl7.TabIndex = 23;
            this.imgCyl7.TabStop = false;
            // 
            // imgCyl4
            // 
            this.imgCyl4.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl4.Location = new System.Drawing.Point(57, 150);
            this.imgCyl4.Name = "imgCyl4";
            this.imgCyl4.Size = new System.Drawing.Size(71, 29);
            this.imgCyl4.TabIndex = 20;
            this.imgCyl4.TabStop = false;
            // 
            // imgCyl6
            // 
            this.imgCyl6.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl6.Location = new System.Drawing.Point(218, 150);
            this.imgCyl6.Name = "imgCyl6";
            this.imgCyl6.Size = new System.Drawing.Size(71, 29);
            this.imgCyl6.TabIndex = 22;
            this.imgCyl6.TabStop = false;
            // 
            // imgCyl5
            // 
            this.imgCyl5.BackColor = System.Drawing.Color.Transparent;
            this.imgCyl5.Location = new System.Drawing.Point(139, 150);
            this.imgCyl5.Name = "imgCyl5";
            this.imgCyl5.Size = new System.Drawing.Size(71, 29);
            this.imgCyl5.TabIndex = 21;
            this.imgCyl5.TabStop = false;
            // 
            // lightningChartUltimate1
            // 
            this.lightningChartUltimate1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(243)))), ((int)(((byte)(245)))));
            this.lightningChartUltimate1.Background = ((Arction.WinForms.Charting.Fill)(resources.GetObject("lightningChartUltimate1.Background")));
            this.lightningChartUltimate1.ChartManager = null;
            this.lightningChartUltimate1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(149)))), ((int)(((byte)(153)))), ((int)(((byte)(155)))));
            this.lightningChartUltimate1.HorizontalScrollBars = ((System.Collections.Generic.List<Arction.WinForms.Charting.HorizontalScrollBar>)(resources.GetObject("lightningChartUltimate1.HorizontalScrollBars")));
            this.lightningChartUltimate1.Location = new System.Drawing.Point(106, 8);
            this.lightningChartUltimate1.Margin = new System.Windows.Forms.Padding(3, 8, 3, 8);
            this.lightningChartUltimate1.MinimumSize = new System.Drawing.Size(110, 219);
            this.lightningChartUltimate1.Name = "lightningChartUltimate1";
            this.lightningChartUltimate1.Options = ((Arction.WinForms.Charting.ChartOptions)(resources.GetObject("lightningChartUltimate1.Options")));
            this.lightningChartUltimate1.OutputStream = null;
            this.lightningChartUltimate1.RenderOptions = ((Arction.WinForms.Charting.Views.RenderOptionsCommon)(resources.GetObject("lightningChartUltimate1.RenderOptions")));
            this.lightningChartUltimate1.Size = new System.Drawing.Size(1504, 458);
            this.lightningChartUltimate1.TabIndex = 8;
            this.lightningChartUltimate1.Title = ((Arction.WinForms.Charting.Titles.ChartTitle)(resources.GetObject("lightningChartUltimate1.Title")));
            this.lightningChartUltimate1.VerticalScrollBars = ((System.Collections.Generic.List<Arction.WinForms.Charting.VerticalScrollBar>)(resources.GetObject("lightningChartUltimate1.VerticalScrollBars")));
            this.lightningChartUltimate1.View3D = ((Arction.WinForms.Charting.Views.View3D.View3D)(resources.GetObject("lightningChartUltimate1.View3D")));
            this.lightningChartUltimate1.ViewPie3D = ((Arction.WinForms.Charting.Views.ViewPie3D.ViewPie3D)(resources.GetObject("lightningChartUltimate1.ViewPie3D")));
            this.lightningChartUltimate1.ViewPolar = ((Arction.WinForms.Charting.Views.ViewPolar.ViewPolar)(resources.GetObject("lightningChartUltimate1.ViewPolar")));
            this.lightningChartUltimate1.ViewSmith = ((Arction.WinForms.Charting.Views.ViewSmith.ViewSmith)(resources.GetObject("lightningChartUltimate1.ViewSmith")));
            this.lightningChartUltimate1.ViewXY = ((Arction.WinForms.Charting.Views.ViewXY.ViewXY)(resources.GetObject("lightningChartUltimate1.ViewXY")));
            // 
            // DialogOpen
            // 
            this.DialogOpen.DefaultExt = "RAW File|*.dat";
            this.DialogOpen.Filter = "RAW File|*.dat";
            this.DialogOpen.Multiselect = true;
            // 
            // DialogSave
            // 
            this.DialogSave.DefaultExt = "CSV File|*.csv";
            this.DialogSave.Filter = "CSV File|*.csv";
            // 
            // labelBottomMsg
            // 
            this.labelBottomMsg.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(230)))), ((int)(((byte)(232)))));
            this.labelBottomMsg.Font = new System.Drawing.Font("맑은 고딕", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelBottomMsg.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(63)))), ((int)(((byte)(36)))));
            this.labelBottomMsg.Location = new System.Drawing.Point(77, 933);
            this.labelBottomMsg.Name = "labelBottomMsg";
            this.labelBottomMsg.Size = new System.Drawing.Size(1707, 21);
            this.labelBottomMsg.TabIndex = 96;
            // 
            // labelAA1
            // 
            this.labelAA1.AutoSize = true;
            this.labelAA1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(230)))), ((int)(((byte)(232)))));
            this.labelAA1.Font = new System.Drawing.Font("맑은 고딕", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.labelAA1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(73)))), ((int)(((byte)(75)))), ((int)(((byte)(76)))));
            this.labelAA1.Location = new System.Drawing.Point(78, 358);
            this.labelAA1.Name = "labelAA1";
            this.labelAA1.Size = new System.Drawing.Size(127, 21);
            this.labelAA1.TabIndex = 97;
            this.labelAA1.Text = "Interest Section";
            // 
            // TFormDignosis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1904, 966);
            this.Controls.Add(this.panelForm);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "TFormDignosis";
            this.Text = "TFormDignosis";
            this.Load += new System.EventHandler(this.TFormDignosis_Load);
            this.panelForm.ResumeLayout(false);
            this.panelForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkAlarm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkOn)).EndInit();
            this.panelGraphKnock.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartKnock)).EndInit();
            this.panelGraph.ResumeLayout(false);
            this.panelGraphRight.ResumeLayout(false);
            this.panelGraphRight.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartPreview)).EndInit();
            this.panelGraphCalc.ResumeLayout(false);
            this.panelGraphCalcLogginData.ResumeLayout(false);
            this.panelGraphCalcLogginData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._chartCalcData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._chartCalcDataRPM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCyl5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Panel panelForm;
        private System.Windows.Forms.PictureBox imgCyl12;
        private System.Windows.Forms.PictureBox imgCyl11;
        private System.Windows.Forms.PictureBox imgCyl10;
        private System.Windows.Forms.PictureBox imgCyl1;
        private System.Windows.Forms.PictureBox imgCyl9;
        private System.Windows.Forms.PictureBox imgCyl2;
        private System.Windows.Forms.PictureBox imgCyl8;
        private System.Windows.Forms.PictureBox imgCyl3;
        private System.Windows.Forms.PictureBox imgCyl7;
        private System.Windows.Forms.PictureBox imgCyl4;
        private System.Windows.Forms.PictureBox imgCyl6;
        private System.Windows.Forms.PictureBox imgCyl5;
        private System.Windows.Forms.PictureBox imgBack;
        private System.Windows.Forms.TextBox editTimeBegin;
        private System.Windows.Forms.TextBox editTimeEnd;
        private System.Windows.Forms.Panel buttonLoad;
        private System.Windows.Forms.Label labelGraphCalc;
        private System.Windows.Forms.Label labelGraph;
        private System.Windows.Forms.Panel panelGraph;
        private Arction.WinForms.Charting.LightningChartUltimate _chart;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panelGraphCalc;
        private System.Windows.Forms.Panel panelGraphCalcLogginData;
        private System.Windows.Forms.Panel comboCalcDataYMax;
        private System.Windows.Forms.Panel comboCalcDataYMin;
        private System.Windows.Forms.Panel comboCalcDataRPMMax;
        private System.Windows.Forms.Panel comboCalcDataRPMYMin;
        private System.Windows.Forms.TextBox editCalcDataRPMYMax;
        private System.Windows.Forms.TextBox editCalcDataRPMYMin;
        private System.Windows.Forms.Panel panelCalcDataYReset;
        private System.Windows.Forms.TextBox editCalcDataYMax;
        private System.Windows.Forms.TextBox editCalcDataYMin;
        private System.Windows.Forms.Panel panelCalcDataRPMYReset;
        private System.Windows.Forms.DataVisualization.Charting.Chart _chartCalcData;
        private System.Windows.Forms.DataVisualization.Charting.Chart _chartCalcDataRPM;
        private System.Windows.Forms.Label labelKnock;
        private System.Windows.Forms.Label labelTDC;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartPreview;
        private System.Windows.Forms.Panel panelPreview;
        private System.Windows.Forms.Panel panelGraphRight;
        private System.Windows.Forms.Panel comboYMax;
        private System.Windows.Forms.Panel comboYMin;
        private System.Windows.Forms.TextBox editGraphPYMax;
        private System.Windows.Forms.TextBox editGraphPYMin;
        private System.Windows.Forms.Panel buttonGraphPAxisReset;
        private System.Windows.Forms.TextBox editIP;
        private System.Windows.Forms.ListBox comboY;
        private System.Windows.Forms.Panel panelGraphKnock;
        private Arction.WinForms.Charting.LightningChartUltimate lightningChartUltimate1;
        public System.Windows.Forms.DataVisualization.Charting.Chart chartKnock;
        private System.Windows.Forms.Panel panelGraphTDC;
        private System.Windows.Forms.PictureBox checkAlarm;
        private System.Windows.Forms.PictureBox checkOn;
        private System.Windows.Forms.TextBox editAlarmFilter;
        private System.Windows.Forms.Panel buttonClear;
        private System.Windows.Forms.Panel buttonExpression;
        private System.Windows.Forms.Panel buttonExport;
        private System.Windows.Forms.OpenFileDialog DialogOpen;
        private System.Windows.Forms.SaveFileDialog DialogSave;
        private System.Windows.Forms.ProgressBar ProgressHistory;
        private System.Windows.Forms.Label labelBottomMsg;
        private System.Windows.Forms.Label labelAA1;
    }
}
﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace ExTextBox
{
    public class ExTextBox : TextBox
    {
        string hint;

        public string Hint
        {
            get { return hint; }
            set { hint = value; this.Invalidate(); }
        }

        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);
            if (m.Msg == 0xf)
            {
                if (!this.Focused && string.IsNullOrEmpty(this.Text)
                    && !string.IsNullOrEmpty(this.Hint))
                {
                    using (var g = this.CreateGraphics())
                    {
                        TextRenderer.DrawText(g, this.Hint, this.Font,
                            this.ClientRectangle, ColorTranslator.FromHtml("#95999B"), this.BackColor,
                            TextFormatFlags.VerticalCenter | TextFormatFlags.HorizontalCenter);
                    }
                }
            }
        }
    }
}
